﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.TKPI_USER_DAL
{
   public class TKPI_USERS_DAL : Log
    {
        
        public TKPI_USER GetUserByUserName(string pUserName, ref UnitOfWork unitOfWork) {
            TKPI_USER rslt = null;
           
            if (!string.IsNullOrEmpty(pUserName))
                rslt = unitOfWork.UserRepository.GetAll(p => p.TKU_LOGIN.ToUpper().Equals(pUserName.ToUpper())).FirstOrDefault();
            
            return rslt;
        }

        public TKPI_ROLE GetUserRoleByUserName(string pUserName, ref UnitOfWork unitOfWork)
        {
            TKPI_ROLE rslt = null;

            if (!string.IsNullOrEmpty(pUserName))
            {
                TKPI_USER user_rslt = unitOfWork.UserRepository.GetAll(p => p.TKU_USER_CODE.ToUpper().Equals(pUserName.ToUpper())).FirstOrDefault();
                if (user_rslt != null)
                {
                    rslt = user_rslt.TKPI_ROLE;
                    //var role = unitOfWork.UserRoleRepository.GetAll(x => x.URO_FK_USER == user_rslt.USR_ROW_ID).FirstOrDefault();
                    //if (role != null)
                    //{
                    //    rslt = unitOfWork.RoleRepository.GetAll(x => x.ROL_ROW_ID == role.URO_FK_ROLE).FirstOrDefault();
                    //}
                }
            }
            
            return rslt;
        }

        public void Save(TKPI_USER data, ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKU_ID = Guid.NewGuid().ToString("N");
                unitOfWork.UserRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKU_CREATED_BY, ex.Message);
                throw ex;
            }
        }

        public void Update(TKPI_USER data, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.UserRepository.GetById(data.TKU_ID);

       
                    #region Set Value
                    if (!string.IsNullOrWhiteSpace(data.TKU_FIRST_NAME))
                    {
                        _search.TKU_FIRST_NAME = data.TKU_FIRST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_LAST_NAME))
                    {
                        _search.TKU_LAST_NAME = data.TKU_LAST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKD_ID))
                    {
                        _search.TKD_ID = data.TKD_ID;
                    }
                    if (!string.IsNullOrWhiteSpace(data.TKA_ID))
                    {
                        _search.TKA_ID = data.TKA_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKP_ID))
                    {
                        _search.TKP_ID = data.TKP_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKC_ID))
                    {
                        _search.TKC_ID = data.TKC_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKS_ID))
                    {
                        _search.TKS_ID = data.TKS_ID;
                    }

                    //if (data.TKR_ID > 0)
                    //{
                        _search.TKR_ID = data.TKR_ID;
                    //}

                    if (!string.IsNullOrWhiteSpace(data.TKU_TEL))
                    {
                        _search.TKU_TEL = data.TKU_TEL;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_STATUS))
                    {
                        _search.TKU_STATUS = data.TKU_STATUS;
                    }

                    _search.TKU_UPDATED_BY = data.TKU_UPDATED_BY;
                    _search.TKU_UPDATED_DATE = data.TKU_UPDATED_DATE;

                    #endregion
                    unitOfWork.UserRepository.Update(_search);
           
      
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKU_UPDATED_BY, ex.Message);
                throw ex;
            }
        }

        public void CreateOrUpdateUserMontior(TKPI_USER data, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.UserRepository.GetAll(c => c.TKU_USER_CODE.Trim().ToUpper() == data.TKU_USER_CODE.Trim().ToUpper()).FirstOrDefault();

                if (_search != null)
                {
                    #region Set Value
                    if (!string.IsNullOrWhiteSpace(data.TKU_FIRST_NAME))
                    {
                        _search.TKU_FIRST_NAME = data.TKU_FIRST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_LAST_NAME))
                    {
                        _search.TKU_LAST_NAME = data.TKU_LAST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKD_ID))
                    {
                        _search.TKD_ID = data.TKD_ID;
                    }
                    if (!string.IsNullOrWhiteSpace(data.TKA_ID))
                    {
                        _search.TKA_ID = data.TKA_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKP_ID))
                    {
                        _search.TKP_ID = data.TKP_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKC_ID))
                    {
                        _search.TKC_ID = data.TKC_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKS_ID))
                    {
                        _search.TKS_ID = data.TKS_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKR_ID))
                    {
                        _search.TKR_ID = data.TKR_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_TEL))
                    {
                        _search.TKU_TEL = data.TKU_TEL;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_STATUS))
                    {
                        _search.TKU_STATUS = data.TKU_STATUS;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_LOGIN))
                    {
                        _search.TKU_LOGIN = data.TKU_LOGIN;
                    }

                    _search.TKU_UPDATED_BY = data.TKU_UPDATED_BY;
                    _search.TKU_UPDATED_DATE = data.TKU_UPDATED_DATE;

                    #endregion
                    unitOfWork.UserRepository.Update(_search);
                }
                else
                {
                    data.TKU_ID = Guid.NewGuid().ToString("N");
                    Save(data, ref unitOfWork);
                }

                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKU_UPDATED_BY, ex.Message);
                throw ex;
            }
        }

        public void CreateOrUpdateUserSnapshot(TKPI_USER_MONITOR_SNAPSHOT data, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.SnapshotRepository.GetAll(c => c.TKMH_USER_CODE.Trim().ToUpper() == data.TKMH_USER_CODE.Trim().ToUpper()).FirstOrDefault();

                if (_search != null)
                {
                    #region Set Value
                    if (!string.IsNullOrWhiteSpace(data.TKMH_FIRST_NAME))
                    {
                        _search.TKMH_FIRST_NAME = data.TKMH_FIRST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKUM_LAST_NAME))
                    {
                        _search.TKUM_LAST_NAME = data.TKUM_LAST_NAME;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKD_ID))
                    {
                        _search.TKD_ID = data.TKD_ID;
                    }
                    if (!string.IsNullOrWhiteSpace(data.TKA_ID))
                    {
                        _search.TKA_ID = data.TKA_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKP_ID))
                    {
                        _search.TKP_ID = data.TKP_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKC_ID))
                    {
                        _search.TKC_ID = data.TKC_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKS_ID))
                    {
                        _search.TKS_ID = data.TKS_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKR_ID))
                    {
                        _search.TKR_ID = data.TKR_ID;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKU_TEL))
                    {
                        _search.TKU_TEL = data.TKU_TEL;
                    }

                    if (!string.IsNullOrWhiteSpace(data.TKMH_LOGIN))
                    {
                        _search.TKMH_LOGIN = data.TKMH_LOGIN;
                    }

                    _search.TKMH_UPDATED_BY = data.TKMH_UPDATED_BY;
                    _search.TKMH_UPDATED_DATE = data.TKMH_UPDATED_DATE;

                    #endregion
                    unitOfWork.SnapshotRepository.Update(_search);
                }
                else
                {
                    data.TKMH_ID = Guid.NewGuid().ToString("N");
                    unitOfWork.SnapshotRepository.Insert(data);
                }

                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKMH_UPDATED_BY, ex.Message);
                throw ex;
            }
        }
        
        public void UpdateStatus(TKPI_USER data, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.UserRepository.GetById(data.TKU_ID);
                #region Set Value
                _search.TKU_STATUS = data.TKU_STATUS;
                _search.TKU_UPDATED_BY = data.TKU_UPDATED_BY;
                _search.TKU_UPDATED_DATE = data.TKU_UPDATED_DATE;
                #endregion
                unitOfWork.UserRepository.Update(data);
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKU_UPDATED_BY, ex.Message);
                throw ex;
            }
        }

        public bool DeleteUserMonitor(ref UnitOfWork unitOfWork, string id = "")
        {
            var result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    unitOfWork.UserMonitorRepository.Delete(id);
                } else
                {
                    unitOfWork.UserMonitorRepository.DeleteAll();
                }

                //unitOfWork.Commit();
                result = true;


            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
                throw ex;
            }

            return result;
        }


        public void Delete(string id, ref UnitOfWork unitOfWork)
        {
            try
            {

                unitOfWork.UserRepository.Delete(id);
                unitOfWork.Commit();

        
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
                throw ex;
            }
        }

    }
}
