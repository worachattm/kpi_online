﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.Utilities
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "");
        T GetById(object id);
        void Insert(T obj);
        void Insert(List<T> obj);
        void Update(T obj);
        void Delete(object id);
        void Save();
        void DeleteAll(Expression<Func<T, bool>> filter = null);

    }
}
