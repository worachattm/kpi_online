﻿using KPI_ONLINE.DAL.Entity;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.Utilities
{
    public class LogService : BaseClass
    {
        
        //private readonly ILog logger_this = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //public List<ErrorLogDetailModel> SelectErrorLog(string FromDate, string ToDate, string Module, string Function)
        //{
        //    try
        //    {
        //        //using (ErrorLogDAL dal = new ErrorLogDAL())
        //        //{

        //        //    return dal.SelectErrorLog(FromDate, ToDate, Module, Function);
        //        //}
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// เก็บ Log Error ที่ Database ส่วนที่ DAL ใช้ Log4Net เก็บอย่างเดียวก็ได้ครับ
        /// </summary>
        /// <param name="logger">Instance Log4Net</param>
        /// <param name="ex">Exception Object</param>
        /// <param name="Severity">ระดับความร้ายแรงของ Exception</param>
        /// <param name="UserAction">User ID ที่เข้าใช้ระบบ</param>
        public void InsertErrorLog(ILog logger, Exception ex, ERROR_SEVERITY Severity, string UserAction = "", string msgDetail = "")
        {
            string errMsg = ex.StackTrace;
            //logger.Error(errMsg);

            TKPI_ERROR_LOG Model = new TKPI_ERROR_LOG();
            //Get a StackTrace object for the exception
            StackTrace st = new StackTrace(ex, true);
            //Get the first stack frame
            StackFrame frame = st.GetFrame(0);

            Model.ROW_ID = Guid.NewGuid().ToString("N");
            Model.MODULE = GetModule(errMsg);
            Model.FUNCTION_ERROR = frame.GetMethod().Name + " => " + frame.GetMethod().DeclaringType.FullName;
            Model.ERROR_SEVERITY = Convert.ToInt32(Severity);
            Model.CREATED_BY = UserAction;
            Model.CREATED_DATE = DateTime.Now;
            SqlException exSqlException = ex as SqlException;
            if (exSqlException != null)
            {
                Model.ERROR_NUMBER = exSqlException.Number;
                Model.ERROR_MESSAGE = "Message : " + msgDetail + " , Error : " + ex.ToString();
                //Model.ERROR_MESSAGE = "Message : " + msgDetail;
                Model.ERROR_LINE = exSqlException.LineNumber;
                Model.ERROR_PROCEDURE = exSqlException.Procedure;
                Model.ERROR_STATE = exSqlException.State;
        
            }
            else
            {
                Model.ERROR_NUMBER = ex.HResult;
                //Model.ERROR_MESSAGE = "Message : " + msgDetail;
                Model.ERROR_MESSAGE = "Message : " + msgDetail + " , Error : " + ex.ToString();
                Model.ERROR_LINE = LineNumber(errMsg);
            }

            unitOfWork.ErrorRepository.Insert(Model);
            unitOfWork.Commit();

        }

        private static int LineNumber(string ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

        private static string GetModule(string ex)
        {
            string str = "";
            string strFrom = "at ";
            string strTo = "in ";
            var idxFrom = ex.LastIndexOf(strFrom);
            var idxTo = ex.LastIndexOf(strTo);
            if (idxFrom != -1)
            {
                if (idxTo != -1)
                {
                    str = ex.Substring(idxFrom + 3, idxTo - idxFrom - 4);
                }
                else
                {
                    str = ex.Substring(idxFrom + 3);
                }


            }
            return str;

        }
    }
}
