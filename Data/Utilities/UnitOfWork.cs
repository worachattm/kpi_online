﻿using KPI_ONLINE.DAL.Entity;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.Utilities
{
    public class UnitOfWork : IDisposable
    {
        private MFKPIEntities _context = new MFKPIEntities();
        private GenericRepository<TKPI_INDICATORS_NAME> KPINameRepository;
        private GenericRepository<TKPI_INDICATORS> indicatorRepository;
        private GenericRepository<TKPI_KPI_DATA> KPIDataRepository;
        private GenericRepository<TKPI_KPI_DATA_FILE> KPIDataFileRepository;
        private GenericRepository<TKPI_MASTER_AREA> areaRepository;

        private GenericRepository<TKPI_MENU> menuRepository;
        private GenericRepository<TKPI_ROLE> roleRepository;
        private GenericRepository<TKPI_ROLE_MENU> roleMenuRepository;
        private GenericRepository<TKPI_ERROR_LOG> errorRepository;
        private GenericRepository<TKPI_USER> userRepository;
        private GenericRepository<TKPI_MASTER_COMPANY> companyRepository;
        private GenericRepository<TKPI_MASTER_PLANT> plantRepository;
        private GenericRepository<TKPI_MASTER_DEPARTMENT> departmentRepository;
        private GenericRepository<TKPI_MASTER_SHIFT> shiftRepository;
        private GenericRepository<TKPI_ROLE_DATA> roleDataRepository;
        private GenericRepository<TKPI_ROLE_INDICATORS> roleIndicatorRepository;
        private GenericRepository<TKPI_USER_MONITOR> userMonitorRepository;
        private GenericRepository<TKPI_MAP_CONFIG> mapConfigRepository;

        private GenericRepository<TKPI_USER_MONITOR_SNAPSHOT> snapshotRepository;
        private GenericRepository<TKPI_INDICATORS_SCALE> indicatorScaleRepository;


        private GenericRepository<TKPI_REPORT_SOFT_KPI> reportSoftKPIRepository;
        private GenericRepository<TKPI_REPORT_CONFIG> reportConfigRepository;
        private GenericRepository<TKPI_REPORT_DISPLAY> reportDisplayRepository;
        private GenericRepository<TKPI_REPORT_WEIGHT> reportWeightRepository;

        public IGenericRepository<TKPI_INDICATORS_SCALE> IndicatorScaleRepository
        {
            get
            {


                if (this.roleDataRepository == null)
                {
                    this.indicatorScaleRepository = new GenericRepository<TKPI_INDICATORS_SCALE>(_context);
                }
                return indicatorScaleRepository;
            }
        }
        public IGenericRepository<TKPI_REPORT_SOFT_KPI> ReportSoftKpiRepository
        {
            get
            {


                if (this.reportSoftKPIRepository == null)
                {
                    this.reportSoftKPIRepository = new GenericRepository<TKPI_REPORT_SOFT_KPI>(_context);
                }
                return reportSoftKPIRepository;
            }
        }
        public IGenericRepository<TKPI_REPORT_CONFIG> ReportConfigRepository
        {
            get
            {
                if (this.reportConfigRepository == null)
                {
                    this.reportConfigRepository = new GenericRepository<TKPI_REPORT_CONFIG>(_context);
                }
                return reportConfigRepository;
            }
        }
        public IGenericRepository<TKPI_REPORT_DISPLAY> ReportDisplayRepository
        {
            get
            {
                if (this.reportDisplayRepository == null)
                {
                    this.reportDisplayRepository = new GenericRepository<TKPI_REPORT_DISPLAY>(_context);
                }
                return reportDisplayRepository;
            }
        }
        public IGenericRepository<TKPI_REPORT_WEIGHT> ReportWeightRepository
        {
            get
            {
                if (this.reportWeightRepository == null)
                {
                    this.reportWeightRepository = new GenericRepository<TKPI_REPORT_WEIGHT>(_context);
                }
                return reportWeightRepository;
            }
        }

        public IGenericRepository<TKPI_KPI_DATA_FILE> KpiDataFileRepository
        {
            get
            {
                if (this.KPIDataFileRepository == null)
                {
                    this.KPIDataFileRepository = new GenericRepository<TKPI_KPI_DATA_FILE>(_context);
                }
                return KPIDataFileRepository;
            }
        }



        public IGenericRepository<TKPI_USER_MONITOR_SNAPSHOT> SnapshotRepository
        {
            get
            {


                if (this.snapshotRepository == null)
                {
                    this.snapshotRepository = new GenericRepository<TKPI_USER_MONITOR_SNAPSHOT>(_context);
                }
                return snapshotRepository;
            }
        }
        public IGenericRepository<TKPI_ROLE_DATA> RoleDataRepository
        {
            get
            {


                if (this.roleDataRepository == null)
                {
                    this.roleDataRepository = new GenericRepository<TKPI_ROLE_DATA>(_context);
                }
                return roleDataRepository;
            }
        }
        public IGenericRepository<TKPI_INDICATORS> IndicatorRepository
        {
            get
            {


                if (this.indicatorRepository == null)
                {
                    this.indicatorRepository = new GenericRepository<TKPI_INDICATORS>(_context);
                }
                return indicatorRepository;
            }
        }
        public IGenericRepository<TKPI_ROLE_INDICATORS> RoleIndicatorRepository
        {
            get
            {


                if (this.roleIndicatorRepository == null)
                {
                    this.roleIndicatorRepository = new GenericRepository<TKPI_ROLE_INDICATORS>(_context);
                }
                return roleIndicatorRepository;
            }
        }
        public IGenericRepository<TKPI_MASTER_SHIFT> ShiftRepository
        {
            get
            {


                if (this.shiftRepository == null)
                {
                    this.shiftRepository = new GenericRepository<TKPI_MASTER_SHIFT>(_context);
                }
                return shiftRepository;
            }
        }
        public IGenericRepository<TKPI_MASTER_DEPARTMENT> DepartmentRepository
        {
            get
            {


                if (this.departmentRepository == null)
                {
                    this.departmentRepository = new GenericRepository<TKPI_MASTER_DEPARTMENT>(_context);
                }
                return departmentRepository;
            }
        }
        public IGenericRepository<TKPI_MASTER_PLANT> PlantRepository
        {
            get
            {


                if (this.plantRepository == null)
                {
                    this.plantRepository = new GenericRepository<TKPI_MASTER_PLANT>(_context);
                }
                return plantRepository;
            }
        }
        public IGenericRepository<TKPI_MASTER_AREA> AreaRepository
        {
            get
            {


                if (this.areaRepository == null)
                {
                    this.areaRepository = new GenericRepository<TKPI_MASTER_AREA>(_context);
                }
                return areaRepository;
            }
        }
        

        
        public IGenericRepository<TKPI_ERROR_LOG> ErrorRepository
        {
            get
            {


                if (this.errorRepository == null)
                {
                    this.errorRepository = new GenericRepository<TKPI_ERROR_LOG>(_context);
                }
                return errorRepository;
            }
        }
        public IGenericRepository<TKPI_MENU> MenuRepository
        {
            get
            {


                if (this.menuRepository == null)
                {
                    this.menuRepository = new GenericRepository<TKPI_MENU>(_context);
                }
                return menuRepository;
            }
        }
        public IGenericRepository<TKPI_MASTER_COMPANY> CompanyRepository
        {
            get
            {


                if (this.companyRepository == null)
                {
                    this.companyRepository = new GenericRepository<TKPI_MASTER_COMPANY>(_context);
                }
                return companyRepository;
            }
        }
        public IGenericRepository<TKPI_ROLE> RoleRepository
        {
            get
            {


                if (this.roleRepository == null)
                {
                    this.roleRepository = new GenericRepository<TKPI_ROLE>(_context);
                }
                return roleRepository;
            }
        }
        public IGenericRepository<TKPI_ROLE_MENU> RoleMenuRepository
        {
            get
            {


                if (this.roleMenuRepository == null)
                {
                    this.roleMenuRepository = new GenericRepository<TKPI_ROLE_MENU>(_context);
                }
                return roleMenuRepository;
            }
        }
        public IGenericRepository<TKPI_USER> UserRepository
        {
            get
            {


                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<TKPI_USER>(_context);
                }
                return userRepository;
            }
        }
        public IGenericRepository<TKPI_KPI_DATA> KpiDataRepository
        {
            get
            {


                if (this.KPIDataRepository == null)
                {
                    this.KPIDataRepository = new GenericRepository<TKPI_KPI_DATA>(_context);
                }
                return KPIDataRepository;
            }
        }
        public IGenericRepository<TKPI_INDICATORS_NAME> KpiNameRepository
        {
            get
            {


                if (this.KPINameRepository == null)
                {
                    this.KPINameRepository = new GenericRepository<TKPI_INDICATORS_NAME>(_context);
                }
                return KPINameRepository;
            }
        }
        public IGenericRepository<TKPI_MAP_CONFIG> MapConfigRepository
        {
            get
            {


                if (this.mapConfigRepository == null)
                {
                    this.mapConfigRepository = new GenericRepository<TKPI_MAP_CONFIG>(_context);
                }
                return mapConfigRepository;
            }
        }
        public IGenericRepository<TKPI_USER_MONITOR> UserMonitorRepository
        {
            get
            {


                if (this.userMonitorRepository == null)
                {
                    this.userMonitorRepository = new GenericRepository<TKPI_USER_MONITOR>(_context);
                }
                return userMonitorRepository;
            }
        }
        public void Commit()
        {
            var i = _context.SaveChanges();
        }

        public void Rollback()
        {

            //_context
            //  .ChangeTracker
            //  .Entries()
            //  .ToList()
            //  .ForEach(x => x.Reload());

            // DetachAllEntities
            var changedEntriesCopy = _context.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
