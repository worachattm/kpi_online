﻿using KPI_ONLINE.DAL.Entity;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.Utilities
{
    public class BaseClass : Log
    {
        public UnitOfWork unitOfWork = new UnitOfWork();

    }

    public class Log
    {
        static public readonly LogService ls = new LogService();
        static public readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
