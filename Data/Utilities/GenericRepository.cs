﻿using KPI_ONLINE.DAL.Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.Utilities
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private MFKPIEntities _context = null;
        private DbSet<T> table = null;

        public GenericRepository()
        {
            this._context = new MFKPIEntities();
            table = _context.Set<T>();
        }

        public GenericRepository(MFKPIEntities _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }

        public T GetById(object id)
        {
            return table.Find(id);
        }

        public IEnumerable<T> GetAll(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            //if(filter != null)
            //{
            //    return table.Where(filter).ToList();
            //} else
            //{
            //    return table.ToList();
            //}

            IQueryable<T> query = table;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public void Insert(T obj)
        {
            table.Add(obj);
        }

        public void Insert(List<T> obj)
        {
            table.AddRange(obj);
        }
        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }

        public void DeleteAll(Expression<Func<T, bool>> filter = null)
        {
            if(filter != null)
            {
                IQueryable<T> query = table.Where(filter);
                table.RemoveRange(query);
            } else
            {
                IQueryable<T> query = table;
                table.RemoveRange(query);
            }

        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
