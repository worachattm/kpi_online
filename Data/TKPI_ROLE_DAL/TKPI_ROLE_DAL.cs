﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.TKPI_ROLE_DAL
{
    public class TKPI_ROLE_DAL : Log
    {
        
        public void Save(TKPI_ROLE data , ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKR_ID = Guid.NewGuid().ToString("N");
                unitOfWork.RoleRepository.Insert(data);

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKR_CREATED_BY, ex.Message);
            }
        }

        public void Update(TKPI_ROLE data, ref UnitOfWork unitOfWork)
        {
            try
            {
           
                var _search = unitOfWork.RoleRepository.GetById(data.TKR_ID);
                #region Set Value
                _search.TKR_NAME = data.TKR_NAME;
                _search.TKR_DESCRIPTION = data.TKR_DESCRIPTION;
                _search.TKR_STATUS = data.TKR_STATUS;
                _search.TKR_UPDATED_BY = data.TKR_UPDATED_BY;
                _search.TKR_UPDATED_DATE = data.TKR_UPDATED_DATE;
                #endregion
                unitOfWork.RoleRepository.Update(_search);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKR_UPDATED_BY, ex.Message);
            }
        }

    }
}
