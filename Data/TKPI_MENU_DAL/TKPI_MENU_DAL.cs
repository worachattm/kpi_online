﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.TKPI_MENU_DAL
{
    public class TKPI_MENU_DAL : Log
    {
        
        public void Save(TKPI_MENU data , ref UnitOfWork unitOfWork)
        {
            try
            {
                data.MEU_ROW_ID = Guid.NewGuid().ToString("N");
                unitOfWork.MenuRepository.Insert(data);
                unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.MEU_CREATED_BY, ex.Message);
                throw ex;
            }
        }


        public void Update(TKPI_MENU data, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.MenuRepository.Update(data);
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.MEU_CREATED_BY, ex.Message);
                throw ex;
            }
        }


        public void UpdateListNo(string MenuRowID, string ListNo, string UpdateBy, DateTime UpdateDate, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.MenuRepository.GetById(MenuRowID);
                #region Set Value
                _search.MEU_LIST_NO = ListNo;
                _search.MEU_UPDATED_BY = UpdateBy;
                _search.MEU_UPDATED_DATE = UpdateDate;
                #endregion

                unitOfWork.MenuRepository.Update(_search);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, UpdateBy, ex.Message);
                throw ex;

            }
        }

        public void UpdateFlagStatus(string MenuRowID,string isActive , string UpdateBy, ref UnitOfWork unitOfWork)
        {
            try
            {
                var _search = unitOfWork.MenuRepository.GetById(MenuRowID);
                #region Set Value
                _search.MEU_ACTIVE = isActive;
                _search.MEU_UPDATED_BY = UpdateBy;
                _search.MEU_UPDATED_DATE = DateTime.Now;
                #endregion

                unitOfWork.MenuRepository.Update(_search);
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, UpdateBy, ex.Message);
                throw ex;

            }
        }

        public void Delete(string RowId, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.MenuRepository.Delete(RowId);
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
                throw ex;
            }
        }

        public void DeleteList(string ListRowId, ref UnitOfWork unitOfWork)
        {
            try
            {
                var res = ListRowId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
               .Distinct()
               .ToList();

                foreach(var c in res)
                {
                    var id = c.Trim(new char[] { (char)39 });
                    unitOfWork.MenuRepository.Delete(id);
                }


                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
                throw;
            }
        }
    }
}
