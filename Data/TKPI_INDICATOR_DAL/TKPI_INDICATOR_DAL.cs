﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.TKPI_INDICATOR_DAL
{
    public class TKPI_INDICATOR_DAL : Log
    {
        
        public void SaveName(TKPI_INDICATORS_NAME data , ref UnitOfWork uow)
        {
            try
            {
                data.TKIN_ID = Guid.NewGuid().ToString("N");
                uow.KpiNameRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKIN_CREATED_BY, ex.Message);
                throw ex;
            }
        }



        public void Save(TKPI_INDICATORS data, ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKI_ID = Guid.NewGuid().ToString("N");
                unitOfWork.IndicatorRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKI_INDI_CREATED_BY, ex.Message);
                throw ex;
            }
        }


        public void Update(TKPI_INDICATORS data, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.IndicatorRepository.Update(data);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKI_INDI_CREATED_BY, ex.Message);
                throw ex;
            }
        }

        public void UpdateName(TKPI_INDICATORS_NAME data, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.KpiNameRepository.Update(data);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKIN_UPDATED_BY, ex.Message);
                throw ex;
            }
        }

        public void UpdateScale(TKPI_INDICATORS_SCALE data, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.IndicatorScaleRepository.Update(data);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKIS_UPDATED_BY, ex.Message);
                throw ex;
            }
        }

    }
}
