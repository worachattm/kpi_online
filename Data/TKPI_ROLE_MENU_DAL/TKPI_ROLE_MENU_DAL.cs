﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_ONLINE.DAL.TKPI_ROLE_MENU_DAL
{
    public class TKPI_ROLE_MENU_DAL : Log
    {
        
        public void Save(TKPI_ROLE_MENU data, ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKRM_ID = Guid.NewGuid().ToString("N");
                unitOfWork.RoleMenuRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, data.TKRM_CREATED_BY, ex.Message);
            }
        }

        public void SaveRoleData(TKPI_ROLE_DATA data, ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKRD_ID = Guid.NewGuid().ToString("N");
                unitOfWork.RoleDataRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
            }
        }

        public void SaveRoleIndicator(TKPI_ROLE_INDICATORS data, ref UnitOfWork unitOfWork)
        {
            try
            {
                data.TKRK_ID = Guid.NewGuid().ToString("N");
                unitOfWork.RoleIndicatorRepository.Insert(data);
                //unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, "", ex.Message);
            }
        }

        public void DeleteRole(string RowId, string pUser, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.RoleIndicatorRepository.DeleteAll(c => c.TKR_ID == RowId);
                unitOfWork.RoleDataRepository.DeleteAll(c => c.TKR_ID == RowId);
                unitOfWork.RoleMenuRepository.DeleteAll(c => c.TKR_ID == RowId);
                unitOfWork.RoleRepository.Delete(RowId);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
                throw;
            }
        }

        public void DeleteAll(string RowId , string pUser, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.RoleMenuRepository.DeleteAll(c => c.TKR_ID == RowId);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }
        }

        public void DeleteKPIAll(string RowId, string pUser, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.RoleIndicatorRepository.DeleteAll(c => c.TKR_ID == RowId);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }
        }

        public void DeleteRoleDataAll(string RowId, string pUser, ref UnitOfWork unitOfWork)
        {
            try
            {
                unitOfWork.RoleDataRepository.DeleteAll(c => c.TKR_ID == RowId);
                //unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }
        }
    }
}
