//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KPI_ONLINE.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TKPI_KPI_DATA
    {
        public string TKID_ID { get; set; }
        public string TKID_DEPARTMENT { get; set; }
        public string TKID_AREA { get; set; }
        public string TKID_PLANT { get; set; }
        public string TKID_SHIFT { get; set; }
        public string TKI_ID { get; set; }
        public System.DateTime TKID_KPI_DATE { get; set; }
        public string TKID_TYPE { get; set; }
        public Nullable<decimal> TKID_ACTUAL_SCORE { get; set; }
        public Nullable<decimal> TKID_YEAR_TO_DATE { get; set; }
        public string TKID_EMP_ID { get; set; }
        public string TKID_FIRST_NAME { get; set; }
        public string TKID_LAST_NAME { get; set; }
        public string TKID_TRANFROM_TYPE { get; set; }
        public string TKDF_ID { get; set; }
        public string TKID_NOTE { get; set; }
        public string TKID_CREATED_BY { get; set; }
        public System.DateTime TKID_CREATED_DATE { get; set; }
        public string TKID_UPDATED_BY { get; set; }
        public System.DateTime TKID_UPDATED_DATE { get; set; }
    
        public virtual TKPI_INDICATORS TKPI_INDICATORS { get; set; }
        public virtual TKPI_KPI_DATA_FILE TKPI_KPI_DATA_FILE { get; set; }
    }
}
