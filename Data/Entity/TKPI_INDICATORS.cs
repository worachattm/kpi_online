//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KPI_ONLINE.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TKPI_INDICATORS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TKPI_INDICATORS()
        {
            this.TKPI_KPI_DATA = new HashSet<TKPI_KPI_DATA>();
        }
    
        public string TKI_ID { get; set; }
        public string TKIN_ID { get; set; }
        public decimal TKI_INDI_NO { get; set; }
        public string TKI_INDI_NAME { get; set; }
        public decimal TKI_INDI_YEAR { get; set; }
        public string TKI_INDI_TYPE { get; set; }
        public string TKI_INDI_CAL_MODE { get; set; }
        public System.DateTime TKI_INDI_FROM { get; set; }
        public System.DateTime TKI_INDI_TO { get; set; }
        public Nullable<decimal> TKI_INDI_1_FROM_VAL { get; set; }
        public Nullable<decimal> TKI_INDI_1_TO_VAL { get; set; }
        public string TKI_INDI_1_DISPLAY { get; set; }
        public Nullable<decimal> TKI_INDI_2_FROM_VAL { get; set; }
        public Nullable<decimal> TKI_INDI_2_TO_VAL { get; set; }
        public string TKI_INDI_2_DISPLAY { get; set; }
        public Nullable<decimal> TKI_INDI_3_FROM_VAL { get; set; }
        public Nullable<decimal> TKI_INDI_3_TO_VAL { get; set; }
        public string TKI_INDI_3_DISPLAY { get; set; }
        public Nullable<decimal> TKI_INDI_4_FROM_VAL { get; set; }
        public Nullable<decimal> TKI_INDI_4_TO_VAL { get; set; }
        public string TKI_INDI_4_DISPLAY { get; set; }
        public Nullable<decimal> TKI_INDI_5_FROM_VAL { get; set; }
        public Nullable<decimal> TKI_INDI_5_TO_VAL { get; set; }
        public string TKI_INDI_5_DISPLAY { get; set; }
        public string TKI_INDI_STATUS { get; set; }
        public string TKI_INDI_CREATED_BY { get; set; }
        public System.DateTime TKI_INDI_CREATED_DATE { get; set; }
        public string TKI_INDI_UPDATED_BY { get; set; }
        public System.DateTime TKI_INDI_UPDATED_DATE { get; set; }
        public string TKI_INDI_1_COLOR { get; set; }
        public string TKI_INDI_2_COLOR { get; set; }
        public string TKI_INDI_3_COLOR { get; set; }
        public string TKI_INDI_4_COLOR { get; set; }
        public string TKI_INDI_5_COLOR { get; set; }
        public string TKD_ID { get; set; }
        public string TKA_ID { get; set; }
        public string TKP_ID { get; set; }
        public string TKS_ID { get; set; }
    
        public virtual TKPI_INDICATORS_NAME TKPI_INDICATORS_NAME { get; set; }
        public virtual TKPI_MASTER_AREA TKPI_MASTER_AREA { get; set; }
        public virtual TKPI_MASTER_DEPARTMENT TKPI_MASTER_DEPARTMENT { get; set; }
        public virtual TKPI_MASTER_PLANT TKPI_MASTER_PLANT { get; set; }
        public virtual TKPI_MASTER_SHIFT TKPI_MASTER_SHIFT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TKPI_KPI_DATA> TKPI_KPI_DATA { get; set; }
    }
}
