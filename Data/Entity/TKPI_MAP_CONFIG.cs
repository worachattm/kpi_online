//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KPI_ONLINE.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TKPI_MAP_CONFIG
    {
        public string TMC_ROW_ID { get; set; }
        public string TMC_KPI_NAME_ID { get; set; }
        public string TMC_VIIEW_MODE { get; set; }
        public string TMC_NOTE { get; set; }
    
        public virtual TKPI_INDICATORS_NAME TKPI_INDICATORS_NAME { get; set; }
    }
}
