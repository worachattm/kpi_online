﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class KPIDetailAreaReportViewModel
    {
        public SearchResult Result { get; set; }//= new SearchResult();
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public RoleViewModel_Detail Role_Detail { get; set; } = new RoleViewModel_Detail(); 
        public List<IndicatorName> IndicatorNameList { get; set; }
        public class SearchCriteria
        {
            public string Department { get; set; }
            public string Area { get; set; }
            public string Plant { get; set; }
            public string Year { get; set; }
            public string PeriodType { get; set; }
            public string TargetPeriod { get; set; }

            public List<SelectListItem> ddYear { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPeriodType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddTargetPeriod { get; set; } = new List<SelectListItem>();
        }

        public class SearchResult
        {
            public List<Plant> PlantList { get; set; } = new List<Plant>();
        }

        public class Plant
        {
            public string PlantName { get; set; }
            public string PlantNameEx { get; set; }
            public string PlantID { get; set; }
            public List<string> ShiftHeaderID { get; set; } = new List<string>();
            public List<ShiftData> DetailList { get; set; } = new List<ShiftData>();
        }
        public class ShiftData {
            public string kpiNo { get; set; }
            public string kpiName { get; set; }
            public decimal? avgScore_A { get; set; }
            public string scaleColor_A { get; set; }
            public string scaleColorName_A { get; set; }
            public decimal? avgScore_B { get; set; }
            public string scaleColor_B { get; set; }
            public string scaleColorName_B { get; set; }
            public decimal? avgScore_C { get; set; }
            public string scaleColor_C { get; set; }
            public string scaleColorName_C { get; set; }
            public decimal? avgScore_D { get; set; }
            public string scaleColor_D { get; set; }
            public string scaleColorName_D { get; set; }


        }
        public class SP_DETAIL_BY_AREA
        {
            public decimal? TKI_INDI_NO { get; set; }
            public string TKI_INDI_CAL_MODE { get; set; }
            public string TKID_DEPARTMENT { get; set; }
            public string TKID_AREA { get; set; }
            public string TKID_PLANT { get; set; }
            public string TKID_SHIFT { get; set; }
            public decimal? AVG_SCORE { get; set; }
            public decimal? TKI_INDI_YEAR { get; set; }
            public decimal? SET_INDEX_COLOR { get; set; }
            public string SCALE_COLOR_CODE { get; set; }
            public string SCALE_COLOR { get; set; }
        }
    }
}