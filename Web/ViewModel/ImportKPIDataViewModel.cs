﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Script.Serialization;
using System.Globalization;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace KPI_ONLINE.ViewModel
{
    public class ExportImportDataViewModel
    {
        public ImportDataCriteria ImportCriteria { get; set; } = new ImportDataCriteria();
        public ExportDataCriteria ExportCriteria { get; set; } = new ExportDataCriteria();
        public List<ImportFileHistory> ImportFileDetail { get; set; } = new List<ImportFileHistory>();
        public class ExportDataCriteria
        {
            public string Plant { get; set; }
            public string Department { get; set; }
            public string Area { get; set; }
            public string Shift { get; set; }
            public string KPIIndicator { get; set; }

            public List<SelectListItem> ddDepartment { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPlant { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddArea { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddShift { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
        }
        public class ImportDataCriteria
        {
            public string KPIIndicator { get; set; }
            public string FilePart { get; set; }
            public string Note { get; set; }

            public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
        }


        public class ImportFileHistory
        {
            public int NO { get; set; }
            public string KPIIndicator { get; set; }
            public string FileName { get; set; }
            public string Note { get; set; }
            public string CreateBy { get; set; }

            /// <summary>
            /// USE FOR ASSING DATE
            /// </summary>
            public DateTime CreateDate { get; set; }
            /// <summary>
            /// USE FOR GET DATE AS STING
            /// FORMAT : dd/MM/yyyy 
            /// </summary>
            public string CreateDatesTRING
            {
                get
                {
                    if (CreateDate != null)
                    {
                        return CreateDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public ImportFileHistory(int pNO, string pKPIIndicator, string pFileName, string pNote, string pCreateBy, DateTime pCreateDate)
            {
                NO = pNO;
                KPIIndicator = pKPIIndicator;
                FileName = pFileName;
                Note = pNote;
                CreateBy = pCreateBy;
                CreateDate = pCreateDate;

            }
        }


    }
    public class SearchKPIDataViewModel
    {
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public List<SearchResult> Result { get; set; } = new List<SearchResult>();
        public RoleViewModel_Detail Role_Detail { get; set; } = new RoleViewModel_Detail();

        public class SearchCriteria
        {
            public string Plant { get; set; }
            public string Department { get; set; }
            public string Area { get; set; }
            public string Shift { get; set; }
            public string KPIIndicator { get; set; }
            public string PeriodFrom { get; set; }
            public string PeriodTo { get; set; }
            public DateTime? PeriodFromDate
            {
                get
                {
                    try
                    {
                        return DateTime.ParseExact(PeriodFrom, "MMMM-yyyy", new CultureInfo("en-US"));
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            public DateTime? PeriodToDate
            {
                get
                {
                    try
                    {
                        return DateTime.ParseExact(PeriodTo, "MMMM-yyyy", new CultureInfo("en-US")).AddMonths(1).AddMinutes(-1);
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            public string FirstName { get; set; }
            public string LastName { get; set; }

            public List<SelectListItem> ddDepartment { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPlant { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddArea { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddShift { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
        }
        public class SearchResult
        {
            public string DepartMent { get; set; }
            public string DepartMentID { get; set; }

            public List<MonthList> MonthList { get; set; } = new List<MonthList>();
        }
        public class MonthList
        {
            /// <summary>
            /// USE FOR ASSING DATE
            /// </summary>
            public DateTime KPIDate { get; set; }
            /// <summary>
            /// USE FOR GET DATE AS STING
            /// FORMAT : Month: MMMM yyyy
            /// </summary>
            public string KPIDateString
            {
                get
                {
                    if (KPIDate != null)
                    {
                        return "Month: " + KPIDate.ToString("MMMM yyyy");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public string KPIDateID { get; set; }

            public List<KPIList> KPIList { get; set; } = new List<KPIList>();
        }
        public class KPIList
        {
            public string KPIName { get; set; }
            public string KPIid { get; set; }
            public decimal KPINo { get; set; }

            public bool IsIndividual { get; set; }
            public List<KPIDetail> Detail { get; set; } = new List<KPIDetail>();
        }
        public class KPIDetail
        {
            public int NO { get; set; }
            public string ID { get; set; }
            public string Area { get; set; }
            public string Shift { get; set; }
            public string Plant { get; set; }
            public string AreaId { get; set; }
            public string ShiftId { get; set; }
            public string PlantId { get; set; }
            public string StreffName { get; set; }
            public string ActualScore { get; set; }
            public string YearToDate { get; set; }
            public string KPIScale { get; set; }
            public string KPIScore { get; set; }
            public string CreateBy { get; set; }
            /// <summary>
            /// USE FOR ASSING DATE
            /// </summary>
            public DateTime CreateDate { get; set; }
            /// <summary>
            /// USE FOR GET DATE AS STING
            /// FORMAT : dd/MM/yyyy 
            /// </summary>
            public string CreateDateString
            {
                get
                {
                    if (CreateDate != null)
                    {
                        return CreateDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            /// <summary>
            /// USE FOR ASSING DATE
            /// </summary>
            public DateTime KPIDate { get; set; }
            public string Note { get; set; }
            public UserAction Action { get; set; }
            public KPIDetail()
            {
            }


            /// <summary>
            /// assing data here
            /// </summary>
            /// <param name="pNO"></param>
            /// <param name="pArea"></param>
            /// <param name="pShift"></param>
            /// <param name="pStreffName"></param>
            /// <param name="pActualScore"></param>
            /// <param name="pKPIScale"></param>
            /// <param name="pKPIScore"></param>
            /// <param name="pCreateBy"></param>
            /// <param name="pCreateDate"></param>
            /// <param name="pAction"></param>
            public KPIDetail(int pNO, string pID, string pArea, string pShift, string pPlant, string pAreaId, string pShiftId, string pPlantId, string pStreffName, string pActualScore, string pYearToDate, string pKPIScale, string pKPIScore, string pCreateBy, DateTime pCreateDate, DateTime pKPIDate, string pNote)
            {
                NO = pNO;
                ID = pID;
                Area = pArea;
                Shift = pShift;
                Plant = pPlant;
                AreaId = pAreaId;
                ShiftId = pShiftId;
                PlantId = pPlantId;
                StreffName = pStreffName;
                ActualScore = pActualScore;
                YearToDate = pYearToDate;
                KPIScale = pKPIScale;
                KPIScore = pKPIScore;
                CreateBy = pCreateBy;
                CreateDate = pCreateDate;
                KPIDate = pKPIDate;
                Note = pNote;
            }

        }
        public class UserAction
        {
            public bool copy { get; set; }
            public bool edit { get; set; }
            public bool delete { get; set; }
        }
    }
    public class ManageKPIData
    {
        public string ID { get; set; }
        public string Department { get; set; }
        public string Area { get; set; }
        public string Plant { get; set; }
        public string Shift { get; set; }
        public string UserId { get; set; }

        public string KPIIndicator { get; set; }
        /// <summary>
        /// USE FOR ASSING DATE
        /// </summary>
        public DateTime KPIDate { get; set; }
        /// <summary>
        /// USE FOR GET DATE AS STING
        /// FORMAT : Month: MMMM yyyy
        /// </summary>
        public string KPIDateString
        {
            get
            {
                if (KPIDate != null)
                {
                    return KPIDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    return null;
                }
            }
        }
        public string ActualScore { get; set; }
        public string YearToDate { get; set; }
        public string Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Note { get; set; }
        public List<SelectListItem> ddDepartment { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddArea { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddPlant { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddShift { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
        public ManageKPIData() { }
        public ManageKPIData(string iD, string department, string area, string plant, string shift, string kPIIndicator, DateTime kPIDate, string actualScore, string yearToDate, string type, string firstName, string lastName, string note)
        {
            ID = iD;
            Department = department;
            Area = area;
            Plant = plant;
            Shift = shift;
            KPIIndicator = kPIIndicator;
            KPIDate = kPIDate;
            ActualScore = actualScore;
            YearToDate = yearToDate;
            Type = type;
            FirstName = firstName;
            LastName = lastName;
            Note = note;
        }
    }
    public class Template
    {

        public List<ExportTemplate> TemplatesList { get; set; } = new List<ExportTemplate>();

    }
    public class TemplateData
    {
        public List<string> Plant { get; set; } = new List<string>();
        public List<string> Department { get; set; } = new List<string>();
        public List<string> Area { get; set; } = new List<string>();
        public List<string> Shift { get; set; } = new List<string>();
        public List<string> KPIIndicator { get; set; } = new List<string>();

    }
    public class ExportTemplate
    {
        public string Type { get; set; }
        public string KPIIndicator { get; set; }
        public List<ExportDataResult> ExportResultList { get; set; } = new List<ExportDataResult>();

    }
    public class ExportDataResult
    {
        public int NO { get; set; }
        public string DepartMent { get; set; }
        public string Area { get; set; }
        public string Plant { get; set; }
        public string Shift { get; set; }
        /// <summary>
        /// USE FOR ASSING DATE
        /// </summary>
        //public DateTime? KPIDate
        //{
        //    get
        //    {
        //        if (KPIDate != null)
        //        {
        //            return DateTime.ParseExact(KPIDatesTRING, "dd/MM/yyyy", new CultureInfo("en-US"));
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}
        /// <summary>
        /// USE FOR GET DATE AS STING
        /// FORMAT : dd/MM/yyyy 
        /// </summary>
        public string KPIDatesTRING { get; set; }
        public string EmpID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ActualScore { get; set; }
        public string YearToDate { get; set; }
        public string Note { get; set; }
        /// <summary>
        /// 
        /// </summary>
    }
}