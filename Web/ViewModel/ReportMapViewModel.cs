﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KPI_ONLINE.Models;

namespace KPI_ONLINE.ViewModel
{
    public class ReportMapViewModel
    {
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public SearchResult Result { get; set; } = new SearchResult();
        public string Json { get; set; } = "";
        public string JsonForExcel { get; set; } = "";
        public List<string> DepartmentHeader { get; set; } = new List<string>();
        public List<sp_executive_summary_Result> DepartmentData { get; set; } = new List<sp_executive_summary_Result>();
        public string viewType { get; set; }
        public List<ReportMapServiceModel.Pin> listpin { get; set; } = new List<ReportMapServiceModel.Pin>();

        public class SearchCriteria
        {
            public string KPIIndicator { get; set; }
            public string Year { get; set; }
            public string PeriodType { get; set; }
            public string TargetPeriod { get; set; }

            public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddYear { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPeriodType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddTargetPeriod { get; set; } = new List<SelectListItem>();
        }
        public class SearchResult
        {
            public string KPIIndicator { get; set; }
            public string TimePeriod { get; set; }
            public List<MapDetail> MapDetailList { get; set; } = new List<MapDetail>();
        }

        public class MapDetail
        {
            public string Plant { get; set; }
            public string Shift { get; set; }
            public string ActualScore { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public MapDetail()
            {
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="pPlant"></param>
            /// <param name="pShift"></param>
            /// <param name="pActualScore"></param>
            public MapDetail(string pPlant, string pShift, string pActualScore)
            {
                Plant = pPlant;
                Shift = pShift;
                ActualScore = pActualScore;
            }

        }


    }
    public class MapConfig
    {
        public List<IndicatorName> IndicatorNameList { get; set; } = new List<IndicatorName>();

        public class IndicatorName
        {
            public string Id { get; set; }
            public string kpiNameID { get; set; }
            public decimal KpiNO { get; set; }
            public string KpiName { get; set; }
            public string ViewMode { get; set; }
            public string Note { get; set; }

            public IndicatorName() { }
            public IndicatorName(string id, string kpiNameID, decimal kpiNO, string kpiName, string viewMode, string note)
            {
                Id = id;
                this.kpiNameID = kpiNameID;
                KpiNO = kpiNO;
                KpiName = kpiName;
                ViewMode = viewMode;
                Note = note;
            }
        }
    }

    public class PlantKPIDetail
    {
        public bool IsIndividual { get; set; }
        public string Plant { get; set; }
        public List<Detail> DetailList { get; set; } = new List<Detail>();
        public string Note { get; set; }

        public class Detail
        {
            public string Shift { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string KPIcolor { get; set; }
            public string KPIScale { get; set; }
            public string KPIDisplay { get; set; }
            public string ActualScore { get; set; }
            public string Note { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public Detail() { }

            public Detail(string shift, string firstName, string lastName, string kPIcolor, string kPIScale, string kPIDisplay, string actualScore, string note)
            {
                Shift = shift;
                FirstName = firstName;// ?? throw new ArgumentNullException(nameof(firstName));
                LastName = lastName;
                KPIcolor = kPIcolor;
                KPIScale = kPIScale;
                KPIDisplay = kPIDisplay;//?? throw new ArgumentNullException(nameof(kPIDisplay));
                ActualScore = actualScore;//?? throw new ArgumentNullException(nameof(actualScore));
                Note = note;
            }

            public Detail(string shift, string kPIcolor, string kPIScale, string kPIDisplay, string actualScore, string note)
            {
                Shift = shift;//?? throw new ArgumentNullException(nameof(shift));
                KPIcolor = kPIcolor;// ?? throw new ArgumentNullException(nameof(kPIcolor));
                KPIScale = kPIScale;// ?? throw new ArgumentNullException(nameof(kPIScale));
                KPIDisplay = kPIDisplay;// ?? throw new ArgumentNullException(nameof(kPIDisplay));
                ActualScore = actualScore;//?? throw new ArgumentNullException(nameof(actualScore));
                Note = note;
            }
        }
    }
}