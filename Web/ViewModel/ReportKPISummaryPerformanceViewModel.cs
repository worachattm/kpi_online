﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class ReportKPISummaryPerformanceViewModel
    {
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public SearchResult Result { get; set; }
        public RoleViewModel_Detail Role_Detail { get; set; } = new RoleViewModel_Detail();
        public List<IndicatorName> IndicatorNameList { get; set; }

        public class SearchCriteria
        {
            public string Department { get; set; }
            public string ReportType { get; set; }
            public string PeriodType { get; set; }
            public string TargetPeriod { get; set; }
            public string Year { get; set; }


            public List<SelectListItem> ddDepartment { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddArea { get; set; } = new List<SelectListItem>();

            public List<SelectListItem> ddReportType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPeriodType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddTargetPeriod { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddYear { get; set; } = new List<SelectListItem>();
        }

        public class SearchResult
        {
            public List<Department> departmentList { get; set; } = new List<Department>();

        }
        public class Department
        {
            public string departmentName { get; set; }
            public string departmentID { get; set; }

            public List<Area> areaList { get; set; } = new List<Area>();
        }
        public class Area
        {
            public string areaName { get; set; }
            public string areaID { get; set; }
            public List<KeyValuePair<string, string>> PlantList { get; set; } = new List<KeyValuePair<string, string>>();
            public List<sp_performance_all_area_Result> detail { get; set; } = new List<sp_performance_all_area_Result>();
        }
    }

    public class sp_performance_all_area_Result
    {
        public decimal? TKI_INDI_NO { get; set; }
        public string TKI_INDI_NAME { get; set; }
        public string TKI_INDI_NAME_ID { get; set; }

        public string TKI_INDI_CAL_MODE { get; set; }
        public string TKID_DEPARTMENT { get; set; }
        public string TKID_AREA { get; set; }
        public string TKID_SHIFT { get; set; }
        public string TKID_SHIFT_ID { get; set; }
        public decimal? AVG_SCORE { get; set; }
        public decimal? TKI_INDI_YEAR { get; set; }
        public string SET_INDEX_COLOR { get; set; }
        public string SCALE_COLOR { get; set; }
        public string SCALE_COLOR_CODE { get; set; }
        public decimal? TOC1 { get; set; }
        public decimal? TOC2 { get; set; }
        public decimal? TOC3 { get; set; }
        public decimal? TOC4 { get; set; }
        public decimal? TOC5 { get; set; }
        public decimal? UTIL { get; set; }
        public decimal? SPP { get; set; }
        public decimal? MOV { get; set; }
        public decimal? OFF { get; set; }
        public decimal? TLB { get; set; }
        public decimal? TPX { get; set; }
        public decimal? LABIX { get; set; }
        public decimal? MARINE { get; set; }
    }
}