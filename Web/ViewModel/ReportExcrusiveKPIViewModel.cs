﻿using KPI_ONLINE.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class ColorScaleCode
    {
        public string ColorCode1 { get; set; }
        public string ColorCode2 { get; set; }
        public string ColorCode3 { get; set; }
        public string ColorCode4 { get; set; }
        public string ColorCode5 { get; set; }
    }
    public class ReportExcrusiveKPIViewModel
    {
        public ColorScaleCode ColorScaleCode { get; set; } = new ColorScaleCode();
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public Dictionary<String, String> ColumnWeight { get; set; } = new Dictionary<string, string>();
        public int CountWeightCol { get; set; }
        public List<SearchResult> Result { get; set; } //= new List<SearchResult>();
        public RoleViewModel_Detail Role_Detail { get; set; } = new RoleViewModel_Detail();
        public HedgeDealSummaryExcelTemplate Template { get; set; } = new HedgeDealSummaryExcelTemplate();
        public List<IndicatorName> IndicatorNameList { get; set; }
        public List<KPIDetail> KPISoftList { get; set; }
        
        public class HedgeDealSummaryExcelTemplate
        {
            public string UserConfigId { get; set; }

            public List<HedgeDealSummaryColumnDetail> SystemConfig { get; set; } = new List<HedgeDealSummaryColumnDetail>();
            public List<HedgeDealSummaryColumnDetail> DefaultConfig { get; set; } = new List<HedgeDealSummaryColumnDetail>();
            public List<HedgeDealSummaryColumnDetail> UserConfig { get; set; } = new List<HedgeDealSummaryColumnDetail>();
        }
        public class HedgeDealSummaryColumnDetail
        {
            public decimal Order { get; set; }
            public string FieldName { get; set; }
            public string ColumnName { get; set; }
            public string DisplayName { get; set; }
            public bool IsEnabled { get; set; }
        }
        public class SearchCriteria
        {
            public string Department { get; set; }
            public string Area { get; set; }
            public string Plant { get; set; }
            public string Shift { get; set; }
            public string PeriodType { get; set; }
            public string TargetPeriod { get; set; }
            public string Year { get; set; } 

            public List<SelectListItem> ddPeriodType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddTargetPeriod { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddYear { get; set; } = new List<SelectListItem>();
        }
        public class SearchResult
        {
            public string DepartMent { get; set; }
            public string DepartMentID { get; set; }
            public List<Area> AreaList { get; set; } = new List<Area>();
        }
        public class Area
        {
            public string AreaName { get; set; }
            public string AreaID { get; set; }
            public List<Plant> PlantList { get; set; } = new List<Plant>();
        }
        public class Plant
        {
            public string PlantName { get; set; }
            public string PlantID { get; set; }
            public List<Shift> ShiftList { get; set; } = new List<Shift>();
        }
        public class Shift
        {
            public string ShiftName { get; set; }
            public string ShiftID { get; set; }
            public List<KPIDetail> Detail { get; set; } = new List<KPIDetail>();
        }
        public class KPIDetail
        {
            public Decimal? NO { get; set; }
            public string Area { get; set; }
            public string ActualScore { get; set; }
            public string Scale1 { get; set; }
            public string Scale2 { get; set; }
            public string Scale3 { get; set; }
            public string Scale4 { get; set; }
            public string Scale5 { get; set; }

            public decimal KpiScore { get; set; }
            public string ColorCode { get; set; }
            public string Scale8 { get; set; }
            public string Scale9 { get; set; }

            public string WeightASM { get; set; }
            public string WeightLTO { get; set; }
            public string WeightLTR { get; set; }
            public string WeightJG7_8 { get; set; }
            public string Weight9_12 { get; set; }

            

            public KPIDetail() { }
            public KPIDetail(int pNO, string pArea, string pActualScore, string pScale1, string pScale2, string pScale3, string pScale4, string pScale5, 
                string pWeightASM, string pWeightLTO, string pWeightLTR, string pWeightJG7_8, string pWeight9_12)
            {
                NO = pNO;
                Area = pArea;
                ActualScore = pActualScore;
                Scale1 = pScale1;
                Scale2 = pScale2;
                Scale3 = pScale3;
                Scale4 = pScale4;
                Scale5 = pScale5;

                WeightASM = pWeightASM;
                WeightLTO = pWeightLTO;
                WeightLTR = pWeightLTR;
                WeightJG7_8 = pWeightJG7_8;
                Weight9_12 = pWeight9_12;

            }

        }
        public class ConfigKPIIndator {
            public decimal? Year { get; set; }
            public List<DisplayConfig> TableDisplayConfig { get; set; } = new List<DisplayConfig>();
            public List<IndicatorConfig> TableIndicatorConfig { get; set; } = new List<IndicatorConfig>();
            public List<TKPI_REPORT_SOFT_KPI> SoftKPIs { get; set; } = new List<TKPI_REPORT_SOFT_KPI>();
            public List<ReportConfig> TableReportConfig { get; set; } = new List<ReportConfig>();
            public HedgeDealSummaryExcelTemplate Template { get; set; } = new HedgeDealSummaryExcelTemplate();
        }

        public class DisplayConfig {
            
            public decimal Order { get; set; }
            public string FieldName { get; set; }
            public string ColumnName { get; set; }
            public string DisplayName { get; set; }
            public bool IsEnabled { get; set; }
        }

        public class IndicatorConfig {
            public string TKWCId { get; set; }
            public string TKINId { get; set; }
            public string TKINName { get; set; }
            public decimal? ASM { get; set; } 
            public decimal? LTO { get; set; }
            public decimal? LTR { get; set; }
            public decimal? JG7_8 { get; set; }
            public decimal? JG9_12 { get; set; } 
        }

        public class ReportConfig {
            public decimal Year { get; set; }
            public string CreateBy { get; set; }
            public DateTime CreateDate { get; set; }
            public string CreateDateEx { get; set; }
            public string TKRCId { get; set; }
            public string ReportName { get; set; }
        }
    }
    public class REPORT_SOFT_KPI : TKPI_REPORT_SOFT_KPI
    {
        public string YEAR { get; set; }
    }
    public class sp_executive_summary_Result
    {
        public string TKID_DEPARTMENT { get; set; }
        public string TKID_AREA { get; set; }
        public string TKID_PLANT { get; set; }
        public string TKID_SHIFT { get; set; }
        public string TKID_TYPE { get; set; }
        public decimal? TKI_INDI_NO { get; set; }
        public string TKI_INDI_NAME { get; set; }
        public decimal? ACTUAL_SCORE { get; set; }
        public string TKI_INDI_CAL_MODE { get; set; }
        public decimal TKRC_YEAR { get; set; }
        public decimal? ASM { get; set; }
        public decimal? LTO { get; set; }
        public decimal? LTR { get; set; }
        public decimal? JG78 { get; set; }
        public decimal? JG912 { get; set; }
        public decimal? SET_INDEX_COLOR { get; set; }
        public string SCALE_COLOR { get; set; }
        public string SCALE_COLOR_CODE { get; set; }
        public string KPI_SCALE
        {
            get
            {
                var disPlay = "";
                switch (SET_INDEX_COLOR)
                {
                    case 1:
                        disPlay = SCALE_1_DISPLAY;
                        break;
                    case 2:
                        disPlay = SCALE_2_DISPLAY;
                        break;
                    case 3:
                        disPlay = SCALE_3_DISPLAY;
                        break;
                    case 4:
                        disPlay = SCALE_4_DISPLAY;
                        break;
                    case 5:
                        disPlay = SCALE_5_DISPLAY;
                        break;
                }
                return disPlay;


            }
        }
        public string SCALE_1_DISPLAY { get; set; }
        public string SCALE_2_DISPLAY { get; set; }
        public string SCALE_3_DISPLAY { get; set; }
        public string SCALE_4_DISPLAY { get; set; }
        public string SCALE_5_DISPLAY { get; set; }
        public string TKID_FIRST_NAME { get; set; }
        public string TKID_LAST_NAME { get; set; }
        public string NOTE { get; set; }
    }
}