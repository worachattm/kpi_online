﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;

namespace KPI_ONLINE.ViewModel
{
    public class SearchIndicatorsCriteriaViewModel
    {
        private List<IndicatorModel> GetDataSearchResult()
        {
            var query = new List<IndicatorModel>();
            return query;
        }

        public SearchIndicatorsCriteriaViewModel()
        {
            var fn = new IndicatorModel();
            this.IndicatorsDDL = IndicatorModel.GetIndicatorDDL();
            this.TypeDDL = IndicatorModel.GetTypeDDL();
            this.YearDDL = IndicatorModel.GetYearDDL();
            this.CalculateModeDDL = IndicatorModel.GetCalculateModeDDL();
            this.SearchResult = GetDataSearchResult();
            this.KPIScaleList = fn.GetKpiScaleMaster();
        }

        public List<KPIScale> KPIScaleList { get; set; } = new List<KPIScale>();
        public List<SelectListItem> IndicatorsDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> TypeDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> YearDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> CalculateModeDDL { get; set; } = new List<SelectListItem>();
        public List<IndicatorModel> SearchResult { get; set; } = new List<IndicatorModel>();

        [Display(Name = "KPI Indicators")]
        public string Indicator { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Display(Name = "Year")]
        public string Year { get; set; }
        [Display(Name = "Calculate Mode")]
        public string CalculateMode { get; set; }
    }

    public class IndicatorsManageViewModel
    {
        public IndicatorsManageViewModel()
        {

        }

        public IndicatorsManageViewModel(string Id)
        {
            for (int i = 1; i <= 5; i++)
            {
                var kpiScale = new KPIScale();
                kpiScale.Scale = i;
                KPIScaleList.Add(kpiScale);
            }
            this.CategoryDDL = IndicatorModel.GetCategoryDDL();
            this.TypeDDL = IndicatorModel.GetTypeDDL();
            this.CalculateModeDDL = IndicatorModel.GetCalculateModeDDL();
            this.IndicatorDDL = IndicatorModel.GetIndicatorDDL();
            this.IndicatorNameList = IndicatorModel.GetIndicatorName();

            if (!string.IsNullOrWhiteSpace(Id))
            {
                var fn = new IndicatorServiceModel();
                var temp = fn.Get(Id);

                this.Id = Id;
                this.IndicatorNameId = (temp.TKPI_INDICATORS_NAME.TKIN_ID).ToString();
                this.Type = temp.TKI_INDI_TYPE;
                this.CalculateMode = temp.TKI_INDI_CAL_MODE;
                //this.Category = temp.TKI_INDI_CATEGORY;
                this.From = temp.TKI_INDI_FROM.ToString("MMMM-yyyy",new CultureInfo("en-Us"));
                this.To = temp.TKI_INDI_TO.ToString("MMMM-yyyy", new CultureInfo("en-Us"));

                this.KPIScaleList.ForEach(scale =>
                {
                    if(scale.Scale == 1)
                    {
                        scale.DisplayColor = temp.TKI_INDI_1_COLOR;
                        scale.DisplayName = temp.TKI_INDI_1_DISPLAY;
                        scale.FromValue = temp.TKI_INDI_1_FROM_VAL;
                        scale.ToValue = temp.TKI_INDI_1_TO_VAL;
                    } else if (scale.Scale == 2)
                    {
                        scale.DisplayColor = temp.TKI_INDI_2_COLOR;
                        scale.DisplayName = temp.TKI_INDI_2_DISPLAY;
                        scale.FromValue = temp.TKI_INDI_2_FROM_VAL;
                        scale.ToValue = temp.TKI_INDI_2_TO_VAL;
                    }
                    else if (scale.Scale == 3)
                    {
                        scale.DisplayColor = temp.TKI_INDI_3_COLOR;
                        scale.DisplayName = temp.TKI_INDI_3_DISPLAY;
                        scale.FromValue = temp.TKI_INDI_3_FROM_VAL;
                        scale.ToValue = temp.TKI_INDI_3_TO_VAL;
                    }
                    else if (scale.Scale == 4)
                    {
                        scale.DisplayColor = temp.TKI_INDI_4_COLOR;
                        scale.DisplayName = temp.TKI_INDI_4_DISPLAY;
                        scale.FromValue = temp.TKI_INDI_4_FROM_VAL;
                        scale.ToValue = temp.TKI_INDI_4_TO_VAL;
                    }
                    else if (scale.Scale == 5)
                    {
                        scale.DisplayColor = temp.TKI_INDI_5_COLOR;
                        scale.DisplayName = temp.TKI_INDI_5_DISPLAY;
                        scale.FromValue = temp.TKI_INDI_5_FROM_VAL;
                        scale.ToValue = temp.TKI_INDI_5_TO_VAL;
                    }

                });
            }
        }

        public List<SelectListItem> IndicatorDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> CategoryDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> TypeDDL { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> CalculateModeDDL { get; set; } = new List<SelectListItem>();

        public string Id { get; set; }
        public bool IsEditMode
        {
            get { return !string.IsNullOrWhiteSpace(this.Id); }
        }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} over of length 200.")]
        [Display(Name = "Indicator Name")]
        [MaxLength(200)]
        public string IndicatorNameId { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        [MaxLength(10)]
        [Display(Name = "Type")]
        public string Type { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        [MaxLength(10)]
        [Display(Name = "Indicator Category")]
        public string Category { get; set; }

        [Required]
        [Display(Name = "From Period")]
        public string From { get; set; }

        [Required]
        [Display(Name = "To Period")]
        public string To { get; set; }

        [Required]
        [StringLength(12, ErrorMessage = "The {0} over of length 12.")]
        [MaxLength(12)]
        [Display(Name = "Calculate Mode")]
        public string CalculateMode { get; set; }

        [ValidateEachItem]
        public List<KPIScale> KPIScaleList { get; set; } = new List<KPIScale>();

        public List<IndicatorName> IndicatorNameList { get; set; } = new List<IndicatorName>();
    }

    public class IndicatorName
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string File { get; set; }
        public string FileName { get; set; }
        public decimal No { get; set; }
    }

    public class KPIScale1
    {

        public List<SelectListItem> DisplayColorDDL { get; set; } = new List<SelectListItem>();

        public string Id { get; set; }
        public string DisplayColor { get; set; }

        public string CodeColor
        {
            get { return this.DisplayColor.Equals("GRAY", StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.GRAY : this.DisplayColor.Equals("YELLOW", StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.YELLOW : this.DisplayColor.Equals("RED", StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.RED : ConstantPrm.ColorCodeKPI.GREEN; }

        }

    }

    public class KPIScale 
    {
        public KPIScale()
        {
            this.DisplayColorDDL = IndicatorModel.GetDisplayColorDDL();
            this.DisplayColor = "GRAY";
        }
        public List<SelectListItem> DisplayColorDDL { get; set; } = new List<SelectListItem>();

        public string Id { get; set; }
        [Required]
        [Display(Name = "Scale")]
        [Range(1, 5, ErrorMessage = "Please enter valid integer Number")]
        public int Scale { get; set; }
        [Required]
        [Display(Name = "From Value")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public decimal? FromValue { get; set; }
        [Required]
        [Display(Name = "To Value")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public decimal? ToValue { get; set; }
        [Required]
        [Display(Name = "Display Name")]
        [MaxLength(50)]
        [StringLength(50, ErrorMessage = "The {0} over of length 50.")]
        public string DisplayName { get; set; }
        [Required]
        [Display(Name = "Display Colors")]
        [MaxLength(20)]
        [StringLength(20, ErrorMessage = "The {0} over of length 20.")]
        public string DisplayColor { get; set; }

        public string CodeColor
        {
            get { return this.DisplayColor.Equals("GRAY" , StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.GRAY : this.DisplayColor.Equals("YELLOW", StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.YELLOW : this.DisplayColor.Equals("RED", StringComparison.OrdinalIgnoreCase) ? ConstantPrm.ColorCodeKPI.RED : ConstantPrm.ColorCodeKPI.GREEN; }
            
        }

    }
}