﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class UsersViewModel
    {
        public UsersViewModel_Search users_Search { get; set; }
        public UsersViewModel_Detail users_Detail { get; set; }

        public List<SelectListItem> ddlCompany { get; set; }
        public List<SelectListItem> ddlDepartment { get; set; }
        public List<SelectListItem> ddlArea { get; set; }
        public List<SelectListItem> ddlPlant { get; set; }
        public List<SelectListItem> ddlRole { get; set; }
        public List<SelectListItem> ddlShift { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }
        public string users_UserRoleJSON { get; set; }
    }

    public class UserAuthorize
    {
        public string menuTree { get; set; } = "[]";
        public string kpiTree { get; set; } = "[]";
        public string departmentTree { get; set; } = "[]";
        public string areaTree { get; set; } = "[]";
        public string plantTree { get; set; } = "[]";
        public string shiftTree { get; set; } = "[]";
    }

    public class UsersViewModel_Search
    {
        public List<string> EmployeeCodeList { get; set; } = new List<string>();
        [Display(Name = "FIRST NAME")]
        public string sFirstName { get; set; }
        [Display(Name = "LAST NAME")]
        public string sLastName { get; set; }
        [Display(Name = "EMPLOYEE CODE")]
        public string sEmployeeCode { get; set; }
        [Display(Name = "DEPARTMENT")]
        public string sDepartmentId { get; set; }
        [Display(Name = "SHIFT")]
        public string sShiftId { get; set; }
        [Display(Name = "AREA")]
        public string sAreaId { get; set; }
        [Display(Name = "PLANT")]
        public string sPlantId { get; set; }
        [Display(Name = "COMPANY")]
        public string sCompanyId { get; set; }
        [Display(Name = "USER ROLE")]
        public string sRoleId { get; set; }
        [Display(Name = "STATUS")]
        public string sStatus { get; set; }
        public List<UsersViewModel_SearchData> sSearchData { get; set; } = new List<UsersViewModel_SearchData>();
    }

    public class UserViewModelSearchModal
    {
        public string sFirstName { get; set; }
        public string sLastName { get; set; }
        public int sDepartmentId { get; set; }
        public int sAreaId { get; set; }
        public int sPlantId { get; set; }
        public int sCompanyId { get; set; }
    }

    public class UsersViewModel_SearchData
    {
        public string fullName
        {
            get { return string.Format("{0} {1}", dFirstName, dLastName); }
        }
        public string dUserCode { get; set; }
        public string Id { get; set; }
        public string dFirstName { get; set; }
        public string dLastName { get; set; }
        public string dDepartmentId { get; set; }
        public string dShiftId { get; set; }
        public string dCompanyId { get; set; }
        public string dRoleId { get; set; }
        public string dPlantId { get; set; }
        public string dAreaId { get; set; }
        public string dStatus { get; set; }


        public string Employee { get; set; }
        public string Status { get; set; }
        public string DepartmentName { get; set; }
        public string ShiftName { get; set; }
        public string CompanyName { get; set; }
        public string UserRoleName { get; set; }
        public string AreaName { get; set; }
        public string PlantName { get; set; }
        public string Tel { get; set; }

    }


    public class UserMonitorViewModel
    {
        public bool IsImportMode { get; set; }
        public List<SelectListItem> RoleDDL { get; set; }
        public List<SelectListItem> DepartmentDDL { get; set; }
        public List<SelectListItem> CompanyDDL { get; set; }
        public List<SelectListItem> AreaDDL { get; set; }
        public List<SelectListItem> ShiftDDL { get; set; }
        public List<SelectListItem> PlantDDL { get; set; }
        public List<UserMonitorDiff> UserMonitorDiffLs { get; set; }
    }

    public class UserMonitorDiff
    {
        public bool IsShowRow
        {
            get { return !(IsDuplicateUsercode 
                    && IsDuplicateFirstName
                    && IsDuplicateLastName
                    && IsDuplicateDepartmentName
                    && IsDuplicateShiftName
                    && IsDuplicateCompanyName
                    && IsDuplicateAreaName
                    && IsDuplicatePlantName
                    && IsDuplicateUserAD) || string.IsNullOrWhiteSpace(TUserCode); }
        }
        public bool IsDuplicateUsercode
        {
            get 
            {
                return (MUserCode.Equals(SUserCode, StringComparison.OrdinalIgnoreCase) || MUserCode.Equals(TUserCode, StringComparison.OrdinalIgnoreCase)); 
            }
        }

        public bool IsDuplicateFirstName
        {
            get 
            {
                return (MFirstName.Equals(SFirstName, StringComparison.OrdinalIgnoreCase) || MFirstName.Equals(TFirstName, StringComparison.OrdinalIgnoreCase)); 
            }
        }

        public bool IsDuplicateLastName
        {
            get { return (MLastName.Equals(SLastName, StringComparison.OrdinalIgnoreCase) || MLastName.Equals(TLastName, StringComparison.OrdinalIgnoreCase)); }
        }

        public bool IsDuplicateDepartmentName
        {
            //get { return (MDepartmentName.Equals(SDepartmentName, StringComparison.OrdinalIgnoreCase) || MDepartmentName.Equals(TDepartmentName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MDepartmentName == SDepartmentName) || (MDepartmentName == TDepartmentName); }
        }

        public bool IsDuplicateShiftName
        {
            //get { return (MShiftName.Equals(SShiftName, StringComparison.OrdinalIgnoreCase) || MShiftName.Equals(TShiftName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MShiftName == SShiftName || MShiftName == TShiftName); }
        }

        public bool IsDuplicateCompanyName
        {
            //get { return (MCompanyName.Equals(SCompanyName, StringComparison.OrdinalIgnoreCase) || MCompanyName.Equals(TCompanyName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MCompanyName == SCompanyName || MCompanyName == TCompanyName); }
        }

  
        public bool IsDuplicateAreaName
        {
            //get { return (MAreaName.Equals(SAreaName, StringComparison.OrdinalIgnoreCase) || MAreaName.Equals(TAreaName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MAreaName == SAreaName) || (MAreaName == TAreaName); }
        }

        public bool IsDuplicatePlantName
        {
            //get { return (MPlantName.Equals(SPlantName, StringComparison.OrdinalIgnoreCase) || MPlantName.Equals(TPlantName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MPlantName == SPlantName || MPlantName == TPlantName); }
        }

        public bool IsDuplicateUserAD
        {
            //get { return (MPlantName.Equals(SPlantName, StringComparison.OrdinalIgnoreCase) || MPlantName.Equals(TPlantName, StringComparison.OrdinalIgnoreCase)); }
            get { return (MUserAD == SUserAD || MUserAD == TUserAD); }
        }


        //public bool IsDuplicateTel
        //{
        //    get 
        //    {
        //        return (MTel == STel || MTel == TTel);
        //    }
        //}

        public string SUserAD { get; set; }

        public string SUserCode { get; set; } 
        public string SFirstName { get; set; } 
        public string SLastName { get; set; } 
        public string SDepartmentName { get; set; } 
        public string SShiftName { get; set; } 
        public string SCompanyName { get; set; } 
        public string SUserRoleId { get; set; } 
        public string SAreaName { get; set; } 
        public string SPlantName { get; set; } 
        //public string STel { get; set; } 

        public string TUserAD { get; set; }
        public string TUserCode { get; set; } 
        public string TFirstName { get; set; } 
        public string TLastName { get; set; } 
        public string TDepartmentName { get; set; } 
        public string TShiftName { get; set; } 
        public string TCompanyName { get; set; } 
        public string TUserRoleId { get; set; } 
        public string TAreaName { get; set; } 
        public string TPlantName { get; set; } 
        //public string TTel { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        public string MUserAD { get; set; }


        [Required]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        [MaxLength(10)]
        public string MUserCode { get; set; } 


        [Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        public string MFirstName { get; set; } 

        [Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        public string MLastName { get; set; } 

        [Required]
        public string MDepartmentName { get; set; } 

        [Required]
        public string MShiftName { get; set; } 

        [Required]
        public string MCompanyName { get; set; } 

        [Required]
        public string MAreaName { get; set; } 

        [Required]
        public string MPlantName { get; set; } 

        [StringLength(20, ErrorMessage = "The {0} over of length 20.")]
        [MaxLength(20)]
        public string MTel { get; set; } 

    }


    public class UsersViewModel_Detail
    {
        public bool IsEditMode 
        {
            get { return !string.IsNullOrWhiteSpace(UsersID); }
                
        }
        public string UsersID { get; set; }

        [Required]
        [Display(Name = "DEPARTMENT")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public string DepartmentId { get; set; }

        //[Required]
        [Display(Name = "AREA")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public string AreaId { get; set; }

        //[Required]
        [Display(Name = "PLANT")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public string PlantId { get; set; }

        [Display(Name = "SHIFT")]
        public string ShiftId { get; set; }

        [Required]
        [Display(Name = "USER ROLE")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public string RoleId { get; set; }

        [Required]
        [Display(Name = "COMPANY")]
        public string CompanyId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        [Display(Name = "FIRST NAME")]
        public string FirstNameTH { get; set; }

        //[Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        [Display(Name = "LAST NAME")]
        public string LastNameTH { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        [MaxLength(10)]
        [Display(Name = "EMPLOYEE ID")]
        public string EmployeeID { get; set; }

        [Display(Name = "TEL")]
        [StringLength(20, ErrorMessage = "The {0} over of length 20.")]
        [MaxLength(20)]
        public string Tel { get; set; }

        [Required]
        [Display(Name = "STATUS")]
        [MaxLength(10)]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        public string Status { get; set; }
    }


}