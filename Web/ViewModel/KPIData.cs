﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class KPIData
    {
        public Search search { get; set; } = new Search();
        public SearchResult result { get; set; } = new SearchResult();

    }

    public class Search
    {
        public string Department { get; set; }
        public string Area { get; set; }
        public string Shift { get; set; }
        public string KPIIndicator { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<SelectListItem> ddDepartment { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddArea { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddShift { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddKPIIndicator { get; set; } = new List<SelectListItem>();
        //public List<SelectListItem> ddPeriodFrom { get; set; } = new List<SelectListItem>();
        //public List<SelectListItem> ddPeriodTo { get; set; } = new List<SelectListItem>();
        //public List<SelectListItem> ddFirstName { get; set; } = new List<SelectListItem>();
        //public List<SelectListItem> ddLastName { get; set; } = new List<SelectListItem>();


    }
    public class SearchResult
    {
        public DateTime KPIDate { get; set; }
        public string kKPIDateString
        {
            get
            {
                return KPIDate.ToString("MMMM YYYY");
            }
        }
        public string DepartMent { get; set; }
        public KPILIST KPI { get; set; }


    }

    public class KPILIST
    {
        public string KPIName { get; set; }
        public KPIDetail Detail { get; set; }


    }

    public class KPIDetail
    {
        public string NO { get; set; }
        public string Area { get; set; }
        public string Shift { get; set; }
        public string StreffName { get; set; }
        public string ActualScore { get; set; }
        public string KPIScale { get; set; }
        public string KPIScore { get; set; }
        public string CreateBy { get; set; }
        public string CreateDate { get; set; }
        public RecAction Action { get; set; }

    }
    public class RecAction
    {
        public bool copy { get; set; }
        public bool edit { get; set; }
        public bool delete { get; set; }
    }

}