﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class VpAndPomKPIResultReportViewModel
    {
        public SearchCriteria Criteria { get; set; } = new SearchCriteria();
        public SearchResult Result { get; set; }// = new SearchResult();
        public ColorScaleCode ColorScaleCode { get; set; } = new ColorScaleCode();
        public List<IndicatorName> IndicatorNameList { get; set; }

        public class SearchResult
        {
            public List<string> HeaderList { get; set; } = new List<string>();
            public List<sp_vp_and_pom> data { get; set; } = new List<sp_vp_and_pom>();

        }

        public class SearchCriteria
        {
            public string ReportMode { get; set; }
            public string Year { get; set; }
            public string PeriodType { get; set; }
            public string TargetPeriod { get; set; }


            public List<SelectListItem> ddReportMode { get; set; } = new List<SelectListItem>() {
                new SelectListItem { Text = "Department", Value = "Department" },
                new SelectListItem { Text = "Area", Value = "Area" }
            };
            public List<SelectListItem> ddYear { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddPeriodType { get; set; } = new List<SelectListItem>();
            public List<SelectListItem> ddTargetPeriod { get; set; } = new List<SelectListItem>();
        }
        public class sp_vp_and_pom
        {

            public Decimal? TKI_INDI_NO { get; set; }
            public String TKI_INDI_ID { get; set; }
            public String  TKI_INDI_CAL_MODE { get; set; }
            public Decimal? TKI_INDI_YEAR { get; set; }
            public Decimal? EVPC_SCORE { get; set; }
            public Decimal? EVPC_INDEX_COLOR { get; set; }
            public Decimal? MRVP_SCORE { get; set; }
            public Decimal? MRVP_INDEX_COLOR { get; set; }
            public Decimal? MMVP_SCORE { get; set; }
            public Decimal? MMVP_INDEX_COLOR { get; set; }
            public Decimal? MPVP_SCORE { get; set; }
            public Decimal? MPVP_INDEX_COLOR { get; set; }
            public Decimal? APU_A_SCORE { get; set; }
            public Decimal? APU_A_INDEX_COLOR { get; set; }
            public Decimal? APU_B_SCORE { get; set; }
            public Decimal? APU_B_INDEX_COLOR { get; set; }
            public Decimal? APU_C_SCORE { get; set; }
            public Decimal? APU_C_INDEX_COLOR { get; set; }
            public Decimal? APU_DM_SCORE { get; set; }
            public Decimal? APU_DM_INDEX_COLOR { get; set; }
            public Decimal? APU_DO_SCORE { get; set; }
            public Decimal? APU_DO_INDEX_COLOR { get; set; }
            public Decimal? APU_E_SCORE { get; set; }
            public Decimal? APU_E_INDEX_COLOR { get; set; }
            public Decimal? APU_F_SCORE { get; set; }
            public Decimal? APU_F_INDEX_COLOR { get; set; }
        }         
                  
    }             
}