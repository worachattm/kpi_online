﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class RoleViewModel
    {
        public bool IsEditMode
        {
            get { return !string.IsNullOrWhiteSpace(RoleId); }
        }
        public string RoleId { get; set; }

        public List<SelectListItem> ddlCompany { get; set; }
        public List<SelectListItem> ddlDepartment { get; set; }
        public List<SelectListItem> ddlArea { get; set; }
        public List<SelectListItem> ddlPlant { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }
        public UsersViewModel_Search users_Search { get; set; }
        public RoleViewModel_Seach role_Search { get; set; }
        public RoleViewModel_Detail role_Detail { get; set; }

        public List<SelectListItem> ddl_RoleName { get; set; }
        public List<SelectListItem> ddl_RoleDesc { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
    }

    public class RoleViewModel_Seach
    {
        [Display(Name = "ROLE NAME")]
        public string sRoleName { get; set; }
        [Display(Name = "DESCRIPTION")]// Display (dd/MM/yyyy to dd/MM/yyyy)  
        public string sRoleDescription { get; set; }
        [Display(Name = "STATUS")]
        public string sRoleStatus { get; set; }
        public List<RoleViewModel_SeachData> sSearchData { get; set; } = new List<RoleViewModel_SeachData>();
    }

    public class RoleViewModel_SeachData
    {
        public bool HaveDelete { get; set; }
        public string dRolID { get; set; }
        [Display(Name = "ROLE NAME")]
        public string dRolName { get; set; }
        [Display(Name = "STATUS")]
        public string dRolStatus { get; set; }
        [Display(Name = "DESCRIPTION")]
        public string dRolDesc { get; set; }

    }

    public class RoleViewModel_Detail
    {
        public string RolID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} over of length 100.")]
        [MaxLength(100)]
        [Display(Name = "ROLE NAME")]
        public string RolName { get; set; }

        public string RolType { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} over of length 200.")]
        [MaxLength(200)]
        [Display(Name = "DESCRIPTION")]
        public string RolDesc { get; set; }

        [Required]
        [Display(Name = "STATUS")]
        [MaxLength(10)]
        [StringLength(10, ErrorMessage = "The {0} over of length 10.")]
        public string RolStatus { get; set; }

  

        public string MenuTree { get; set; }
        public string MenuTreeId { get; set; }

        public string KPITree { get; set; }
        public string KPITreeId { get; set; }
        public string DepartmentTree { get; set; }
        public string DepartmentTreeId { get; set; }
        public string AreaTree { get; set; }
        public string AreaTreeId { get; set; }
        public string ShiftTree { get; set; }
        public string ShiftTreeId { get; set; }
        public string PlantTree { get; set; }
        public string PlantTreeId { get; set; }
        public List<RoleViewModel_Menu> RoleMenu { get; set; }
        public List<UserInRole> UserInRoleList { get; set; } = new List<UserInRole>();

    }

    public class UserInRole
    {
        public string EmpId { get; set; }
        [Display(Name = "Employee Code")]
        public string EmpCode { get; set; }
        [Display(Name = "Name")]
        public string FullName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Display(Name = "Department")]
        public string Department { get; set; }
        [Display(Name = "Area")]
        public string Area { get; set; }
        [Display(Name = "Plant")]
        public string Plant { get; set; }
        [Display(Name = "Company")]
        public string Company { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }

    public class RoleViewModel_Menu
    {
        public string RoleMenuID { get; set; }
        public string MenuID { get; set; }
        public string MenuGroup { get; set; }
        public string MenuParentID { get; set; }
        public string MenuLevel { get; set; }
        public string MenuListNo { get; set; }
        public string MenuDesc { get; set; }
    }


    public class RoleViewModel_MenuWithRole
    {
        public string dMenuID { get; set; }
        public string dMenuGroup { get; set; }
        public string dMenuParentID { get; set; }
        public string dMenuLevel { get; set; }
        public string dMenuListNo { get; set; }
        public string dMenuDesc { get; set; }
        public string dMenuType { get; set; }
        public string dRoleID { get; set; }
        public string dRoleName { get; set; }
        public string dRoleDesc { get; set; }
        public string dRoleStatus { get; set; }
        public string dRoleMenuID { get; set; }
    }

}