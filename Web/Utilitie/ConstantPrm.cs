﻿using System.Collections.Generic;

namespace Web.Utilitie
{
    public static class ConstantPrm
    {
        public static string AppName = System.Configuration.ConfigurationManager.AppSettings["AppName"];

        public static string MessageSuccess = "Successfully!"; // For return message
        public static string DefaultRole = "DEFAULT ROLE";

        public static class ACIONMENU
        {
            public static readonly string ACTION_CREATE = "create";
            public static readonly string ACTION_EDIT = "edit";
            public static readonly string ACTION_SEARCH = "search";
            public static readonly string ACTION_DELETE = "delete";
            public static readonly string ACTION_EXPORT = "export";
            public static readonly string ACTION_SETTING = "setting";
        }

        public static class ColorCodeKPI
        {
            public static readonly string GRAY = "#808080";
            public static readonly string RED = "#ff6a6a";
            public static readonly string YELLOW = "#ffa500";
            public static readonly string GREEN = "#2e8b57";
        }

        public static class MenuAuthorize
        {
            public const string KPI_MANAGE_MENU = "KPI Manage";
            public const string KPI_MANAGE_MENU_SEARCH = "KPI Manage/search";
            public const string KPI_MANAGE_MENU_CREATE = "KPI Manage/create";
            public const string KPI_MANAGE_MENU_EDIT = "KPI Manage/edit";
            public const string KPI_MANAGE_MENU_DELETE = "KPI Manage/delete";

            public const string USER_MENU = "User";
            public const string USER_SEARCH = "User/search";
            public const string USER_CREATE = "User/create";
            public const string USER_EDIT = "User/edit";
            public const string USER_DELETE = "User/delete";
            public const string USER_EXPORT = "User/export";

            public const string ROLE_MENU = "User Roles";
            public const string ROLE_SEARCH = "User Roles/search";
            public const string ROLE_CREATE = "User Roles/create";
            public const string ROLE_EDIT = "User Roles/edit";
            public const string ROLE_DELETE = "User Roles/delete";

            public const string PIS_MENU = "PIS User Manage";
            public const string PIS_CREATE = "PIS User Manage/create";
            public const string PIS_IMPORT = "PIS User Manage/import";
            public const string PIS_EXPORT = "PIS User Manage/export";

            public const string DASHBOARD_MENU = "Dashboard";
            public const string ADMIN_MENU = "Admin";
            public const string REPORT_MENU = "Report";
            public const string IMPORT_MENU = "Import Data";


            public const string EXECUTIVE_REPORT_MENU = "Executive KPI Summary";
            public const string EXECUTIVE_REPORT_SEARCH = "Executive KPI Summary/search";
            public const string EXECUTIVE_REPORT_EXPORT = "Executive KPI Summary/export";
            public const string EXECUTIVE_REPORT_CONFIG = "Executive KPI Summary/setting";

            public const string KPI_PERFORMANCE_REPORT_MENU = "KPI Summary - KPI Performance";
            public const string KPI_PERFORMANCE_REPORT_SEARCH = "KPI Summary - KPI Performance/search";
            public const string KPI_PERFORMANCE_REPORT_EXPORT = "KPI Summary - KPI Performance/export";

            public const string KPI_SUMMARY_MENU = "KPI Summary - VP & POM Result";
            public const string KPI_SUMMARY_SEARCH = "KPI Summary - VP & POM Result/search";
            public const string KPI_SUMMARY_EXPORT = "KPI Summary - VP & POM Result/export";

            public const string KPI_DETAIL_REPORT_MENU = "KPI Detail by Area";
            public const string KPI_DETAIL_REPORT_SEARCH= "KPI Detail by Area/search";
            public const string KPI_DETAIL_REPORT_EXPORT= "KPI Detail by Area/export";

            public const string MAP_REPORT_MENU = "KPI Summary - Map View";
            public const string MAP_REPORTT_SEARCH = "KPI Summary - Map View/search";
            public const string MAP_REPORTT_EXPORT = "KPI Summary - Map View/export";
            public const string MAP_REPORTT_CONFIG = "KPI Summary - Map View/setting";
            public const string MAP_REPORTT_ACTION = "KPI Summary - Map View/action";

            public const string ERROR_MENU = "Error";

        }

        public static class FieldExportUserMonitor
        {
            public static readonly string EMP_CODE = "EmployeeCode";
            public static readonly string FIRST_NAME = "FirstName";
            public static readonly string LAST_NAME = "LastName";
            public static readonly string DEPARTMENT = "Department";
            public static readonly string AREA = "Area";
            public static readonly string PLANT = "Plant";
            public static readonly string SHIFT = "Shift";
            public static readonly string COMPANY = "Company";
            //public static readonly string TEL = "Tel";
            public static readonly string USER_AD = "UserAD";
        }

    }
}