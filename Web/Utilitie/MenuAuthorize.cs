﻿using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Utilitie;

namespace KPI_ONLINE.Utilitie
{
    public class MenuAuthorize : AuthorizeAttribute
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private readonly string[] menus;
        public MenuAuthorize(params string[] menus)
        {
            this.menus = menus;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var username = Const.User.UserName;
            var roleId = Const.User.RoleId;
            bool authorize = false;
            foreach (var menu in menus)
            {
                var tempMenu = menu.Split('/');

                if (tempMenu.Count() == 1)
                {
                    var parentMenu = tempMenu.ElementAtOrDefault(0);

                    var IdMenu = unitOfWork.MenuRepository.GetAll(c => c.LNG_DESCRIPTION == parentMenu).FirstOrDefault()?.MEU_ROW_ID;

                    if (IdMenu != null)
                    {
                        var IdMenuAuthUser = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId && c.TKRM_MENU == IdMenu).FirstOrDefault();

                        if (IdMenuAuthUser != null)
                        {
                            var AuthUser = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId).Select(c => c.TKRM_MENU).ToList();
                            var SubMenuAction = new List<string>();

                            SubMenuAction = unitOfWork.MenuRepository.GetAll(c => c.MEU_PARENT_ID == IdMenu && AuthUser.Contains(c.MEU_ROW_ID)).Select(c => c.LNG_DESCRIPTION).ToList();

                            var AuthMenu = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId && (c.TKPI_MENU.MEU_LEVEL == "1" || c.TKPI_MENU.MEU_LEVEL == "2")).Select(c => c.TKPI_MENU.LNG_DESCRIPTION).ToList();

                            if (AuthMenu.Count > 0)
                            {
                                SubMenuAction.AddRange(AuthMenu);
                            }

                            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(username ?? ""), SubMenuAction.ToArray());
                            authorize = true;
                        }
                    }

                    if (parentMenu == "Error")
                    {
                        var AuthMenu = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId && (c.TKPI_MENU.MEU_LEVEL == "1" || c.TKPI_MENU.MEU_LEVEL == "2")).Select(c => c.TKPI_MENU.LNG_DESCRIPTION).ToList();

                        HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(username ?? ""), AuthMenu.ToArray());

                        authorize = true;
                    }

                }
                else if (tempMenu.Count() == 2)
                {
                    var parentMenu = menu.Split('/').ElementAtOrDefault(0);
                    var childMenu = menu.Split('/').ElementAtOrDefault(1);

                    var IdMenu = unitOfWork.MenuRepository.GetAll(c => c.LNG_DESCRIPTION == parentMenu).FirstOrDefault()?.MEU_ROW_ID;
                    var IdSubMenu = unitOfWork.MenuRepository.GetAll(c => c.MEU_PARENT_ID == IdMenu && c.LNG_DESCRIPTION == childMenu).FirstOrDefault()?.MEU_ROW_ID;

                    if (IdSubMenu != null)
                    {
                        var IdMenuAuthUser = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId && c.TKRM_MENU == IdSubMenu).FirstOrDefault();

                        if (IdMenuAuthUser != null)
                        {
                            var AuthUser = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId).Select(c => c.TKRM_MENU).ToList();
                            var SubMenuAction = unitOfWork.MenuRepository.GetAll(c => c.MEU_PARENT_ID == IdMenu && AuthUser.Contains(c.MEU_ROW_ID)).Select(c => c.LNG_DESCRIPTION).ToList();

                            var AuthMenu = unitOfWork.RoleMenuRepository.GetAll(c => c.TKR_ID == roleId && (c.TKPI_MENU.MEU_LEVEL == "1" || c.TKPI_MENU.MEU_LEVEL == "2")).Select(c => c.TKPI_MENU.LNG_DESCRIPTION).ToList();

                            if (AuthMenu.Count > 0)
                            {
                                SubMenuAction.AddRange(AuthMenu);
                            }

                            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(username ?? ""), SubMenuAction.ToArray());

                            authorize = true;
                        }
                    }
                }

            }

            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {//filterContext.HttpContext.User.Identity.IsAuthenticated
            if (Const.User.UserName == null)
            {

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "HttpNonUser" }));
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
            }

            //filterContext.Result = new HttpUnauthorizedResult();
        }

        //public CustomAuthorize()
        //{
        //    View = "error";
        //    Master = String.Empty;
        //}

        //public String View { get; set; }
        //public String Master { get; set; }

        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    base.OnAuthorization(filterContext);
        //    CheckIfUserIsAuthenticated(filterContext);
        //}

        //private void CheckIfUserIsAuthenticated(AuthorizationContext filterContext)
        //{
        //    // If Result is null, we're OK: the user is authenticated and authorized. 
        //    if (filterContext.Result == null)
        //        return;

        //    // If here, you're getting an HTTP 401 status code. In particular,
        //    // filterContext.Result is of HttpUnauthorizedResult type. Check Ajax here. 
        //    if (filterContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        if (String.IsNullOrEmpty(View))
        //            return;
        //        var result = new ViewResult { ViewName = View, MasterName = Master };
        //        filterContext.Result = result;
        //    }
        //}

        //=====================

        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        //if not logged, it will work as normal Authorize and redirect to the Login
        //        base.HandleUnauthorizedRequest(filterContext);

        //    }
        //    else
        //    {
        //        //logged and wihout the role to access it - redirect to the custom controller action
        //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
        //    }
        //}
    }
}