﻿using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace KPI_ONLINE.Utilitie
{
    public class ShareFn : BaseClass
    {
        public static DateTime? ConvertStringToDateTime(string DateString, string DateStringFormat = "dd/MM/yyyy")
        {
            try
            {
                if (String.IsNullOrEmpty(DateString))
                {
                    return null;
                }
                else
                {
                    DateTime _Dt;

                    DateTime.TryParseExact(DateString,
                           DateStringFormat,
                           new CultureInfo("en-Us"),
                           DateTimeStyles.None,
                           out _Dt);

                    if (_Dt != null)
                    {
                        _Dt = _Dt.Date;
                        return _Dt;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch
            {
                return null;
            }

        }

        public static string ConvertDateTimeToString(DateTime pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            return pDate.ToString(DateStringFormat, new CultureInfo("en-Us"));
        }

        public string GetSiteRootUrl(string url)
        {
            try
            {
                string protocol;

                if (HttpContext.Current.Request.IsSecureConnection)
                    protocol = "https";
                else
                    protocol = "http";

                log.Debug(" protocol : " + protocol);

                StringBuilder uri = new StringBuilder(protocol + "://");

                string hostname = HttpContext.Current.Request.Url.Host;

                log.Debug(" hostname : " + hostname);

                uri.Append(hostname);

                int port = HttpContext.Current.Request.Url.Port;

                log.Debug(" port : " + port);

                if (port != 80 && port != 443)
                {
                    uri.Append(":");
                    uri.Append(port.ToString());
                }

                string APPName = System.Configuration.ConfigurationManager.AppSettings["AppName"];
                log.Debug(" APPName : " + APPName);

                if (url != "")
                {
                    return Path.Combine(uri.ToString(), APPName, url);
                }
                else
                {
                    return "";
                }

            }
            catch (Exception e)
            {
                log.Error("# error :  " + e.StackTrace);
                log.Error("# error :  " + e.InnerException);
                log.Error("# error :  " + e.Message);
                return "";
            }

        }

        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        public string GetPathofPersonalImg(Int64 EmployeeCode , string CompanyName)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            string pathSignature = GetSiteRootUrl("content/image/Emp/NoImage.png");
            //string tmpPath = GetSiteRootUrl("content/image/Emp/" + UserName);
            foreach (string _itemExt in extentions)
            {
                string PathTemp = string.Format("http://srieng02/pic/{0}/{1}{2}", CompanyName, EmployeeCode, _itemExt);
                if (RemoteFileExists(PathTemp))
                {
                    pathSignature = PathTemp;
                    break;
                }
            }

            //pathSignature = GetSiteRootUrl("content/image/Emp/NoImage.png");
            
            return pathSignature;
        }


    }
}