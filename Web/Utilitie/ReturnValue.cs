﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Utilitie
{
    public class ReturnValue
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Result
        {
            get { return Status ? "success" : "danger"; }
        }
        

    }
}