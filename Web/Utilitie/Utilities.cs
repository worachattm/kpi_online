﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Web.Utilitie
{
    public class Utilities
    {
        public string ConvertFileToBase64(HttpPostedFileBase FileUpload)
        {
            string theFileName = Path.GetFileName(FileUpload.FileName);
            byte[] dataTemp = new byte[FileUpload.ContentLength];
            using (BinaryReader theReader = new BinaryReader(FileUpload.InputStream))
            {
                dataTemp = theReader.ReadBytes(FileUpload.ContentLength);
            }
            string result = Convert.ToBase64String(dataTemp);

            return result;
        }


        private void AllBorders(Border _borders)
        {
            _borders.Top.Style = ExcelBorderStyle.Thin;
            _borders.Left.Style = ExcelBorderStyle.Thin;
            _borders.Right.Style = ExcelBorderStyle.Thin;
            _borders.Bottom.Style = ExcelBorderStyle.Thin;
        }

        public void WriteExcelDetail(ref ExcelWorksheet ws, int fromRow, int fromCol, int toRow, int toCol, bool isMerge = false, bool isBold = false, bool isUnderLine = false,
            string value = "", float fontSize = 0, ExcelHorizontalAlignment horizontal = ExcelHorizontalAlignment.Center, ExcelVerticalAlignment vertical = ExcelVerticalAlignment.Center,
            Color? fontColor = null, string backgroundColor = "", bool isTable = true , bool isWrap = false)
        {
            ws.Cells[fromRow, fromCol, toRow, toCol].Merge = isMerge;
            ws.Cells[fromRow, fromCol, toRow, toCol].Value = value.ToString();
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Bold = isBold;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.HorizontalAlignment = horizontal;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.VerticalAlignment = vertical;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.UnderLine = isUnderLine;

            //ws.Cells[romRow, fromCol, toRow, toCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
            //ws.Cells[fromRow, fromCol, toRow, toCol].AutoFitColumns();


            if (fontSize > 0)
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Size = fontSize;
            }

            if (fontColor.HasValue)
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Color.SetColor(fontColor.Value);
            }

            if (isTable)
            {
                AllBorders(ws.Cells[fromRow, fromCol, toRow, toCol].Style.Border);
            }

            if (!string.IsNullOrWhiteSpace(backgroundColor))
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(backgroundColor));
            }

            if (isWrap)
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.WrapText = isWrap;
            }

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
            ws.DefaultRowHeight = 30;
            ws.DefaultColWidth = 30;
        }
    }
}