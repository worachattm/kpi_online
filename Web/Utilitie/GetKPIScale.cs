﻿using KPI_ONLINE.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPI_ONLINE.Utilitie
{
    public class GetKPIScale
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Display { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public int scale { get; set; }
    }
}