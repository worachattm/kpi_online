﻿using KPI_ONLINE.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Models;
using Newtonsoft.Json;
using KPI_ONLINE.ViewModel;

namespace KPI_ONLINE.Utilitie
{
    public class SetDropDownList : BaseClass
    {
        public ColorScaleCode GetColorScaleCode()
        {
            var scale = unitOfWork.IndicatorScaleRepository.GetAll();
            var colorcode = new ColorScaleCode();
            colorcode.ColorCode1 = scale.FirstOrDefault(x => x.TKIS_INDI_SCALE_NO == 1).TKIS_INDI_SCALE_CODE;
            colorcode.ColorCode2 = scale.FirstOrDefault(x => x.TKIS_INDI_SCALE_NO == 2).TKIS_INDI_SCALE_CODE;
            colorcode.ColorCode3 = scale.FirstOrDefault(x => x.TKIS_INDI_SCALE_NO == 3).TKIS_INDI_SCALE_CODE;
            colorcode.ColorCode4 = scale.FirstOrDefault(x => x.TKIS_INDI_SCALE_NO == 4).TKIS_INDI_SCALE_CODE;
            colorcode.ColorCode5 = scale.FirstOrDefault(x => x.TKIS_INDI_SCALE_NO == 5).TKIS_INDI_SCALE_CODE;
            return colorcode;
        }

        public List<SelectListItem> GetPeriodType()
        {
            List<SelectListItem> rtn = new List<SelectListItem>() {
                new SelectListItem() { Text = "Monthly", Value = "Monthly" },
                new SelectListItem() { Text = "Quater", Value = "Quater" },
                new SelectListItem() { Text = "Year", Value = "Year" }
            };
            return rtn;
        }
        public List<SelectListItem> GetTargetPeriod(string periodType)
        {
            List<SelectListItem> rtn = new List<SelectListItem>() {
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "January", Value = "January" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "February", Value = "February" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "March", Value = "March" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "April", Value = "April" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "May", Value = "May" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "June", Value = "June" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "July", Value = "July" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "August", Value = "August" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "September", Value = "September" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "October", Value = "October" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "November", Value = "November" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Monthly" }, Text = "December", Value = "December" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Quater" }, Text = "Q1", Value = "Q1" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Quater" }, Text = "Q2", Value = "Q2" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Quater" }, Text = "Q3", Value = "Q3" },
                new SelectListItem() { Group = new SelectListGroup{ Name = "Quater" }, Text = "Q4", Value = "Q4" },
            };
            rtn = rtn.Where(w => w.Group.Name == periodType).ToList();
            rtn.ForEach(s => s.Group = null);
            return rtn;
        }
        public List<SelectListItem> GetYear()
        {
            List<SelectListItem> rtn = new List<SelectListItem>() {
                new SelectListItem() { Text = (DateTime.Now.Year).ToString(), Value = (DateTime.Now.Year).ToString() },
                new SelectListItem() { Text = (DateTime.Now.Year + 1).ToString(), Value = (DateTime.Now.Year + 1).ToString() }
            };
            return rtn;
        }

        public List<SelectListItem> GetKPI(List<string> authKpiIdLs = null)
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var kpiList = unitOfWork.KpiNameRepository.GetAll().Where(x => authKpiIdLs?.Contains(x.TKIN_ID) ?? true);
            foreach (var test in kpiList)
            {
                selectListItems.Add(new SelectListItem { Text = "KPI-" + test.TKIN_NO + ":" + test.TKIN_NAME, Value = test.TKIN_ID });
            }
            return selectListItems;
        }

        public List<SelectListItem> GetDeppaetment()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var list = unitOfWork.DepartmentRepository.GetAll(x => x.TKD_ID != "1").ToList();
            list.ForEach(x =>
            {
                selectListItems.Add(new SelectListItem { Text = x.TKD_DEPARTMENT_NAME, Value = x.TKD_ID.ToString() });
            });
            return selectListItems;
        }
        public List<SelectListItem> GetArea(string parentID = null)
        {
            var listID = JsonConvert.DeserializeObject<List<string>>(parentID);
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var list = unitOfWork.AreaRepository.GetAll().Where(x => listID.Contains(x.TKD_ID)).ToList();
            list.ForEach(x =>
            {
                selectListItems.Add(new SelectListItem { Text = x.TKA_AREA_NAME, Value = x.TKA_ID.ToString() });
            });
            return selectListItems;
        }
        public List<SelectListItem> GetPlant(string parentID = null)
        {
            var listID = JsonConvert.DeserializeObject<List<string>>(parentID);
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var list = unitOfWork.PlantRepository.GetAll().Where(x => listID.Contains(x.TKA_ID)).ToList();
            list.ForEach(x =>
            {
                selectListItems.Add(new SelectListItem { Text = x.TKP_PLANT_NAME, Value = x.TKP_ID.ToString() });
            });
            return selectListItems;
        }
        public List<SelectListItem> GetShift(string parentID = null)
        {
            var listID = JsonConvert.DeserializeObject<List<string>>(parentID);
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var list = unitOfWork.ShiftRepository.GetAll().Where(x => listID.Contains(x.TKP_ID)).ToList();
            list.ForEach(x =>
            {
                selectListItems.Add(new SelectListItem { Text = x.TKS_SHIFT_NAME + " (" + x.TKPI_MASTER_PLANT.TKP_PLANT_NAME + ")", Value = x.TKS_ID.ToString() });

            });
            return selectListItems;
        }

    }
}