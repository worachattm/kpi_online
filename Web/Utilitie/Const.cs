﻿using KPI_ONLINE.DAL.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.Utilitie
{
    static class Const
    {
        private static readonly LogService ls = new LogService();
        private static readonly ILog logger = LogManager.GetLogger(typeof(Const));
        public static UserModel User
        {
            get
            {
                try
                {
                    var user = System.Web.HttpContext.Current.Session["User"] as UserModel;
                    if (user == null) user = new UserModel();

                    return user;
                }
                catch (Exception ex)
                {
                    ls.InsertErrorLog(logger, ex, ERROR_SEVERITY.MEDUIM, "");
                    return null;
                }
            }
            set
            {
                System.Web.HttpContext.Current.Session["User"] = value;
            }
        }

        public static string UserActionMenu
        {
            get
            {
                try
                {
                    var action = System.Web.HttpContext.Current.Session["action_menu"] as string;
                    if (action == null) action = "";

                    return action;
                }
                catch (Exception ex)
                {

                    ls.InsertErrorLog(logger, ex, ERROR_SEVERITY.MEDUIM, "");
                    return null;
                }
            }
            set
            {
                System.Web.HttpContext.Current.Session["action_menu"] = value;
            }
        }
    }
}