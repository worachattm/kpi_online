﻿using KPI_ONLINE.Models;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static KPI_ONLINE.ViewModel.ReportKPISummaryPerformanceViewModel;
using KPI_ONLINE.Utilitie;
using Web.Utilitie;
using Web.Controllers;
using System.IO;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Newtonsoft.Json.Linq;

namespace KPI_ONLINE.Controllers
{
    public class ReportKPISummaryPerformanceController : BaseController
    {
        // GET: ReportKPISummaryPerformance
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_PERFORMANCE_REPORT_MENU)]
        public ActionResult Index()
        {
            var Model = new ReportKPISummaryPerformanceViewModel();
            var roleserviceModel = new RoleServiceModel();
            var service = new ReportKPISummaryPerformanceServiceModel();
            InitialSearchCritiria(Model);
            var searchCriteria = Model.Criteria;
            searchCriteria.Year = DateTime.Now.Year.ToString();
            searchCriteria.PeriodType = "Year";
            //service.GetSearchResult(Model);
            //var test = Model.Result.departmentList[0].areaList[0].PlantList.FirstOrDefault(x => x.Value == "TOC=-1");
            Model.Role_Detail.DepartmentTree = roleserviceModel.GetDepartmentTreeWithLayer(null, showPlant: false, showEVPM: false);
            return View(Model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_PERFORMANCE_REPORT_SEARCH)]
        public ActionResult Search(ReportKPISummaryPerformanceViewModel pModel)
        {

            var serviceModel = new ReportKPISummaryPerformanceServiceModel();
            var roleserviceModel = new RoleServiceModel();
            serviceModel.GetSearchResult(pModel);
            InitialSearchCritiria(pModel);
            return View("Index", pModel);
        }

        public SearchCriteria InitialSearchCritiria(ReportKPISummaryPerformanceViewModel Model)
        {
            var searchCriteria = Model.Criteria;
            var serviceModel = new SetDropDownList();
            searchCriteria.ddPeriodType = serviceModel.GetPeriodType();
            searchCriteria.ddTargetPeriod = serviceModel.GetTargetPeriod(Model.Criteria.PeriodType);
            searchCriteria.ddYear = serviceModel.GetYear();
            searchCriteria.ddReportType = new List<SelectListItem> {
                new SelectListItem { Text = "Show sum all shift", Value = "" },
                new SelectListItem { Text = "Show sum by shift", Value = "" }
            };
            return searchCriteria;
        }

        [HttpGet]
        public JsonResult GetTargetPeriodByPeriodTypeDDL(string periodType)
        {
            try
            {
                var serviceModel = new SetDropDownList();
                var json = serviceModel.GetTargetPeriod(periodType);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void Header(ExcelRange excelRange, Color blueHeader, string Name)
        {
            excelRange.Merge = true;
            excelRange.Value = Name;
            excelRange.Style.Font.Bold = true;
            //excelRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
            excelRange.Style.Font.Color.SetColor(Color.White);
            excelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
            excelRange.Style.Fill.BackgroundColor.SetColor(blueHeader);
        }

        public string CreateExcel(ReportKPISummaryPerformanceViewModel model)
        {
            string SearchResult = "";
            if (model.Criteria.PeriodType == "Year")
            {
                SearchResult = "Yearly report of " + model.Criteria.Year;
            }
            else if (model.Criteria.PeriodType == "Monthly")
            {
                SearchResult = "Monthly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }
            else if (model.Criteria.PeriodType == "Quater")
            {
                SearchResult = "Quarterly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }

            int totalColumn = 0;
            string file_name = string.Format("ReporSummaryPerformance_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_name_pdf = string.Format("ReporSummaryPerformance_{0}.pdf", DateTime.Now.ToString("dd-MMM-yy"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);
                //fileCreate.SetAccessControl(System.IO.Directory.GetAccessControl(file_path_folder, AccessControlSections.All));
            }
            FileInfo excelFile = new FileInfo(file_path);
            var columnHeader = new List<string>() {
                "KPI", "Score"
            };

            Color bgColor = System.Drawing.ColorTranslator.FromHtml("#D3E2F9");
            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#2196f3");
            //Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#D3E2F9");
            var Jobject = JToken.Parse(model.Role_Detail.DepartmentTree);
            var chkDraw = false;
            var listid = new List<string>();
            foreach (var department in Jobject.Children())
            {
                var itemProperties = department.Children<JProperty>();
                //you could do a foreach or a linq here depending on what you need to do exactly with the value
                var state = itemProperties.FirstOrDefault(x => x.Name == "state");
                var stateElement = state.Value; ////This is a JValue type
                var stateProperties = stateElement.Children<JProperty>();
                var check = stateProperties.FirstOrDefault(x => x.Name == "checked");
                var isCheck = check.Value;
                if ((bool)isCheck)
                {
                    var href = itemProperties.FirstOrDefault(x => x.Name == "href");
                    listid.Add(href.Value.ToString().Replace("#node-", ""));
                }
            }
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet wsSummaryPer = excel.Workbook.Worksheets.Add("default");
                wsSummaryPer.View.ShowGridLines = false;
                wsSummaryPer.PrinterSettings.Orientation = eOrientation.Landscape;
                var row = 3;

                foreach (var department in model.Result.departmentList)
                {
                    foreach (var area in department.areaList)
                    {
                        if (!listid.Contains(area.areaID.IsNullOrEmpty() ? ("department_" + department.departmentID) : ("area_" + area.areaID)))
                            continue;
                        var columnSubHeader = new List<string>() {
                            "KPI No", "Process/Movements/Offsite", "Aver Score.", "Scale"
                        };
                        string text = "";
                        text = area.areaName;
                        text = (department.departmentName ?? "(ALL)") + " > " + (area.areaName ?? "(ALL)");
                        //Count Column And Add Column
                        var countWeightCol = 0;
                        if (!string.IsNullOrEmpty(area.areaName))
                        {
                            //columnSubHeader.Add("Shift");
                            //countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-1").Key != null)
                        {
                            columnSubHeader.Add("TOC-1");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-2").Key != null)
                        {
                            columnSubHeader.Add("TOC-2");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-3").Key != null)
                        {
                            columnSubHeader.Add("TOC-3");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-4").Key != null)
                        {
                            columnSubHeader.Add("TOC-4");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-5").Key != null)
                        {
                            columnSubHeader.Add("TOC-5");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "UTIL").Key != null)
                        {
                            columnSubHeader.Add("UTIL");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "SPP").Key != null)
                        {
                            columnSubHeader.Add("SPP");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "OIL MOVEMENT").Key != null)
                        {
                            columnSubHeader.Add("OIL MOVEMENT");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "OFFSITE").Key != null)
                        {
                            columnSubHeader.Add("OFFSITE");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TLB").Key != null)
                        {
                            columnSubHeader.Add("TLB");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "TPX").Key != null)
                        {
                            columnSubHeader.Add("TPX");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "LABIX").Key != null)
                        {
                            columnSubHeader.Add("LABIX");
                            countWeightCol++;
                        }
                        if (area.PlantList.FirstOrDefault(x => x.Value == "MARINE").Key != null)
                        {
                            columnSubHeader.Add("MARINE");
                            countWeightCol++;
                        }

                        row++;
                        Header(wsSummaryPer.Cells[row, 1, row, countWeightCol + 8], blueHeader, text);
                        row++;

                        if (totalColumn < (countWeightCol + 8))
                            totalColumn = countWeightCol + 8;

                        var columnCountHead = 1;
                        foreach (var column in columnHeader)
                        {


                            if (columnCountHead == 1)
                            {
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Merge = true;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Value = column;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Font.Bold = true;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Font.Size = 9;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Fill.BackgroundColor.SetColor(Color.CadetBlue);
                                columnCountHead += 6;
                            }
                            else
                            {
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Merge = true;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Value = column;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.Font.Bold = true;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.Font.Size = 9;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //wsSummaryPer.Cells[row, columnCountHead, row, columnCountHead + countWeightCol].Style.Fill.BackgroundColor.SetColor(Color.CadetBlue);

                                var cellTemp = wsSummaryPer.Cells[row, columnCountHead + 2, row, 8 + countWeightCol];
                                cellTemp.Merge = true;
                                cellTemp.Value = column;
                                cellTemp.Style.Font.Bold = true;
                                cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                cellTemp.Style.Font.Size = 9;
                                cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                            }
                            //columnCountHead++;
                        }
                        row++;

                        var columnCountSub = 1;
                        int loopCount = 0;
                        foreach (var column in columnSubHeader)
                        {
                            loopCount++;
                            if (columnCountSub == 2)
                            {
                                if (loopCount != 2)
                                {
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Merge = true;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Value = column;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.Font.Bold = true;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.Font.Size = 9;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSummaryPer.Cells[row, columnCountSub, row, columnCountSub + 4].Style.Fill.BackgroundColor.SetColor(bgColor);
                                }
                                
                                columnCountSub += 5;
                            }
                            else
                            {

                                wsSummaryPer.Cells[row, columnCountSub].Value = column;
                                wsSummaryPer.Cells[row, columnCountSub].Style.Font.Bold = true;
                                wsSummaryPer.Cells[row, columnCountSub].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnCountSub].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnCountSub].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                wsSummaryPer.Cells[row, columnCountSub].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                wsSummaryPer.Cells[row, columnCountSub].Style.Fill.BackgroundColor.SetColor(bgColor);
                                columnCountSub++;
                            }

                            if (loopCount == 1)
                            {
                                var cellTemp = wsSummaryPer.Cells[row - 1, 1, row, 1];
                                cellTemp.Merge = true;
                                cellTemp.Value = column;
                                cellTemp.Style.Font.Size = 9;
                                cellTemp.Style.Font.Bold = true;
                                cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                            else if (loopCount == 2)
                            {
                                var cellTemp = wsSummaryPer.Cells[row - 1, 2, row, 6];
                                cellTemp.Merge = true;
                                cellTemp.Value = column;
                                cellTemp.Style.Font.Size = 9;
                                cellTemp.Style.Font.Bold = true;
                                cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                            else if (loopCount == 3)
                            {
                                var cellTemp = wsSummaryPer.Cells[row - 1, 7, row, 7];
                                cellTemp.Merge = true;
                                cellTemp.Value = column;
                                cellTemp.Style.Font.Size = 9;
                                cellTemp.Style.Font.Bold = true;
                                cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                            else if (loopCount == 4)
                            {
                                var cellTemp = wsSummaryPer.Cells[row - 1, 8, row, 8];
                                cellTemp.Merge = true;
                                cellTemp.Value = column;
                                cellTemp.Style.Font.Size = 9;
                                cellTemp.Style.Font.Bold = true;
                                cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                        }
                        row++;

                        foreach (var detail in area.detail)
                        {
                            int columnDetail = 1;
                            if (!string.IsNullOrEmpty(area.areaName))
                            {
                                if (string.IsNullOrEmpty(detail.TKID_SHIFT))
                                {
                                    wsSummaryPer.Cells[row, columnDetail].Value = detail.TKI_INDI_NO;
                                    wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail++;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Value = detail.TKI_INDI_NAME;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Merge = true;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Bold = false;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Size = 9;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail += 5;
                                }
                                else
                                {
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Value = detail.TKID_SHIFT;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Merge = true;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Style.Font.Bold = false;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Style.Font.Size = 9;
                                    wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 5].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail += 6;
                                }
                            }
                            else
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TKI_INDI_NO;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Value = detail.TKI_INDI_NAME;
                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Merge = true;
                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail, row, columnDetail + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail += 5;
                            }

                            Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.SCALE_COLOR_CODE ?? "#ffffff");
                            var cell = wsSummaryPer.Cells[row, columnDetail];
                            cell.Value = detail.AVG_SCORE;
                            cell.Style.Font.Bold = true;
                            cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            cell.Style.Font.Size = 9;
                            cell.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            cell.Style.Fill.BackgroundColor.SetColor(scalcolor);
                            cell.Style.Font.Color.SetColor(Color.White);

                            columnDetail++;

                            var cell2 = wsSummaryPer.Cells[row, columnDetail];
                            cell2.Value = detail.SET_INDEX_COLOR;
                            cell2.Style.Font.Bold = true;
                            cell2.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            cell2.Style.Font.Size = 9;
                            cell2.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            //cell2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //cell.Style.Fill.BackgroundColor.SetColor(scalcolor);
                            //cell.Style.Font.Color.SetColor(Color.White);

                            columnDetail++;

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-1").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TOC1;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-2").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TOC2;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-3").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TOC3;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-4").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TOC4;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TOC-5").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TOC5;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "UTIL").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.UTIL;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "SPP").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.SPP;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "MOV").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.MOV;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "OIL MOVEMENT").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.MOV;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "OFF").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.OFF;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "OFFSITE").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.OFF;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TLB").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TLB;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "TPX").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.TPX;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "LABIX").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.LABIX;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }

                            if (area.PlantList.FirstOrDefault(x => x.Value == "MARINE").Key != null)
                            {
                                wsSummaryPer.Cells[row, columnDetail].Value = detail.MARINE;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsSummaryPer.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsSummaryPer.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsSummaryPer.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                            }
                            row++;
                        }

                    }

                    if (totalColumn != 0)
                    {
                        var cell = wsSummaryPer.Cells[1, 1, 1, totalColumn];
                        cell.Merge = true;
                        cell.Value = "KPI Summary - Performance All Area";
                        cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 15;

                        cell = wsSummaryPer.Cells[2, 1, 2, totalColumn];
                        cell.Merge = true;
                        cell.Value = SearchResult;
                        cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        cell.Style.Font.Bold = false;
                        cell.Style.Font.Size = 14;
                    }

                }




                excel.Save();
            }
            return file_path;
        }

        public ActionResult ExportExcel(string pPeriod_Type, string pTarget_Period, string pYear, string pTree)
        {
            ReportKPISummaryPerformanceViewModel pModel = new ReportKPISummaryPerformanceViewModel();
            if (pModel != null && pModel.Result == null)
            {
                pModel.Result = new SearchResult();
            }

            pModel.Criteria.PeriodType = pPeriod_Type;
            pModel.Criteria.TargetPeriod = pTarget_Period;
            pModel.Criteria.Year = pYear;
            pModel.Role_Detail.DepartmentTree = pTree;

            var serviceModel = new ReportKPISummaryPerformanceServiceModel();
            serviceModel.GetSearchResult(pModel);

            string file_path = CreateExcel(pModel);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }

        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }
    }
}