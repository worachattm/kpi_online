﻿using KPI_ONLINE.Models;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPI_ONLINE.DAL.Utilities;
using System.Web.Mvc;
using static KPI_ONLINE.ViewModel.KPIDetailAreaReportViewModel;
using KPI_ONLINE.Utilitie;
using Web.Utilitie;
using Web.Controllers;
using System.IO;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Newtonsoft.Json.Linq;

namespace KPI_ONLINE.Controllers
{
    public class KPIDetailAreaReportController : BaseController
    {
        KPIDetailAreaReportServiceModel _ = new KPIDetailAreaReportServiceModel();
        // GET: KPIDetailAreaReport
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_DETAIL_REPORT_MENU)]
        public ActionResult Index()
        {
            var Model = new KPIDetailAreaReportViewModel();
            var roleserviceModel = new RoleServiceModel();
            Model.Criteria.Year = DateTime.Now.Year.ToString();
            Model.Criteria.PeriodType = "Year";
            //_.GetSearchResult(Model);
            InitialSearchCritiria(Model);
            Model.Role_Detail.DepartmentTree = roleserviceModel.GetDepartmentTreeWithLayer(null, showEVPM: false, showShift: false);
            return View(Model);
        }
        public void InitialSearchCritiria(KPIDetailAreaReportViewModel Model)
        {
            var searchCriteria = Model.Criteria;
            var serviceModel = new SetDropDownList();
            searchCriteria.ddPeriodType = serviceModel.GetPeriodType();
            searchCriteria.ddTargetPeriod = serviceModel.GetTargetPeriod(Model.Criteria.PeriodType);
            searchCriteria.ddYear = serviceModel.GetYear();

        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search"), MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_DETAIL_REPORT_SEARCH)]
        public ActionResult Search(KPIDetailAreaReportViewModel pModel)
        {
            if (pModel.Result == null)
                pModel.Result = new SearchResult();

            _.GetSearchResult(pModel);
            InitialSearchCritiria(pModel);
            return View("Index", pModel);
        }
        [HttpGet]
        public JsonResult GetTargetPeriodByPeriodTypeDDL(string periodType)
        {
            try
            {
                var serviceModel = new SetDropDownList();
                var json = serviceModel.GetTargetPeriod(periodType);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        private void Header(ExcelRange excelRange, Color blueHeader, string Name)
        {
            excelRange.Merge = true;
            excelRange.Value = Name;
            excelRange.Style.Font.Bold = true;
            //excelRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
            excelRange.Style.Font.Color.SetColor(Color.White);
            excelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
            excelRange.Style.Fill.BackgroundColor.SetColor(blueHeader);
        }
        public string CreateExcel(KPIDetailAreaReportViewModel model)
        {
            string SearchResult = "";
            if (model.Criteria.PeriodType == "Year")
            {
                SearchResult = "Yearly report of " + model.Criteria.Year;
            }
            else if (model.Criteria.PeriodType == "Monthly")
            {
                SearchResult = "Monthly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }
            else if (model.Criteria.PeriodType == "Quater")
            {
                SearchResult = "Quarterly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }

            string file_name = string.Format("KPIDetailAreaReport_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_name_pdf = string.Format("KPIDetailAreaReport_{0}.pdf", DateTime.Now.ToString("dd-MMM-yy"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);

            }
            FileInfo excelFile = new FileInfo(file_path);
            var columnHeader = new List<string>() {
                "KPI No.", "Process/Movements/Offsites", "A", "B", "C", "D"
            };

            var listid = new List<string>();
            var Jobject = JToken.Parse(model.Role_Detail.DepartmentTree);
            foreach (var department in Jobject.Children())
            {
                var itemProperties = department.Children<JProperty>();
                //you could do a foreach or a linq here depending on what you need to do exactly with the value
                var state = itemProperties.FirstOrDefault(x => x.Name == "state");
                var stateElement = state.Value; ////This is a JValue type

                var stateProperties = stateElement.Children<JProperty>();
                var check = stateProperties.FirstOrDefault(x => x.Name == "checked");
                var isCheck = check.Value;
                if ((bool)isCheck)
                {
                    var href = itemProperties.FirstOrDefault(x => x.Name == "href");
                    listid.Add(href.Value.ToString().Replace("#node-", ""));
                }
            }
            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#2196f3");
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet wsKPIDetailArea = excel.Workbook.Worksheets.Add("default");
                wsKPIDetailArea.View.ShowGridLines = false;
                wsKPIDetailArea.PrinterSettings.Orientation = eOrientation.Landscape;
                var row = 3;

                var cell = wsKPIDetailArea.Cells[1, 1, 1, 10];
                cell.Merge = true;
                cell.Value = "KPI Summary - KPI Detail Area Report";
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15;

                cell = wsKPIDetailArea.Cells[2, 1, 2, 10];
                cell.Merge = true;
                cell.Value = SearchResult;
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = false;
                cell.Style.Font.Size = 14;


                foreach (var plant in model?.Result?.PlantList ?? new List<KPI_ONLINE.ViewModel.KPIDetailAreaReportViewModel.Plant>())
                {
                    if (!listid.Contains("plant_" + plant.PlantID)) { continue; }
                    var columnCountHead = 1;
                    var text = plant.PlantNameEx;
                    row++;
                    Header(wsKPIDetailArea.Cells[row, 1, row, columnCountHead + 9], blueHeader, text);
                    row++; 
                    //HEADER
                    columnCountHead = 1;
                    foreach (var column in columnHeader)
                    {
                        if (columnCountHead == 2)
                        {
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Merge = true;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Value = column;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Font.Bold = true;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Font.Size = 9;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Fill.BackgroundColor.SetColor(Color.CadetBlue);
                            columnCountHead += 5;
                        }
                        else
                        {
                            wsKPIDetailArea.Cells[row, columnCountHead].Merge = true;
                            wsKPIDetailArea.Cells[row, columnCountHead].Value = column;
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.Font.Bold = true;
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.Font.Size = 9;
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnCountHead].Style.Fill.BackgroundColor.SetColor(Color.CadetBlue);
                            columnCountHead++;
                        }
                    }
                    row++;
                    //BODY
                    foreach (var detail in plant?.DetailList ?? new List<KPI_ONLINE.ViewModel.KPIDetailAreaReportViewModel.ShiftData>())
                    {
                        var columnDetail = 1;

                        wsKPIDetailArea.Cells[row, columnDetail].Value = detail.kpiNo;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        columnDetail++;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Value = detail.kpiName;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Merge = true;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail, row, columnDetail + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        columnDetail += 5;

                        Color scalcolorA = System.Drawing.ColorTranslator.FromHtml(detail.scaleColor_A);
                        wsKPIDetailArea.Cells[row, columnDetail].Value = detail.avgScore_A;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        if (detail.avgScore_A != null)
                        {
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolorA);
                        }

                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                        columnDetail++;
                        Color scalcolorB = System.Drawing.ColorTranslator.FromHtml(detail.scaleColor_B);
                        wsKPIDetailArea.Cells[row, columnDetail].Value = detail.avgScore_B;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        if (detail.avgScore_B != null)
                        {
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolorB);
                        }

                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                        columnDetail++;
                        Color scalcolorC = System.Drawing.ColorTranslator.FromHtml(detail.scaleColor_C);
                        wsKPIDetailArea.Cells[row, columnDetail].Value = detail.avgScore_C;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        if (detail.avgScore_C != null)
                        {
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolorC);
                        }
                      
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                        columnDetail++;
                        Color scalcolorD = System.Drawing.ColorTranslator.FromHtml(detail.scaleColor_D);
                        wsKPIDetailArea.Cells[row, columnDetail].Value = detail.avgScore_D;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        if (detail.avgScore_D != null)
                        {
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsKPIDetailArea.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolorD);
                        }
                    
                        wsKPIDetailArea.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                        columnDetail++;

                        row++;
                    }
                }
                excel.Save();
            }

            return file_path;
        }
        public ActionResult ExportExcel(string pPeriod_Type, string pTarget_Period, string pYear, string pTree)
        {
            KPIDetailAreaReportViewModel pModel = new KPIDetailAreaReportViewModel();
            if (pModel != null && pModel.Result == null)
            {
                pModel.Result = new SearchResult();
            }
            pModel.Criteria.PeriodType = pPeriod_Type;
            pModel.Criteria.TargetPeriod = pTarget_Period;
            pModel.Criteria.Year = pYear;
            pModel.Role_Detail.DepartmentTree = pTree;

            _.GetSearchResult(pModel);

            string file_path = CreateExcel(pModel);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }
        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }
    }
}