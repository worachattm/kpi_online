﻿using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.Controllers
{
    public class KPIDataController : Controller
    {
        // GET: ImportData
        public ActionResult Search()
        {
            var Model = new KPIData();
            return View(Model);
        }
    }
}