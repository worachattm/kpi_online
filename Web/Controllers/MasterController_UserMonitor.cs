﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    public partial class MasterController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.PIS_MENU)]
        public ActionResult UserMonitor()
        {
            ViewBag.Message = TempData["ReponseMessage"];
            ViewBag.Result = TempData["ReponseResult"];

            UsersServiceModel fnUser = new UsersServiceModel();
            RoleServiceModel fnRole = new RoleServiceModel();
            var result = new UserMonitorViewModel();
            List<UserMonitorDiff> model = (List<UserMonitorDiff>)TempData["ResultImportData"];
            if (model != null && model.Count > 0)
            {
                result.UserMonitorDiffLs = model;

            }
            else
            {
                var diffUser = fnUser.SearchUserMonitor();
                result.UserMonitorDiffLs = diffUser;
                //result.UserMonitorDiffLs = new List<UserMonitorDiff>();
            }

            result.RoleDDL = fnRole.GetRoleName();
            result.ShiftDDL = fnRole.GetShiftDistinctDDL();
            result.PlantDDL = fnRole.GetPlantDistinctDDL();
            result.AreaDDL = fnRole.GetAreaDistinctDDL();
            result.CompanyDDL = fnUser.GetCompanyCode();
            result.DepartmentDDL = fnRole.GetDepartmentDistinctDDL();
            

            return View(result);
        }


        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.PIS_EXPORT)]
        public string UserMonitorGenerateExcel()
        {
            var service = new UsersServiceModel();
            var file_name = service.UserMonitorGenerateExcel();
            //var appName = ConstantPrm.AppName;

            return file_name;
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.PIS_IMPORT , ConstantPrm.MenuAuthorize.PIS_CREATE)]
        public ActionResult ImportUserMonitor(UserMonitorViewModel model, HttpPostedFileBase fileToUpload , string buttonAction)
        {
            var service = new UsersServiceModel();

            if (buttonAction == "Import")
            {
                if (fileToUpload != null && fileToUpload.ContentLength > 0)
                {
                  
                    var resultData = service.ImportUserMonitorFromExcel(fileToUpload);
                    TempData["ResultImportData"] = resultData.listDiff;

                    TempData["ReponseMessage"] = resultData.rtn.Message;
                    TempData["ReponseResult"] = resultData.rtn.Result;

                }
                else
                {
                    TempData["ReponseMessage"] = "Please select file";
                    TempData["ReponseResult"] = "danger";
                }

            }
            else
            {
                var resultData = service.CreateOrEditUserMonitor(model.UserMonitorDiffLs , Const.User.UserName);
                
                TempData["ReponseMessage"] = resultData.Message;
                TempData["ReponseResult"] = resultData.Result;

                if (!resultData.Status)
                {
                    TempData["ResultImportData"] = model.UserMonitorDiffLs;
                }
            }

            return RedirectToAction("UserMonitor");
        }


    }
}