﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    public partial class MasterController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_MENU)]
        public ActionResult Role()
        {
            ViewBag.Message = TempData["ReponseMessage"];
            ViewBag.Result = TempData["ReponseResult"];

            RoleViewModel model = InitialRoleModel();
            return View(model);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_SEARCH)]
        public ActionResult Role(RoleViewModel tempModel)
        {

            RoleViewModel model = InitialRoleModel();

            RoleServiceModel serviceModel = new RoleServiceModel();

            var viewModelSearch = tempModel.role_Search;
            serviceModel.Search(ref viewModelSearch);
            model.role_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_CREATE)]
        public ActionResult CreateRole()
        {
            RoleViewModel model = InitialRoleModel();
            RoleServiceModel serviceModel = new RoleServiceModel();
            //model.role_Detail = serviceModel.Get("Template");
            model.role_Detail = serviceModel.Get("");
            model.role_Detail.DepartmentTree = serviceModel.GetDepartmentTree(null);
            model.role_Detail.KPITree = serviceModel.GetKPIIndicators("");
            return View("CreateOrEditRole", model);
        }


        [HttpGet]
        public JsonResult GetAreaByDepartment(string departmentId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in departmentId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetArea("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetAreaByDepartmentDDL(string departmentId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetAreaDDL(departmentId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetPlantByAreaDDL(string areaId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetPlantDDL(areaId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetPlantByArea(string areaId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in areaId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetPlant("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetShiftByPlant(string plantId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in plantId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetShift("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetShiftByPlantDDL(string plantId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetShiftDDL(plantId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_CREATE, ConstantPrm.MenuAuthorize.ROLE_EDIT)]
        public ActionResult CreateOrEditRole(string roleID, RoleViewModel model)
        {
            RoleServiceModel serviceModel = new RoleServiceModel();

            if (model.IsEditMode)
            {
                var rtn = serviceModel.Edit(model.role_Detail, Const.User.UserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseResult"] = rtn.Result;

            } else
            {
                var rtn = serviceModel.Add(model.role_Detail, Const.User.UserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseResult"] = rtn.Result;
            }

            return RedirectToAction("Role");
        }


        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_DELETE)]
        public ActionResult DeleteRole(string roleID)
        {
            RoleServiceModel serviceModel = new RoleServiceModel();
            
            var rtn = serviceModel.Delete(roleID, Const.User.UserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseResult"] = rtn.Result;

 
            return RedirectToAction("Role");
        }

        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.ROLE_EDIT)]
        public ActionResult EditRole(string roleID)
        {
            RoleServiceModel serviceModel = new RoleServiceModel();

            RoleViewModel model = InitialRoleModel();
            model.role_Detail = serviceModel.Get(roleID);
            model.RoleId = roleID;
            return View("CreateOrEditRole", model);
        }

        public RoleViewModel InitialRoleModel()
        {
            RoleServiceModel serviceModel = new RoleServiceModel();
            var userServiceModel = new UsersServiceModel();
            RoleViewModel model = new RoleViewModel() {
                ddl_RoleName = serviceModel.GetRoleName(),
                //ddl_RoleDesc = serviceModel.GetRoleDesc(),
                ddl_Status = serviceModel.GetMasterStatus(),
                ddlDepartment = userServiceModel.GetDepartmentCode(),
                ddlArea = new List<SelectListItem>(),
                ddlPlant = new List<SelectListItem>(),
                ddlCompany = userServiceModel.GetCompanyCode(),
                role_Search = new RoleViewModel_Seach(),
                role_Detail = new RoleViewModel_Detail()
            };

            return model;
        }
    }
}