﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    public partial class MasterController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_MENU)]
        public ActionResult User()
        {
         
            ViewBag.Message = TempData["ReponseMessage"];
            ViewBag.Result = TempData["ReponseResult"];

            UsersViewModel model = InitialUserModel();

            return View(model);
        }


        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_SEARCH)]
        public JsonResult GetUserList(UsersViewModel viewModel)
        {
            UsersViewModel model = InitialUserModel();

            UsersServiceModel serviceModel = new UsersServiceModel();

            var viewModelSearch = viewModel.users_Search;
            serviceModel.Search(ref viewModelSearch);
            model.users_Search = viewModelSearch;

            return Json(new { isSuccess = true, result = model.users_Search.sSearchData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_SEARCH)]
        public new ActionResult User(UsersViewModel viewModel)
        {
            UsersViewModel model = InitialUserModel(departmentId: viewModel.users_Search.sDepartmentId, areaId: viewModel.users_Search.sAreaId , plantId: viewModel.users_Search.sPlantId);

            UsersServiceModel serviceModel = new UsersServiceModel();
          
            var viewModelSearch = viewModel.users_Search;
            serviceModel.Search(ref viewModelSearch);
            model.users_Search = viewModelSearch;

            return View(model);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_EXPORT)]
        public string UserExportExcel(UsersViewModel viewModel)
        {
            var service = new UsersServiceModel();
            var file_name = service.UserExportExcel(viewModel.users_Search);

            return file_name;
        }

        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_CREATE)]
        public ActionResult CreateUser()
        {

            UsersViewModel model = InitialUserModel();

            return View("CreateOrEditUser" , model);
        }


        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_EDIT)]
        public ActionResult EditUser(string id)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();

            UsersViewModel model = InitialUserModel();
            model.users_Detail = serviceModel.Get(id);
 

            return View("CreateOrEditUser", model);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.USER_CREATE ,ConstantPrm.MenuAuthorize.USER_EDIT)]
        public ActionResult CreateOrEditUser(string id,UsersViewModel viewModel)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            ReturnValue rtn = new ReturnValue();

            if (!viewModel.users_Detail.IsEditMode)
            {
                rtn = serviceModel.Add(viewModel.users_Detail, Const.User.UserName);
            }
            else
            {
                rtn = serviceModel.Edit(viewModel.users_Detail, Const.User.UserName);
            }
        
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseResult"] = rtn.Result;

            return RedirectToAction("User");
       
        }


        public UsersViewModel InitialUserModel(string departmentId = "" , string areaId = "", string plantId = "")
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            UsersViewModel model = new UsersViewModel();
            var roleServiceModel = new RoleServiceModel();

            model.users_Search = new UsersViewModel_Search();
            model.users_Detail = new UsersViewModel_Detail();

            model.ddlCompany = serviceModel.GetCompanyCode();
            model.ddlStatus = serviceModel.GetStatus();
            model.ddlShift = serviceModel.GetShiftCode(plantId);
            model.ddlDepartment = serviceModel.GetDepartmentCode();
            model.ddlArea = serviceModel.GetAreaCode(departmentId);
            model.ddlRole = roleServiceModel.GetRoleName();
            model.ddlPlant = serviceModel.GetPlantCode(areaId);


            return model;
        }


        [HttpGet]
        public JsonResult GetRoleMenu(string frm)
        {
            var result = new UserAuthorize();
            var roleFn = new RoleServiceModel();
            var fn = new UsersServiceModel();
            if (!string.IsNullOrWhiteSpace(frm))
            {
                result.menuTree = UsersServiceModel.GetMenu((frm));
                result.kpiTree = fn.GetKPIIndicators((frm));
                result.departmentTree = roleFn.GetDepartmentTreePageInRole((frm));
            }
       
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

    }
}