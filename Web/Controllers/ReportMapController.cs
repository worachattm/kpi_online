﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    public class ReportMapController : BaseController
    {
        // GET: Map
        ReportMapServiceModel _ = new ReportMapServiceModel();
        [MenuAuthorize(ConstantPrm.MenuAuthorize.MAP_REPORT_MENU)]
        public ActionResult Index()
        {
            var Model = new ReportMapViewModel();
            Model.Criteria.Year = DateTime.Now.Year.ToString();
            Model.Criteria.PeriodType = "Year";
            InitialSearchCritiria(Model);
            _.GetSearchResult(Model);
            //_.CombineImages(Model);
            return View("Index", Model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.MAP_REPORTT_ACTION)]
        public ActionResult Search(ReportMapViewModel pModel)
        {
            var serviceModel = new ReportMapServiceModel();
            var roleserviceModel = new RoleServiceModel();
            _.GetSearchResult(pModel);
            InitialSearchCritiria(pModel);
            return View("Index", pModel);
        }
        public void InitialSearchCritiria(ReportMapViewModel Model)
        {
            var searchCriteria = Model.Criteria;
            var serviceModel = new SetDropDownList();
            searchCriteria.ddKPIIndicator = serviceModel.GetKPI(Const.User.AuthKpiIdLs);
            searchCriteria.ddPeriodType = serviceModel.GetPeriodType();
            searchCriteria.ddTargetPeriod = serviceModel.GetTargetPeriod(Model.Criteria.PeriodType);
            searchCriteria.ddYear = serviceModel.GetYear();

        }
        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.MAP_REPORTT_ACTION)]
        public ActionResult PlantKPIDetail(string Plant, string Shift, string kpiNo, string targetperiod, string year, string periodtype)
        {
            PlantKPIDetail model = new PlantKPIDetail();
            _.GetPlantKPIDetail(model, Plant, Shift, kpiNo, targetperiod, year, periodtype);
            return PartialView("_PlatKPIDetailView", model);
        }
        [HttpPost]
        public ActionResult MapConfig()
        {
            MapConfig model = new MapConfig();
            _.GetMapConfig(model);

            return PartialView("_MapConfig", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "SaveMapConfig")]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.MAP_REPORT_MENU)]
        public ActionResult SaveMapConfig(MapConfig model)
        {
            var rtn = _.SaveMapConfig(model);
            return Index();
        }

        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }

        public ActionResult ExportExcel(string pPeriod_Type, string pTarget_Period, string pYear, string pKpi)
        {
            ReportMapViewModel pModel = new ReportMapViewModel();
            pModel.Criteria.PeriodType = pPeriod_Type;
            pModel.Criteria.TargetPeriod = pTarget_Period;
            pModel.Criteria.Year = pYear;
            pModel.Criteria.KPIIndicator = pKpi;
            string file_path = _.CreateExcel(pModel);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }

    }



}