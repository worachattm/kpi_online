﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    [MenuAuthorize(ConstantPrm.MenuAuthorize.IMPORT_MENU)]
    public class ImportKPIDataController : BaseController
    {
        ImportKPIDataServiceModel _ = new ImportKPIDataServiceModel();
        SetDropDownList _S = new SetDropDownList();
        // GET: ImportKPIData/Search
        [MenuAuthorize(ConstantPrm.MenuAuthorize.IMPORT_MENU)]
        public ActionResult Search()
        {
            var Model = new SearchKPIDataViewModel();
            var roleserviceModel = new RoleServiceModel();

            Model.Criteria.PeriodFrom = DateTime.Now.ToString("MMMM-yyyy");
            Model.Criteria.PeriodTo = DateTime.Now.AddMonths(1).ToString("MMMM-yyyy");
            _.SetSearchCriteria(Model);
            _.GetSearchResult(Model);
            Model.Role_Detail.DepartmentTree = roleserviceModel.GetDepartmentTreeWithLayer(null,showEVPM: false);
            return View(Model);
        }
        // GET: ImportKPIData/ImportData
        public ActionResult ImportData()
        {
            ViewBag.Excel = TempData["Errorexcel"];
            ViewBag.Result = TempData["ImportKPIResult"];
            var Model = new ExportImportDataViewModel();
            _.SetExportCriteria(Model);
            _.GetImportFileHistory(Model);

            return View(Model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(SearchKPIDataViewModel Model)
        {
            _.GetSearchResult(Model);
            _.SetSearchCriteria(Model);

            return View("Search", Model);
        }
        // pRecId: recId, pAction: action, pNote: $('#').val(), pKpiDate:  $('#').val(), pScore: $('#').val() 
        public ReturnValue UpdateKpiData(string pRecId, string pAction, string pNote, string pKpiDate, string pScore, string pYearToScore)
        {
            return _.UpdateKpiData(pRecId, pAction, pNote, pKpiDate, pScore, pYearToScore);
        }
        public ReturnValue DeleteKpiData(string pRecId)
        {
            return _.DeleteKpiData(pRecId);
        }
        public ReturnValue AddKpiData(string pData, string pKpiDate)
        {
            return _.AddKpiData(pData, pKpiDate);
        }
        public ActionResult DrawManagePopup(string pRecId, string pAction)
        {

            var Model = _.GetManageKPIData(pRecId, pAction);
            ViewData["action"] = pAction;
            return PartialView("_ManageKPIData", Model);
        }
        public ActionResult DrawExportPopup(string pRec, string pAction)
        {

            var Model = _.DrawTempplate(pRec, pAction);
            string file_path = CreateExcel(Model);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }

        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }

        public ActionResult ImportExcel(HttpPostedFileBase fileToUpload, string Note, ExportImportDataViewModel Model)
        {
            var fn = new ImportKPIDataServiceModel();
            if (fileToUpload != null && fileToUpload.ContentLength > 0)
            {
                var resultData = fn.ImportExcel(Model.ImportCriteria.Note, fileToUpload);
                TempData["ImportKPIResult"] = resultData.Item1;

                TempData["Errorexcel"] = resultData.Item2.Split('\\').LastOrDefault();

            }
            else
            {
                var resultData = new ReturnValue();
                resultData.Message = "Please select file";
                TempData["ImportKPIResult"] = resultData;
            }
            return RedirectToAction("ImportData");
        }

        [HttpGet]
        public JsonResult GetAreaByDepartment(string departmentId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in departmentId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetArea("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult CheckIndicator(string KpiID, string KPIdate)
        {
            try
            {
                var inidcator = _.CheckIndicator(KpiID, KPIdate);
                if (inidcator == null)
                {
                    return Json(new { isSuccess = false, result = "Can't find indicator for this KPI in given period " }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { isSuccess = true, result = inidcator.TKI_INDI_TYPE, id = inidcator.TKI_ID }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetAreaByDepartmentDDL(string departmentId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetAreaDDL(departmentId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetPlantByAreaDDL(string areaId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetPlantDDL(areaId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetShiftByPlantDDL(string plantId)
        {
            try
            {
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetShiftDDL(plantId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetUserByShiftDDL(string shift)
        {
            try
            {
                var json = _.Getuser(shift);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetMultipleDDL(string listID, string ddlName)
        {

            try
            {
                var json = new List<SelectListItem>();
                if (ddlName == "department")
                {
                    json = _S.GetArea(listID);
                }
                if (ddlName == "area")
                {
                    json = _S.GetPlant(listID);
                }
                if (ddlName == "plant")
                {
                    json = _S.GetShift(listID);
                }
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string CreateExcel(Template templates)
        {

            string file_name = string.Format("ExportTemplate_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);
            }
            FileInfo excelFile = new FileInfo(file_path);

            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#2489c5");
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {

                var sheet = 1;
                foreach (var template in templates.TemplatesList)
                {
                    var row = 1;

                    var col = 1;
                    ExcelWorksheet wsTemplate = excel.Workbook.Worksheets.Add(template.KPIIndicator?.Split(' ')[0] ?? "sheet-" + sheet++);
                    wsTemplate.Cells[row, col].Value = "*หมายเหตุ:";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.Font.Color.SetColor(Color.Red);
                    wsTemplate.Cells[row, ++col, row, col + 15].Value = "1.หากมีข้อมูลที่นำเข้าเข้ามี Department , Area, Plant, Shift, เหมือนกันและอยู่ในเดือนเดียวกัน มากกว่า 1 แถว ระบบจะบันทึกวันที่มากที่สุดในเดือนนั้นเพียงค่าเดียว";
                    wsTemplate.Cells[row, col, row, col + 15].Merge = true;
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Color.SetColor(Color.Red);

                    row++;
                    wsTemplate.Cells[row, col, row, col + 15].Value = "2.กรณีที่ข้อมูลที่นำเข้ายังไม่มีระบบ ระบบจะทำการ insert แต่ถ้าข้อมูลมีอยู่ในระบบแล้ว ระบบจำทำการอัพเดท";
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Color.SetColor(Color.Red);
                    wsTemplate.Cells[row, col, row, col + 15].Merge = true;
                    row++;
                    wsTemplate.Cells[row, col, row, col + 15].Value = "3.Format ของ KPI Date\"DD/MM/YYYY\"";
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col, row, col + 15].Style.Font.Color.SetColor(Color.Red);
                    wsTemplate.Cells[row, col, row, col + 15].Merge = true;
                    col = 1;
                    row++;
                    wsTemplate.View.ShowGridLines = true;
                    wsTemplate.PrinterSettings.Orientation = eOrientation.Landscape;
                    wsTemplate.Cells.Style.Font.Size = 12;
                    //wsTemplate.Cells[row, 1].Merge = true;
                    wsTemplate.Cells[row, col].Value = "NO";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Department";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Area";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Plant";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Shift";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    if (!(template?.Type?.ToUpper() == "SHIFT"))
                    {
                        wsTemplate.Cells[row, col].Value = "Emp ID";
                        wsTemplate.Cells[row, col].Style.Font.Bold = true;
                        wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                        col++;
                        wsTemplate.Cells[row, col].Value = "First Name";
                        wsTemplate.Cells[row, col].Style.Font.Bold = true;
                        wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                        col++;
                        wsTemplate.Cells[row, col].Value = "Last Name";
                        wsTemplate.Cells[row, col].Style.Font.Bold = true;
                        wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                        col++;
                    }

                    wsTemplate.Cells[row, col].Value = "KPI Date";
                    wsTemplate.Cells[row, col].Style.Numberformat.Format = "@";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Actual Score";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    //wsTemplate.Cells[row, col].Value = "Year To Date";
                    //wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    //wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    //wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    //wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    //col++;
                    wsTemplate.Cells[row, col].Value = "Note";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    col++;
                    wsTemplate.Cells[row, col].Value = "Error Describe";
                    wsTemplate.Cells[row, col].Style.Font.Bold = true;
                    wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsTemplate.Cells[row, col].Style.Font.Size = 9;
                    wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                    wsTemplate.Cells[row, col].Style.Font.Color.SetColor(Color.Red);
                    row++;
                    col = 1;

                    foreach (var detail in template.ExportResultList)
                    {
                        wsTemplate.Cells[row, col].Value = detail.NO;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                        col++;

                        wsTemplate.Cells[row, col].Value = detail.DepartMent;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                        col++;

                        wsTemplate.Cells[row, col].Value = detail.Area;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                        col++;

                        wsTemplate.Cells[row, col].Value = detail.Plant;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                        col++;

                        wsTemplate.Cells[row, col].Value = detail.Shift;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                        col++;
                        if (!(template?.Type?.ToUpper() == "SHIFT"))
                        {

                            wsTemplate.Cells[row, col].Value = detail.EmpID;
                            wsTemplate.Cells[row, col].Style.Font.Bold = false;
                            wsTemplate.Cells[row, col].Style.Font.Size = 9;
                            wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                            col++;

                            wsTemplate.Cells[row, col].Value = detail.FirstName;
                            wsTemplate.Cells[row, col].Style.Font.Bold = false;
                            wsTemplate.Cells[row, col].Style.Font.Size = 9;
                            wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                            col++;

                            wsTemplate.Cells[row, col].Value = detail.LastName;
                            wsTemplate.Cells[row, col].Style.Font.Bold = false;
                            wsTemplate.Cells[row, col].Style.Font.Size = 9;
                            wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            wsTemplate.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsTemplate.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
                            col++;
                        }


                        wsTemplate.Cells[row, col].Value = detail.KPIDatesTRING;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        col++;

                        wsTemplate.Cells[row, col].Value = detail.ActualScore;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        col++;

                        //wsTemplate.Cells[row, col].Value = detail.YearToDate;
                        //wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        //wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        //wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        //col++;

                        wsTemplate.Cells[row, col].Value = detail.Note;
                        wsTemplate.Cells[row, col].Style.Font.Bold = false;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        col++;

                        wsTemplate.Cells[row, col].Style.Font.Bold = true;
                        wsTemplate.Cells[row, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsTemplate.Cells[row, col].Style.Font.Size = 9;
                        wsTemplate.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        col++;
                        row++;
                        col = 1;
                    }
                    wsTemplate.Cells.AutoFitColumns();
                }
                excel.Save();
            }

            return file_path;
        }

        private void Header(ExcelRange excelRange, Color blueHeader, string Name)
        {
            excelRange.Merge = true;
            excelRange.Value = Name;
            excelRange.Style.Font.Bold = true;
            //excelRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
            excelRange.Style.Font.Color.SetColor(Color.White);
            excelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
            excelRange.Style.Fill.BackgroundColor.SetColor(blueHeader);
        }

        public string CreateExportDataSeach(SearchKPIDataViewModel model)
        {
            string file_name = string.Format("ReporSummaryPerformance_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_name_pdf = string.Format("ReporSummaryPerformance_{0}.pdf", DateTime.Now.ToString("dd-MMM-yy"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);
                //fileCreate.SetAccessControl(System.IO.Directory.GetAccessControl(file_path_folder, AccessControlSections.All));
            }
            FileInfo excelFile = new FileInfo(file_path);
            var columnHeader = new List<string>() {
                "NO", "Area", "Plant", "Shift", "Staff Name", "Actual Score", "Year To Date", "KPI Date", "Note", "Create By",
                "Create Date"
            };

            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#2489c5");
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet wsKPIData = excel.Workbook.Worksheets.Add("default");
                wsKPIData.View.ShowGridLines = true;
                wsKPIData.PrinterSettings.Orientation = eOrientation.Landscape;
                var row = 1;
                foreach (var department in model?.Result ?? new List<KPI_ONLINE.ViewModel.SearchKPIDataViewModel.SearchResult>()) {
                    var text = department.DepartMent;
                    var chkIsdividual = false;
                    foreach (var month in department.MonthList) {
                        chkIsdividual = month.KPIList.Where(w => w.IsIndividual).Select(s => s.IsIndividual).FirstOrDefault();
                    }
                    var countWeightCol = chkIsdividual == true ? columnHeader.Count : columnHeader.Count - 1;

                    Header(wsKPIData.Cells[row, 1, row, countWeightCol], blueHeader, text);
                    row++;
                    foreach (var month in department?.MonthList ?? new List<KPI_ONLINE.ViewModel.SearchKPIDataViewModel.MonthList>()) {
                        text = month.KPIDateString;
                        Header(wsKPIData.Cells[row, 1, row, countWeightCol], blueHeader, text);
                        row++;
                        foreach (var kpi in month?.KPIList?.OrderBy(x => x.KPINo)?.ToList() ?? new List<KPI_ONLINE.ViewModel.SearchKPIDataViewModel.KPIList>()) {
                            text = kpi.KPIName;
                            Header(wsKPIData.Cells[row, 1, row, countWeightCol], blueHeader, text);
                            row++;
                            //HEADER
                            var columnCountHead = 1;
                            foreach (var column in columnHeader) {
                                if (column == "Staff Name")
                                {
                                    if (kpi.IsIndividual)
                                    {
                                        wsKPIData.Cells[row, columnCountHead].Value = column;
                                        wsKPIData.Cells[row, columnCountHead].Style.Font.Bold = true;
                                        wsKPIData.Cells[row, columnCountHead].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        wsKPIData.Cells[row, columnCountHead].Style.Font.Size = 9;
                                        wsKPIData.Cells[row, columnCountHead].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        wsKPIData.Cells[row, columnCountHead].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsKPIData.Cells[row, columnCountHead].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                                        columnCountHead++;
                                    }
                                }
                                else {
                                    wsKPIData.Cells[row, columnCountHead].Value = column;
                                    wsKPIData.Cells[row, columnCountHead].Style.Font.Bold = true;
                                    wsKPIData.Cells[row, columnCountHead].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsKPIData.Cells[row, columnCountHead].Style.Font.Size = 9;
                                    wsKPIData.Cells[row, columnCountHead].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    wsKPIData.Cells[row, columnCountHead].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsKPIData.Cells[row, columnCountHead].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D3E2F9"));
                                    columnCountHead++;
                                }
                            }
                            row++;

                            //BODY
                            foreach (var detail in kpi?.Detail ?? new List<KPI_ONLINE.ViewModel.SearchKPIDataViewModel.KPIDetail>()) {
                                var columnDetail = 1;
                                wsKPIData.Cells[row, columnDetail].Value = detail.NO;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.Area;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.Plant;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.Shift;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                if (kpi.IsIndividual) {
                                    wsKPIData.Cells[row, columnDetail].Value = detail.StreffName;
                                    wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail++;
                                }

                                wsKPIData.Cells[row, columnDetail].Value = detail.ActualScore;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.YearToDate;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.KPIDate.ToString("dd/MM/yyyy");
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.Note;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.CreateBy;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;

                                wsKPIData.Cells[row, columnDetail].Value = detail.CreateDateString;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Bold = false;
                                wsKPIData.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                wsKPIData.Cells[row, columnDetail].Style.Font.Size = 9;
                                wsKPIData.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                columnDetail++;
                                row++;
                            }
                        }
                    }
                }
                excel.Save();
            }
            return file_path;
        }

        public ActionResult ExportExcel(string pPeriod_From, string pPeriod_To, string pFirstName, string pLastName, string pKPIIndicator, string pTree)
        {
            SearchKPIDataViewModel Model = new SearchKPIDataViewModel();
            Model.Criteria.PeriodFrom = pPeriod_From;
            Model.Criteria.PeriodTo = pPeriod_To;
            Model.Criteria.FirstName = pFirstName;
            Model.Criteria.LastName = pLastName;
            Model.Criteria.KPIIndicator = pKPIIndicator;
            Model.Criteria.Department = pTree;
            _.GetSearchResult(Model);

            string file_path = CreateExportDataSeach(Model);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }
    }

}