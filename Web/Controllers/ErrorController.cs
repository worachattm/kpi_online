﻿using KPI_ONLINE.Utilitie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    [MenuAuthorize(ConstantPrm.MenuAuthorize.ERROR_MENU)]
    public class ErrorController : BaseController
    {
        public ActionResult AccessDenied()
        {
            //Response.StatusCode = 403;
            return View();
        }

        public ActionResult HttpError404()
        {
            return View();
        }

        public ActionResult HttpError500()
        {
            return View();
        }

        public ActionResult HttpNonUser()
        {
            return View();
        }
    }
}