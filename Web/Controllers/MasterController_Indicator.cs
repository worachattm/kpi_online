﻿using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Controllers;
using Web.Utilitie;

namespace KPI_ONLINE.Controllers
{
    public partial class MasterController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU)]
        public ActionResult IndicatorsManage()
        {
            ViewBag.Message = TempData["ReponseMessage"];
            ViewBag.Result = TempData["ReponseResult"];

            var model = new SearchIndicatorsCriteriaViewModel();
            return View(model);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_SEARCH)]
        public ActionResult IndicatorsManage(SearchIndicatorsCriteriaViewModel viewModel)
        {
            var fn = new IndicatorServiceModel();
            fn.SearchResult(ref viewModel);
            return View(viewModel);
        }

        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_DELETE)]
        public ActionResult DeleteIndicator(string Id)
        {
            IndicatorServiceModel serviceModel = new IndicatorServiceModel();

            var rtn = serviceModel.Delete(Id, Const.User.UserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseResult"] = rtn.Result;


            return RedirectToAction("Role");
        }

        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE)]
        public ActionResult CreateOrEditIndicator(string Id)
        {
            var model = new IndicatorsManageViewModel(Id);

            return View(model);
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE)]
        public JsonResult CreatOrEditKPIName(IndicatorName model , HttpPostedFileBase FileUpload)
        {
            var count = Request.Files.Count;
            var Utilities = new Utilities();
            var fn = new IndicatorServiceModel();

            if(FileUpload != null)
            {
                model.File = Utilities.ConvertFileToBase64(FileUpload);
                model.FileName = FileUpload.FileName;
            }
    
            var res = fn.CreateOrEditIndicatorName(model , Const.User.UserName);
            return Json(new { isSuccess = true, result = res }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE , ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_EDIT)]
        public JsonResult GetKPINameList()
        {
            var list = IndicatorModel.GetIndicatorName();
            return Json(new { isSuccess = true, result = list }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE, ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_EDIT)]
        public JsonResult GetKPINameLast(string IdIndiatorName)
        {
            var viewModel = new SearchIndicatorsCriteriaViewModel();
            viewModel.Indicator = IdIndiatorName;
            var fn = new IndicatorServiceModel();
            fn.SearchResult(ref viewModel);

            return Json(new { isSuccess = true, result = viewModel.SearchResult.OrderByDescending(c => c.Year).FirstOrDefault() }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE, ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_EDIT)]
        public ActionResult CreateOrEditIndicator(IndicatorsManageViewModel input)
        {
            var serviceModel = new IndicatorServiceModel();

            if (!string.IsNullOrWhiteSpace(input.Id))
            {
                var rtn = serviceModel.Edit(input, Const.User.UserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseResult"] = rtn.Result;

                return RedirectToAction("IndicatorsManage");
             
            } else
            {
                var rtn = serviceModel.Add(input, Const.User.UserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseResult"] = rtn.Result;

                return RedirectToAction("IndicatorsManage");
            
            }
        }

        [HttpPost]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_CREATE, ConstantPrm.MenuAuthorize.KPI_MANAGE_MENU_EDIT)]
        public JsonResult EditScaleColor(List<KPIScale> input)
        {
            var serviceModel = new IndicatorServiceModel();

            var rtn = serviceModel.EditScale(input, Const.User.UserName);

            return Json(new { isSuccess = rtn.Status , result = rtn }, JsonRequestBehavior.AllowGet);

        }

    }
}