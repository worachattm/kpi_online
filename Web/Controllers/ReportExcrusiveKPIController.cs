﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.TKPI_USER_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Controllers;
using Web.Models;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.ReportExcrusiveKPIViewModel;

namespace KPI_ONLINE.Controllers
{
    public class ReportExcrusiveKPIController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.EXECUTIVE_REPORT_MENU)]
        public ActionResult Index()
        {
            var Model = new ReportExcrusiveKPIViewModel();
            var roleserviceModel = new RoleServiceModel();
            var serviceModel = new ReportExcusiveKPIServiceModel();
            InitialSearchCritiria(Model);
            var searchCriteria = Model.Criteria;
            searchCriteria.Year = DateTime.Now.Year.ToString();
            searchCriteria.PeriodType = "Year";
            Model.Role_Detail.DepartmentTree = roleserviceModel.GetDepartmentTreeWithLayer(null);
            serviceModel.testc(Model);
            //-----------------For Test Login
            GetUrlParameter(Request, "userID");
            //-------------------------------
            return View(Model);
        }

        private string GetUrlParameter(HttpRequestBase request, string parName)
        {
            string result = string.Empty;
            TKPI_USERS_DAL dal = new TKPI_USERS_DAL();
            var urlParameters = HttpUtility.ParseQueryString(request.Url.Query);
            if (urlParameters.AllKeys.Contains(parName))
            {
                result = urlParameters.Get(parName);
            }
            if (!string.IsNullOrEmpty(result))
            {
                ShareFn fn = new ShareFn();
                UnitOfWork unitOfWork = new UnitOfWork();
                var dataUser = dal.GetUserByUserName(result, ref unitOfWork);
                if (dataUser != null)
                {
                    UserModel user = new UserModel();

                    user.UserName = result;
                    user.EmployeeId = dataUser.TKU_USER_CODE;
                    user.Name = string.Format("{0} {1}", dataUser.TKU_FIRST_NAME, dataUser.TKU_LAST_NAME);
                    user.RoleId = dataUser.TKR_ID;
                    user.RoleName = dataUser.TKPI_ROLE.TKR_NAME;
                    user.CompanyName = dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME;
                    user.ImagePath = fn.GetPathofPersonalImg(Convert.ToInt64(dataUser.TKU_USER_CODE), dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME);

                    Const.User = user;
                    Const.User.AuthenView(ref unitOfWork);
                }

            }

            return result;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        [MenuAuthorize(ConstantPrm.MenuAuthorize.EXECUTIVE_REPORT_SEARCH)]
        public ActionResult Search(ReportExcrusiveKPIViewModel pModel)
        {
            if (pModel != null && pModel.Result == null)
            {
                pModel.Result = new List<SearchResult>();
            }

            var serviceModel = new ReportExcusiveKPIServiceModel();
            var roleserviceModel = new RoleServiceModel();
            serviceModel.GetSearchResult(pModel);
            serviceModel.testc(pModel);
            InitialSearchCritiria(pModel);
            return View("Index", pModel);
        }
        public void InitialSearchCritiria(ReportExcrusiveKPIViewModel Model)
        {
            var searchCriteria = Model.Criteria;
            var serviceModel = new SetDropDownList();
            searchCriteria.ddPeriodType = serviceModel.GetPeriodType();
            searchCriteria.ddTargetPeriod = serviceModel.GetTargetPeriod(Model.Criteria.PeriodType);
            searchCriteria.ddYear = serviceModel.GetYear();
            Model.ColorScaleCode = serviceModel.GetColorScaleCode();
        }
        #region dropdown
        [HttpGet]
        public JsonResult GetAreaByDepartment(string departmentId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in departmentId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetArea("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetAreaByDepartmentDDL(string departmentId)
        {
            try
            {
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var json = serviceModel.GetAreaCode(departmentId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetPlantByArea(string areaId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in areaId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetPlant("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetPlantByAreaDDL(string areaId)
        {
            try
            {
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var json = serviceModel.GetPlantCode(areaId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetShiftByPlant(string plantId)
        {
            try
            {
                var list = new List<string>();

                foreach (var item in plantId.Split('|').Where(c => !string.IsNullOrWhiteSpace(c)).ToList())
                {
                    if (item != "")
                    {
                        var temp = (item.Replace("&", "")).Replace("&", "");
                        list.Add((temp));
                    }
                }
                var serviceModel = new RoleServiceModel();
                var json = serviceModel.GetShift("", list);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetShiftByPlantDDL(int plantId)
        {
            try
            {
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var json = serviceModel.GetShiftCode(plantId);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetTargetPeriodByPeriodTypeDDL(string periodType)
        {
            try
            {
                var serviceModel = new SetDropDownList();
                var json = serviceModel.GetTargetPeriod(periodType);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public string CreateExcelExcusive(ReportExcrusiveKPIViewModel modelMock)
        {
            string SearchResult = "";
            if (modelMock.Criteria.PeriodType == "Year")
            {
                SearchResult = "Yearly report of " + modelMock.Criteria.Year;
            }
            else if (modelMock.Criteria.PeriodType == "Monthly")
            {
                SearchResult = "Monthly report of " + modelMock.Criteria.Year + " - " + modelMock.Criteria.TargetPeriod;
            }
            else if (modelMock.Criteria.PeriodType == "Quater")
            {
                SearchResult = "Quarterly report of " + modelMock.Criteria.Year + " - " + modelMock.Criteria.TargetPeriod;
            }

            ReportExcusiveKPIServiceModel serviceModel = new ReportExcusiveKPIServiceModel();
            serviceModel.GetSearchResult(modelMock);
            InitialSearchCritiria(modelMock);

            string file_name = string.Format("ReporExcusiveKPI_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_name_pdf = string.Format("ReporExcusiveKPI_{0}.pdf", DateTime.Now.ToString("dd-MMM-yy"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);
                //fileCreate.SetAccessControl(System.IO.Directory.GetAccessControl(file_path_folder, AccessControlSections.All));
            }


            FileInfo excelFile = new FileInfo(file_path);
            var columnHeader = new List<string>() {
                "", "Scale", "Weight"
            };
            var columnSubHeader = new List<string>() {
                "KPI No", "Process/Movements/Offsite", "Actual", "1", "2", "3", "4", "5"
            };
            Color blueColumnHeader = System.Drawing.ColorTranslator.FromHtml("#D3E2F9");
            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#2489c5");
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet wsExcusiveKPI = excel.Workbook.Worksheets.Add("default");
                wsExcusiveKPI.View.ShowGridLines = false;

                wsExcusiveKPI.PrinterSettings.Orientation = eOrientation.Landscape;
                int row = 3;
                var listid = new List<string>();
                var Jobject = JToken.Parse(modelMock.Role_Detail.DepartmentTree);
                foreach (var department in Jobject.Children())
                {
                    var itemProperties = department.Children<JProperty>();
                    //you could do a foreach or a linq here depending on what you need to do exactly with the value
                    var state = itemProperties.FirstOrDefault(x => x.Name == "state");
                    var stateElement = state.Value; ////This is a JValue type

                    var stateProperties = stateElement.Children<JProperty>();
                    var check = stateProperties.FirstOrDefault(x => x.Name == "checked");
                    var isCheck = check.Value;
                    if ((bool)isCheck)
                    {
                        var href = itemProperties.FirstOrDefault(x => x.Name == "href");
                        listid.Add(href.Value.ToString().Replace("#node-", ""));
                    }
                }

                var cell = wsExcusiveKPI.Cells[1, 1, 1, 11 + modelMock.CountWeightCol];
                cell.Merge = true;
                cell.Value = "Executive KPI Report";
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15;

                cell = wsExcusiveKPI.Cells[2, 1, 2, 11 + modelMock.CountWeightCol];
                cell.Merge = true;
                cell.Value = SearchResult;
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = false;
                cell.Style.Font.Size = 14;



                //JArray jArray = JArray.parse(modelMock.Role_Detail.DepartmentTree);
                foreach (var result in modelMock.Result.OrderBy(x => x.DepartMentID))
                {
                    foreach (var area in result.AreaList.OrderBy(x => x.AreaID))
                    {
                        foreach (var plant in area.PlantList.OrderBy(x => x.PlantID))
                        {
                            foreach (var shift in plant.ShiftList.OrderBy(x => x.ShiftID))
                            {

                                var three_id = "";
                                if (result.DepartMent.IsNullOrEmpty())
                                {
                                    three_id = "department_1";
                                    if (!listid.Contains(three_id))
                                    {
                                        continue;
                                    }

                                }
                                if (area.AreaName.IsNullOrEmpty())
                                {

                                    if (!string.IsNullOrWhiteSpace(shift.ShiftID))
                                    {
                                        three_id = "department_" + shift.ShiftID;
                                        if (!listid.Contains(three_id))
                                        {
                                            continue;
                                        }
                                    }
                                }
                                else if (plant.PlantName.IsNullOrEmpty())
                                {
                                    three_id = "area_" + shift.ShiftID;
                                    if (!listid.Contains(three_id))
                                    {
                                        continue;
                                    }
                                }
                                else if (shift.ShiftName.IsNullOrEmpty())
                                {
                                    three_id = "plant_" + shift.ShiftID;
                                    if (!listid.Contains(three_id))
                                    {
                                        continue;
                                    }
                                }
                                else if (!shift.ShiftName.IsNullOrEmpty())
                                {
                                    three_id = "shift_" + shift.ShiftID;
                                    if (!listid.Contains(three_id))
                                    {
                                        continue;
                                    }
                                }
                                var text = "";
                                if (result.DepartMent == null)
                                {
                                    text = (string.IsNullOrEmpty(result.DepartMent) ? "All > (EVPC)" : result.DepartMent + " (All)");
                                }
                                else if (area.AreaName == null)
                                {
                                    text = (string.IsNullOrEmpty(result.DepartMent) ? "All > (EMPC)" : result.DepartMent + " (All)");
                                }
                                else if (plant.PlantName == null)
                                {
                                    text = result.DepartMent + " > " + area.AreaName + " (All)";
                                }
                                else if (shift.ShiftName == null)
                                {
                                    text = result.DepartMent + " > " + area.AreaName + " > " + plant.PlantName + " (All)";
                                }
                                else
                                {
                                    text = result.DepartMent + " > " + area.AreaName + " > " + plant.PlantName + " > " + shift.ShiftName;
                                }
                                row++;
                                Header(wsExcusiveKPI.Cells[row, 1, row, 11 + modelMock.CountWeightCol], blueHeader, text);
                                row++;

                                int totalColumn = 0;
                                int columnCountHead = 1;
                                foreach (var column in columnHeader)
                                {
                                    if (columnCountHead == 1)
                                    {
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Merge = true;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Value = column;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Font.Bold = true;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Font.Size = 9;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        //wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 5].Style.Fill.BackgroundColor.SetColor(blueColumnHeader);
                                        columnCountHead += 6;
                                        totalColumn = columnCountHead;
                                    }
                                    else if (column == "Weight")
                                    {
                                        if (modelMock.CountWeightCol > 0)
                                        {
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Merge = true;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Value = column;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.Font.Bold = true;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.Font.Size = 9;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + modelMock.CountWeightCol - 1].Style.Fill.BackgroundColor.SetColor(blueColumnHeader);
                                            totalColumn = columnCountHead + modelMock.CountWeightCol - 1;
                                        }
                                    }
                                    else
                                    {
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Merge = true;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Value = column;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Font.Size = 9;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnCountHead, row, columnCountHead + 4].Style.Fill.BackgroundColor.SetColor(blueColumnHeader);
                                        columnCountHead += 5;
                                        totalColumn = columnCountHead;
                                    }
                                }


                                row++;
                                int coulumnCountSub = 1;
                                int loopCount = 0;
                                foreach (var column in columnSubHeader)
                                {
                                    loopCount++;
                                    var bgColor = blueColumnHeader;
                                    switch (column)
                                    {
                                        case "1":
                                            bgColor = ColorTranslator.FromHtml(modelMock.ColorScaleCode.ColorCode1);
                                            wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Color.SetColor(Color.White);
                                            break;
                                        case "2":
                                            bgColor = ColorTranslator.FromHtml(modelMock.ColorScaleCode.ColorCode2);
                                            break;
                                        case "3":
                                            bgColor = ColorTranslator.FromHtml(modelMock.ColorScaleCode.ColorCode3);
                                            break;
                                        case "4":
                                            bgColor = ColorTranslator.FromHtml(modelMock.ColorScaleCode.ColorCode4);
                                            wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Color.SetColor(Color.White);
                                            break;
                                        case "5":
                                            bgColor = ColorTranslator.FromHtml(modelMock.ColorScaleCode.ColorCode5);
                                            wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Color.SetColor(Color.White);
                                            break;
                                    }

                                    if (coulumnCountSub == 2)
                                    {
                                        if (loopCount != 2)
                                        {
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Merge = true;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Value = column;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.Font.Bold = true;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.Font.Size = 9;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsExcusiveKPI.Cells[row, coulumnCountSub, row, coulumnCountSub + 3].Style.Fill.BackgroundColor.SetColor(bgColor);

                                        }

                                        coulumnCountSub += 4;
                                    }
                                    else
                                    {
                                        //wsExcusiveKPI.Cells[row, coulumnCountSub].Merge = true;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Value = column;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Size = 9;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Fill.BackgroundColor.SetColor(bgColor);
                                        coulumnCountSub++;
                                    }

                                    if (loopCount == 1)
                                    {
                                        var cellTemp = wsExcusiveKPI.Cells[row - 1, 1, row, 1];
                                        cellTemp.Merge = true;
                                        cellTemp.Value = column;
                                        cellTemp.Style.Font.Size = 9;
                                        cellTemp.Style.Font.Bold = true;
                                        cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                        cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else if (loopCount == 2)
                                    {
                                        var cellTemp = wsExcusiveKPI.Cells[row - 1, 2, row, 5];
                                        cellTemp.Merge = true;
                                        cellTemp.Value = column;
                                        cellTemp.Style.Font.Size = 9;
                                        cellTemp.Style.Font.Bold = true;
                                        cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                        cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else if (loopCount == 3)
                                    {
                                        var cellTemp = wsExcusiveKPI.Cells[row - 1, 6, row, 6];
                                        cellTemp.Merge = true;
                                        cellTemp.Value = column;
                                        cellTemp.Style.Font.Size = 9;
                                        cellTemp.Style.Font.Bold = true;
                                        cellTemp.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        cellTemp.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        cellTemp.Style.Fill.BackgroundColor.SetColor(bgColor);
                                        cellTemp.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        cellTemp.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                }

                                foreach (var column in modelMock.ColumnWeight)
                                {
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Value = column.Value;
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Bold = true;
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsExcusiveKPI.Cells[row, coulumnCountSub].Style.Fill.BackgroundColor.SetColor(blueColumnHeader);
                                    coulumnCountSub++;
                                }
                                row++;

                                foreach (var detail in shift.Detail)
                                {
                                    int columnDetail = 1;
                                    //wsExcusiveKPI.Cells[row, columnDetail].Merge = true;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.NO;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Merge = true;
                                    wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Value = detail.Area;
                                    wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.Font.Bold = false;
                                    //wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.WrapText = true;
                                    //wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.ShrinkToFit = true;
                                    //wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail, row, columnDetail + 3].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail += 4;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.ActualScore;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Scale1;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    if (detail.KpiScore == 1)
                                    {
                                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.ColorCode);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                                    }
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Scale2;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    if (detail.KpiScore == 2)
                                    {
                                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.ColorCode);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.Black);
                                    }
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Scale3;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    if (detail.KpiScore == 3)
                                    {
                                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.ColorCode);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.Black);
                                    }
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Scale4;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    if (detail.KpiScore == 4)
                                    {
                                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.ColorCode);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                                    }
                                    columnDetail++;
                                    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Scale5;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    if (detail.KpiScore == 5)
                                    {
                                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(detail.ColorCode);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                                    }
                                    columnDetail++;
                                    foreach (var colW in modelMock.ColumnWeight)
                                    {
                                        var colValue = "";
                                        switch (colW.Key)
                                        {
                                            case "ASM":
                                                colValue = detail.WeightASM;
                                                break;
                                            case "LTO":
                                                colValue = detail.WeightLTO;
                                                break;
                                            case "LTR":
                                                colValue = detail.WeightLTR;
                                                break;
                                            case "JG 7-8":
                                                colValue = detail.WeightJG7_8;
                                                break;
                                            case "JG 9-12":
                                                colValue = detail.Weight9_12;
                                                break;

                                        }
                                        wsExcusiveKPI.Cells[row, columnDetail].Value = colValue;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        columnDetail++;
                                    }

                                    {
                                        //if (detail.ChkDrawASM)
                                        //{
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.WeightASM;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //    columnDetail++;
                                        //}
                                        //if (detail.ChkDrawLTO)
                                        //{
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.WeightLTO;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //    columnDetail++;
                                        //}
                                        //if (detail.ChkDrawLTR)
                                        //{
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.WeightLTR;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //    columnDetail++;
                                        //}
                                        //if (detail.ChkDrawJG7_8)
                                        //{
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.WeightJG7_8;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //    columnDetail++;
                                        //}
                                        //if (detail.ChkDraw9_12)
                                        //{
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Value = detail.Weight9_12;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                                        //    wsExcusiveKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                        //}
                                    }
                                    row++;
                                }
                            }
                        }
                    }
                }
                excel.Save();
            }
            var ffff = Newtonsoft.Json.JsonConvert.SerializeObject(modelMock);
            return file_path;
        }

        private void Header(ExcelRange excelRange, Color blueHeader, string Name)
        {
            excelRange.Merge = true;
            excelRange.Value = Name;
            excelRange.Style.Font.Bold = true;
            //excelRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
            excelRange.Style.Font.Color.SetColor(Color.White);
            excelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
            excelRange.Style.Fill.BackgroundColor.SetColor(blueHeader);
        }

        [MenuAuthorize(ConstantPrm.MenuAuthorize.EXECUTIVE_REPORT_EXPORT)]
        public ActionResult ExportExcel(string pPeriod_Type, string pTarget_Period, string pYear, string pTree)
        {
            var modelMock = new ReportExcrusiveKPIViewModel();
            modelMock.Criteria.PeriodType = pPeriod_Type;
            modelMock.Criteria.TargetPeriod = pTarget_Period;
            modelMock.Criteria.Year = pYear;
            modelMock.Role_Detail.DepartmentTree = pTree;

            string file_path = CreateExcelExcusive(modelMock);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }


        [MenuAuthorize(ConstantPrm.MenuAuthorize.EXECUTIVE_REPORT_CONFIG)]
        public JsonResult GetDefaultConfigReportKPIExcusive(decimal? year)
        {
            var serviceModel = new ReportExcusiveKPIServiceModel();
            if (year == null)
            {
                year = DateTime.Now.Year;
            }
            var json = serviceModel.GetConfigKPIIndator(year);


            //json.TableDisplayConfig = pModel.Template;

            json.Year = year;
            return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }


        [HttpPost]
        public ActionResult SaveExcelConfig(string year, string model)
        {
            try
            {
                //model = "{\"TableDisplayConfig\":[]}";
                year = year ?? DateTime.Now.Year.ToString();
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigKPIIndator>(model);
                var boo = serviceModel.SaveConfigKPIIndicator(decimal.Parse(year), obj);

                return Json(new { isSuccess = true, result = boo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var errorMessage = ex.GetaAllMessages();
                return Json(new { success = false, url = string.Empty, message = errorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSoftKPI(string model)
        {
            try
            {
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TKPI_REPORT_SOFT_KPI>(model);
                var boo = serviceModel.SaveSoftKpi(obj);

                return Json(new { isSuccess = true, result = boo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var errorMessage = ex.GetaAllMessages();
                return Json(new { success = false, url = string.Empty, message = errorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteSoftKPI(string model)
        {
            try
            {
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var boo = serviceModel.DeleteSoftKpi(model);

                return Json(new { isSuccess = true, result = boo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var errorMessage = ex.GetaAllMessages();
                return Json(new { success = false, url = string.Empty, message = errorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteReportExcusiveKPI(string year)
        {
            try
            {
                //year = "2019";
                var serviceModel = new ReportExcusiveKPIServiceModel();
                var result = serviceModel.DeleteConfigKPIIndicator(decimal.Parse(year));
                return Json(new { isSuccess = true, result = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errorMessage = GetErrorHtmlMessage(ex);
                return Json(new { success = false, url = string.Empty, message = errorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetErrorHtmlMessage(Exception ex)
        {
            var inner = ex.FromHierarchy(c => c.InnerException).LastOrDefault();
            var sb = new System.Text.StringBuilder();
            sb.Append("<h3>Error has occurred.</h3>");
            sb.AppendFormat("<span>Source : {0}</span><br/>", inner?.Source);
            sb.AppendFormat("<span>HResult: {0}</span><br/>", inner?.HResult);
            sb.AppendFormat("<span>Message: {0}</span><br/>", inner?.Message);
            return sb.ToString();
        }

    }
}