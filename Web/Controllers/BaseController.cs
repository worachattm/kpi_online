﻿using KPI_ONLINE.DAL.TKPI_USER_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Utilitie;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Models;
using Web.Utilitie;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        public string isWindowMode = System.Configuration.ConfigurationManager.AppSettings["isWindowMode"];
        public BaseController()
        {
            LoadUserInfo(true);
            ViewData["lbUserName"] = Const.User.UserName;
            ViewData["empImage"] = Const.User.ImagePath;
        }



        private void LoadUserInfo(bool mock = false)
        {
            ShareFn fn = new ShareFn();
            if (Const.User != null && Const.User.UserName != null)
            {
                Const.User.AuthenView(ref unitOfWork);
                //กรณีที่ Window Login ผ่านแล้ว จะเข้าเงื่อนไขนี้และไม่ต้องทำอะไรเพิ่ม
            }
            else
            {
                TKPI_USERS_DAL dal = new TKPI_USERS_DAL();

                if(isWindowMode != "1")
                {
                    if (mock && Const.User.UserName == null)
                    {
                        var dataUser = dal.GetUserByUserName("PHATCHARANAN", ref unitOfWork);
                        UserModel user = new UserModel();

                        user.UserName = "PHATCHARANAN";
                        user.EmployeeId = dataUser.TKU_USER_CODE;
                        user.Name = string.Format("{0} {1}", dataUser.TKU_FIRST_NAME, dataUser.TKU_LAST_NAME);
                        user.RoleId = dataUser.TKR_ID;
                        user.RoleName = dataUser.TKPI_ROLE.TKR_NAME;
                        user.CompanyName = dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME;
                        user.ImagePath = fn.GetPathofPersonalImg(Convert.ToInt64(dataUser.TKU_USER_CODE), dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME);

                        Const.User = user;
                        Const.User.AuthenView(ref unitOfWork);

                    }
                }
                else
                {
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        string domainUser = System.Web.HttpContext.Current.User.Identity.Name;
                        string username = string.IsNullOrEmpty(domainUser) ? "" : domainUser.Split('\\')[1].ToString();

                        var dataUser = dal.GetUserByUserName(username, ref unitOfWork);

                        if (dataUser != null)
                        {
                            UserModel user = new UserModel();

                            user.UserName = username.ToUpper();
                            user.EmployeeId = dataUser.TKU_USER_CODE;
                            user.Name = string.Format("{0} {1}", dataUser.TKU_FIRST_NAME, dataUser.TKU_LAST_NAME);
                            user.RoleId = dataUser.TKR_ID;
                            user.RoleName = dataUser.TKPI_ROLE.TKR_NAME;
                            user.CompanyName = dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME;
                            user.ImagePath = fn.GetPathofPersonalImg(Convert.ToInt64(dataUser.TKU_USER_CODE), dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME);

                            Const.User = user;
                            Const.User.AuthenView(ref unitOfWork);
                        }
                    }
                } 
            }
        }

    }
}