﻿using KPI_ONLINE.Models;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static KPI_ONLINE.ViewModel.VpAndPomKPIResultReportViewModel;
using KPI_ONLINE.Utilitie;
using Web.Utilitie;
using Web.Controllers;
using System.IO;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace KPI_ONLINE.Controllers
{
    public class VpAndPomKPIResultReportController : BaseController
    {
        // GET: VPAndPomKPIResultReport
        VpAndPomKPIResultReportServiceModel _ = new VpAndPomKPIResultReportServiceModel();
        [MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_SUMMARY_MENU)]
        public ActionResult Index()
        {
            var Model = new VpAndPomKPIResultReportViewModel();
            Model.Criteria.ReportMode = "Department";
            Model.Criteria.Year = DateTime.Now.Year.ToString();
            Model.Criteria.PeriodType = "Year";
            InitialSearchCritiria(Model);
            //_.GetSearchResult(Model);
            return View(Model);
        }

        public void InitialSearchCritiria(VpAndPomKPIResultReportViewModel Model)
        {
            var searchCriteria = Model.Criteria;

            var serviceModel = new SetDropDownList();
            searchCriteria.ddPeriodType = serviceModel.GetPeriodType();
            searchCriteria.ddTargetPeriod = serviceModel.GetTargetPeriod(Model.Criteria.PeriodType);
            searchCriteria.ddYear = serviceModel.GetYear();
            Model.ColorScaleCode = serviceModel.GetColorScaleCode();
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search"), MenuAuthorize(ConstantPrm.MenuAuthorize.KPI_SUMMARY_SEARCH)]
        public ActionResult Search(VpAndPomKPIResultReportViewModel pModel)
        {
            if (pModel != null && pModel.Result == null)
            {
                pModel.Result = new SearchResult();
            }
            _.GetSearchResult(pModel);
            InitialSearchCritiria(pModel);
            return View("Index", pModel);
        }
        [HttpGet]
        public JsonResult GetTargetPeriodByPeriodTypeDDL(string periodType)
        {
            try
            {
                var serviceModel = new SetDropDownList();
                var json = serviceModel.GetTargetPeriod(periodType);
                return Json(new { isSuccess = true, result = json }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string CreateExcel(VpAndPomKPIResultReportViewModel model)
        {
            string SearchResult = "";
            if (model.Criteria.PeriodType == "Year")
            {
                SearchResult = "Yearly report of " + model.Criteria.Year;
            }
            else if (model.Criteria.PeriodType == "Monthly")
            {
                SearchResult = "Monthly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }
            else if (model.Criteria.PeriodType == "Quater")
            {
                SearchResult = "Quarterly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
            }

            string file_name = string.Format("VpAndPomKPIResultReport_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_name_pdf = string.Format("VpAndPomKPIResultReport_{0}.pdf", DateTime.Now.ToString("dd-MMM-yy"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);

            if (!exists)
            {
                var fileCreate = System.IO.Directory.CreateDirectory(file_path_folder);
                //fileCreate.SetAccessControl(System.IO.Directory.GetAccessControl(file_path_folder, AccessControlSections.All));
            }
            FileInfo excelFile = new FileInfo(file_path);
            Color blueHeader = System.Drawing.ColorTranslator.FromHtml("#D3E2F9"); //2196f3
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet wsVpAndPomKPI = excel.Workbook.Worksheets.Add("default");
                wsVpAndPomKPI.View.ShowGridLines = false;
                wsVpAndPomKPI.PrinterSettings.Orientation = eOrientation.Landscape;
                var row = 4;
                var columnCountHead = 1;

                //Header
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Merge = true;
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Value = "";
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.Font.Bold = true;
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.Font.Size = 9;
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                //wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 6].Style.Fill.BackgroundColor.SetColor(Color.CadetBlue);

                columnCountHead += 7;

                foreach (var department in model.Result.HeaderList)
                {
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Merge = true;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Value = department;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.Font.Bold = true;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.Font.Size = 9;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsVpAndPomKPI.Cells[row, columnCountHead, row, columnCountHead + 1].Style.Fill.BackgroundColor.SetColor(blueHeader);
                    columnCountHead += 2;
                }

                var cell = wsVpAndPomKPI.Cells[1, 1, 1, columnCountHead - 1];
                cell.Merge = true;
                cell.Value = "KPI Summary - VP and POM KPI Result";
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15;

                cell = wsVpAndPomKPI.Cells[2, 1, 2, columnCountHead - 1];
                cell.Merge = true;
                cell.Value = SearchResult;
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.Font.Bold = false;
                cell.Style.Font.Size = 14;


                row++;

                //SubHeader
                columnCountHead = 1;
                //wsVpAndPomKPI.Cells[row, columnCountHead].Merge = true;
                cell = wsVpAndPomKPI.Cells[row - 1, 1, row, 1];
                cell.Merge = true;
                cell.Value = "KPI No.";
                cell.Style.Font.Bold = true;
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Style.Font.Size = 9;
                cell.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(blueHeader);
                columnCountHead += 1;

                cell = wsVpAndPomKPI.Cells[row - 1, 2, row, 7];
                cell.Merge = true;
                cell.Value = "Process/Movements/Offsite";
                cell.Style.Font.Bold = true;
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Style.Font.Size = 9;
                cell.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(blueHeader);

                columnCountHead += 6;
                foreach (var department in model.Result.HeaderList)
                {
                    wsVpAndPomKPI.Cells[row, columnCountHead].Value = "Aver. Score";
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Font.Bold = true;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Font.Size = 9;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Fill.BackgroundColor.SetColor(blueHeader);
                    columnCountHead += 1;

                    wsVpAndPomKPI.Cells[row, columnCountHead].Value = "KPI";
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Font.Bold = true;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Font.Size = 9;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsVpAndPomKPI.Cells[row, columnCountHead].Style.Fill.BackgroundColor.SetColor(blueHeader);
                    columnCountHead += 1;
                }
                row++;

                //Body
                foreach (var data in model.Result.data)
                {
                    var columnDetail = 1;
                    wsVpAndPomKPI.Cells[row, columnDetail].Value = data.TKI_INDI_NO;
                    wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                    wsVpAndPomKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                    wsVpAndPomKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    columnDetail++;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Value = data.TKI_INDI_CAL_MODE;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Merge = true;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Style.Font.Bold = false;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Style.Font.Size = 9;
                    wsVpAndPomKPI.Cells[row, columnDetail, row, columnDetail + 5].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    columnDetail += 6;

                    foreach (var Department in model?.Result?.HeaderList ?? new List<string>())
                    {
                        decimal? score = 0;
                        decimal? index = 0;
                        if (Department == "EVPC")
                        {
                            score = data.EVPC_SCORE;
                            index = data.EVPC_INDEX_COLOR;
                        }
                        else if (Department == "MRVP")
                        {
                            score = data.MRVP_SCORE;
                            index = data.MRVP_INDEX_COLOR;
                        }
                        else if (Department == "MMVP")
                        {
                            score = data.MMVP_SCORE;
                            index = data.MMVP_INDEX_COLOR;
                        }
                        else if (Department == "MPVP")
                        {
                            score = data.MPVP_SCORE;
                            index = data.MPVP_INDEX_COLOR;
                        }
                        else if (Department == "APU-A")
                        {
                            score = data.APU_A_SCORE;
                            index = data.APU_A_INDEX_COLOR;
                        }
                        else if (Department == "APU-B")
                        {
                            score = data.APU_B_SCORE;
                            index = data.APU_B_INDEX_COLOR;
                        }
                        else if (Department == "APU-C")
                        {
                            score = data.APU_C_SCORE;
                            index = data.APU_C_INDEX_COLOR;
                        }
                        else if (Department == "APU-D(M)")
                        {
                            score = data.APU_DM_SCORE;
                            index = data.APU_DM_INDEX_COLOR;
                        }
                        else if (Department == "APU-D(O)")
                        {
                            score = data.APU_DO_SCORE;
                            index = data.APU_DO_INDEX_COLOR;
                        }
                        else if (Department == "APU-E")
                        {
                            score = data.APU_E_SCORE;
                            index = data.APU_E_INDEX_COLOR;
                        }
                        else if (Department == "APU-F")
                        {
                            score = data.APU_F_SCORE;
                            index = data.APU_F_INDEX_COLOR;
                        }
                        var hexcode = "";
                        var serviceModel = new SetDropDownList();
                        model.ColorScaleCode = serviceModel.GetColorScaleCode();
                        switch (index)
                        {
                            case 1:
                                hexcode = model.ColorScaleCode.ColorCode1;
                                break;
                            case 2:
                                hexcode = model.ColorScaleCode.ColorCode2;
                                break;
                            case 3:
                                hexcode = model.ColorScaleCode.ColorCode3;
                                break;
                            case 4:
                                hexcode = model.ColorScaleCode.ColorCode4;
                                break;
                            case 5:
                                hexcode = model.ColorScaleCode.ColorCode5;
                                break;
                            default:
                                hexcode = "#808080";
                                break;
                        }
                        wsVpAndPomKPI.Cells[row, columnDetail].Value = score;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Bold = false;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        columnDetail++;

                        Color scalcolor = System.Drawing.ColorTranslator.FromHtml(hexcode);
                        wsVpAndPomKPI.Cells[row, columnDetail].Value = index;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Bold = true;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Size = 9;
                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                       
                        if (index != null)
                        {
                            wsVpAndPomKPI.Cells[row, columnDetail].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsVpAndPomKPI.Cells[row, columnDetail].Style.Fill.BackgroundColor.SetColor(scalcolor);
                        }
                           

                        wsVpAndPomKPI.Cells[row, columnDetail].Style.Font.Color.SetColor(Color.White);
                        columnDetail++;
                    }
                    row++;
                }
                excel.Save();
            }
            return file_path;
        }

        public ActionResult ExportExcel(string pReportMode, string pPeriod_Type, string pTarget_Period, string pYear)
        {
            VpAndPomKPIResultReportViewModel pModel = new VpAndPomKPIResultReportViewModel();
            if (pModel != null && pModel.Result == null)
            {
                pModel.Result = new SearchResult();
            }
            pModel.Criteria.ReportMode = pReportMode;
            pModel.Criteria.PeriodType = pPeriod_Type;
            pModel.Criteria.TargetPeriod = pTarget_Period;
            pModel.Criteria.Year = pYear;

            _.GetSearchResult(pModel);

            string file_path = CreateExcel(pModel);
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            return new JsonResult()
            {
                Data = new { FileName = file_name }
            };
        }

        public ActionResult LoadExcel(string file_path)
        {
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            string file_name = file_path.Replace(root_path + "\\", "");
            var byteArr = System.IO.File.ReadAllBytes(root_path + "\\" + file_path);
            return File(byteArr, "application/vnd.ms-excel", file_path);
        }
    }
}