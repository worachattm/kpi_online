﻿using KPI_ONLINE.DAL.TKPI_USER_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Models;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.DashboardViewModel;

namespace Web.Controllers
{
    [MenuAuthorize(ConstantPrm.MenuAuthorize.DASHBOARD_MENU)]
    public class DashboardController : BaseController
    {
        [MenuAuthorize(ConstantPrm.MenuAuthorize.DASHBOARD_MENU)]
        public ActionResult Index()
        {
            var pModel = new ReportExcrusiveKPIViewModel();
            GetUrlParameter(Request, "userID");
            var serviceModel = new ReportExcusiveKPIServiceModel();
            var searchCriteria = pModel.Criteria;
            searchCriteria.Year = DateTime.Now.Year.ToString();
            searchCriteria.PeriodType = "Year";
            var serviceDD = new SetDropDownList();
            pModel.ColorScaleCode = serviceDD.GetColorScaleCode();
            serviceModel.GetSearchResult(pModel,true);
            return View(pModel);
        }
        private string GetUrlParameter(HttpRequestBase request, string parName)
        {
            string result = string.Empty;
            TKPI_USERS_DAL dal = new TKPI_USERS_DAL();
            var urlParameters = HttpUtility.ParseQueryString(request.Url.Query);
            if (urlParameters.AllKeys.Contains(parName))
            {
                result = urlParameters.Get(parName);
            }
            if(!string.IsNullOrEmpty(result))
            {
                ShareFn fn = new ShareFn();
                UnitOfWork unitOfWork = new UnitOfWork();
                var dataUser = dal.GetUserByUserName(result, ref unitOfWork);
                if(dataUser != null)
                {
                    UserModel user = new UserModel();

                    user.UserName = result;
                    user.EmployeeId = dataUser.TKU_USER_CODE;
                    user.Name = string.Format("{0} {1}", dataUser.TKU_FIRST_NAME, dataUser.TKU_LAST_NAME);
                    user.RoleId = dataUser.TKR_ID;
                    user.RoleName = dataUser.TKPI_ROLE.TKR_NAME;
                    user.CompanyName = dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME;
                    user.ImagePath = fn.GetPathofPersonalImg(Convert.ToInt64(dataUser.TKU_USER_CODE), dataUser.TKPI_MASTER_COMPANY.TKC_COM_NAME);

                    Const.User = user;
                    Const.User.AuthenView(ref unitOfWork);
                }
                
            }

            return result;
        }

        [HttpPost]
        public string GenerateExcel1()
        {
            var fn1 = new ReportKPIPerformaceAreaServiceModel();
            var file_name = fn1.GenerateExcel();

            return file_name;
        }

        [HttpPost]
        public string GenerateExcel2()
        {
            var fn2 = new ReportKPIResultDashboardServiceModel();
            var file_name = fn2.GenerateExcel();

            return file_name;
        }

        public JsonResult GraphKPI()
        {
            List<Series> series = new List<Series>();
            //SCALE1
            Series scale1 = new Series();
            List<Object> dataGenGraphList = new List<Object>();
            for (int i = 0; i < 16; i++)
            {
                if (i == 1 || i == 5 || i == 8)
                {
                    dataGenGraphList.Add(1);
                }
                else {
                    dataGenGraphList.Add(null);
                }
            }
            //data Graph
            scale1.data = dataGenGraphList;
            scale1.name = "Scale1";
            //scale1.type = "";
            scale1.tooltip = new Tooltip(false, "");
            scale1.marker = new Marker(true);
            scale1.yAxis = 0;
            scale1.color = "#FF0000";
            series.Add(scale1);
            //SCALE2
            Series scale2 = new Series();
            dataGenGraphList = new List<Object>();
            for (int i = 0; i < 16; i++)
            {
                if (i == 2 || i == 6 || i == 12)
                {
                    dataGenGraphList.Add(2);
                }
                else
                {
                    dataGenGraphList.Add(null);
                }
            }
            scale2.data = dataGenGraphList;
            scale2.name = "Scale2";
            //scale2.type = "bar";
            scale2.tooltip = new Tooltip(false, "");
            scale2.marker = new Marker(true);
            scale2.yAxis = 0;
            scale2.color = "#ffff00";
            series.Add(scale2);
            //SCALE3
            Series scale3 = new Series();
            dataGenGraphList = new List<Object>();
            for (int i = 0; i < 16; i++)
            {
                if (i == 0 || i == 7 || i == 9 || i == 15)
                {
                    dataGenGraphList.Add(3);
                }
                else
                {
                    dataGenGraphList.Add(null);
                }
            }
            scale3.data = dataGenGraphList;
            scale3.name = "Scale3";
            //scale3.type = "bar";
            scale3.tooltip = new Tooltip(false, "");
            scale3.marker = new Marker(true);
            scale3.yAxis = 0;
            scale3.color = "#7CFC00";
            series.Add(scale3);
            //SCALE4
            Series scale4 = new Series();
            dataGenGraphList = new List<Object>();
            for (int i = 0; i < 16; i++)
            {
                if (i == 3 || i == 10 || i == 13)
                {
                    dataGenGraphList.Add(4);
                }
                else
                {
                    dataGenGraphList.Add(null);
                }
            }
            scale4.data = dataGenGraphList;
            scale4.name = "Scale4";
            //scale4.type = "bar";
            scale4.tooltip = new Tooltip(false, "");
            scale4.marker = new Marker(true);
            scale4.yAxis = 0;
            scale4.color = "#7CFC00";
            series.Add(scale4);
            //SCALE5
            Series scale5 = new Series();
            dataGenGraphList = new List<Object>();
            for (int i = 0; i < 16; i++)
            {
                if (i == 4 || i == 11 || i == 14)
                {
                    dataGenGraphList.Add(5);
                }
                else
                {
                    dataGenGraphList.Add(null);
                }
            }
            scale5.data = dataGenGraphList;
            scale5.name = "Scale5";
            //scale5.type = "bar";
            scale5.tooltip = new Tooltip(false, "");
            scale5.marker = new Marker(true);
            scale5.yAxis = 0;
            scale5.color = "#7CFC00";
            series.Add(scale5);

            Graph graph = new Graph();
            graph.Series = series;

            return Json(graph, JsonRequestBehavior.AllowGet);

        }
    }
}