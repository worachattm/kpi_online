﻿using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;

namespace KPI_ONLINE.Models
{
    public class IndicatorModel : BaseClass
    {
        public bool HaveDelete { get; set; }
        public string IndicatorNameId { get; set; }
        public string Id { get; set; }
        [Display(Name = "KPI No")]
        public string No { get; set; }
        [Display(Name = "KPI Indicators Name")]
        public string Name { get; set; }
        [Display(Name = "KPI Type")]
        public string Type { get; set; }
        [Display(Name = "KPI Year")]
        public string Year { get; set; }
        [Display(Name = "Calculate Mode")]
        public string CalculateMode { get; set; }
        [Display(Name = "Period From")]
        public string PeriodFrom { get; set; }
        [Display(Name = "Period To")]
        public string PeriodTo { get; set; }
        
        public List<KPIScale> KPIScaleList { get; set; } = new List<KPIScale>();

        public static List<SelectListItem> GetIndicatorDDL()
        {
            var fn = new IndicatorServiceModel();
            var ddl = fn.GetNameKpi();

            return ddl;
        }

        public static List<IndicatorName> GetIndicatorName()
        {
            var fn = new IndicatorServiceModel();
            var data = fn.GetKpi();

            return data;
        }
        
        public static List<SelectListItem> GetTypeDDL()
        {
            var ddl = new List<SelectListItem>();
            ddl.Add(new SelectListItem() { Text = "INDIVIDUAL", Value = "INDIVIDUAL" });
            ddl.Add(new SelectListItem() { Text = "SHIFT" , Value = "SHIFT" });
            return ddl;
        }
        public static List<SelectListItem> GetCategoryDDL()
        {
            var ddl = new List<SelectListItem>();
            ddl.Add(new SelectListItem() { Text = "Number", Value = "Number" });
            ddl.Add(new SelectListItem() { Text = "Text", Value = "Text" });
            return ddl;
        }

        public static List<SelectListItem> GetYearDDL()
        {
            var fn = new IndicatorServiceModel();
            var ddl = fn.GetYearKpi();
            return ddl;
        }

        public static List<SelectListItem> GetCalculateModeDDL()
        {
            var ddl = new List<SelectListItem>();
            ddl.Add(new SelectListItem() { Value = "ACCUMULATE", Text = "Accumulated" });
            ddl.Add(new SelectListItem() { Value = "SNAPSHOT", Text = "Snapshot" });
            ddl.Add(new SelectListItem() { Value = "AVERAGE", Text = "Average" });

            return ddl;
        }

        public static List<SelectListItem> GetDisplayColorDDL()
        {
            var ddl = new List<SelectListItem>();
            //ddl.Add(new SelectListItem() { Text = "-- กรุณาเลือก --", Value = "" });
            ddl.Add(new SelectListItem() { Text = "Gray", Value = "GRAY" });
            ddl.Add(new SelectListItem() { Text = "Red", Value = "RED" });
            ddl.Add(new SelectListItem() { Text = "Yellow", Value = "YELLOW" });
            ddl.Add(new SelectListItem() { Text = "Green", Value = "GREEN" });

            return ddl;
        }

        public List<KPIScale> GetKpiScaleMaster()
        {
            var result = new List<KPIScale>();
            try
            {
                result = unitOfWork.IndicatorScaleRepository.GetAll(c => c.TKIS_STATUS == "ACTIVE").OrderBy(c => c.TKIS_INDI_SCALE_NO).Select(item => new KPIScale()
                {
                    Id = item.TKIS_ID,
                    Scale = Convert.ToInt32(item.TKIS_INDI_SCALE_NO),
                    DisplayColor = item.TKIS_INDI_SCALE_COLOR
                }).ToList();
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);

            }
            return result;
        }

    }
}