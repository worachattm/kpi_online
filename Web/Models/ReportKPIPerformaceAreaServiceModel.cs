﻿using KPI_ONLINE.DAL.Utilities;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Web.Utilitie;

namespace Web.Models
{
    public class ReportKPIPerformaceAreaServiceModel : BaseClass
    {
        public string GenerateExcel()
        {
            var fn = new Utilities();
            string file_name = string.Format("KPIPerformaceArea_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "KPIPerformaceArea", file_name);

            var resultData = new KPIList();
            try
            {
                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    var groupHeaderOne = resultData.kpiList.FirstOrDefault().groupOne;
                    var groupHeaderTwo = resultData.kpiList.FirstOrDefault().groupTwo;
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("KPI Performace Area");
                    var rowHeader = 1;
                    var colTemp = 1;
                    //ROW 1
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, 8 + groupHeaderOne.Count + groupHeaderTwo.Count, isMerge: true, isBold: true, value: "KPI Performance Dashboard" + " year");
                    rowHeader++;

                    //ROW 2
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader + 1, colTemp, isMerge: true, isBold: true, value: "KPI No");
                    colTemp++;
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader + 1, colTemp, isMerge: true, isBold: true, value: "KPI No");
                    colTemp++;
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader + 1, colTemp + 5, isMerge: true, isBold: true, value: "Process / Movements / Off sites");
                    colTemp += 6;
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp + groupHeaderOne.Count - 1 , isMerge: true, isBold: true, value: "Dashboard Summarized");
                    colTemp += groupHeaderOne.Count;
                    fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp + groupHeaderTwo.Count - 1, isMerge: true, isBold: true, value: "Toc KPI");
                    colTemp += groupHeaderTwo.Count;
                    rowHeader++;

                    //ROW 3
                    colTemp = 9;
                    groupHeaderOne.ForEach(itemGroup =>
                    {
                        fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: itemGroup.positionName);
                        colTemp++;
                    });

                    groupHeaderTwo.ForEach(itemGroup =>
                    {
                        fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: itemGroup.positionName);
                        colTemp++;
                    });
                    rowHeader++;

                    //BODY
                    resultData.kpiList.Select((value, index) => new { index = index + 1 , value }).ToList().ForEach(kpi =>
                    {
                        colTemp = 1;
                        fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: kpi.index.ToString());
                        colTemp++;
                        fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: kpi.value.no.ToString());
                        colTemp++;
                        fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp + 5, isMerge: true, isBold: true, value: kpi.value.headerKPI);
                        colTemp += 6;

                        groupHeaderOne.ForEach(itemGroup =>
                        {
                            fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: ((int)itemGroup.scoreColor).ToString());
                            colTemp++;
                        });

                        groupHeaderTwo.ForEach(itemGroup =>
                        {
                            fn.WriteExcelDetail(ref ws, rowHeader, colTemp, rowHeader, colTemp, isMerge: true, isBold: true, value: ((int)itemGroup.scoreColor).ToString());
                            colTemp++;
                        });

                        rowHeader++;
                    });

                    package.Save();

                }
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                File.Delete(file_path);
                file_name = string.Empty;
            }

            return file_name;

        }
    }
}