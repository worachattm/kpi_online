﻿using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static KPI_ONLINE.ViewModel.ExportImportDataViewModel;
using static KPI_ONLINE.ViewModel.SearchKPIDataViewModel;
using OfficeOpenXml;
using System.IO;
using KPI_ONLINE.DAL.Entity;
using System.Globalization;
using KPI_ONLINE.DAL.Utilities;
using Web.Utilitie;
using System.Web.Mvc;
using System.Drawing;

namespace KPI_ONLINE.Models
{
    enum colname
    {
        department,
        area,
        plant,
        shift,
        kpidate,
        actualscore,
        yeartodate,
        note,
        empid,
        firstname,
        lastname,
        errordescribe,

    }

    public class ImportKPIDataServiceModel : BaseClass
    {

        public Tuple<ReturnValue, string> ImportExcel(string note, HttpPostedFileBase fileToUpload)
        {
            bool isFail = false;
            var insert = 0;
            var update = 0;
            int error = 0;
            var rtn = new ReturnValue();
            var isIndividual = false;
            var columnName = new Dictionary<colname, int>();

            var alldep = unitOfWork.DepartmentRepository.GetAll();
            var allare = unitOfWork.AreaRepository.GetAll();
            var allpla = unitOfWork.PlantRepository.GetAll();
            var allshi = unitOfWork.ShiftRepository.GetAll();
            var alluse = unitOfWork.UserRepository.GetAll();
            var allind = unitOfWork.IndicatorRepository.GetAll();
            columnName.Add(colname.department, -1);
            columnName.Add(colname.area, -1);
            columnName.Add(colname.plant, -1);
            columnName.Add(colname.shift, -1);
            columnName.Add(colname.kpidate, -1);
            columnName.Add(colname.actualscore, -1);
            //columnName.Add(colname.yeartodate, -1);
            columnName.Add(colname.note, -1);
            columnName.Add(colname.errordescribe, -1);
            {
                columnName.Add(colname.empid, -1);
                columnName.Add(colname.firstname, -1);
                columnName.Add(colname.lastname, -1);
            }
            var nameList = columnName.Select(x => x.Key.ToString());
            try
            {
                var listTKD = new List<TKPI_KPI_DATA>();
                var KPI_DATA_FILE = new TKPI_KPI_DATA_FILE();
                using (ExcelPackage excelPackage = new ExcelPackage(fileToUpload.InputStream))
                {

                    //loop all worksheets
                    foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                    {
                        var kpi = unitOfWork.KpiNameRepository.GetAll().FirstOrDefault(x => x.TKIN_NO.ToString() == worksheet.Name.Replace("KPI-", ""));
                        if (kpi == null)
                        {
                            continue;
                        }
                        var kpiTemplate = new ExportTemplate();
                        kpiTemplate.KPIIndicator = "KPI-" + kpi.TKIN_NO + ":" + kpi.TKIN_NAME;

                        var kpiID = kpi.TKIN_ID;
                        var endColumn = worksheet.Dimension.End.Column + 1;
                        var endRow = worksheet.Dimension.End.Row;
                        {
                            for (int i = worksheet.Dimension.End.Row; i > worksheet.Dimension.Start.Row; i--)
                            {
                                object text = null;
                                for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                                {
                                    if (worksheet.Cells[i, j].Value != null)
                                    {
                                        text = worksheet.Cells[i, j].Value;
                                        endRow = i;
                                        break;
                                    }
                                }
                                if (text != null)
                                {
                                    break;
                                }
                            }
                        }
                        for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                        {
                            //add the cell data to the List
                            try
                            {
                                if (worksheet.Cells[4, j].Value != null)
                                {
                                    var text = worksheet.Cells[4, j].Value.ToString()?.ToLower()?.Replace(" ", "");
                                    if (nameList.Contains(text ?? ""))
                                    {
                                        columnName[(colname)Enum.Parse(typeof(colname), text)] = j;
                                    }
                                }
                            }
                            catch
                            {

                            }
                        }

                        var inidactorList = unitOfWork.IndicatorRepository.GetAll(x => x.TKIN_ID == kpiID);
                        //loop all rows

                        for (int i = 5; i <= endRow; i++)
                        {
                            try
                            {
                                //i = 3277;
                                //loop all columns in a row
                                var TKD = new TKPI_KPI_DATA();
                                // PI DATA ID
                                // SET VALUE TO -1 AS DEFULT VALUE 
                                // AND SET VALUE WHEN GROUPING 
                                TKD.TKID_ID = Guid.NewGuid().ToString().Replace("-", "");
                                if (columnName[colname.department] > 0)
                                    TKD.TKID_DEPARTMENT = Castvalue<string>(worksheet.Cells[i, columnName[colname.department]].Value);
                                if (columnName[colname.area] > 0)
                                    TKD.TKID_AREA = Castvalue<string>(worksheet.Cells[i, columnName[colname.area]].Value);
                                if (columnName[colname.plant] > 0)
                                    TKD.TKID_PLANT = Castvalue<string>(worksheet.Cells[i, columnName[colname.plant]].Value);
                                if (columnName[colname.shift] > 0)
                                    TKD.TKID_SHIFT = Castvalue<string>(worksheet.Cells[i, columnName[colname.shift]].Value);
                                if (columnName[colname.kpidate] > 0)
                                {
                                    TKD.TKID_KPI_DATE = Castvalue<DateTime>(worksheet.Cells[i, columnName[colname.kpidate]].Value);
                                    {
                                        var inidactor = inidactorList.FirstOrDefault(x => x.TKI_INDI_FROM <= TKD.TKID_KPI_DATE && x.TKI_INDI_TO >= TKD.TKID_KPI_DATE);
                                        TKD.TKI_ID = inidactor?.TKI_ID;
                                        isIndividual = inidactor?.TKI_INDI_TYPE?.ToUpper() == "INDIVIDUAL";
                                        //GetIndicator(pIndicatorID, );
                                    }
                                }
                                if (columnName[colname.empid] > 0)
                                    TKD.TKID_EMP_ID = isIndividual ? Castvalue<string>(worksheet.Cells[i, columnName[colname.empid]].Value) : null;
                                if (columnName[colname.firstname] > 0)
                                    TKD.TKID_FIRST_NAME = isIndividual ? Castvalue<string>(worksheet.Cells[i, columnName[colname.firstname]].Value) : null;
                                if (columnName[colname.lastname] > 0)
                                    TKD.TKID_LAST_NAME = isIndividual ? Castvalue<string>(worksheet.Cells[i, columnName[colname.lastname]].Value) : null;
                                if (columnName[colname.actualscore] > 0)
                                {
                                    TKD.TKID_ACTUAL_SCORE = Castvalue<decimal?>(worksheet.Cells[i, columnName[colname.actualscore]].Value);
                                }
                                
                                if (columnName[colname.note] > 0)
                                    TKD.TKID_NOTE = Castvalue<string>(worksheet.Cells[i, columnName[colname.note]].Value);

                                TKD.TKID_CREATED_BY = "System";
                                TKD.TKID_CREATED_DATE = DateTime.Now;
                                TKD.TKID_UPDATED_BY = "System";
                                TKD.TKID_UPDATED_DATE = DateTime.Now;
                                // FILE ID
                                // SET VALUE WHEN GROUPING 
                                TKD.TKDF_ID = KPI_DATA_FILE.TKDF_ID;

                                TKD.TKID_TRANFROM_TYPE = "IMPORT";
                                TKD.TKID_TYPE = isIndividual ? "Individual" : "Shift";
                                listTKD.Add(TKD);
                                var errormessage = checkedImporrtData(TKD,
                                    alldep,
                                    allare,
                                    allpla,
                                    allshi,
                                    alluse,
                                    allind
                                    );
                                if (errormessage.Any())
                                {
                                    isFail = true;
                                    var errorColumn = columnName[colname.errordescribe] > 0 ? columnName[colname.errordescribe] : endColumn;
                                    worksheet.Cells[i, errorColumn].Value = string.Join(", ", errormessage);
                                    worksheet.Cells[i, errorColumn].Style.Font.Color.SetColor(Color.Red);
                                    error++;

                                }
                                else
                                {

                                    if (columnName[colname.errordescribe] > 0)
                                        worksheet.Cells[i, columnName[colname.errordescribe]].Value = "";

                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        worksheet.Cells.AutoFitColumns();
                    }
                    var file_path = "";
                    string path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
                    bool exists = System.IO.Directory.Exists(path_folder);

                    if (!exists)
                    {
                        var fileCreate = System.IO.Directory.CreateDirectory(path_folder);
                    }
                    string file_name = fileToUpload.FileName;
                    var temp_file_path = "";
                    file_path = Path.Combine(path_folder, file_name);
                    if (file_path.Contains("."))
                    {
                        temp_file_path = file_path.Split('.')[0] + "({0})." + file_path.Split('.')[1];
                    }
                    else
                    {
                        temp_file_path = file_path.Split('.')[0] + "({0})";
                    }
                    int index = 1;
                    while (File.Exists(file_path))
                    {
                        file_path = string.Format(temp_file_path, index++);
                    }
                    excelPackage.SaveAs(new FileInfo(file_path));
                    if (isFail)
                    {

                        rtn.Message = "Can't read " + error;
                        rtn.Status = false;
                        return new Tuple<ReturnValue, string>(rtn, file_path);
                    }
                    else
                    {
                        var savedata = listTKD.GroupBy(x => new
                        {
                            x.TKI_ID,
                            x.TKID_DEPARTMENT,
                            x.TKID_AREA,
                            x.TKID_PLANT,
                            x.TKID_SHIFT,
                            x.TKID_EMP_ID,
                            x.TKID_FIRST_NAME,
                            x.TKID_LAST_NAME,
                            x.TKID_KPI_DATE.Month,
                            x.TKID_KPI_DATE.Year,

                        }).ToList().Select(x => x.OrderByDescending(y => y.TKID_KPI_DATE).First()).ToList();
                        {
                            var kpilist = string.Join(", ", excelPackage.Workbook.Worksheets.Select(x => x.Name));
                            KPI_DATA_FILE = new TKPI_KPI_DATA_FILE()
                            {
                                TKDF_ID = Guid.NewGuid().ToString().Replace("-", ""),
                                TKDF_FILE_PATH = file_path,
                                TKDF_FILE_NAME = file_path.Split('\\').Last(),
                                TKDF_NOTE = note,
                                TKDF_STATUS = "Active",
                                TKDF_CREATED_DATE = DateTime.Now,
                                TKDF_CREATED_BY = "SYSTEM",
                                TKDF_KPI = kpilist
                            };
                            unitOfWork.KpiDataFileRepository.Insert(KPI_DATA_FILE);
                            //unitOfWork.Commit();
                            var data = unitOfWork.KpiDataRepository.GetAll();

                            foreach (var x in savedata)
                            {
                                var updatedata = data.FirstOrDefault(y =>
                                     x.TKI_ID == y.TKI_ID &&
                                     x.TKID_DEPARTMENT == y.TKID_DEPARTMENT &&
                                     x.TKID_AREA == y.TKID_AREA &&
                                     x.TKID_PLANT == y.TKID_PLANT &&
                                     x.TKID_SHIFT == y.TKID_SHIFT &&
                                     x.TKID_EMP_ID == y.TKID_EMP_ID &&
                                     x.TKID_FIRST_NAME == y.TKID_FIRST_NAME &&
                                     x.TKID_LAST_NAME == y.TKID_LAST_NAME &&
                                     x.TKID_KPI_DATE.Month == y.TKID_KPI_DATE.Month &&
                                     x.TKID_KPI_DATE.Year == y.TKID_KPI_DATE.Year
                                           );
                                if (updatedata == null)
                                {
                                    unitOfWork.KpiDataRepository.Insert(x);
                                    //unitOfWork.Commit();
                                    insert++;
                                }
                                else
                                {
                                    updatedata.TKI_ID = x.TKI_ID;
                                    updatedata.TKID_KPI_DATE = x.TKID_KPI_DATE;
                                    updatedata.TKID_UPDATED_BY = x.TKID_UPDATED_BY;
                                    updatedata.TKID_UPDATED_DATE = x.TKID_UPDATED_DATE;
                                    updatedata.TKID_YEAR_TO_DATE = x.TKID_YEAR_TO_DATE;
                                    updatedata.TKID_ACTUAL_SCORE = x.TKID_ACTUAL_SCORE;
                                    updatedata.TKID_NOTE = x.TKID_NOTE;
                                    updatedata.TKID_TRANFROM_TYPE = x.TKID_TRANFROM_TYPE;
                                    updatedata.TKDF_ID = x.TKDF_ID;
                                    unitOfWork.KpiDataRepository.Update(updatedata);
                                    //unitOfWork.Commit();
                                    update++;
                                }

                            }
                            unitOfWork.Commit();
                        }
                        rtn.Message = +insert + ":insert, " + +update + ":update";
                        rtn.Status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = "System error: " + ex.GetaAllMessages().LastOrDefault() ?? "ImportDataCriteria FAIL";
                unitOfWork.Rollback();
            }
            return new Tuple<ReturnValue, string>(rtn, "");
        }

        private List<string> checkedImporrtData(TKPI_KPI_DATA data, IEnumerable<TKPI_MASTER_DEPARTMENT> alldep, IEnumerable<TKPI_MASTER_AREA> allare, IEnumerable<TKPI_MASTER_PLANT> allpla, IEnumerable<TKPI_MASTER_SHIFT> allshi, IEnumerable<TKPI_USER> alluse, IEnumerable<TKPI_INDICATORS> allind)
        {
            var error = new List<string>();

            if (data.TKID_KPI_DATE == new DateTime())
            {
                error.Add(" Date format is not correct");
            }
            if (data.TKI_ID.IsNullOrEmpty())
            {
                error.Add(" Cant find indicator for this Kpi Date");
            }
            if (!data.TKID_ACTUAL_SCORE.HasValue)
            {
                error.Add("Actual score and Year to date can't both be empty");
            }
            //var isSnapshot = allind.FirstOrDefault(x => x.TKI_ID == data.TKI_ID)?.TKI_INDI_CAL_MODE.ToLower() == "snapshot";
            var isSnapshot = true;
            if (alldep.FirstOrDefault(x => x.TKD_DEPARTMENT_NAME == data.TKID_DEPARTMENT) == null)
            {
                error.Add("Cant find this Department");
            }
            else if (allare.FirstOrDefault(x => x.TKA_AREA_NAME == data.TKID_AREA && x.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME == data.TKID_DEPARTMENT) == null)
            {
                if (isSnapshot && data.TKID_AREA.IsNullOrEmpty())
                    return error;
                error.Add("Cant find this Area in Department " + data.TKID_DEPARTMENT);

            }
            else if (allpla.FirstOrDefault(x => x.TKP_PLANT_NAME == data.TKID_PLANT && x.TKPI_MASTER_AREA.TKA_AREA_NAME == data.TKID_AREA && x.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME == data.TKID_DEPARTMENT) == null)
            {
                if (isSnapshot && data.TKID_PLANT.IsNullOrEmpty())
                    return error;
                error.Add("Cant find this Plant in Area " + data.TKID_AREA);

            }
            else if (allshi.FirstOrDefault(x => x.TKS_SHIFT_NAME == data.TKID_SHIFT && x.TKPI_MASTER_PLANT.TKP_PLANT_NAME == data.TKID_PLANT && x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME == data.TKID_AREA && x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME == data.TKID_DEPARTMENT) == null)
            {
                if (isSnapshot && data.TKID_SHIFT.IsNullOrEmpty())
                    return error;
                error.Add("Cant find this Shift in Plant " + data.TKID_PLANT);
            }
            else if (allind.FirstOrDefault(x => x.TKI_ID == data.TKI_ID)?.TKI_INDI_TYPE.ToUpper() == "INDIVIDUAL")
            {
                if (isSnapshot && data.TKID_EMP_ID.IsNullOrEmpty() && data.TKID_FIRST_NAME.IsNullOrEmpty() && data.TKID_LAST_NAME.IsNullOrEmpty())
                    return error;
                else if (alluse.FirstOrDefault(x =>
                    x.TKU_USER_CODE == data.TKID_EMP_ID &&
                    x.TKU_FIRST_NAME == data.TKID_FIRST_NAME &&
                    x.TKU_LAST_NAME == data.TKID_LAST_NAME &&
                    x.TKPI_MASTER_SHIFT?.TKS_SHIFT_NAME == data.TKID_SHIFT
                    ) == null)
                {
                    error.Add("Cant find this Enployee in Shift " + data.TKID_SHIFT + " (" + data.TKID_PLANT + ")");
                }
            }
            return error;
        }

        public Template DrawTempplate(string pRec, string pAction)
        {
            var allUser = unitOfWork.UserRepository.GetAll();
            var TemplateData = Newtonsoft.Json.JsonConvert.DeserializeObject<TemplateData>(pRec);
            TemplateData.KPIIndicator = TemplateData.KPIIndicator.Where(x => !x.IsNullOrEmpty()).ToList();
            TemplateData.Department = TemplateData.Department.Where(x => !x.IsNullOrEmpty()).ToList();
            TemplateData.Area = TemplateData.Area.Where(x => !x.IsNullOrEmpty()).ToList();
            TemplateData.Plant = TemplateData.Plant.Where(x => !x.IsNullOrEmpty()).ToList();
            TemplateData.Shift = TemplateData.Shift.Where(x => !x.IsNullOrEmpty()).ToList();
            var kpiList = unitOfWork.KpiNameRepository.GetAll();//.Where(x => TemplateData.KPIIndicator.Contains(x.TKIN_ID));
            if (TemplateData.KPIIndicator.Any())
            {
                kpiList = unitOfWork.KpiNameRepository.GetAll().Where(x => TemplateData.KPIIndicator.Contains(x.TKIN_ID));
            }
            var shiList = unitOfWork.ShiftRepository.GetAll(x => x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_ID != "1");
            //if (!TemplateData.Department.Any())
            //{

            //}
            //else if (!TemplateData.Area.Any())
            //{
            //    shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Department.Contains(x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_ID));
            //}
            //else if (!TemplateData.Plant.Any())
            //{
            //    shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Area.Contains(x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_ID));
            //}
            //else if (!TemplateData.Shift.Any())
            //{
            //    shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Plant.Contains(x.TKPI_MASTER_PLANT.TKP_ID));
            //}
            //else
            //{
            //    shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Shift.Contains(x.TKS_ID));
            //}

            if (TemplateData.Shift.Any())
            {
                shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Shift.Contains(x.TKS_ID));
            }
            else if (TemplateData.Plant.Any())
            {
                shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Plant.Contains(x.TKPI_MASTER_PLANT.TKP_ID));
            }
            else if (TemplateData.Area.Any())
            {
                shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Area.Contains(x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_ID));
            }
            else if (TemplateData.Department.Any())
            {
                shiList = unitOfWork.ShiftRepository.GetAll().Where(x => TemplateData.Department.Contains(x.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_ID));
            }
            var exportTemplate = new Template();
            foreach (var kpi in kpiList)
            {
                var i = 1;
                var template = new ExportTemplate();
                exportTemplate.TemplatesList.Add(template);
                template.KPIIndicator = "KPI-" + kpi.TKIN_NO + " : " + kpi.TKIN_NAME;
                template.Type = kpi.TKPI_INDICATORS.OrderBy(x => x.TKI_INDI_YEAR).FirstOrDefault()?.TKI_INDI_TYPE;
                //if (kpi.TKPI_INDICATORS.OrderBy(x => x.TKI_INDI_YEAR).FirstOrDefault()?.TKI_INDI_CAL_MODE == "SNAPSHOT")
                //{
                    var depList = shiList.GroupBy(X => new
                    {
                        dep = X.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME,
                        aer = X.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME,
                        pnt = X.TKPI_MASTER_PLANT.TKP_PLANT_NAME,
                    }).GroupBy(x => new
                    {
                        x.Key.dep,
                        x.Key.aer

                    }).GroupBy(x =>
                       x.Key.dep
                    );
                    foreach (var dep in depList)
                    {
                        var Data = new ExportDataResult
                        {
                            NO = i++,
                            DepartMent = dep.Key
                        };
                        template.ExportResultList.Add(Data);
                        foreach (var aer in dep)
                        {
                            Data = new ExportDataResult
                            {
                                NO = i++,
                                DepartMent = aer.Key.dep,
                                Area = aer.Key.aer,
                            };
                            template.ExportResultList.Add(Data);
                            foreach (var pnt in aer)
                            {
                                Data = new ExportDataResult
                                {
                                    NO = i++,
                                    DepartMent = pnt.Key.dep,
                                    Area = pnt.Key.aer,
                                    Plant = pnt.Key.pnt,
                                };
                                template.ExportResultList.Add(Data);
                                foreach (var shift in pnt)
                                {
                                    if (template?.Type?.ToUpper() == "SHIFT")
                                    {
                                        Data = new ExportDataResult
                                        {
                                            NO = i++,
                                            DepartMent = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME,
                                            Area = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME,
                                            Plant = shift.TKPI_MASTER_PLANT.TKP_PLANT_NAME,
                                            Shift = shift.TKS_SHIFT_NAME
                                        };
                                        template.ExportResultList.Add(Data);
                                    }
                                    else
                                    {
                                        var ListUser = allUser.Where(x => x.TKS_ID == shift.TKS_ID).ToList();
                                        foreach (var user in ListUser)
                                        {
                                            Data = new ExportDataResult
                                            {
                                                NO = i++,
                                                DepartMent = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME.ToString(),
                                                Area = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME.ToString(),
                                                Plant = shift.TKPI_MASTER_PLANT.TKP_PLANT_NAME.ToString(),
                                                Shift = shift.TKS_SHIFT_NAME.ToString(),
                                                EmpID = user.TKU_USER_CODE,
                                                FirstName = user.TKU_FIRST_NAME,
                                                LastName = user.TKU_LAST_NAME
                                            };
                                            template.ExportResultList.Add(Data);
                                        }
                                    }
                                }
                            }
                        }
                    }
                //}
                //else
                //{
                //    foreach (var shift in shiList)
                //    {
                //        if (template?.Type?.ToUpper() == "SHIFT")
                //        {
                //            var Data = new ExportDataResult();
                //            Data = new ExportDataResult
                //            {
                //                NO = i++,
                //                DepartMent = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME,
                //                Area = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME,
                //                Plant = shift.TKPI_MASTER_PLANT.TKP_PLANT_NAME,
                //                Shift = shift.TKS_SHIFT_NAME
                //            };
                //            template.ExportResultList.Add(Data);
                //        }
                //        else
                //        {
                //            var ListUser = allUser.Where(x => x.TKS_ID == shift.TKS_ID).ToList();
                //            foreach (var user in ListUser)
                //            {
                //                var Data = new ExportDataResult();
                //                Data = new ExportDataResult
                //                {
                //                    NO = i++,
                //                    DepartMent = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME.ToString(),
                //                    Area = shift.TKPI_MASTER_PLANT.TKPI_MASTER_AREA.TKA_AREA_NAME.ToString(),
                //                    Plant = shift.TKPI_MASTER_PLANT.TKP_PLANT_NAME.ToString(),
                //                    Shift = shift.TKS_SHIFT_NAME.ToString(),
                //                    EmpID = user.TKU_USER_CODE,
                //                    FirstName = user.TKU_FIRST_NAME,
                //                    LastName = user.TKU_LAST_NAME
                //                };
                //                template.ExportResultList.Add(Data);
                //            }
                //        }
                //    }
                //}
            }
            return exportTemplate;
        }

        static T Castvalue<T>(object text, string datetimeFormat = "")
        {
            try
            {
                if (typeof(string) == typeof(T))
                {
                    return (T)text;
                }
                else if (typeof(DateTime) == typeof(T))
                {
                    if (text.GetType() == typeof(string))
                    {
                        var dd = new string[4] { "dd/MM/yyyy", "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy" };
                        return (T)(object)DateTime.ParseExact((string)text, dd, new CultureInfo("en-US"), DateTimeStyles.None);
                    }
                    else if (text.GetType() == typeof(DateTime))
                    {
                        return (T)text;
                    }
                    else
                    {
                        return (T)(object)DateTime.FromOADate((double)text);
                    }
                }
                else if (typeof(decimal?) == typeof(T))
                {
                    var result = 0m;
                    if (decimal.TryParse(text?.ToString(), out result))
                        return (T)(object)result;
                    else
                        return default(T);
                }
                else if (typeof(decimal) == typeof(T))
                {
                    var result = 0m;
                    return (T)(object)decimal.Parse(text?.ToString());
                }
                else if (typeof(int?) == typeof(T))
                {
                    var result = 0;
                    if (int.TryParse(text?.ToString(), out result))
                        return (T)(object)result;
                    else
                        return default(T);
                }
                else
                {
                    return default(T);
                }
            }
            catch
            {
                return default(T);
            }
        }

        #region SearchKPIDataViewModel

        public ReturnValue DeleteKpiData(string pRecId)
        {
            try
            {
                unitOfWork.KpiDataRepository.Delete(pRecId);
                unitOfWork.Commit();
                return new ReturnValue { Message = "Delete Data Success", Status = true };
            }
            catch (Exception ex)
            {
                return new ReturnValue { Message = ex.Message, Status = false };

            }
        }
        public ReturnValue UpdateKpiData(string pRecId, string pAction, string pNote, string pKpiDate, string pScore, string pYearToScore)
        {
            try
            {
                var data = unitOfWork.KpiDataRepository.GetById(pRecId);
                data.TKID_NOTE = pNote;
                data.TKID_KPI_DATE = DateTime.ParseExact(pKpiDate, "dd/MM/yyyy", new CultureInfo("en-US"));
                data.TKID_ACTUAL_SCORE = decimal.Parse(pScore);
                if (pYearToScore.IsNullOrEmpty())
                    data.TKID_YEAR_TO_DATE = null;
                else
                    data.TKID_YEAR_TO_DATE = decimal.Parse(pYearToScore);
                data.TKID_UPDATED_BY = "System";
                data.TKID_UPDATED_DATE = DateTime.Now;
                data.TKID_TRANFROM_TYPE = "MANUAL";
                unitOfWork.KpiDataRepository.Update(data);
                unitOfWork.Commit();
                return new ReturnValue { Message = "Update Data Success", Status = true };
            }
            catch (Exception ex)
            {
                return new ReturnValue { Message = ex.Message, Status = false };

            }
        }


        public TKPI_INDICATORS CheckIndicator(string kpiID, string kPIdate)
        {
            try
            {
                var date = DateTime.ParseExact(kPIdate, "dd/MM/yyyy", new CultureInfo("en-US"));
                var kpi = unitOfWork.IndicatorRepository.GetAll(x => x.TKIN_ID == kpiID);
                var indicator = kpi.FirstOrDefault(x => x.TKI_INDI_TO <= date && date >= x.TKI_INDI_FROM);
                return indicator;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public ReturnValue AddKpiData(string pData, string pKpiDate)
        {
            try
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<TKPI_KPI_DATA>(pData);
                var user = new TKPI_USER();
                if (data.TKID_TYPE?.ToUpper() == "INDIVIDUAL")
                {
                    user = unitOfWork.UserRepository.GetById(data.TKID_EMP_ID);
                    data.TKID_EMP_ID = user.TKU_USER_CODE;
                    data.TKID_FIRST_NAME = user.TKU_FIRST_NAME;
                    data.TKID_LAST_NAME = user.TKU_LAST_NAME;
                }
                data.TKID_KPI_DATE = DateTime.ParseExact(pKpiDate, "dd/MM/yyyy", new CultureInfo("en-US"));
                data.TKID_UPDATED_BY = "System";
                data.TKID_UPDATED_DATE = DateTime.Now;
                data.TKID_CREATED_BY = "System";
                data.TKID_CREATED_DATE = DateTime.Now;
                data.TKID_TRANFROM_TYPE = "MANUAL";
                data.TKID_ID = Guid.NewGuid().ToString().Replace("-", "");
                data.TKID_DEPARTMENT = unitOfWork.DepartmentRepository.GetById(data.TKID_DEPARTMENT).TKD_DEPARTMENT_NAME;
                data.TKID_AREA = unitOfWork.AreaRepository.GetById(data.TKID_AREA).TKA_AREA_NAME;
                data.TKID_PLANT = unitOfWork.PlantRepository.GetById(data.TKID_PLANT).TKP_PLANT_NAME;
                data.TKID_SHIFT = unitOfWork.ShiftRepository.GetById(data.TKID_SHIFT).TKS_SHIFT_NAME;
                var checkUpdateData = unitOfWork.KpiDataRepository.GetAll(x =>
                   data.TKID_DEPARTMENT == x.TKID_DEPARTMENT &&
                   data.TKID_AREA == x.TKID_AREA &&
                   data.TKID_PLANT == x.TKID_PLANT &&
                   data.TKID_SHIFT == x.TKID_SHIFT &&
                   data.TKI_ID == x.TKI_ID &&
                   data.TKID_EMP_ID == x.TKID_EMP_ID
                    ).FirstOrDefault();
                if (checkUpdateData == null)
                {
                    unitOfWork.KpiDataRepository.Insert(data);
                    unitOfWork.Commit();
                    return new ReturnValue { Message = "Add Data Success", Status = true };
                }
                else
                {
                    checkUpdateData.TKID_NOTE = data.TKID_NOTE;
                    checkUpdateData.TKID_ACTUAL_SCORE = data.TKID_ACTUAL_SCORE;
                    checkUpdateData.TKID_YEAR_TO_DATE = data.TKID_YEAR_TO_DATE;
                    checkUpdateData.TKID_UPDATED_DATE = data.TKID_UPDATED_DATE;
                    checkUpdateData.TKID_UPDATED_BY = data.TKID_UPDATED_BY;
                    checkUpdateData.TKID_KPI_DATE = data.TKID_KPI_DATE;
                    unitOfWork.KpiDataRepository.Update(checkUpdateData);
                    unitOfWork.Commit();
                    return new ReturnValue { Message = "Update Data Success", Status = true };
                }
            }
            catch (Exception ex)
            {
                return new ReturnValue { Message = ex.Message, Status = false };

            }
        }
        public ManageKPIData GetManageKPIData(string pRecId, string pAction)
        {
            if (pAction == "Add")
            {
                var userServiceModel = new UsersServiceModel();
                var SetDropDownListServiceModel = new SetDropDownList();

                var model = new ManageKPIData();
                model.ddDepartment = userServiceModel.GetDepartmentCode();
                model.ddKPIIndicator = SetDropDownListServiceModel.GetKPI();
                model.KPIDate = DateTime.Now;
                return model;
            }
            else
            {
                var rec = unitOfWork.KpiDataRepository.GetById(pRecId);
                return new ManageKPIData(
                    rec.TKID_ID,
                    rec.TKID_DEPARTMENT,
                    rec.TKID_AREA,
                    rec.TKID_PLANT,
                    rec.TKID_SHIFT,
                    "KPI-" + rec.TKPI_INDICATORS.TKPI_INDICATORS_NAME.TKIN_NO + ":" + rec.TKPI_INDICATORS.TKPI_INDICATORS_NAME.TKIN_NAME,
                    rec.TKID_KPI_DATE,
                    rec.TKID_ACTUAL_SCORE.ToString(),
                    rec.TKID_YEAR_TO_DATE.ToString(),
                    rec.TKID_TYPE, rec.TKID_FIRST_NAME,
                    rec.TKID_LAST_NAME, rec.TKID_NOTE);
            }
        }
        public void GetSearchResult(SearchKPIDataViewModel Model)
        {
            var allkpi = unitOfWork.KpiNameRepository.GetAll();
            var allPlant = unitOfWork.PlantRepository.GetAll();
            var allArea = unitOfWork.AreaRepository.GetAll();
            var allDepartment = unitOfWork.DepartmentRepository.GetAll();

            var KPIdata = unitOfWork.KpiDataRepository.GetAll(x =>
            (x.TKPI_INDICATORS.TKPI_INDICATORS_NAME.TKIN_ID == Model.Criteria.KPIIndicator || Model.Criteria.KPIIndicator == null) &&
            (x.TKID_KPI_DATE >= Model.Criteria.PeriodFromDate || Model.Criteria.PeriodFromDate == null) &&
            (x.TKID_KPI_DATE <= Model.Criteria.PeriodToDate || Model.Criteria.PeriodToDate == null)
            ).OrderBy(d => d.TKID_KPI_DATE).ToList();
            if (!Model.Criteria.FirstName.IsNullOrEmpty())
                KPIdata = KPIdata.Where(x => (x.TKID_FIRST_NAME?.ToUpper().Contains(Model.Criteria.FirstName?.ToUpper()) ?? false)).ToList();
            if (!Model.Criteria.LastName.IsNullOrEmpty())
                KPIdata = KPIdata.Where(x => (x.TKID_LAST_NAME?.ToUpper().Contains(Model.Criteria.LastName?.ToUpper()) ?? false)).ToList();

            var group = KPIdata.GroupBy(x => new { x.TKID_KPI_DATE.Month, x.TKID_KPI_DATE.Year, x.TKID_DEPARTMENT, x.TKI_ID });
            KPIdata.GroupBy(x => x.TKID_DEPARTMENT).ToList().ForEach(
                 dept =>
                 {
                     SearchResult result = new SearchResult();
                     Model.Result.Add(result);
                     result.DepartMent = dept.Key;
                     result.DepartMentID = allDepartment.FirstOrDefault(x => x.TKD_DEPARTMENT_NAME == dept.Key).TKD_ID;
                     dept.GroupBy(x => new { x.TKID_KPI_DATE.Month, x.TKID_KPI_DATE.Year }).ToList().ForEach(
                         mon =>
                         {
                             MonthList month = new MonthList();
                             result.MonthList.Add(month);
                             month.KPIDate = new DateTime(mon.Key.Year, mon.Key.Month, 1);
                             month.KPIDateID = result.DepartMentID + month.KPIDate.ToString("_MM_yyyy");
                             mon.GroupBy(x => x.TKI_ID).ToList().ForEach(
                                 indi =>
                                 {
                                     KPIList KPIList = new KPIList();
                                     month.KPIList.Add(KPIList);
                                     var i = 1;
                                     indi.ToList().ForEach(kpi =>
                                     {
                                         var kpiname = kpi.TKPI_INDICATORS.TKPI_INDICATORS_NAME;
                                         KPIList.KPIName = "KPI-" + kpiname.TKIN_NO + " : " + kpiname.TKIN_NAME;
                                         KPIList.KPIid = kpiname.TKIN_NO + "_" + month.KPIDateID;
                                         KPIList.KPINo = kpiname.TKIN_NO;
                                         KPIList.IsIndividual = kpi.TKPI_INDICATORS.TKI_INDI_TYPE.ToUpper() == "INDIVIDUAL";
                                         var department = allDepartment.FirstOrDefault(x => x.TKD_DEPARTMENT_NAME == kpi.TKID_DEPARTMENT);
                                         var area = department?.TKPI_MASTER_AREA.FirstOrDefault(x => x.TKA_AREA_NAME == kpi.TKID_AREA);
                                         var plant = area?.TKPI_MASTER_PLANT.FirstOrDefault(x => x.TKP_PLANT_NAME == kpi.TKID_PLANT);
                                         var shift = plant?.TKPI_MASTER_SHIFT.FirstOrDefault(x => x.TKS_SHIFT_NAME == kpi.TKID_SHIFT);

                                         KPIList.Detail.Add(new KPIDetail(
                                             i++,
                                             kpi.TKID_ID,
                                             kpi.TKID_AREA,
                                             kpi.TKID_SHIFT,
                                             kpi.TKID_PLANT,
                                             area?.TKA_ID,
                                             shift?.TKS_ID,
                                             plant?.TKP_ID,
                                             kpi.TKID_FIRST_NAME + " " + kpi.TKID_LAST_NAME,
                                             kpi.TKID_ACTUAL_SCORE.ToString(),
                                             kpi.TKID_YEAR_TO_DATE?.ToString(),
                                              "No Scale",
                                              "No Scale",
                                             kpi.TKID_CREATED_BY,
                                             kpi.TKID_CREATED_DATE,
                                             kpi.TKID_KPI_DATE,
                                             kpi.TKID_NOTE));
                                     });
                                     KPIList.Detail.OrderBy(x => x.Area).ThenBy(x => x.Plant).ThenBy(x => x.Shift);
                                 });
                         });
                 });
        }
        public void SetSearchCriteria(SearchKPIDataViewModel Model)
        {
            var setDropDownList = new SetDropDownList();
            Model.Criteria.ddKPIIndicator = setDropDownList.GetKPI();
        }
        #endregion
        #region ExportImportDataViewModel
        public void GetImportFileHistory(ExportImportDataViewModel Model)
        {
            SearchResult result = new SearchResult();
            foreach (var file in unitOfWork.KpiDataFileRepository.GetAll())
            {
                Model.ImportFileDetail.Add(new ImportFileHistory(1, file.TKDF_KPI, file.TKDF_FILE_NAME, file.TKDF_NOTE, file.TKDF_CREATED_BY, file.TKDF_CREATED_DATE ?? DateTime.Now));
            }
            Model.ImportFileDetail = Model.ImportFileDetail.OrderByDescending(x => x.CreateDate).ToList();
        }
        public void SetExportCriteria(ExportImportDataViewModel Model)
        {
            var setDropDownList = new SetDropDownList();
            Model.ExportCriteria.ddKPIIndicator = setDropDownList.GetKPI();
            Model.ExportCriteria.ddDepartment = setDropDownList.GetDeppaetment();
        }
        #endregion
        public List<SelectListItem> Getuser(string shift)
        {
            var userlist = unitOfWork.UserRepository.GetAll(x => x.TKS_ID == shift);
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Value = "", Text = "-- กรุณาเลือก --" });
            foreach (var user in userlist)
            {
                list.Add(new SelectListItem { Value = user.TKU_ID, Text = user.TKU_FIRST_NAME + " " + user.TKU_LAST_NAME });
            }
            return list;
        }
    }
}
