﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class KPI
    {
        public int no { get; set; }
        public string headerKPI { get; set; }
        public List<GroupPosition> groupOne { get; set; }
        public List<GroupPosition> groupTwo { get; set; }

    }

    public class KPIList
    {
        public KPIList()
        {
            var test = new List<GroupPosition>
            {
                new GroupPosition(){ positionName = "TOC-1"},
                 new GroupPosition(){ positionName = "TOC-3"},
                  new GroupPosition(){ positionName = "TOC-4"},
                   new GroupPosition(){ positionName = "TOC-5"},
                    new GroupPosition(){ positionName = "TOC-6"},
                     new GroupPosition(){ positionName = "TOC-7"},
                      new GroupPosition(){ positionName = "TOC-8"},
                       new GroupPosition(){ positionName = "TOC-9"},


            };

            var test2 = new List<GroupPosition>
            {
                new GroupPosition(){ positionName = "AA-1"},
                 new GroupPosition(){ positionName = "AA-3"}


            };

            kpiList = new List<KPI>
            {
               new KPI(){ headerKPI = "1", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "2", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "3", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "4", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "5", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "6", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "7", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "8", groupOne = test , groupTwo = test2},
               new KPI(){ headerKPI = "9", groupOne = test , groupTwo = test2},

            };
        }
        public List<KPI> kpiList { get; set; }


    } 
}