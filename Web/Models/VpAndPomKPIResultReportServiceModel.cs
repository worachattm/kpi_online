﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.VpAndPomKPIResultReportViewModel;

namespace KPI_ONLINE.Models
{
    public class VpAndPomKPIResultReportServiceModel : BaseClass
    {

        public void GetSearchResult(VpAndPomKPIResultReportViewModel Model)
        {
            using (MFKPIEntities context = new MFKPIEntities())
            {
                using (context.Database.Connection)
                {
                    context.Database.Connection.Open();
                    DbCommand cmd = context.Database.Connection.CreateCommand();
                    cmd.CommandText = "sp_vp_and_pom";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new OracleParameter("DEPARTMENT", ""));
                    cmd.Parameters.Add(new OracleParameter("TYPE", Model.Criteria.PeriodType));
                    cmd.Parameters.Add(new OracleParameter("YEAR", Model.Criteria.Year));
                    cmd.Parameters.Add(new OracleParameter("PERIOD", Model.Criteria.TargetPeriod));
                    cmd.Parameters.Add(new OracleParameter("MODE_DEPARTMENT_OR_AREA", Model.Criteria.ReportMode));
                    OracleParameter oraP = new OracleParameter();
                    oraP.ParameterName = "p_recordset";
                    oraP.OracleDbType = OracleDbType.RefCursor;
                    oraP.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(oraP);
                    using (var reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            var dd = reader.GetSchemaTable();
                            Model.Result.data = reader.MapToList<sp_vp_and_pom>() ?? new List<sp_vp_and_pom>();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            if (Model.Criteria.ReportMode == "Department")
            {
                Model.Result.HeaderList = unitOfWork.DepartmentRepository.GetAll().Select(x => x.TKD_DEPARTMENT_NAME).Where(x => Const.User.AuthDepartmentNameLs.Contains(x)).ToList();
            }
            else
            {
                Model.Result.HeaderList = unitOfWork.AreaRepository.GetAll().Select(x => x.TKA_AREA_NAME).Distinct().Where(x => Const.User.AuthAreaNameLs.Contains(x)).ToList();
            }
            var allkpi = unitOfWork.KpiNameRepository.GetAll();
            Model.Result.data = Model.Result.data.OrderBy(x => x.TKI_INDI_NO).ToList();
            Model.Result.data.ForEach(x =>
            {
                var kpiname = allkpi.FirstOrDefault(y => y.TKIN_NO == x.TKI_INDI_NO);
                x.TKI_INDI_CAL_MODE = kpiname?.TKIN_NAME;
                x.TKI_INDI_ID = kpiname?.TKIN_ID;
            });
            Model.Result.data = Model.Result.data.Where(x => Const.User.AuthKpiIdLs.Contains(x.TKI_INDI_ID)).ToList();
            Model.IndicatorNameList = IndicatorModel.GetIndicatorName();
        }
    }
}