﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.TKPI_INDICATOR_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;

namespace KPI_ONLINE.Models
{
    public class IndicatorServiceModel : BaseClass
    {
        public ReturnValue Delete(string Id, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                unitOfWork.IndicatorRepository.Delete(Id);
                unitOfWork.Commit();

                rtn.Message = ConstantPrm.MessageSuccess;
                rtn.Status = true;

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }



        public List<SelectListItem> GetNameKpi()
        {

            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.KpiNameRepository.GetAll(c => c.TKIN_STATUS == "ACTIVE" && Const.User.AuthKpiIdLs.Contains(c.TKIN_ID)).ToList();

                if (query != null)
                {

                    foreach (var item in query.OrderBy(p => p.TKIN_NO))
                    {
                        rtn.Add(new SelectListItem { Value = item.TKIN_ID.ToString(), Text = "KPI-" + item.TKIN_NO + ":" + item.TKIN_NAME });
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<IndicatorName> GetKpi()
        {
            var result = new List<IndicatorName>();
            try
            {
                result = unitOfWork.KpiNameRepository.GetAll(c => Const.User.AuthKpiIdLs.Contains(c.TKIN_ID)).Select(c => new IndicatorName { Id = c.TKIN_ID , Name = c.TKIN_NAME , No = c.TKIN_NO , Status = c.TKIN_STATUS , File = c.TKIN_FILE , FileName = c.TKIN_FILE_NAME }).OrderBy(c => c.No).ToList();
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);

            }

            return result;
        }

        public List<SelectListItem> GetYearKpi()
        {

            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.IndicatorRepository.GetAll(c => Const.User.AuthKpiIdLs.Contains(c.TKIN_ID)).GroupBy(c => c.TKI_INDI_YEAR).Select(c => c.First()).OrderByDescending(c => c.TKI_INDI_YEAR).Select(c => c.TKI_INDI_YEAR).ToList();

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.ToString(), Text = item.ToString() });
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public ReturnValue SearchResult(ref SearchIndicatorsCriteriaViewModel Model)
        {
            var rtn = new ReturnValue();


            try
            {
                var query = unitOfWork.IndicatorRepository.GetAll(c => c.TKI_INDI_STATUS == "ACTIVE").OrderByDescending(c => c.TKI_INDI_YEAR).ThenByDescending(c => c.TKI_INDI_NO).Select(item => new IndicatorModel() {
                    CalculateMode = item.TKI_INDI_CAL_MODE.ToUpper(),
                    Id = item.TKI_ID,
                    IndicatorNameId = item.TKIN_ID,
                    Name = item.TKI_INDI_NAME,
                    No = item.TKI_INDI_NO.ToString(),
                    PeriodFrom = item.TKI_INDI_FROM.ToString("MMMM-yyyy", new CultureInfo("en-Us")),
                    PeriodTo = item.TKI_INDI_TO.ToString("MMMM-yyyy", new CultureInfo("en-Us")),
                    Type = item.TKI_INDI_TYPE.ToUpper(),
                    Year = item.TKI_INDI_YEAR.ToString(),
                    HaveDelete = item.TKPI_KPI_DATA.Count() == 0,
                    KPIScaleList = new List<KPIScale>()
                    {
                        new KPIScale(){ Scale = 1 , FromValue = item.TKI_INDI_1_FROM_VAL , ToValue = item.TKI_INDI_1_TO_VAL , DisplayName = item.TKI_INDI_1_DISPLAY },
                        new KPIScale(){ Scale = 2 , FromValue = item.TKI_INDI_2_FROM_VAL , ToValue = item.TKI_INDI_2_TO_VAL , DisplayName = item.TKI_INDI_2_DISPLAY },
                        new KPIScale(){ Scale = 3 , FromValue = item.TKI_INDI_3_FROM_VAL , ToValue = item.TKI_INDI_3_TO_VAL , DisplayName = item.TKI_INDI_3_DISPLAY },
                        new KPIScale(){ Scale = 4 , FromValue = item.TKI_INDI_4_FROM_VAL , ToValue = item.TKI_INDI_4_TO_VAL , DisplayName = item.TKI_INDI_4_DISPLAY },
                        new KPIScale(){ Scale = 5 , FromValue = item.TKI_INDI_5_FROM_VAL , ToValue = item.TKI_INDI_5_TO_VAL , DisplayName = item.TKI_INDI_5_DISPLAY },
                    }
                   
                }).ToList();
                

                var indicator = Model.Indicator?.Trim();
                var type = Model.Type?.Trim().ToUpper();
                var year = Model.Year?.Trim().ToUpper();
                var calculateMode = Model.CalculateMode?.Trim().ToUpper();
                if (!string.IsNullOrWhiteSpace(Model.Indicator))
                {
                    query = query.Where(c => c.IndicatorNameId == (indicator)).ToList();
                }

                if (!string.IsNullOrWhiteSpace(Model.CalculateMode))
                {
                    query = query.Where(c => c.CalculateMode.Trim().ToUpper() == calculateMode).ToList();
                }

                if (!string.IsNullOrWhiteSpace(Model.Year))
                {
                    query = query.Where(c => c.Year == (year)).ToList();
                }

                if (!string.IsNullOrWhiteSpace(Model.Type))
                {
                    query = query.Where(c => c.Type.Trim().ToUpper() == type).ToList();
                }

                Model.SearchResult = query;


                rtn.Status = true;
                rtn.Message = ConstantPrm.MessageSuccess;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public TKPI_INDICATORS Get(string Id)
        {
            var result = new TKPI_INDICATORS();

            try
            {
                result = unitOfWork.IndicatorRepository.GetById(Id);
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);

            }

            return result; 
        }

        public ReturnValue CreateOrEditIndicatorName(IndicatorName pModel, string User)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                var dal = new TKPI_INDICATOR_DAL();
                var ent = new TKPI_INDICATORS_NAME();
                var now = DateTime.Now;

                try
                {
                    if (!string.IsNullOrWhiteSpace(pModel.Id))
                    {
                        ent = unitOfWork.KpiNameRepository.GetById(pModel.Id);
                    }
                    ent.TKIN_NO = pModel.No;
                    ent.TKIN_NAME = pModel.Name;
                    ent.TKIN_STATUS = pModel.Status;

                    if (!string.IsNullOrWhiteSpace(pModel.File))
                    {
                        ent.TKIN_FILE = pModel.File;
                        ent.TKIN_FILE_NAME = pModel.FileName;
                    }
                    
                    ent.TKIN_UPDATED_BY = User;
                    ent.TKIN_UPDATED_DATE = now;

                    if (!string.IsNullOrWhiteSpace(pModel.Id))
                    {
                        dal.UpdateName(ent , ref unitOfWork);
                        
                    } else
                    {
                        ent.TKIN_CREATED_BY = User;
                        ent.TKIN_CREATED_DATE = now;
                        dal.SaveName(ent , ref unitOfWork);
                    }

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, User, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, User, ex.Message);
            }

            return rtn;
        }

        public ReturnValue Add(IndicatorsManageViewModel pModel, string User)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                var dal = new TKPI_INDICATOR_DAL();
                var ent = new TKPI_INDICATORS();
                var now = DateTime.Now;

                try
                {
                    var indicator = unitOfWork.KpiNameRepository.GetById((pModel.IndicatorNameId));
                    ent.TKIN_ID = (pModel.IndicatorNameId);
                    ent.TKI_INDI_NO = indicator.TKIN_NO;
                    ent.TKI_INDI_NAME = indicator.TKIN_NAME;
                    ent.TKI_INDI_YEAR = DateTime.ParseExact(pModel.To, "MMMM-yyyy", new CultureInfo("en-Us")).Year;
                    ent.TKI_INDI_TYPE = pModel.Type;
                    //ent.TKI_INDI_CATEGORY = pModel.Category;
                    ent.TKI_INDI_CAL_MODE = pModel.CalculateMode;
                    ent.TKI_INDI_FROM = DateTime.ParseExact(pModel.From, "MMMM-yyyy", new CultureInfo("en-Us"));
                    ent.TKI_INDI_TO = DateTime.ParseExact(pModel.To, "MMMM-yyyy", new CultureInfo("en-Us"));
                    pModel.KPIScaleList.ForEach(scale =>
                    {
                        if (scale.Scale == 1)
                        {
                            ent.TKI_INDI_1_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_1_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_1_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_1_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 2)
                        {
                            ent.TKI_INDI_2_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_2_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_2_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_2_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 3)
                        {
                            ent.TKI_INDI_3_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_3_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_3_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_3_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 4)
                        {
                            ent.TKI_INDI_4_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_4_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_4_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_4_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 5)
                        {
                            ent.TKI_INDI_5_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_5_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_5_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_5_TO_VAL = scale.ToValue;
                        }

                    });

                    ent.TKI_INDI_STATUS = "ACTIVE";

                    ent.TKI_INDI_CREATED_BY = User;
                    ent.TKI_INDI_CREATED_DATE = now;
                    ent.TKI_INDI_UPDATED_BY = User;
                    ent.TKI_INDI_UPDATED_DATE = now;


                    dal.Save(ent, ref unitOfWork);

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, User, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, User, ex.Message);
            }

            return rtn;
        }
        public ReturnValue EditScale(List<KPIScale> scales, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                var dal = new TKPI_INDICATOR_DAL();

                DateTime now = DateTime.Now;

                try
                {
                    scales.ForEach(scale =>
                    {
                        var ent = unitOfWork.IndicatorScaleRepository.GetAll(c => c.TKIS_ID == scale.Id).FirstOrDefault();

                        if (ent != null)
                        {
                            ent.TKIS_INDI_SCALE_COLOR = scale.DisplayColor;
                            ent.TKIS_INDI_SCALE_CODE = scale.CodeColor;
                            ent.TKIS_UPDATED_BY = pUser;
                            ent.TKIS_UPDATED_DATE = now;

                            dal.UpdateScale(ent, ref unitOfWork);
                        }
                    });
                    

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }


        public ReturnValue Edit(IndicatorsManageViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                var dal = new TKPI_INDICATOR_DAL();

                DateTime now = DateTime.Now;

                try
                {
                    var indicator = unitOfWork.KpiNameRepository.GetById((pModel.IndicatorNameId));
                    var ent = unitOfWork.IndicatorRepository.GetById(pModel.Id);
                    ent.TKIN_ID = (pModel.IndicatorNameId);
                    ent.TKI_INDI_NO = indicator.TKIN_NO;
                    ent.TKI_INDI_NAME = indicator.TKIN_NAME;
                    ent.TKI_INDI_YEAR = DateTime.ParseExact(pModel.To, "MMMM-yyyy", new CultureInfo("en-Us")).Year;
                    ent.TKI_INDI_TYPE = pModel.Type;
                    ent.TKI_INDI_CAL_MODE = pModel.CalculateMode;
                    ent.TKI_INDI_FROM = DateTime.ParseExact(pModel.From, "MMMM-yyyy", new CultureInfo("en-Us"));
                    ent.TKI_INDI_TO = DateTime.ParseExact(pModel.To, "MMMM-yyyy", new CultureInfo("en-Us"));
                    pModel.KPIScaleList.ForEach(scale =>
                    {
                        if (scale.Scale == 1)
                        {
                            ent.TKI_INDI_1_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_1_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_1_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_1_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 2)
                        {
                            ent.TKI_INDI_2_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_2_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_2_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_2_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 3)
                        {
                            ent.TKI_INDI_3_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_3_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_3_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_3_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 4)
                        {
                            ent.TKI_INDI_4_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_4_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_4_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_4_TO_VAL = scale.ToValue;
                        }
                        else if (scale.Scale == 5)
                        {
                            ent.TKI_INDI_5_COLOR = scale.DisplayColor;
                            ent.TKI_INDI_5_DISPLAY = scale.DisplayName;
                            ent.TKI_INDI_5_FROM_VAL = scale.FromValue;
                            ent.TKI_INDI_5_TO_VAL = scale.ToValue;
                        }

                    });

                    ent.TKI_INDI_STATUS = "ACTIVE";

                    ent.TKI_INDI_UPDATED_BY = pUser;
                    ent.TKI_INDI_UPDATED_DATE = now;

                    dal.Update(ent, ref unitOfWork);

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }
    }
}