﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.TKPI_ROLE_DAL;
using KPI_ONLINE.DAL.TKPI_ROLE_MENU_DAL;
using KPI_ONLINE.DAL.TKPI_USER_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;

namespace KPI_ONLINE.Models
{
    public class RoleServiceModel : BaseClass
    {

        private readonly string SubMenuFormat = "{{ \"text\": \"   {0}\" " +
                               ", \"color\": \"#428bca\" " +
                               ", \"backColor\": \"{1}\" " +
                               ", \"href\": \"#node-{2}\" " +
                                ",\"state\": {{ " +
                                " \"expanded\": \"true\" " +
                                " {3} " +
                                "}} " +
                               ", \"nodes\": {4} " +
                               "}}";

        private readonly string ParentMenuFormat = "{{ \"text\": \"  {0}\" " +
                                ",\"color\": \"#428bca\" " +
                                ",\"backColor\": \"{1}\" " +
                                ",\"href\": \"#node-{2}\" " +
                                ",\"selectable\": \"false\" " +
                                ",\"state\": {{ " +
                                " \"expanded\": \"true\" " +
                                " {3} " +
                                "}} " +
                                ",\"nodes\": {4} " +
                                "}}";
        private readonly string Selectall = "{\"text\":\"Select All\",\"color\":\"#428bca\",\"backColor\":\"\",\"href\":\"#node-select-all\",\"state\":{\"expanded\":\"true\",\"checked\":\"true\"},\"nodes\":null},";

        public ReturnValue Search(ref RoleViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                var sName = String.IsNullOrEmpty(pModel.sRoleName) ? "" : pModel.sRoleName.Trim();
                var sDesc = String.IsNullOrEmpty(pModel.sRoleDescription) ? "" : pModel.sRoleDescription.Trim();
                var sStatus = String.IsNullOrEmpty(pModel.sRoleStatus) ? "" : pModel.sRoleStatus.Trim();

                var query = unitOfWork.RoleRepository.GetAll().Select(v => new RoleViewModel_SeachData
                {
                    dRolID = v.TKR_ID,
                    dRolName = v.TKR_NAME,
                    dRolDesc = v.TKR_DESCRIPTION,
                    dRolStatus = v.TKR_STATUS,
                    HaveDelete = unitOfWork.UserRepository.GetAll(c => c.TKR_ID == v.TKR_ID).Count() == 0
                });

                if (!string.IsNullOrEmpty(sName))
                    query = query.Where(p => p.dRolID == (sName));

                if (!string.IsNullOrEmpty(sDesc))
                    query = query.Where(p => p.dRolID == (sDesc));

                if (!string.IsNullOrEmpty(sStatus))
                    query = query.Where(p => p.dRolStatus.ToUpper().Contains(sStatus.Trim().ToUpper()));

                if (query != null)
                {
                    pModel.sSearchData = new List<RoleViewModel_SeachData>();
                    foreach (var g in query)
                    {
                        pModel.sSearchData.Add(
                                                new RoleViewModel_SeachData
                                                {
                                                    dRolID = g.dRolID,
                                                    dRolName = g.dRolName,
                                                    dRolDesc = g.dRolDesc,
                                                    dRolStatus = g.dRolStatus,
                                                    HaveDelete = g.HaveDelete
                                                });
                    }
                    rtn.Status = true;
                    rtn.Message = ConstantPrm.MessageSuccess;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data is null";
                }
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Add(RoleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                TKPI_USERS_DAL userDAL = new TKPI_USERS_DAL();
                TKPI_ROLE_DAL dal = new TKPI_ROLE_DAL();
                TKPI_ROLE ent = new TKPI_ROLE();
                TKPI_USER userEnt = new TKPI_USER();

                TKPI_ROLE_MENU_DAL dalCtr = new TKPI_ROLE_MENU_DAL();
                TKPI_ROLE_INDICATORS roleIndicatorCtr = new TKPI_ROLE_INDICATORS();
                TKPI_ROLE_MENU entCtr = new TKPI_ROLE_MENU();
                TKPI_ROLE_DATA roleDataCtr = new TKPI_ROLE_DATA();

                DateTime now = DateTime.Now;

                try
                {
                    //var roleCode = ShareFn.GenerateCodeByDate("R");
                    //ent.TKR_ID = roleCode;
                    ent.TKR_NAME = pModel.RolName.Trim();
                    ent.TKR_STATUS = pModel.RolStatus; //CPAIConstantUtil.ACTIVE;
                    ent.TKR_DESCRIPTION = pModel.RolDesc.Trim();

                    ent.TKR_CREATED_BY = pUser;
                    ent.TKR_CREATED_DATE = now;
                    ent.TKR_UPDATED_BY = pUser;
                    ent.TKR_UPDATED_DATE = now;

                    ent.TKPI_ROLE_MENU = new List<TKPI_ROLE_MENU>();
                    ent.TKPI_ROLE_INDICATORS = new List<TKPI_ROLE_INDICATORS>();
                    ent.TKPI_ROLE_DATA = new List<TKPI_ROLE_DATA>();

                    dal.Save(ent, ref unitOfWork);

                    if (pModel.UserInRoleList != null)
                    {
                        pModel.UserInRoleList.ForEach(user =>
                        {
                            userEnt = new TKPI_USER()
                            {
                                TKU_ID = user.EmpId,
                                TKR_ID = ent.TKR_ID,
                                TKU_UPDATED_BY = pUser,
                                TKU_UPDATED_DATE = now
                            };
                            userDAL.Update(userEnt, ref unitOfWork);
                        });
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.MenuTreeId))
                    {
                        foreach (var item in pModel.MenuTreeId.Split('|').ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(item))
                            {
                                entCtr = new TKPI_ROLE_MENU()
                                {
                                    TKRM_MENU = (item.Replace("&", "")).Replace("&", ""),
                                    TKR_ID = ent.TKR_ID,
                                    TKRM_CREATED_BY = pUser,
                                    TKRM_CREATED_DATE = now,
                                    TKRM_UPDATED_BY = pUser,
                                    TKRM_UPDATED_DATE = now
                                };

                                dalCtr.Save(entCtr, ref unitOfWork);
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.DepartmentTreeId))
                    {
                        foreach (var item in pModel.DepartmentTreeId.Split('|').ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(item))
                            {
                                var idShift = ((item.Replace("&", "")).Replace("&", ""));
                                var idPlant = unitOfWork.ShiftRepository.GetAll(c => c.TKS_ID == idShift).FirstOrDefault().TKP_ID;
                                var idArea = unitOfWork.PlantRepository.GetAll(c => c.TKP_ID == idPlant).FirstOrDefault().TKA_ID;
                                var idDepartment = unitOfWork.AreaRepository.GetAll(c => c.TKA_ID == idArea).FirstOrDefault().TKD_ID;
                                roleDataCtr = new TKPI_ROLE_DATA()
                                {
                                    TKS_ID = idShift,
                                    TKR_ID = ent.TKR_ID,
                                    TKP_ID = idPlant,
                                    TKA_ID = idArea,
                                    TKD_ID = idDepartment

                                };
                                dalCtr.SaveRoleData(roleDataCtr, ref unitOfWork);
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.KPITreeId))
                    {
                        foreach (var item in pModel.KPITreeId.Split('|').ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(item) && (item.Replace("&", "")).Replace("&", "") != "9999")
                            {
                                var idIndicator = ((item.Replace("&", "")).Replace("&", ""));

                                roleIndicatorCtr = new TKPI_ROLE_INDICATORS()
                                {
                                    TKR_ID = ent.TKR_ID,
                                    TKRK_INDICATOR_NO = idIndicator

                                };
                                dalCtr.SaveRoleIndicator(roleIndicatorCtr, ref unitOfWork);
                            }
                        }
                    }


                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }

        public ReturnValue Edit(RoleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                TKPI_ROLE_DAL dal = new TKPI_ROLE_DAL();
                TKPI_USERS_DAL userDAL = new TKPI_USERS_DAL();
                TKPI_ROLE ent = new TKPI_ROLE();

                TKPI_ROLE_MENU_DAL dalCtr = new TKPI_ROLE_MENU_DAL();
                TKPI_ROLE_MENU entCtr = new TKPI_ROLE_MENU();

                TKPI_ROLE_INDICATORS roleIndicatorCtr = new TKPI_ROLE_INDICATORS();
                TKPI_ROLE_DATA roleDataCtr = new TKPI_ROLE_DATA();

                TKPI_USER userEnt = new TKPI_USER();

                DateTime now = DateTime.Now;

                try
                {
                    ent.TKR_ID = pModel.RolID;
                    ent.TKR_NAME = pModel.RolName.Trim();
                    ent.TKR_STATUS = pModel.RolStatus;
                    ent.TKR_DESCRIPTION = pModel.RolDesc.Trim();
                    ent.TKR_UPDATED_BY = pUser;
                    ent.TKR_UPDATED_DATE = now;

                    dal.Update(ent, ref unitOfWork);

                    dalCtr.DeleteAll(pModel.RolID, pUser, ref unitOfWork);
                    dalCtr.DeleteKPIAll(pModel.RolID, pUser, ref unitOfWork);
                    dalCtr.DeleteRoleDataAll(pModel.RolID, pUser, ref unitOfWork);

                    var RoleDefaultId = unitOfWork.RoleRepository.GetAll(c => c.TKR_NAME.Contains(ConstantPrm.DefaultRole)).Select(c => c.TKR_ID).FirstOrDefault();
                    var UserInRoleLs = unitOfWork.UserRepository.GetAll(c => c.TKR_ID == pModel.RolID).ToList();

                    if (UserInRoleLs != null)
                    {
                        // OLD ROLE
                        UserInRoleLs.ForEach(user =>
                        {
                            userEnt = new TKPI_USER()
                            {
                                TKU_ID = user.TKU_ID,
                                TKR_ID = RoleDefaultId,
                                TKU_UPDATED_BY = pUser,
                                TKU_UPDATED_DATE = now
                            };
                            userDAL.Update(userEnt, ref unitOfWork);
                        });
                    }

                    if (pModel.UserInRoleList != null)
                    {
                        // NEW ROLE
                        pModel.UserInRoleList.ForEach(user =>
                        {
                            userEnt = new TKPI_USER()
                            {
                                TKU_ID = user.EmpId,
                                TKR_ID = pModel.RolID,
                                TKU_UPDATED_BY = pUser,
                                TKU_UPDATED_DATE = now
                            };
                            userDAL.Update(userEnt, ref unitOfWork);
                        });
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.MenuTreeId))
                    {
                        foreach (var item in pModel.MenuTreeId.Split('|').ToList())
                        {
                            if (item != "")
                            {
                                entCtr = new TKPI_ROLE_MENU()
                                {
                                    TKRM_MENU = (item.Replace("&", "")).Replace("&", ""),
                                    TKR_ID = pModel.RolID,
                                    TKRM_CREATED_BY = pUser,
                                    TKRM_CREATED_DATE = now,
                                    TKRM_UPDATED_BY = pUser,
                                    TKRM_UPDATED_DATE = now
                                };

                                dalCtr.Save(entCtr, ref unitOfWork);
                            }

                        }
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.DepartmentTreeId))
                    {
                        foreach (var item in pModel.DepartmentTreeId.Split('|').Select(c => c.Replace("&", "")).Where(c => !string.IsNullOrWhiteSpace(c)).GroupBy(c => c).Select(c => c.First()).ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(item))
                            {
                                var idShift = (item);
                                var idPlant = unitOfWork.ShiftRepository.GetAll(c => c.TKS_ID == idShift).FirstOrDefault()?.TKP_ID;
                                var idArea = unitOfWork.PlantRepository.GetAll(c => c.TKP_ID == idPlant).FirstOrDefault().TKA_ID;
                                var idDepartment = unitOfWork.AreaRepository.GetAll(c => c.TKA_ID == idArea).FirstOrDefault().TKD_ID;
                                roleDataCtr = new TKPI_ROLE_DATA()
                                {
                                    TKS_ID = idShift,
                                    TKR_ID = ent.TKR_ID,
                                    TKP_ID = idPlant,
                                    TKA_ID = idArea,
                                    TKD_ID = idDepartment

                                };
                                dalCtr.SaveRoleData(roleDataCtr, ref unitOfWork);
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(pModel.KPITreeId))
                    {
                        foreach (var item in pModel.KPITreeId.Split('|').ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(item) && (item.Replace("&", "")).Replace("&", "") != "9999")
                            {
                                var idIndicator = ((item.Replace("&", "")).Replace("&", ""));

                                roleIndicatorCtr = new TKPI_ROLE_INDICATORS()
                                {
                                    TKR_ID = ent.TKR_ID,
                                    TKRK_INDICATOR_NO = idIndicator
                                };
                                dalCtr.SaveRoleIndicator(roleIndicatorCtr, ref unitOfWork);
                            }
                        }
                    }

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }

        public ReturnValue Delete(string RoleId, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                TKPI_ROLE_MENU_DAL dalCtr = new TKPI_ROLE_MENU_DAL();
                dalCtr.DeleteRole(RoleId, pUser, ref unitOfWork);
                unitOfWork.Commit();
                rtn.Message = ConstantPrm.MessageSuccess;
                rtn.Status = true;

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, pUser, ex.Message);
            }

            return rtn;
        }

        private string GetColor(string MenuType)
        {
            if (MenuType == "MENU")
            {
                return "undefined";
            }
            else
            {
                return "#E0E9F8";
            }
        }

        public string GetChecked(string RoleID)
        {
            if (!string.IsNullOrWhiteSpace(RoleID))
            {
                return ",\"checked\": \"true\" ";
            }
            else
            {
                return "";
            }
        }

        public string GetCheckedDepartment(string RoleID, Func<TKPI_ROLE_DATA, bool> expression)
        {
            var isChecked = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == RoleID).Where(expression).FirstOrDefault();
            if (isChecked != null)
            {
                return ",\"checked\": \"true\" ";
            }
            else
            {
                return "";
            }
        }


        public string GetSubMenu(string MenuParentID, List<RoleViewModel_MenuWithRole> menuLst, ref RoleViewModel_Detail model)
        {
            StringBuilder _html = new StringBuilder();

            try
            {
                if (menuLst != null)
                {
                    var SubMenu = menuLst.Where(p => p.dMenuParentID == MenuParentID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                    if (SubMenu.Count > 0)
                    {
                        int kCount = 0;
                        foreach (var k in SubMenu)
                        {
                            if (!string.IsNullOrWhiteSpace(k.dRoleID))
                                model.MenuTreeId += (model.MenuTreeId != "" ? "|&" + k.dMenuID + "&" : "&" + k.dMenuID + "&");

                            var qSubMenu = menuLst.Where(p => p.dMenuParentID == k.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                            //var MenuFormat = !String.IsNullOrEmpty(k.dRoleID) ? SubMenuCheckedFormat : SubMenuFormat;
                            var strMenu = "";
                            if (qSubMenu.Count > 0)
                            {
                                var strSubMenu = GetSubMenu(k.dMenuID, menuLst, ref model);
                                strMenu = String.Format(SubMenuFormat, k.dMenuDesc, GetColor(k.dMenuType), k.dMenuID, GetChecked(k.dRoleID), "[" + strSubMenu + "]");
                            }
                            else
                            {
                                strMenu = String.Format(ParentMenuFormat, k.dMenuDesc, GetColor(k.dMenuType), k.dMenuID, GetChecked(k.dRoleID), "null");
                            }

                            if (kCount < SubMenu.Count - 1)
                                _html.Append(strMenu + ",");
                            else
                                _html.Append(strMenu);

                            kCount++;
                        }
                    }
                    else
                    {
                        _html.Append("");
                    }
                }

                return _html.ToString();
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User.EmployeeId, ex.Message);
                return _html.ToString();
            }
        }

        //private string GetKPIIndicatorsPageRole(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        //var query = unitOfWork
        //        //    .RoleIndicatorRepository.GetAll(roleIndicatorData => roleIndicatorData.TKR_ID == pRoleID).OrderBy(c => c.TKRK_INDICATOR_NO).ToList();

        //        var query = unitOfWork.IndicatorRepository.GetAll()
        //            .GroupJoin(
        //                unitOfWork.RoleIndicatorRepository.GetAll(roleIndicator => roleIndicator.TKR_ID == pRoleID),
        //                indicator => indicator.TKI_ID,
        //                roleIndicator => roleIndicator.TKRK_INDICATOR_NO,
        //                (indicator, roleIndicator) => new { indicatorParent = indicator, roleIndicatorParent = roleIndicator }
        //            )
        //            .SelectMany(
        //                x => x.roleIndicatorParent.DefaultIfEmpty(),
        //                (x, y) => new { indicator = x.indicatorParent, roleIndicator = y }
        //            ).ToList();
        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.indicator.TKI_INDI_NO + ". " + j.indicator.TKI_INDI_NAME, GetColor("MENU"), j.indicator.TKI_ID, GetChecked(j.roleIndicator?.TKR_ID), "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private string GetDepartmentPageRole(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {

        //        var query = unitOfWork.DepartmentRepository.GetAll()
        //            .GroupJoin(
        //                unitOfWork.RoleDataRepository.GetAll(roleData => roleData.TKR_ID == pRoleID).GroupBy(c => c.TKD_ID).Select(c => c.First()),
        //                department => department.TKD_ID,
        //                roleData => roleData.TKD_ID,
        //                (department, roleData) => new { departmentParent = department, roleDataParent = roleData }
        //            )
        //            .SelectMany(
        //                x => x.roleDataParent.DefaultIfEmpty(),
        //                (x, y) => new { department = x.departmentParent, roleData = y }
        //            ).ToList();
        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.department.TKD_DEPARTMENT_NAME, GetColor("MENU"), j.department.TKD_ID, GetChecked(j.roleData?.TKD_ID), "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private string GetTreeId(string pRoleID, string type)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            try
            {

                var query = new List<string>();

                if (type == "D")
                {
                    query = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == pRoleID).GroupBy(c => c.TKD_ID).Select(c => c.First().TKD_ID).ToList();
                }
                else if (type == "A")
                {
                    query = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == pRoleID).GroupBy(c => c.TKA_ID).Select(c => c.First().TKA_ID).ToList();
                }
                else if (type == "S")
                {
                    query = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == pRoleID).GroupBy(c => c.TKS_ID).Select(c => c.First().TKS_ID).ToList();
                }
                else if (type == "P")
                {
                    query = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == pRoleID).GroupBy(c => c.TKP_ID).Select(c => c.First().TKP_ID).ToList();
                }
                else if (type == "K")
                {
                    query = unitOfWork.RoleIndicatorRepository.GetAll(c => c.TKR_ID == pRoleID).GroupBy(c => c.TKRK_INDICATOR_NO).Select(c => c.First().TKRK_INDICATOR_NO).ToList();
                }


                if (query != null)
                {
                    foreach (var j in query)
                    {
                        _html.Append(string.Format("&{0}&|", j));
                    }

                    menuJSON = _html.ToString().TrimEnd('|');
                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RoleViewModel_Detail Get(string pRoleID)
        {

            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                var queryRole = unitOfWork.RoleRepository.GetById(pRoleID);

                model.RolID = queryRole?.TKR_ID;
                model.RolName = queryRole?.TKR_NAME;
                model.RolDesc = queryRole?.TKR_DESCRIPTION;
                model.RolStatus = queryRole?.TKR_STATUS;
                //model.BoolStatus = queryRole?.TKR_STATUS == "ACTIVE" ? true : false;

                var query = unitOfWork.MenuRepository.GetAll()
                    .GroupJoin(
                        unitOfWork.RoleMenuRepository.GetAll(roleMenu => roleMenu.TKR_ID == pRoleID),
                        menu => menu.MEU_ROW_ID,
                        roleMenu => roleMenu.TKRM_MENU,
                        (menu, roleMenu) => new { menuParent = menu, roleMenuParent = roleMenu })
                    .SelectMany(
                        x => x.roleMenuParent.DefaultIfEmpty(),
                        (x, y) => new { menu = x.menuParent, roleMenu = y })
                    .Select(item => new RoleViewModel_MenuWithRole
                    {
                        dMenuID = item?.menu?.MEU_ROW_ID
                                     ,
                        dMenuGroup = item?.menu?.MEU_GROUP_MENU
                                     ,
                        dMenuParentID = item?.menu?.MEU_PARENT_ID
                                     ,
                        dMenuLevel = item?.menu?.MEU_LEVEL
                                     ,
                        dMenuListNo = item?.menu?.MEU_LIST_NO
                                     ,
                        dMenuDesc = item?.menu?.LNG_DESCRIPTION
                                     ,
                        dMenuType = item?.menu?.MEU_CONTROL_TYPE
                                     ,
                        dRoleID = item?.roleMenu?.TKPI_ROLE?.TKR_ID
                                     ,
                        dRoleName = item?.roleMenu?.TKPI_ROLE?.TKR_NAME
                                     ,
                        dRoleDesc = item?.roleMenu?.TKPI_ROLE?.TKR_DESCRIPTION
                                     ,
                        dRoleStatus = item?.roleMenu?.TKPI_ROLE?.TKR_STATUS
                                     ,
                        dRoleMenuID = item?.roleMenu?.TKRM_ID

                    }).ToList();

                if (query != null)
                {


                    var MenuLevel1 = query.Where(p => p.dMenuLevel == "1").ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                    if (MenuLevel1.Count > 0)
                    {
                        int jCount = 0;
                        foreach (var j in MenuLevel1)
                        {
                            if (!string.IsNullOrWhiteSpace(j.dRoleID))
                                model.MenuTreeId += (model.MenuTreeId != "" ? "|&" + j.dMenuID + "&" : "&" + j.dMenuID + "&");

                            var qSubMenu = query.Where(p => p.dMenuParentID == j.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                            //var MenuFormat = !String.IsNullOrEmpty(j.dRoleID) ? ParentMenuCheckedFormat : ParentMenuFormat;
                            var strMenu = "";
                            if (qSubMenu.Count > 0)
                            {
                                var strSubMenu = GetSubMenu(j.dMenuID, query.ToList(), ref model);
                                strMenu = String.Format(ParentMenuFormat, j.dMenuDesc, GetColor(j.dMenuType), j.dMenuID, GetChecked(j.dRoleID), "[" + strSubMenu + "]");
                            }
                            else
                            {
                                strMenu = String.Format(ParentMenuFormat, j.dMenuDesc, GetColor(j.dMenuType), j.dMenuID, GetChecked(j.dRoleID), "null");
                            }

                            if (jCount < MenuLevel1.Count - 1)
                                _html.Append(strMenu + ",");
                            else
                                _html.Append(strMenu);

                            jCount++;
                        }
                    }

                    string menuJSON = "[" + _html.ToString() + "]";

                    model.MenuTree = menuJSON;


                    model.RoleMenu = new List<RoleViewModel_Menu>();
                    int i = 1;
                    foreach (var g in query)
                    {
                        RoleViewModel_Menu menu = new RoleViewModel_Menu()
                        {
                            RoleMenuID = g.dRoleMenuID,
                            MenuID = g.dMenuID,
                            MenuGroup = g.dMenuGroup,
                            MenuParentID = g.dMenuParentID,
                            MenuLevel = g.dMenuLevel,
                            MenuListNo = g.dMenuListNo,
                            MenuDesc = g.dMenuDesc,
                        };


                        model.RoleMenu.Add(menu);

                        i++;
                    }



                }
                //GetKPIIndicators
                model.KPITree = GetKPIIndicators(pRoleID);
                model.DepartmentTree = GetDepartmentTree(pRoleID);

                model.KPITreeId = GetTreeId(pRoleID, "K");
                //model.AreaTreeId = GetTreeId(pRoleID, "A");
                model.DepartmentTreeId = GetTreeId(pRoleID, "S");
                //model.PlantTreeId = GetTreeId(pRoleID, "P");
                //model.ShiftTreeId = GetTreeId(pRoleID, "S");

                var UserInRoleLit = new List<UserInRole>();
                var repoUser = unitOfWork.UserRepository.GetAll(c => c.TKR_ID == pRoleID).ToList();

                if (repoUser.Count > 0)
                {
                    repoUser.ForEach(user =>
                    {
                        var userTemp = new UserInRole();
                        userTemp.Company = user.TKPI_MASTER_COMPANY.TKC_COM_NAME;
                        userTemp.Department = user.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME;
                        userTemp.EmpCode = user.TKU_USER_CODE;
                        userTemp.EmpId = user.TKU_ID;
                        userTemp.Area = user.TKPI_MASTER_AREA?.TKA_AREA_NAME;
                        userTemp.Plant = user.TKPI_MASTER_PLANT?.TKP_PLANT_NAME;
                        userTemp.FirstName = user.TKU_FIRST_NAME;
                        userTemp.LastName = user.TKU_LAST_NAME;
                        model.UserInRoleList.Add(userTemp);
                    });
                }

                return model;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string GetDepartmentTree(string pRoleID)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                var query = unitOfWork.DepartmentRepository.GetAll();
                var strDepartment = string.Empty;
                query.ToList().ForEach(department =>
                {
                    var strArea = string.Empty;

                    department.TKPI_MASTER_AREA.ToList().ForEach(area =>
                    {
                        var strPlant = string.Empty;

                        area.TKPI_MASTER_PLANT.ToList().ForEach(plant =>
                        {
                            var strShift = string.Empty;

                            plant.TKPI_MASTER_SHIFT.ToList().ForEach(shift =>
                            {
                                strShift += String.Format(SubMenuFormat, shift.TKS_SHIFT_NAME, "", "shift_" + shift.TKS_ID, GetCheckedDepartment(pRoleID, c => c.TKS_ID == shift.TKS_ID), "null") + ",";
                            });

                            strShift = "[" + strShift.TrimEnd(',') + "]";
                            strPlant += String.Format(SubMenuFormat, plant.TKP_PLANT_NAME, "", plant.TKP_ID, GetCheckedDepartment(pRoleID, c => c.TKP_ID == plant.TKP_ID), strShift) + ",";
                        });

                        strPlant = "[" + strPlant.TrimEnd(',') + "]";

                        strArea += String.Format(SubMenuFormat, area.TKA_AREA_NAME, "", area.TKA_ID, GetCheckedDepartment(pRoleID, c => c.TKA_ID == area.TKA_ID), strPlant) + ",";

                    });

                    strArea = "[" + strArea.TrimEnd(',') + "]";

                    strDepartment += String.Format(ParentMenuFormat, department.TKD_DEPARTMENT_NAME, "", department.TKD_ID, GetCheckedDepartment(pRoleID, c => c.TKD_ID == department.TKD_ID), strArea) + ",";
                });

                strDepartment = strDepartment.TrimEnd(',');

                menuJSON = "[" + strDepartment.ToString() + "]";


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return menuJSON;

        }

        public string GetDepartmentTreeWithLayer(int? pRoleID, bool showPlant = true, bool showEVPM = true, bool showShift = true)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                var query = unitOfWork.DepartmentRepository.GetAll().Where(x => Const.User.AuthDepartmentIdLs.Contains(x.TKD_ID));

                var strDepartment = string.Empty;
                query.ToList().ForEach(department =>
                {
                    var strArea = string.Empty;
                    if (!showEVPM && department.TKD_ID == "1")
                    {
                        goto _continue;
                    }
                    if (department.TKD_ID != "1")
                    {

                        foreach (var area in department.TKPI_MASTER_AREA.ToList())
                        {
                            if (!Const.User.AuthAreaIdLs.Contains(area.TKA_ID)) continue;
                            var strPlant = string.Empty;
                            if (showPlant)
                            {
                                foreach (var plant in area.TKPI_MASTER_PLANT.ToList())
                                {
                                    if (!Const.User.AuthPlantIdLs.Contains(plant.TKP_ID)) continue;
                                    var strShift = string.Empty;
                                    if (showShift)
                                    {
                                        foreach (var shift in plant.TKPI_MASTER_SHIFT.ToList())
                                        {
                                            if (!Const.User.AuthShiftIdLs.Contains(shift.TKS_ID)) continue;
                                            strShift += String.Format(SubMenuFormat, shift.TKS_SHIFT_NAME, "", "shift_" + shift.TKS_ID, ",\"checked\": \"true\" ", "null") + ",";
                                        }

                                        strShift = "[" + strShift.TrimEnd(',') + "]";
                                    }
                                    else
                                    {
                                        strShift = "null";
                                    }
                                    strPlant += String.Format(SubMenuFormat, plant.TKP_PLANT_NAME, "", "plant_" + plant.TKP_ID, ",\"checked\": \"true\" ", strShift) + ",";

                                }
                                strPlant = "[" + strPlant.TrimEnd(',') + "]";
                            }
                            else
                            {
                                strPlant = "null";
                            }
                            strArea += String.Format(SubMenuFormat, area.TKA_AREA_NAME, "", "area_" + area.TKA_ID, ",\"checked\": \"true\" ", strPlant) + ",";

                        }
                        strArea = "[" + strArea.TrimEnd(',') + "]";
                    }
                    else
                    {
                        strArea = "null";
                    }
                    strDepartment += String.Format(ParentMenuFormat, department.TKD_DEPARTMENT_NAME, "", "department_" + department.TKD_ID, ",\"checked\": \"true\" ", strArea) + ",";
                _continue:;
                });

                strDepartment = (Selectall + strDepartment).TrimEnd(',');

                menuJSON = "[" + strDepartment.ToString() + "]";


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return menuJSON;

        }


        public string GetDepartmentTreePageInRole(string pRoleID)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                var query = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == pRoleID);
                var strDepartment = string.Empty;

                query.GroupBy(department => department.TKPI_MASTER_DEPARTMENT).Select(department => new { key = department.Key, area = department.OrderBy(o => o.TKD_ID).ToList() }).ToList().ForEach(department =>
                {
                    var strArea = string.Empty;

                    department.area.GroupBy(area => area.TKPI_MASTER_AREA).Select(area => new { key = area.Key, plant = area.OrderBy(o => o.TKA_ID).ToList() }).ToList().ForEach(area =>
                   {
                       var strPlant = string.Empty;

                       area.plant.GroupBy(plant => plant.TKPI_MASTER_PLANT).Select(plant => new { key = plant.Key, result = plant.OrderBy(o => o.TKP_ID).ToList() }).ToList().ForEach(plant =>
                       {
                           var strShift = string.Empty;

                           plant.result.OrderBy(o => o.TKS_ID).ToList().ForEach(shift =>
                           {
                               strShift += String.Format(SubMenuFormat, shift.TKPI_MASTER_SHIFT.TKS_SHIFT_NAME, "", shift.TKPI_MASTER_SHIFT.TKS_ID, "", "null") + ",";
                           });

                           strShift = "[" + strShift.TrimEnd(',') + "]";
                           strPlant += String.Format(SubMenuFormat, plant.key.TKP_PLANT_NAME, "", plant.key.TKP_ID, "", strShift) + ",";
                       });

                       strPlant = "[" + strPlant.TrimEnd(',') + "]";

                       strArea += String.Format(SubMenuFormat, area.key.TKA_AREA_NAME, "", area.key.TKA_ID, "", strPlant) + ",";

                   });

                    strArea = "[" + strArea.TrimEnd(',') + "]";

                    strDepartment += String.Format(ParentMenuFormat, department.key.TKD_DEPARTMENT_NAME, "", department.key.TKD_ID, "", strArea) + ",";
                });

                strDepartment = strDepartment.TrimEnd(',');

                menuJSON = "[" + strDepartment.ToString() + "]";


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return menuJSON;

        }

        //public string GetDepartment(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    RoleViewModel_Detail model = new RoleViewModel_Detail();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        var query = unitOfWork.DepartmentRepository.GetAll()
        //        .GroupJoin(
        //            unitOfWork.RoleDataRepository.GetAll(roleData => roleData.TKR_ID == pRoleID),
        //            department => department.TKD_ID,
        //            roleMenu => roleMenu.TKD_ID,
        //            (department, roleMenu) => new { departmentParent = department, roleMenuParent = roleMenu })
        //        .SelectMany(
        //            x => x.roleMenuParent.DefaultIfEmpty(),
        //            (x, y) => new { department = x.departmentParent, roleMenu = y })

        //             .ToList();

        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.department.TKD_LEVEL == 1 ? j.department.TKD_DEPARTMENT_NAME.Trim() + " (ALL)" : j.department.TKD_DEPARTMENT_NAME.Trim(), GetColor("MENU"), j.department.TKD_ID, GetChecked(j.roleMenu?.TKD_ID), "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";


        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }


        //}

        public string GetArea(string pRoleID, List<string> departmentIdList)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {

                var query = unitOfWork.AreaRepository.GetAll(c => departmentIdList.Count > 0 && departmentIdList.Contains(c.TKD_ID))
                .GroupJoin(
                    unitOfWork.RoleDataRepository.GetAll(roleData => roleData.TKR_ID == pRoleID),
                    area => area.TKD_ID,
                    roleMenu => roleMenu.TKD_ID,
                    (area, roleMenu) => new { areaParent = area, roleMenuParent = roleMenu })
                .SelectMany(
                    x => x.roleMenuParent.DefaultIfEmpty(),
                    (x, y) => new { area = x.areaParent, roleMenu = y })

                     .ToList();

                if (query != null)
                {
                    foreach (var j in query)
                    {
                        var strMenu = String.Format(ParentMenuFormat, j.area.TKA_AREA_NAME.Trim(), GetColor("MENU"), j.area.TKA_ID, GetChecked(j.roleMenu?.TKA_ID), "null");


                        _html.Append(strMenu + ",");
                    }

                    menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";


                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<SelectListItem> GetDepartmentDistinctDDL()
        {
            List<SelectListItem> rtn = new List<SelectListItem>() {
                new SelectListItem() { Text = "-- กรุณาเลือก --", Value = "" }
            };
            try
            {
                var query = unitOfWork.DepartmentRepository.GetAll(c => Const.User.AuthDepartmentNameLs.Contains(c.TKD_DEPARTMENT_NAME)).GroupBy(c => c.TKD_DEPARTMENT_NAME).Select(c => c.First()).OrderBy(c => c.TKD_DEPARTMENT_NAME).ToList();
                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKD_DEPARTMENT_NAME.ToString(), Text = item.TKD_DEPARTMENT_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetAreaDistinctDDL()
        {
            var ddl = new List<SelectListItem>() {
                new SelectListItem() { Text = "", Value = "" }
            };

            try
            {
                var query = unitOfWork.AreaRepository.GetAll(c => Const.User.AuthAreaNameLs.Contains(c.TKA_AREA_NAME)).GroupBy(c => c.TKA_AREA_NAME).Select(c => c.First()).OrderBy(c => c.TKA_AREA_NAME).ToList().Select(c => new { Id = c.TKA_ID.ToString(), Name = c.TKA_AREA_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Name });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }


        public List<SelectListItem> GetAreaDDL(string departmentId)
        {
            var ddl = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.AreaRepository.GetAll(c => c.TKD_ID == departmentId && Const.User.AuthAreaIdLs.Contains(c.TKA_ID)).Select(c => new { Id = c.TKA_ID.ToString(), Name = c.TKA_AREA_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Id });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }

        public string GetPlant(string pRoleID, List<string> areaIdList)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {

                var query = unitOfWork.PlantRepository.GetAll(c => areaIdList.Count > 0 && areaIdList.Contains(c.TKA_ID))
                .GroupJoin(
                    unitOfWork.RoleDataRepository.GetAll(roleData => roleData.TKR_ID == pRoleID),
                    plant => plant.TKA_ID,
                    roleMenu => roleMenu.TKA_ID,
                    (plant, roleMenu) => new { plantParent = plant, roleMenuParent = roleMenu })
                .SelectMany(
                    x => x.roleMenuParent.DefaultIfEmpty(),
                    (x, y) => new { plant = x.plantParent, roleMenu = y })

                     .ToList();

                if (query != null)
                {
                    foreach (var j in query)
                    {
                        var strMenu = String.Format(ParentMenuFormat, j.plant.TKP_PLANT_NAME.Trim(), GetColor("MENU"), j.plant.TKP_ID, GetChecked(j.roleMenu?.TKP_ID), "null");


                        _html.Append(strMenu + ",");
                    }

                    menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";


                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<SelectListItem> GetPlantDistinctDDL()
        {
            var ddl = new List<SelectListItem>() {
                new SelectListItem() { Text = "", Value = "" }
            };

            try
            {
                var query = unitOfWork.PlantRepository.GetAll(c => Const.User.AuthPlantNameLs.Contains(c.TKP_PLANT_NAME)).GroupBy(c => c.TKP_PLANT_NAME).Select(c => c.First()).OrderBy(c => c.TKP_PLANT_NAME).ToList().Select(c => new { Id = c.TKP_ID.ToString(), Name = c.TKP_PLANT_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Name });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }

        public List<SelectListItem> GetPlantDDL(string areaId)
        {
            var ddl = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.PlantRepository.GetAll(c => c.TKA_ID == areaId && Const.User.AuthPlantIdLs.Contains(c.TKP_ID)).Select(c => new { Id = c.TKP_ID.ToString(), Name = c.TKP_PLANT_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Id });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }

        public string GetShift(string pRoleID, List<string> plantIdList)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {

                var query = unitOfWork.ShiftRepository.GetAll(c => plantIdList.Count > 0 && plantIdList.Contains(c.TKP_ID))
                .GroupJoin(
                    unitOfWork.RoleDataRepository.GetAll(roleData => roleData.TKR_ID == pRoleID),
                    shift => shift.TKP_ID,
                    roleMenu => roleMenu.TKP_ID,
                    (shift, roleMenu) => new { shiftParent = shift, roleMenuParent = roleMenu })
                .SelectMany(
                    x => x.roleMenuParent.DefaultIfEmpty(),
                    (x, y) => new { shift = x.shiftParent, roleMenu = y })

                     .ToList();

                if (query != null)
                {
                    foreach (var j in query)
                    {
                        var strMenu = String.Format(ParentMenuFormat, "(" + j.shift.TKPI_MASTER_PLANT.TKP_PLANT_NAME + ") " + j.shift.TKS_SHIFT_NAME.Trim(), GetColor("MENU"), j.shift.TKS_ID, GetChecked(j.roleMenu?.TKS_ID), "null");

                        _html.Append(strMenu + ",");
                    }

                    menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";


                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<SelectListItem> GetShiftDistinctDDL()
        {
            var ddl = new List<SelectListItem>() {
                new SelectListItem() { Text = "", Value = "" }
            };

            try
            {
                var query = unitOfWork.ShiftRepository.GetAll(c => Const.User.AuthShifNameLs.Contains(c.TKS_SHIFT_NAME)).GroupBy(c => c.TKS_SHIFT_NAME).Select(c => c.First()).OrderBy(c => c.TKS_SHIFT_NAME).ToList().Select(c => new { Id = c.TKS_ID.ToString(), Name = c.TKS_SHIFT_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Name });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }

        public List<SelectListItem> GetShiftDDL(string plantId)
        {
            var ddl = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.ShiftRepository.GetAll(c => c.TKP_ID == plantId && Const.User.AuthShiftIdLs.Contains(c.TKS_ID)).Select(c => new { Id = c.TKS_ID.ToString(), Name = c.TKS_SHIFT_NAME }).ToList();

                query.ForEach(c =>
                {
                    ddl.Add(new SelectListItem() { Text = c.Name, Value = c.Id });
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ddl;
        }

        public string GetKPIIndicators(string pRoleID)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                //if (String.IsNullOrEmpty(pRoleID) == false)
                //{
                var query = unitOfWork.KpiNameRepository.GetAll()
                .GroupJoin(
                    unitOfWork.RoleIndicatorRepository.GetAll(roleIndicatorData => roleIndicatorData.TKR_ID == pRoleID),
                    roleIndicatorData => roleIndicatorData.TKIN_ID,
                    indicator => indicator.TKRK_INDICATOR_NO,
                    (roleIndicatorData, indicator) => new { roleIndicatorDataParent = roleIndicatorData, indicatorParent = indicator })
                .SelectMany(
                    x => x.indicatorParent.DefaultIfEmpty(),
                    (x, y) => new { roleIndicatorData = x.roleIndicatorDataParent, indicator = y })

                     .ToList();

                if (query != null)
                {
                    String checkAll = query.Where(c => c.indicator == null).FirstOrDefault() != null ? null : "9999";

                    foreach (var j in query.OrderBy(c => c.roleIndicatorData.TKIN_NO))
                    {
                        var strMenu = string.Empty;
                 
                        strMenu = String.Format(ParentMenuFormat, j.roleIndicatorData.TKIN_NO + "." + j.roleIndicatorData.TKIN_NAME.Trim(), GetColor("MENU"), j.roleIndicatorData.TKIN_ID, GetChecked(j.indicator?.TKRK_INDICATOR_NO), "null");
                        _html.Append(strMenu + ",");

                    }

                    var textSelectAll = String.Format(ParentMenuFormat, "Select All", GetColor("MENU"), "9999", GetChecked(checkAll), "[" + _html.ToString().TrimEnd(',') + "]");
                    //_html.Insert(0 , textSelectAll + ",");

                    //menuJSON = textSelectAll.TrimEnd(',');


                    menuJSON = "[" + textSelectAll.ToString().TrimEnd(',') + "]";


                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<SelectListItem> GetRoleName(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.RoleRepository.GetAll();

                if (string.IsNullOrEmpty(pStatus) == false)
                    query = query.Where(q => q.TKR_STATUS.Equals(pStatus)).OrderBy(c => c.TKR_NAME);

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKR_ID.ToString(), Text = item.TKR_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetRoleDesc(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.RoleRepository.GetAll();

                if (string.IsNullOrEmpty(pStatus) == false)
                    query = query.OrderBy(c => c.TKR_DESCRIPTION).Where(q => q.TKR_STATUS.Equals(pStatus));

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKR_ID.ToString(), Text = item.TKR_DESCRIPTION });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetMasterStatus()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {
                selectList.Add(new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" });
                selectList.Add(new SelectListItem { Value = "INACTIVE", Text = "INACTIVE" });
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
            }
            return selectList;
        }

    }
}