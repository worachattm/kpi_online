﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.ViewModel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.ReportKPISummaryPerformanceViewModel;

namespace KPI_ONLINE.Models
{
    public class ReportKPISummaryPerformanceServiceModel : BaseClass
    {
        public void GetSearchResult(ReportKPISummaryPerformanceViewModel Model)
        {
            var querydata = new List<sp_performance_all_area_Result>();
            using (MFKPIEntities context = new MFKPIEntities())
            {
                using (context.Database.Connection)
                {
                    context.Database.Connection.Open();
                    DbCommand cmd = context.Database.Connection.CreateCommand();
                    cmd.CommandText = "sp_performance_all_area";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new OracleParameter("department", ""));
                    cmd.Parameters.Add(new OracleParameter("type", Model.Criteria.PeriodType ?? ""));
                    cmd.Parameters.Add(new OracleParameter("year", Model.Criteria.Year ?? ""));
                    cmd.Parameters.Add(new OracleParameter("period", Model.Criteria.TargetPeriod ?? ""));
                    OracleParameter oraP = new OracleParameter();
                    oraP.ParameterName = "p_recordset";
                    oraP.OracleDbType = OracleDbType.RefCursor;
                    oraP.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(oraP);
                    using (var reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            var sch = reader.GetSchemaTable();
                            querydata = reader.MapToList<sp_performance_all_area_Result>() ?? new List<sp_performance_all_area_Result>();
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                }
            }
            var allkpi = unitOfWork.KpiNameRepository.GetAll();
            var allShift = unitOfWork.ShiftRepository.GetAll();
            var allPlant = unitOfWork.PlantRepository.GetAll();
            var allArea = unitOfWork.AreaRepository.GetAll();
            var allDepartment = unitOfWork.DepartmentRepository.GetAll();
            var groupdata = querydata?.GroupBy(x => new { x.TKID_DEPARTMENT, x.TKID_AREA }).GroupBy(x => x.Key.TKID_DEPARTMENT).Where(x=>Const.User.AuthDepartmentNameLs.Contains(x.Key));
            Model.Result = new SearchResult();
            foreach (var deptData in groupdata)
            {
                var department = new Department();
                department.departmentName = deptData.Key;
                department.departmentID = allDepartment.FirstOrDefault(x => x.TKD_DEPARTMENT_NAME == deptData.Key)?.TKD_ID;

                foreach (var areaData in deptData)
                {
                    var area = new Area();
                    area.areaName = areaData.Key.TKID_AREA;
                    area.areaID = allArea.FirstOrDefault(x => x.TKA_AREA_NAME == area.areaName && x.TKPI_MASTER_DEPARTMENT.TKD_ID == department.departmentID)?.TKA_ID;
                    if (!(Const.User.AuthAreaIdLs.Contains(area.areaID ?? "") || areaData.Key.TKID_AREA.IsNullOrEmpty())) continue;

                    if (areaData.Key.TKID_DEPARTMENT.IsNullOrEmpty() && areaData.Key.TKID_AREA.IsNullOrEmpty())
                    {
                        area.PlantList = allPlant.Where(x => x.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_ID != "1").Select(x => new KeyValuePair<string, string>(x.TKP_ID, x.TKP_PLANT_NAME)).ToList();
                    }
                    else if (!areaData.Key.TKID_AREA.IsNullOrEmpty())
                    {
                        var master_area = allArea.Where(x => x.TKA_AREA_NAME == areaData.Key.TKID_AREA && x.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME == areaData.Key.TKID_DEPARTMENT).FirstOrDefault();
                        var master_plant_list = allPlant.Where(x => x.TKA_ID == master_area.TKA_ID);
                        area.PlantList = master_plant_list.Select(x => new KeyValuePair<string, string>(x.TKP_ID, x.TKP_PLANT_NAME)).ToList();
                    }
                    else if (!areaData.Key.TKID_DEPARTMENT.IsNullOrEmpty())
                    {
                        var master_area_list = allArea.Where(x => x.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME == areaData.Key.TKID_DEPARTMENT).ToList();
                        var master_plant_list = allPlant.Where(x => master_area_list.Select(s => s.TKA_ID).Contains(x.TKA_ID));
                        area.PlantList = master_plant_list.Select(x => new KeyValuePair<string, string>(x.TKP_ID, x.TKP_PLANT_NAME)).ToList();
                    }
                    area.PlantList = area.PlantList.Where(d => Const.User.AuthPlantIdLs.Contains(d.Key)).ToList();
                    area.detail = areaData.OrderBy(x => x.TKI_INDI_NO).ThenBy(x => x.TKID_SHIFT).ToList();
                    foreach (var x in area.detail)
                    {
                        var kpiname = allkpi.FirstOrDefault(y => y.TKIN_NO == x.TKI_INDI_NO);
                        x.TKI_INDI_NAME = kpiname?.TKIN_NAME;
                        x.TKI_INDI_NAME_ID = kpiname?.TKIN_ID;
                    }
                    area.detail = area.detail
                        .Where(x => Const.User.AuthKpiIdLs.Contains(x.TKI_INDI_NAME_ID))
                        .ToList();

                    department.areaList.Add(area);
                }
                Model.Result.departmentList.Add(department);

            }
            Model.IndicatorNameList = IndicatorModel.GetIndicatorName();
        }

    }
}