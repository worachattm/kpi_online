﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.TKPI_USER_DAL;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Utilitie;

namespace KPI_ONLINE.Models
{
    public class UsersServiceModel : BaseClass
    {
        
        private readonly string[] ImportUserArray = new string[] { "EmployeeCode", "FirstName", "LastName", "Department", "Area", "Plant", "Shift", "Company", "UserAD" };
        private static readonly string ParentMenuFormat = "{{ \"text\": \"  {0}\" " +
                                ",\"color\": \"#428bca\" " +
                                ",\"backColor\": \"{1}\" " +
                                ",\"href\": \"#node-{2}\" " +
                                ",\"selectable\": \"false\" " +
                                ",\"state\": {{ " +
                                " \"expanded\": \"true\" " +
                                " {3} " +
                                "}} " +
                                ",\"nodes\": {4} " +
                                "}}";
        ~UsersServiceModel()
        {
            //unitOfWork.Dispose();
        }

        private void AssignValueToModel(ref UserMonitorDiff User , int Col , string value)
        {
            var type = ImportUserArray[Col];


            if(type == ConstantPrm.FieldExportUserMonitor.COMPANY)
            {
                User.MCompanyName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.FIRST_NAME)
            {
                User.MFirstName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.LAST_NAME)
            {
                User.MLastName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.EMP_CODE)
            {
                User.MUserCode = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.DEPARTMENT)
            {
                User.MDepartmentName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.AREA)
            {
                User.MAreaName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.PLANT)
            {
                User.MPlantName = value;
            }
            if (type == ConstantPrm.FieldExportUserMonitor.SHIFT)
            {
                User.MShiftName = value;
            }
            //if (type == ConstantPrm.FieldExportUserMonitor.TEL)
            //{
            //    User.MTel = value;
            //}

            if (type == ConstantPrm.FieldExportUserMonitor.USER_AD)
            {
                User.MUserAD = value;
            }
        }

        public ReturnValue CreateOrEditUserMonitor(List<UserMonitorDiff> ListUserDiff, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            { 
                TKPI_USERS_DAL dal = new TKPI_USERS_DAL();
                TKPI_USER ent = new TKPI_USER();


                DateTime now = DateTime.Now;

                try
                {
                    //var resDeleteAllUser = dal.DeleteUserMonitor();
                    //Validate
                    var valid = true;
                    var lsMessage = new List<string>();
                    ListUserDiff.ForEach(c =>
                    {
                        var department = unitOfWork.DepartmentRepository.GetAll(g => g.TKD_DEPARTMENT_NAME.ToUpper().Trim() == c.MDepartmentName.ToUpper().Trim()).FirstOrDefault();
                        var area = department?.TKPI_MASTER_AREA.Where(a => a.TKA_AREA_NAME.Equals(c.MAreaName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        var plant = area?.TKPI_MASTER_PLANT.Where(a => a.TKP_PLANT_NAME.Equals(c.MPlantName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        var shift = plant?.TKPI_MASTER_SHIFT.Where(a => a.TKS_SHIFT_NAME.Equals(c.MShiftName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                        if (!string.IsNullOrWhiteSpace(c.MShiftName) && (string.IsNullOrWhiteSpace(c.MAreaName) || string.IsNullOrWhiteSpace(c.MPlantName)))
                        {
                            var message = string.Format("Employee Code {0} = Department {1} -> Area {2} -> Plant {3} {4}", c.MUserCode, c.MDepartmentName, c.MAreaName, c.MPlantName, !string.IsNullOrWhiteSpace(c.MShiftName) ? " -> Shift " + c.MShiftName : "");
                            lsMessage.Add(message);
                            valid = false;
                        } else if (!string.IsNullOrWhiteSpace(c.MPlantName) && string.IsNullOrWhiteSpace(c.MAreaName))
                        {
                            var message = string.Format("Employee Code {0} = Department {1} -> Area {2} -> Plant {3} {4}", c.MUserCode, c.MDepartmentName, c.MAreaName, c.MPlantName, !string.IsNullOrWhiteSpace(c.MShiftName) ? " -> Shift " + c.MShiftName : "");
                            lsMessage.Add(message);
                            valid = false;
                        }
                        
                    });

                    if (valid)
                    {
                        // update or create user
                        ListUserDiff.ForEach(c =>
                        {
                            ent = new TKPI_USER();
                            ent.TKU_USER_CODE = c.MUserCode;
                            ent.TKU_FIRST_NAME = c.MFirstName;
                            ent.TKU_LAST_NAME = c.MLastName;

                            ent.TKD_ID = unitOfWork.DepartmentRepository.GetAll(g => g.TKD_DEPARTMENT_NAME.Equals(c.MDepartmentName , StringComparison.OrdinalIgnoreCase)).FirstOrDefault().TKD_ID;
                            ent.TKA_ID = unitOfWork.AreaRepository.GetAll(g => g.TKA_AREA_NAME.Equals(c.MAreaName, StringComparison.OrdinalIgnoreCase) && g.TKD_ID == ent.TKD_ID).FirstOrDefault()?.TKA_ID;
                            ent.TKP_ID = unitOfWork.PlantRepository.GetAll(g => g.TKP_PLANT_NAME.Equals(c.MPlantName, StringComparison.OrdinalIgnoreCase) && g.TKA_ID == ent.TKA_ID).FirstOrDefault()?.TKP_ID;
                            ent.TKC_ID = unitOfWork.CompanyRepository.GetAll(g => g.TKC_COM_NAME.ToUpper().Trim() == c.MCompanyName.ToUpper().Trim()).FirstOrDefault()?.TKC_ID;
                            ent.TKS_ID = unitOfWork.ShiftRepository.GetAll(g => g.TKS_SHIFT_NAME.Equals(c.MShiftName, StringComparison.OrdinalIgnoreCase) && g.TKP_ID == ent.TKP_ID).FirstOrDefault()?.TKS_ID;
                            ent.TKR_ID = (c.TUserRoleId);
                            ent.TKU_UPDATED_BY = pUser;
                            ent.TKU_UPDATED_DATE = DateTime.Now;
                            ent.TKU_CREATED_BY = pUser;
                            ent.TKU_CREATED_DATE = DateTime.Now;
                            //ent.TKU_TEL = c.MTel;
                            ent.TKU_LOGIN = c.MUserAD;
                            ent.TKU_STATUS = "ACTIVE";

                            dal.CreateOrUpdateUserMontior(ent, ref unitOfWork);
                        });

                        //snapshot
                        ListUserDiff.ForEach(c =>
                        {
                            var monitorUser = unitOfWork.UserMonitorRepository.GetAll(m => m.TKUM_USER_CODE == c.MUserCode).FirstOrDefault();
                            var snapshot = new TKPI_USER_MONITOR_SNAPSHOT();

                            if (monitorUser != null)
                            {
                                //var test = monitorUser.TKU_TEL ?? DBNull.Value;
                                snapshot.TKMH_USER_CODE = monitorUser.TKUM_USER_CODE;
                                snapshot.TKMH_FIRST_NAME = monitorUser.TKUM_FIRST_NAME;
                                snapshot.TKUM_LAST_NAME = monitorUser.TKUM_LAST_NAME;

                                snapshot.TKD_ID = monitorUser.TKD_ID;
                                snapshot.TKA_ID = monitorUser.TKA_ID;
                                snapshot.TKP_ID = monitorUser.TKP_ID;
                                snapshot.TKC_ID = monitorUser.TKC_ID;
                                snapshot.TKS_ID = monitorUser.TKS_ID;
                                snapshot.TKR_ID = monitorUser.TKR_ID;
                                snapshot.TKMH_UPDATED_BY = pUser;
                                snapshot.TKMH_UPDATED_DATE = DateTime.Now;
                                snapshot.TKMH_CREATED_BY = pUser;
                                snapshot.TKMH_CREATED_DATE = DateTime.Now;
                                snapshot.TKMH_LOGIN = monitorUser.TKM_LOGIN;
                                //snapshot.TKU_TEL = monitorUser.TKU_TEL;

                            }
                            else
                            {
                                snapshot.TKMH_USER_CODE = c.MUserCode ?? ""; 
                                snapshot.TKMH_FIRST_NAME = c.MFirstName ?? "";
                                snapshot.TKUM_LAST_NAME = c.MLastName ?? "";

                                snapshot.TKD_ID = unitOfWork.DepartmentRepository.GetAll(g => g.TKD_DEPARTMENT_NAME.Equals(c.MDepartmentName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().TKD_ID;
                                snapshot.TKA_ID = unitOfWork.AreaRepository.GetAll(g => g.TKA_AREA_NAME.Equals(c.MAreaName, StringComparison.OrdinalIgnoreCase) && g.TKD_ID == snapshot.TKD_ID).FirstOrDefault()?.TKA_ID;
                                snapshot.TKP_ID = unitOfWork.PlantRepository.GetAll(g => g.TKP_PLANT_NAME.Equals(c.MPlantName, StringComparison.OrdinalIgnoreCase) && g.TKA_ID == snapshot.TKA_ID).FirstOrDefault()?.TKP_ID;
                                snapshot.TKC_ID = unitOfWork.CompanyRepository.GetAll(g => g.TKC_COM_NAME.ToUpper().Trim() == c.MCompanyName.ToUpper().Trim()).FirstOrDefault()?.TKC_ID;
                                snapshot.TKS_ID = unitOfWork.ShiftRepository.GetAll(g => g.TKS_SHIFT_NAME.Equals(c.MShiftName, StringComparison.OrdinalIgnoreCase) && g.TKP_ID == snapshot.TKP_ID).FirstOrDefault()?.TKS_ID;
                                snapshot.TKR_ID = (c.TUserRoleId);
                                snapshot.TKMH_UPDATED_BY = pUser;
                                snapshot.TKMH_UPDATED_DATE = DateTime.Now;
                                snapshot.TKMH_CREATED_BY = pUser;
                                snapshot.TKMH_CREATED_DATE = DateTime.Now;
                                snapshot.TKMH_LOGIN = c.MUserAD ?? "";
                                //snapshot.TKU_TEL = c.MTel ?? "";
                            }

                            //if()

                            dal.CreateOrUpdateUserSnapshot(snapshot, ref unitOfWork);
                        });


                        unitOfWork.Commit();
                        rtn.Message = ConstantPrm.MessageSuccess;
                        rtn.Status = true;
                    }
                    else
                    {
                        rtn.Message = "Is not unrelate " + Environment.NewLine + String.Join(Environment.NewLine, lsMessage.ToArray()); 
                        rtn.Status = false;
                    }


                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public (ReturnValue rtn , List<UserMonitorDiff> listDiff) ImportUserMonitorFromExcel(HttpPostedFileBase fileToUpload)
        {
            var result = new List<UserMonitorDiff>();
            var rtn = new ReturnValue();

            try
            {
                using (var package = new ExcelPackage(fileToUpload.InputStream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    var defaultRoleId = unitOfWork.RoleRepository.GetAll(c => c.TKR_NAME.Equals(ConstantPrm.DefaultRole, StringComparison.CurrentCultureIgnoreCase)).Select(c => c.TKR_ID).FirstOrDefault();

                    for (int row = 3; worksheet.Cells[row, 1].Value != null; row++)
                    {
                        var user = new UserMonitorDiff();
                        for (int col = 1; worksheet.Cells[row, col].Value != null; col++)
                        {
                           AssignValueToModel(ref user ,(col-1), (worksheet.Cells[row, col].Value?.ToString()?.Trim()));
           
                        }
                        user.TUserRoleId = defaultRoleId;

                        result.Add(user);
                    }
                }

                rtn.Message = "Preview Data";
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
            }
    

            return (rtn , result);
        }

        public string UserExportExcel(UsersViewModel_Search dataResult)
        {
            string file_name = string.Format("USER_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "User", file_name);

            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload" , "User");

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string blueSoft = "#03befc";
            string greenSoft = "#13f0bc";
            try
            {
                //var dataResult = new UsersViewModel_Search();

                Search(ref dataResult);
                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    var fnUtilites = new Utilities();
                    var ws = package.Workbook.Worksheets.Add("USER");
                    var rowHeader = 1;

                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 9, isTable: true, isBold: true, isMerge: true, value: "USER", backgroundColor: blueSoft, fontColor: Color.Black);

                    rowHeader++;
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 1, isBold: true, value: "Employee Code" + Environment.NewLine, backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 2, rowHeader, 2, isBold: true, value: "First Name", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 3, rowHeader, 3, isBold: true, value: "Last Name", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 4, rowHeader, 4, isBold: true, value: "Department", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 5, rowHeader, 5, isBold: true, value: "Area", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 6, rowHeader, 6, isBold: true, value: "Plant", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 7, rowHeader, 7, isBold: true, value: "Shift", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 8, rowHeader, 8, isBold: true, value: "Company", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, isBold: true, value: "Tel", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, isBold: true, value: "User Role", backgroundColor: greenSoft, fontColor: Color.Black);

                    if (dataResult != null)
                    {
                        dataResult.sSearchData.ToList().ForEach(item =>
                        {
                            rowHeader++;
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 1 ,value: item.dUserCode , backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 2, rowHeader, 2 ,value: item.dFirstName, backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 3, rowHeader, 3 ,value: item.dLastName, backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 4, rowHeader, 4 ,value: item.DepartmentName, backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 5, rowHeader, 5 ,value: (!string.IsNullOrWhiteSpace(item.AreaName) ? item.AreaName : "-"), backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 6, rowHeader, 6 ,value: (!string.IsNullOrWhiteSpace(item.PlantName) ? item.PlantName : "-"), backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 7, rowHeader, 7 ,value: (!string.IsNullOrWhiteSpace(item.ShiftName) ? item.ShiftName : "-"), backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 8, rowHeader, 8 ,value: item.CompanyName, backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, value: !string.IsNullOrWhiteSpace(item.Tel) ? item.Tel : "-" , backgroundColor: "");
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, value: item.UserRoleName, backgroundColor: "");

                        });
                    }

                    #region Document PrinterSettings
                    //ws.View.ShowGridLines = false;
                    ws.HeaderFooter.OddHeader.RightAlignedText = string.Format("Date {0} {1}", ExcelHeaderFooter.CurrentDate, ExcelHeaderFooter.CurrentTime);
                    ws.HeaderFooter.OddFooter.RightAlignedText = string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
                    ws.HeaderFooter.OddFooter.LeftAlignedText = string.Format("© Copyright 2019 a company of Thai Oil Group. All rights reserved.");
                    ws.PrinterSettings.Orientation = eOrientation.Landscape;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.LeftMargin = 0.38m;
                    ws.PrinterSettings.RightMargin = 0.40m;
                    ws.PrinterSettings.FitToPage = true;
                    ws.PrinterSettings.FitToHeight = 100;
                    ws.PrinterSettings.FitToWidth = 1;
                    #endregion


                    package.Save();
                }
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                File.Delete(file_path);
                file_name = string.Empty;
            }
            return file_name;
        }

        public string UserMonitorGenerateExcel()
        {
            string file_name = string.Format("USER_MONITOR_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "UserMonitor", file_name);
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "UserMonitor");

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string blueSoft = "#03befc";
            string greenSoft = "#13f0bc";
            try
            {
                var dataResult = SearchUserMonitor();

                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    var fnUtilites = new Utilities();
                    var ws = package.Workbook.Worksheets.Add("USER_DIFF");
                    var rowHeader = 1;

                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 9, isTable: true, isBold: true, isMerge: true, value: "USER MONITOR", backgroundColor: blueSoft, fontColor: Color.Black);

                    rowHeader++;
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 1, isBold: true, value: "Employee Code" + Environment.NewLine, backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 2, rowHeader, 2, isBold: true, value: "First Name", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 3, rowHeader, 3, isBold: true, value: "Last Name", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 4, rowHeader, 4, isBold: true, value: "Department", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 5, rowHeader, 5, isBold: true, value: "Area", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 6, rowHeader, 6, isBold: true, value: "Plant", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 7, rowHeader, 7, isBold: true, value: "Shift", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 8, rowHeader, 8, isBold: true, value: "Company", backgroundColor: greenSoft, fontColor: Color.Black);
                    fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, isBold: true, value: "User AD", backgroundColor: greenSoft, fontColor: Color.Black);

                    if(dataResult != null)
                    {
                        dataResult.ToList().ForEach(item =>
                        {
                            rowHeader++;
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 1, rowHeader, 1, isBold: true, value: item.IsDuplicateUsercode ? item.MUserCode : item.MUserCode + Environment.NewLine + item.TUserCode, backgroundColor: "", fontColor: item.IsDuplicateUsercode ? Color.Black : Color.Red, isWrap: !item.IsDuplicateUsercode);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 2, rowHeader, 2, isBold: true, value: item.IsDuplicateFirstName ? item.MFirstName : item.MFirstName + Environment.NewLine + item.TFirstName, backgroundColor: "", fontColor: item.IsDuplicateFirstName ? Color.Black : Color.Red , isWrap: !item.IsDuplicateFirstName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 3, rowHeader, 3, isBold: true, value: item.IsDuplicateLastName ? item.MLastName : item.MLastName + Environment.NewLine + item.TLastName, backgroundColor: "", fontColor: item.IsDuplicateLastName ? Color.Black : Color.Red, isWrap: !item.IsDuplicateLastName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 4, rowHeader, 4, isBold: true, value: item.IsDuplicateDepartmentName ? item.MDepartmentName : item.MDepartmentName + Environment.NewLine + item.TDepartmentName, backgroundColor: "", fontColor: item.IsDuplicateDepartmentName ? Color.Black : Color.Red, isWrap: !item.IsDuplicateDepartmentName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 5, rowHeader, 5, isBold: true, value: item.IsDuplicateAreaName ? item.MAreaName : item.MAreaName + Environment.NewLine + item.TAreaName, backgroundColor: "", fontColor: item.IsDuplicateAreaName ? Color.Black : Color.Red, isWrap: !item.IsDuplicateAreaName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 6, rowHeader, 6, isBold: true, value: item.IsDuplicatePlantName ? item.MPlantName : item.MPlantName + Environment.NewLine + item.TPlantName, backgroundColor: "", fontColor: item.IsDuplicatePlantName ? Color.Black : Color.Red, isWrap: !item.IsDuplicatePlantName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 7, rowHeader, 7, isBold: true, value: item.IsDuplicateShiftName ? item.MShiftName : item.MShiftName + Environment.NewLine + item.TShiftName, backgroundColor: "", fontColor: item.IsDuplicateShiftName ? Color.Black : Color.Red, isWrap: !item.IsDuplicateShiftName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 8, rowHeader, 8, isBold: true, value: item.IsDuplicateCompanyName ? item.MCompanyName : item.MCompanyName + Environment.NewLine + item.TCompanyName, backgroundColor: "", fontColor: item.IsDuplicateCompanyName ? Color.Black : Color.Red, isWrap: !item.IsDuplicateCompanyName);
                            fnUtilites.WriteExcelDetail(ref ws, rowHeader, 9, rowHeader, 9, isBold: true, value: item.IsDuplicateUserAD ? item.MUserAD : item.MUserAD + Environment.NewLine + item.TUserAD, backgroundColor: "", fontColor: item.IsDuplicateUserAD ? Color.Black : Color.Red, isWrap: !item.IsDuplicateUserAD);


                        });
                    }

                    #region Document PrinterSettings
                    //ws.View.ShowGridLines = false;
                    ws.HeaderFooter.OddHeader.RightAlignedText = string.Format("Date {0} {1}", ExcelHeaderFooter.CurrentDate, ExcelHeaderFooter.CurrentTime);
                    ws.HeaderFooter.OddFooter.RightAlignedText = string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
                    ws.HeaderFooter.OddFooter.LeftAlignedText = string.Format("© Copyright 2019 a company of Thai Oil Group. All rights reserved.");
                    ws.PrinterSettings.Orientation = eOrientation.Landscape;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.LeftMargin = 0.38m;
                    ws.PrinterSettings.RightMargin = 0.40m;
                    ws.PrinterSettings.FitToPage = true;
                    ws.PrinterSettings.FitToHeight = 100;
                    ws.PrinterSettings.FitToWidth = 1;
                    #endregion


                    package.Save();
                }
            }
            catch(Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                File.Delete(file_path);
                file_name = string.Empty;
            }
            return file_name;
        }

        public List<SelectListItem> GetCompanyCode()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.CompanyRepository.GetAll();

                if (query != null)
                {
         
                    foreach (var item in query.OrderBy(p => p.TKC_ID))
                    {
                        rtn.Add(new SelectListItem { Value = item.TKC_ID.ToString(), Text = item.TKC_COM_NAME });
                    }
                }
                
                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetPlantCode(string areaId = "")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.PlantRepository.GetAll(c => c.TKA_ID == areaId);

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKP_ID.ToString(), Text = item.TKP_PLANT_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetAreaCode(string departmentId = "")
        {

            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.AreaRepository.GetAll(c => c.TKD_ID == departmentId);

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKA_ID.ToString(), Text = item.TKA_AREA_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;
            }
        }

        public List<SelectListItem> GetDepartmentCode()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.DepartmentRepository.GetAll(c => Const.User.AuthDepartmentIdLs.Contains(c.TKD_ID));
                if (query != null)
                {
              
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKD_ID.ToString(), Text = item.TKD_DEPARTMENT_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetShiftCode(string plantId = "")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.ShiftRepository.GetAll(c => c.TKP_ID == plantId);
                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKS_ID.ToString(), Text = item.TKS_SHIFT_NAME });
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public List<SelectListItem> GetStatus()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                rtn.Add(new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" });
                rtn.Add(new SelectListItem { Value = "INACTIVE", Text = "INACTIVE" });


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public ReturnValue Add(UsersViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                TKPI_USERS_DAL dal = new TKPI_USERS_DAL();
                TKPI_USER ent = new TKPI_USER();

                DateTime now = DateTime.Now;

                try
                {
                    // Update data in USERS
                    ent.TKU_USER_CODE = pModel.EmployeeID;
                    ent.TKU_FIRST_NAME = pModel.FirstNameTH;
                    ent.TKU_LAST_NAME = pModel.LastNameTH;

                    ent.TKD_ID = pModel.DepartmentId;
                    ent.TKA_ID = pModel.AreaId;
                    ent.TKP_ID = pModel.PlantId;
                    ent.TKC_ID = pModel.CompanyId;
                    ent.TKS_ID = pModel.ShiftId;
                    ent.TKR_ID = pModel.RoleId;

                    ent.TKU_TEL = pModel.Tel;
                    ent.TKU_STATUS = pModel.Status;

                    ent.TKU_CREATED_BY = pUser;
                    ent.TKU_UPDATED_BY = pUser;
                    ent.TKU_UPDATED_DATE = now;
                    ent.TKU_CREATED_DATE = now;

                    dal.Save(ent, ref unitOfWork);

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
              
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(UsersViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                TKPI_USERS_DAL userDAL = new TKPI_USERS_DAL();
                TKPI_USER userDetail = new TKPI_USER();

                DateTime now = DateTime.Now;

                try
                {
                    userDetail.TKU_ID = pModel.UsersID;
                    userDetail.TKU_USER_CODE = pModel.EmployeeID;
                    userDetail.TKU_FIRST_NAME = pModel.FirstNameTH;
                    userDetail.TKU_LAST_NAME = pModel.LastNameTH;

                    userDetail.TKD_ID = pModel.DepartmentId;
                    userDetail.TKA_ID = pModel.AreaId;
                    userDetail.TKP_ID = pModel.PlantId;
                    userDetail.TKC_ID = pModel.CompanyId;
                    userDetail.TKS_ID = pModel.ShiftId;
                    userDetail.TKR_ID = pModel.RoleId;

                    userDetail.TKU_TEL = pModel.Tel;
                    userDetail.TKU_STATUS = pModel.Status;
                    userDetail.TKU_UPDATED_DATE = now;
                    userDetail.TKU_UPDATED_BY = pUser;

                    userDAL.Update(userDetail, ref unitOfWork);

                 

                    unitOfWork.Commit();
                    rtn.Message = ConstantPrm.MessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
           
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        //public List<UserMonitorDiff> SearchUser()
        //{
        //    var query = new List<UserMonitorDiff>();
        //    //UserMonitorViewModel

        //    try
        //    {
        //        query = unitOfWork.UserRepository.GetAll()
        //        .Select(v => new UserMonitorDiff
        //        {
        //            sUsercode = v.TKU_USER_CODE?.Trim(),
        //            sFirstName = v.TKU_FIRST_NAME?.Trim(),
        //            sLastName = v.TKU_LAST_NAME?.Trim(),
        //            sTel = v.TKU_TEL?.Trim(),
        //            sCompanyName = v.TKPI_MASTER_COMPANY.TKC_COM_NAME?.Trim(),
        //            sDepartmentName = v.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME?.Trim(),
        //            sAreaName = v.TKPI_MASTER_AREA.TKA_AREA_NAME?.Trim(),
        //            sPlantName = v.TKPI_MASTER_PLANT.TKP_PLANT_NAME?.Trim(),
        //            sShiftName = v.TKPI_MASTER_SHIFT?.TKS_SHIFT_NAME?.Trim(),
        //            sUserRoleId = v.TKR_ID.ToString()?.Trim()
        //        }).ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
        //    }
        //    return query;
        //}

        public List<UserMonitorDiff> SearchUserMonitor()
        {
            var query = new List<UserMonitorDiff>();
            //UserMonitorViewModel

            try
            {
                var defaultRoleId = unitOfWork.RoleRepository.GetAll(c => c.TKR_NAME.Equals(ConstantPrm.DefaultRole, StringComparison.CurrentCultureIgnoreCase)).Select(c => c.TKR_ID).FirstOrDefault();
                query = unitOfWork.UserMonitorRepository.GetAll()
                    .GroupJoin(
                        unitOfWork.UserRepository.GetAll(),
                        Monitor => Monitor.TKUM_USER_CODE,
                        User => User.TKU_USER_CODE,
                        (Monitor, User) => new { MonitorParent = Monitor, UserParent = User }
                    )
                    .SelectMany(
                        x => x.UserParent.DefaultIfEmpty(),
                        (x, y) => new { Monitor = x.MonitorParent, User = y }
                    )
                    .GroupJoin(
                        unitOfWork.SnapshotRepository.GetAll(),
                        Temp => Temp.Monitor.TKUM_USER_CODE,
                        Snapshot => Snapshot.TKMH_USER_CODE,
                      //(temp, snapshot) => new { temp.Monitor, temp.User, Snapshot = snapshot })
                        (Temp, Snapshot) => new { Monitor = Temp.Monitor , User = Temp.User , Snapshot = Snapshot }
                    )
                    .SelectMany(
                        x => x.Snapshot.DefaultIfEmpty(),
                        (x, y) => new { Monitor = x.Monitor , User = x.User, Snapshot = y }
                    )
                .Select(v => new UserMonitorDiff
                {
                    MUserCode = v.Monitor.TKUM_USER_CODE?.Trim() ?? "",
                    MFirstName = v.Monitor.TKUM_FIRST_NAME?.Trim() ?? "",
                    MLastName = v.Monitor.TKUM_LAST_NAME?.Trim() ?? "",
                    MUserAD = v.Monitor.TKM_LOGIN?.Trim() ?? "",
                    //MTel = v.Monitor.TKU_TEL?.Trim() ?? "",
                    MCompanyName = unitOfWork.CompanyRepository.GetById(v?.Monitor?.TKC_ID)?.TKC_COM_NAME?.Trim() ?? "",
                    MDepartmentName = unitOfWork.DepartmentRepository.GetById(v.Monitor?.TKD_ID)?.TKD_DEPARTMENT_NAME?.Trim() ?? "",
                    MAreaName = unitOfWork.AreaRepository.GetById(v.Monitor?.TKA_ID)?.TKA_AREA_NAME?.Trim() ?? "",
                    MPlantName = unitOfWork.PlantRepository.GetById(v.Monitor?.TKP_ID)?.TKP_PLANT_NAME?.Trim() ?? "",
                    MShiftName = unitOfWork.ShiftRepository.GetById(v.Monitor?.TKS_ID)?.TKS_SHIFT_NAME?.Trim() ?? "",

                    SUserCode = v.Snapshot?.TKMH_USER_CODE?.Trim() ?? "",
                    SFirstName = v.Snapshot?.TKMH_FIRST_NAME?.Trim() ?? "",
                    SLastName = v.Snapshot?.TKUM_LAST_NAME?.Trim() ?? "",
                    SUserAD = v.Snapshot?.TKMH_LOGIN?.Trim() ?? "",
                    //STel = v.Snapshot == null ? null : v.Snapshot?.TKU_TEL?.Trim() ?? "",
                    SCompanyName = unitOfWork.CompanyRepository.GetById(v?.Snapshot?.TKC_ID)?.TKC_COM_NAME?.Trim() ?? "",
                    SDepartmentName = unitOfWork.DepartmentRepository.GetById(v.Snapshot?.TKD_ID)?.TKD_DEPARTMENT_NAME?.Trim() ?? "",
                    SAreaName = v.Snapshot == null ? null : unitOfWork.AreaRepository.GetById(v.Snapshot?.TKA_ID)?.TKA_AREA_NAME?.Trim() ?? "",
                    SPlantName = v.Snapshot == null ? null : unitOfWork.PlantRepository.GetById(v.Snapshot?.TKP_ID)?.TKP_PLANT_NAME?.Trim() ?? "",
                    SShiftName = v.Snapshot == null ? null : unitOfWork.ShiftRepository.GetById(v.Snapshot?.TKS_ID)?.TKS_SHIFT_NAME?.Trim() ?? "",


                    TUserCode = v.User?.TKU_USER_CODE?.Trim(),
                    TFirstName = v.User?.TKU_FIRST_NAME?.Trim(),
                    TLastName = v.User?.TKU_LAST_NAME?.Trim() ?? "",
                    //TTel = v.User == null ? null : v.User?.TKU_TEL?.Trim() ?? "",
                    TUserAD = v.User == null ? null : v.User?.TKU_LOGIN?.Trim() ?? "",
                    TCompanyName = v.User?.TKPI_MASTER_COMPANY.TKC_COM_NAME?.Trim(),
                    TDepartmentName = v.User?.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME?.Trim(),
                    TAreaName = v.User == null ? null : v.User?.TKPI_MASTER_AREA?.TKA_AREA_NAME?.Trim() ?? "",
                    TPlantName = v.User == null ? null : v.User?.TKPI_MASTER_PLANT?.TKP_PLANT_NAME?.Trim() ?? "",
                    TShiftName = v.User == null ? null : v.User?.TKPI_MASTER_SHIFT?.TKS_SHIFT_NAME?.Trim() ?? "",
                    TUserRoleId = v.User?.TKR_ID.ToString()?.Trim() ?? defaultRoleId
                }).Where(c => c.IsShowRow).ToList();

             }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
            }
            return query;
        }

        public ReturnValue Search(ref UsersViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                var sFirstName = pModel.sFirstName == null ? "" : pModel.sFirstName.ToUpper();
                var sLastName = pModel.sLastName == null ? "" : pModel.sLastName.ToUpper();
                var sAreaId = pModel.sAreaId;
                var sDepartmentId = pModel.sDepartmentId;
                var sPlantId = pModel.sPlantId;
                var sShiftId = pModel.sShiftId;
                var sCompanyId = pModel.sCompanyId;
                var sEmployeeCode = pModel.sEmployeeCode;
                var sRoleId = pModel.sRoleId;
                var sStatus = pModel.sStatus == null ? "" : pModel.sStatus.ToUpper();
                var empCodeList = pModel.EmployeeCodeList;

                //unitOfWork.Fetch();

                var query = unitOfWork.UserRepository.GetAll().Select(v => new UsersViewModel_SearchData
                {
                    Id = v.TKU_ID,
                    dUserCode = v.TKU_USER_CODE,
                    dFirstName = v.TKU_FIRST_NAME,
                    dLastName = v.TKU_LAST_NAME,
                    dDepartmentId = v.TKD_ID,
                    dShiftId = v.TKS_ID,
                    dRoleId = v.TKR_ID,
                    dPlantId = v.TKP_ID,
                    dCompanyId = v.TKC_ID,
                    dAreaId = v.TKA_ID,
                    dStatus = v.TKU_STATUS,
                    Tel = v.TKU_TEL,
                    CompanyName = v.TKPI_MASTER_COMPANY.TKC_COM_NAME,
                    DepartmentName = v.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME,
                    AreaName = v.TKPI_MASTER_AREA?.TKA_AREA_NAME,
                    PlantName = v.TKPI_MASTER_PLANT?.TKP_PLANT_NAME,
                    ShiftName = v.TKPI_MASTER_SHIFT?.TKS_SHIFT_NAME,
                    UserRoleName = v.TKPI_ROLE.TKR_NAME,
                    Status = v.TKU_STATUS
                });

                if (!string.IsNullOrWhiteSpace(sEmployeeCode))
                {
                    query = query.Where(p => p.dUserCode.ToUpper().Contains(sEmployeeCode));
                }

                if (!string.IsNullOrWhiteSpace(sFirstName))
                    query = query.Where(p => p.dFirstName.ToUpper().Contains(sFirstName));

                if (!string.IsNullOrWhiteSpace(sLastName))
                    query = query.Where(p => p.dLastName.ToUpper().Contains(sLastName));

                if (!string.IsNullOrWhiteSpace(sShiftId))
                    query = query.Where(p => p.dShiftId == sShiftId);

                if (!string.IsNullOrWhiteSpace(sAreaId))
                    query = query.Where(p => p.dAreaId == sAreaId);

                if (!string.IsNullOrWhiteSpace(sPlantId))
                    query = query.Where(p => p.dPlantId == sPlantId);

                if (!string.IsNullOrWhiteSpace(sDepartmentId))
                    query = query.Where(p => p.dDepartmentId == sDepartmentId);

                if (!string.IsNullOrWhiteSpace(sCompanyId))
                    query = query.Where(p => p.dCompanyId == sCompanyId);

                if (!string.IsNullOrWhiteSpace(sRoleId))
                    query = query.Where(p => p.dRoleId == sRoleId);

                if (!string.IsNullOrEmpty(sStatus))
                    query = query.Where(p => p.dStatus.ToUpper().Contains(sStatus));

                if(empCodeList.Count > 0)
                {
                    query = query.Where(p => !empCodeList.Any(c => c == p.dUserCode)).ToList();
                }


                if (query != null)
                {

                    pModel.sSearchData = query.ToList();
                    rtn.Status = true;
                    rtn.Message = ConstantPrm.MessageSuccess;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data is null";
                }
                

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public UsersViewModel_Detail Get(string pUserId)
        {
            UsersViewModel_Detail model = new UsersViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pUserId) == false)
                {
                    var query = unitOfWork.UserRepository.GetById(pUserId);
        
                    model.UsersID = query.TKU_ID;
                    model.FirstNameTH = query.TKU_FIRST_NAME;
                    model.LastNameTH = query.TKU_LAST_NAME;
                    model.EmployeeID = query.TKU_USER_CODE;
                    model.DepartmentId = query.TKD_ID;
                    model.CompanyId = query.TKC_ID;
                    model.PlantId = query.TKP_ID;
                    model.AreaId = query.TKA_ID;
                    model.ShiftId = query.TKS_ID;
                    model.RoleId = query.TKR_ID;
                    model.Status = query.TKU_STATUS;
                    model.Tel = query.TKU_TEL;
                           
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string GetKPIIndicators(string pRoleID)
        {
            string menuJSON = string.Empty;
            StringBuilder _html = new StringBuilder();
            try
            {

                var query = unitOfWork.RoleIndicatorRepository.GetAll(roleIndicatorData => roleIndicatorData.TKR_ID == pRoleID).OrderBy(c => c.TKPI_INDICATORS_NAME.TKIN_NO).ToList();
             
                if (query != null)
                {
                    foreach (var j in query)
                    {
                        //var strMenu = String.Format(ParentMenuFormat, j.kpi.TKIN_NO + "." + j.kpi.TKIN_NAME.Trim(), "", j.roleIndicator?.TKRK_INDICATOR_NO, "", "null");
                        var strMenu = String.Format(ParentMenuFormat, j.TKPI_INDICATORS_NAME.TKIN_NO + "." + j.TKPI_INDICATORS_NAME.TKIN_NAME.Trim(), "", j.TKPI_INDICATORS_NAME.TKIN_ID, "", "null");
                        _html.Append(strMenu + ",");
                    }

                    menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
                }

                return menuJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static string GetAreaTree(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        var query = unitOfWork.RoleDataRepository.GetAll(data => data.TKR_ID == pRoleID).GroupBy(c => c.TKA_ID).Select(c => c.First()).OrderBy(c => c.TKA_ID).ToList();

        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.TKPI_MASTER_AREA.TKA_AREA_NAME.Trim(), "", j.TKPI_MASTER_AREA.TKA_ID, "", "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static string GetDepartmentTree(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        var query = unitOfWork.RoleDataRepository.GetAll(data => data.TKR_ID == pRoleID).GroupBy(c => c.TKD_ID).Select(c => c.First()).OrderBy(c => c.TKD_ID).ToList();

        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.TKPI_MASTER_DEPARTMENT.TKD_LEVEL == 1 ? j.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME.Trim() + " (ALL)" : j.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME.Trim(), "", j.TKPI_MASTER_DEPARTMENT.TKD_ID, "", "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static string GetPlantTree(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        var query = unitOfWork.RoleDataRepository.GetAll(data => data.TKR_ID == pRoleID).GroupBy(c => c.TKP_ID).Select(c => c.First()).OrderBy(c => c.TKP_ID).ToList();

        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, j.TKPI_MASTER_PLANT.TKP_PLANT_NAME.Trim(), "", j.TKPI_MASTER_PLANT.TKP_ID, "", "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static string GetShiftTree(int pRoleID)
        //{
        //    string menuJSON = string.Empty;
        //    StringBuilder _html = new StringBuilder();
        //    try
        //    {
        //        //if (String.IsNullOrEmpty(pRoleID) == false)
        //        //{
        //        var query = unitOfWork.RoleDataRepository.GetAll(data => data.TKR_ID == pRoleID).GroupBy(c => c.TKS_ID).Select(c => c.First()).OrderBy(c => c.TKS_ID).ToList();

        //        if (query != null)
        //        {
        //            foreach (var j in query)
        //            {
        //                var strMenu = String.Format(ParentMenuFormat, "("+ j.TKPI_MASTER_PLANT.TKP_PLANT_NAME + ") " +  j.TKPI_MASTER_SHIFT.TKS_SHIFT_NAME.Trim(), "", j.TKPI_MASTER_SHIFT.TKS_ID, "", "null");


        //                _html.Append(strMenu + ",");
        //            }

        //            menuJSON = "[" + _html.ToString().TrimEnd(',') + "]";
        //        }

        //        return menuJSON;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static string GetMenu(string userRole)
        {
            string json = "";
            try
            {
                var idRole = (userRole);
                using (MFKPIEntities context = new MFKPIEntities())
                {
                    List<TKPI_MENU> menuList = new List<TKPI_MENU>();

                    var allMenu = (from rl in context.TKPI_ROLE
                                   where rl.TKR_ID == idRole
                                   join rm in context.TKPI_ROLE_MENU on rl.TKR_ID equals rm.TKR_ID into viewR
                                   from r in viewR.DefaultIfEmpty()
                                   join m in context.TKPI_MENU on r.TKRM_MENU equals m.MEU_ROW_ID into viewM
                                   from vm in viewM.DefaultIfEmpty()
                                       //where vm.MEU_ACTIVE.Equals("ACTIVE") && vm.MEU_PARENT_ID.Equals("#") && vm.MEU_CONTROL_TYPE.Equals("MENU")
                                   where vm.MEU_ACTIVE.Equals("ACTIVE")
                                   select vm).ToList();

                    List<MenuDetail.MenuRootObject> menuRoot = new List<MenuDetail.MenuRootObject>();

                    if (allMenu != null)
                    {
                        var mainMenu = allMenu.Where(item => String.IsNullOrEmpty(item.MEU_PARENT_ID));

                        if (mainMenu != null)
                        {
                            //int iMax = 1;
                            foreach (var item in mainMenu.OrderBy(x => int.Parse(x.MEU_LEVEL)).ThenBy(y => int.Parse(y.MEU_LIST_NO)))
                            {

                                MenuDetail.Node nDetail = new MenuDetail.Node();
                                List<MenuDetail.Node> lstDetail = new List<MenuDetail.Node>();
                                lstDetail = GetSubMenu(allMenu.Where(c => !string.IsNullOrWhiteSpace(c.MEU_PARENT_ID)).ToList(), item.MEU_ROW_ID);

                                //var index = menuRoot.FindIndex(a => a.text == item.LNG_DESCRIPTION);
                                //menuRoot[index].nodes.Add(lstDetail);

                                menuRoot.Add(new MenuDetail.MenuRootObject() { menu_id = item.MEU_ROW_ID, text = item.LNG_DESCRIPTION, nodes = lstDetail, tags = new List<string>(new string[] { "M" }) });
                            }
                        }
                    }
                    json = new JavaScriptSerializer().Serialize(menuRoot);


                }
                return json;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return json;
            }
        }

        public static List<MenuDetail.Node> GetSubMenu(List<TKPI_MENU> menuLst, string parentID)
        {
            List<MenuDetail.Node> listDetail = new List<MenuDetail.Node>();
            MenuDetail.Node nDetail = new MenuDetail.Node();

            try
            {
                using (MFKPIEntities context = new MFKPIEntities())
                {
                    List<TKPI_MENU> menuList = new List<TKPI_MENU>();

                    var query = menuLst.Where(c => !string.IsNullOrWhiteSpace(c.MEU_PARENT_ID) && c.MEU_PARENT_ID.Equals(parentID)).ToList();
                    //var query = (from m in menuLst
                    //             where m.MEU_PARENT_ID.Where(c => c.para).Equals(parentID)
                    //             select m).ToList();

                    if (query != null)
                    {

                        foreach (var iSub in query.OrderBy(x => int.Parse(x.MEU_LEVEL)).ThenBy(y => int.Parse(y.MEU_LIST_NO)))
                        {
                            nDetail = new MenuDetail.Node() {
                                text = iSub.LNG_DESCRIPTION,
                                menu_id = iSub.MEU_ROW_ID,
                                tags = new List<string>(new string[] { "M" })
                            };
   
                            //nDetail.nodes = new List<MenuDetail.Node>();
                            listDetail.Add(nDetail);

                            var querySub = (from m in menuLst
                                            where m.MEU_PARENT_ID.Equals(iSub.MEU_ROW_ID)
                                            select m).ToList();

                            if (querySub != null)
                            {
                                if (querySub.Count > 0) nDetail.nodes = new List<MenuDetail.Node>();
                                foreach (var iSub2 in querySub.OrderBy(s => s.MEU_LIST_NO))
                                {
                                    MenuDetail.Node nSubDetail = new MenuDetail.Node() {
                                        text = iSub2.LNG_DESCRIPTION,
                                        menu_id = iSub2.MEU_ROW_ID,
                                        tags = new List<string>(new string[] { "F" }),
                                        backColor = "#E0E9F8"
                                    };
              
                                    nDetail.nodes.Add(nSubDetail);
                                }
                            }
                        }
                    }
                }
                return listDetail;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return listDetail;
            }
        }


    }
}