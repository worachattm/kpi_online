﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.KPIDetailAreaReportViewModel;
using static KPI_ONLINE.ViewModel.VpAndPomKPIResultReportViewModel;

namespace KPI_ONLINE.Models
{
    public class KPIDetailAreaReportServiceModel : BaseClass
    {

        public void GetSearchResult(KPIDetailAreaReportViewModel Model)
        {
            var sp_detail_by_area = new List<SP_DETAIL_BY_AREA>();
            using (MFKPIEntities context = new MFKPIEntities())
            {
                using (context.Database.Connection)
                {
                    context.Database.Connection.Open();
                    DbCommand cmd = context.Database.Connection.CreateCommand();
                    cmd.CommandText = "SP_DETAIL_BY_AREA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new OracleParameter("AREANAME", ""));
                    cmd.Parameters.Add(new OracleParameter("TYPE", Model.Criteria.PeriodType));
                    cmd.Parameters.Add(new OracleParameter("YEAR", Model.Criteria.Year));
                    cmd.Parameters.Add(new OracleParameter("PERIOD", Model.Criteria.TargetPeriod));
                    //cmd.Parameters.Add(new OracleParameter("TYPE", "Year"));
                    //cmd.Parameters.Add(new OracleParameter("YEAR", 2019));
                    //cmd.Parameters.Add(new OracleParameter("PERIOD", "1"));
                    OracleParameter oraP = new OracleParameter();
                    oraP.ParameterName = "p_recordset";
                    oraP.OracleDbType = OracleDbType.RefCursor;
                    oraP.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(oraP);
                    using (var reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            var schema = reader.GetSchemaTable();
                            sp_detail_by_area = reader.MapToList<SP_DETAIL_BY_AREA>() ?? new List<SP_DETAIL_BY_AREA>();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    var group = sp_detail_by_area.Where(x => Const.User.AuthPlantNameLs.Contains(x.TKID_PLANT)).GroupBy(x => new { x.TKID_DEPARTMENT, x.TKID_AREA, x.TKID_PLANT });
                    var allPlant = unitOfWork.PlantRepository.GetAll();
                    var allShift = unitOfWork.ShiftRepository.GetAll();
                    foreach (var sP_DETAILs in group)
                    {
                        var plant = new Plant();
                        Model.Result.PlantList.Add(plant);
                        plant.PlantName = sP_DETAILs.Key.TKID_DEPARTMENT + " > " + sP_DETAILs.Key.TKID_AREA + " > <strong style='font-size:1.25rem'>" + sP_DETAILs.Key.TKID_PLANT + "</strong>";
                        plant.PlantNameEx = sP_DETAILs.Key.TKID_DEPARTMENT + " > " + sP_DETAILs.Key.TKID_AREA + " > " + sP_DETAILs.Key.TKID_PLANT;
                        plant.PlantID = allPlant.FirstOrDefault(x => x.TKP_PLANT_NAME == sP_DETAILs.Key.TKID_PLANT && sP_DETAILs.Key.TKID_DEPARTMENT == x.TKPI_MASTER_AREA.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME)?.TKP_ID;

                        plant.ShiftHeaderID = allShift.Where(c => c.TKP_ID == plant.PlantID && Const.User.AuthShiftIdLs.Contains(c.TKS_ID)).Select(x => x.TKS_SHIFT_NAME).ToList();
                        var groupByIndicator = sP_DETAILs.OrderBy(x => x.TKI_INDI_NO).GroupBy(x => x.TKI_INDI_NO);
                        foreach (var data in groupByIndicator)
                        {
                            var shiftData = new ShiftData();
                            var kpi = unitOfWork.KpiNameRepository.GetAll(x => x.TKIN_NO == data.Key)?.FirstOrDefault();
                            if (!Const.User.AuthKpiIdLs.Contains(kpi?.TKIN_ID))
                            {
                                continue;
                            }
                            var sh = data.Where(x => plant.ShiftHeaderID.Contains(x.TKID_SHIFT)).ToList();
                            shiftData.kpiName = kpi?.TKIN_NAME;
                            shiftData.kpiNo = data.Key?.ToString();

                            //shiftData.avgScore_A = sh.FirstOrDefault(x => x.TKID_SHIFT == "A")?.AVG_SCORE;
                            //shiftData.scaleColor_A = sh.FirstOrDefault(x => x.TKID_SHIFT == "A")?.SCALE_COLOR_CODE;
                            //shiftData.avgScore_B = sh.FirstOrDefault(x => x.TKID_SHIFT == "B")?.AVG_SCORE;
                            //shiftData.scaleColor_B = sh.FirstOrDefault(x => x.TKID_SHIFT == "B")?.SCALE_COLOR_CODE;
                            //shiftData.avgScore_C = sh.FirstOrDefault(x => x.TKID_SHIFT == "C")?.AVG_SCORE;
                            //shiftData.scaleColor_C = sh.FirstOrDefault(x => x.TKID_SHIFT == "C")?.SCALE_COLOR_CODE;
                            //shiftData.avgScore_D = sh.FirstOrDefault(x => x.TKID_SHIFT == "D")?.AVG_SCORE;
                            //shiftData.scaleColor_D = sh.FirstOrDefault(x => x.TKID_SHIFT == "D")?.SCALE_COLOR_CODE;

                            var filterData = sh.FirstOrDefault(x => x.TKID_SHIFT == "A");
                            shiftData.avgScore_A = filterData?.AVG_SCORE;
                            shiftData.scaleColor_A = filterData?.SCALE_COLOR_CODE;
                            shiftData.scaleColorName_A = filterData?.SCALE_COLOR == "YELLOW" || filterData?.SCALE_COLOR == "LIGHT GREEN" ? "#000000" : "#ffffff";

                            filterData = sh.FirstOrDefault(x => x.TKID_SHIFT == "B");
                            shiftData.avgScore_B = filterData?.AVG_SCORE;
                            shiftData.scaleColor_B = filterData?.SCALE_COLOR_CODE;
                            shiftData.scaleColorName_B = filterData?.SCALE_COLOR == "YELLOW" || filterData?.SCALE_COLOR == "LIGHT GREEN" ? "#000000" : "#ffffff";

                            filterData = sh.FirstOrDefault(x => x.TKID_SHIFT == "C");
                            shiftData.avgScore_C = filterData?.AVG_SCORE;
                            shiftData.scaleColor_C = filterData?.SCALE_COLOR_CODE;
                            shiftData.scaleColorName_C = filterData?.SCALE_COLOR == "YELLOW" || filterData?.SCALE_COLOR == "LIGHT GREEN" ? "#000000" : "#ffffff";

                            filterData = sh.FirstOrDefault(x => x.TKID_SHIFT == "D");
                            shiftData.avgScore_D = filterData?.AVG_SCORE;
                            shiftData.scaleColor_D = filterData?.SCALE_COLOR_CODE;
                            shiftData.scaleColorName_D = filterData?.SCALE_COLOR == "YELLOW" || filterData?.SCALE_COLOR == "LIGHT GREEN" ? "#000000" : "#ffffff";

                            if (shiftData.avgScore_A.HasValue ||
                                shiftData.avgScore_B.HasValue ||
                                shiftData.avgScore_C.HasValue ||
                                shiftData.avgScore_D.HasValue
                                )
                            {
                                plant.DetailList.Add(shiftData);
                            }
                        }
                    } 
                } 
            }
            Model.IndicatorNameList = IndicatorModel.GetIndicatorName(); 
        }

    }
}