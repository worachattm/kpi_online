﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.Utilitie;
using KPI_ONLINE.ViewModel;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.VpAndPomKPIResultReportViewModel;

namespace KPI_ONLINE.Models
{
    public class ReportMapServiceModel : BaseClass
    {
        private readonly string PinFormat =
                                    "\"{0}\": {{" +
                                        "\"plant\": \"{1}\"," +
                                        "\"shift\": \"{2}\"," +
                                        "\"pinbutton\": \"{3}\", " +
                                        "\"coords\": {{" +
                                            "\"lat\": \"{4}\"," +
                                            "\"long\": \"{5}\"" +
                                        "}}" +
                                    "}}";
        public class Coordinate
        {
            [JsonProperty(PropertyName = "difflat")]
            public float diffLat { get; set; }
            [JsonProperty(PropertyName = "difflong")]
            public float diffatLong { get; set; }
            [JsonProperty(PropertyName = "coords")]
            public List<Pin> Coords { get; set; } = new List<Pin>();
        }

        public class Pin
        {
            [JsonProperty(PropertyName = "plant")]
            public string Plant { get; set; }
            [JsonProperty(PropertyName = "shift")]
            public string Shift { get; set; }
            [JsonProperty(PropertyName = "pinbutton")]
            public string Pinbutton { get; set; }
            [JsonProperty(PropertyName = "coords")]
            public Coords Coords { get; set; } = new Coords();
        }
        public class Coords
        {
            [JsonProperty(PropertyName = "lat")]
            public float Lat { get; set; }
            [JsonProperty(PropertyName = "long")]
            public float Long { get; set; }
        }

        public void GetPlantKPIDetail(PlantKPIDetail model, string plant, string shift, string kpiNo, string targetperiod, string year, string periodtype)
        {
            var Model = new ReportMapViewModel()
            {
                Criteria = new ReportMapViewModel.SearchCriteria()
                {
                    Year = year,
                    KPIIndicator = kpiNo,
                    TargetPeriod = targetperiod,
                    PeriodType = periodtype
                }

            };
            model.Plant = plant;
            model.IsIndividual = unitOfWork.IndicatorRepository.GetAll().FirstOrDefault(x => x.TKI_INDI_NO.ToString() == kpiNo && x.TKI_INDI_YEAR.ToString() == year)?.TKI_INDI_TYPE.ToUpper() == "INDIVIDUAL";

            if (model.IsIndividual)
            {
                var querydata = CallStore(Model, "sp_maps_view");
                querydata = querydata?.Where(x => x.TKID_PLANT == plant && x.TKID_SHIFT == shift).ToList() ?? new List<sp_executive_summary_Result>();
                foreach (var data in querydata)
                {

                    model.DetailList.Add(new PlantKPIDetail.Detail(data.TKID_SHIFT, data.TKID_FIRST_NAME, data.TKID_LAST_NAME, data.SCALE_COLOR_CODE, data.SET_INDEX_COLOR.ToString(), data.KPI_SCALE, data.ACTUAL_SCORE.ToString(), data.NOTE));
                }
            }
            else
            {
                var querydata = CallStore(Model, "sp_maps_view");
                querydata = querydata?.Where(x => x.TKID_PLANT == plant && x.TKID_SHIFT != null).ToList();
                foreach (var data in querydata ?? new List<sp_executive_summary_Result>())
                {

                    model.DetailList.Add(new PlantKPIDetail.Detail(data.TKID_SHIFT, data.SCALE_COLOR_CODE, data.SET_INDEX_COLOR.ToString(), data.KPI_SCALE, data.ACTUAL_SCORE.ToString(), data.NOTE));
                }
            }

        }

        public void GetMapConfig(MapConfig model)
        {
            var allindi = unitOfWork.KpiNameRepository.GetAll();
            var allconfig = unitOfWork.MapConfigRepository.GetAll();
            foreach (var indi in allindi)
            {
                var config = indi.TKPI_MAP_CONFIG.FirstOrDefault();
                model.IndicatorNameList.Add(new MapConfig.IndicatorName(
                    config?.TMC_ROW_ID
                    , config?.TMC_KPI_NAME_ID ?? indi.TKIN_ID
                    , indi.TKIN_NO
                    , indi.TKIN_NAME
                    , config?.TMC_VIIEW_MODE ?? "SHIFT"
                    , config?.TMC_NOTE
                    ));
            }
        }
        public ReturnValue SaveMapConfig(MapConfig model)
        {
            var rtn = new ReturnValue();
            try
            {
                var allconfig = unitOfWork.MapConfigRepository.GetAll();
                foreach (var data in model.IndicatorNameList)
                {
                    if (!data.Id.IsNullOrEmpty())
                    {
                        var update = unitOfWork.MapConfigRepository.GetById(data.Id);
                        update.TMC_KPI_NAME_ID = data.kpiNameID;
                        update.TMC_VIIEW_MODE = data.ViewMode;
                        update.TMC_NOTE = data.Note;
                        unitOfWork.MapConfigRepository.Update(update);
                    }
                    else
                    {
                        var insert = new TKPI_MAP_CONFIG();
                        insert.TMC_ROW_ID = Guid.NewGuid().ToString().Replace("-", "");
                        insert.TMC_KPI_NAME_ID = data.kpiNameID;
                        insert.TMC_VIIEW_MODE = data.ViewMode;
                        insert.TMC_NOTE = data.Note;
                        unitOfWork.MapConfigRepository.Insert(insert);
                    }
                }
                unitOfWork.Commit();
                rtn.Message = "Change Config Success";
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
                rtn.Message = ex.GetaAllMessages().LastOrDefault();
                rtn.Status = false;
            }
            return rtn;
        }

        public Bitmap CombineImages(ReportMapViewModel Model)
        {

            //change the location to store the final image.
            var mapPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "Image", "PlantMapKIP.png");
            string finalImage = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "reportImage" + Guid.NewGuid().ToString().Replace("-", "") + ".jpg");
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "coordinateExcel.json");
            List<int> imageHeights = new List<int>();
            imageHeights.Sort();
            Image mapImg = Image.FromFile(mapPath);
            Bitmap comBindBitMap = new Bitmap(mapImg.Width, mapImg.Height);
            Graphics g = Graphics.FromImage(comBindBitMap);
            List<Pin> pins = Model.listpin;
            g.Clear(Color.White);
            {
                g.DrawImage(mapImg, new Point(0, 0));
                foreach (var pin in pins)
                {
                    var brush = new SolidBrush(ColorTranslator.FromHtml(pin.Pinbutton));
                    if (Model.viewType == "SHIFT")
                        g.FillEllipse(brush, pin.Coords.Lat, pin.Coords.Long, 20, 20);
                    else
                        g.FillEllipse(brush, pin.Coords.Lat, pin.Coords.Long, 33, 33);
                    brush.Dispose();
                    //var pinPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "Image", pin.Pinbutton);
                    //Image pinImg = Image.FromFile(pinPath);
                    //Bitmap objBitmap = new Bitmap(pinImg, new Size(33, 33));
                    //g.DrawImage(objBitmap, new Point(pin.Coords.Lat.StringToInt() ?? 0, pin.Coords.Long.StringToInt() ?? 0));

                }
                //mapImg.Dispose();
            }
            //g.Dispose();
            comBindBitMap.Save(finalImage, System.Drawing.Imaging.ImageFormat.Jpeg);
            //comBindBitMap.Dispose();
            return comBindBitMap;
            //imageLocation.Image = Image.FromFile(finalImage);
        }
        public string CreateExcel(ReportMapViewModel model)
        {
            var queryData = GetSearchResult(model, true);
            var reportImage = CombineImages(model);
            string file_name = string.Format("KPIMapReport_{0}.xlsx", DateTime.Now.ToString("dd-MMM-yy hhmmssfff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", file_name);

            string file_path_folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload");
            bool exists = System.IO.Directory.Exists(file_path_folder);
            if (!exists)
            {
                Directory.CreateDirectory(file_path_folder);
            }
            FileInfo excelFile = new FileInfo(file_path);
            var columnHeader = new List<string>() {
                "KPI No.", "Process/Movements/Offsites", "A", "B", "C", "D"
            };

            var listid = new List<string>();
            Color blueHeader = ColorTranslator.FromHtml("#2489c5");
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                ExcelWorksheet ws = excel.Workbook.Worksheets.Add("Map");
                ws.View.ShowGridLines = true;
                ws.PrinterSettings.Orientation = eOrientation.Landscape;
                var picture = ws.Drawings.AddPicture("Map", reportImage);
                picture.SetPosition(50, 50);
                var col = 3;
                var kpiname = model.Criteria?.KPIIndicator?.IsNullOrEmpty() ?? false ? null : unitOfWork.KpiNameRepository.GetById(model.Criteria.KPIIndicator);
                var result = "";
                if (model.Criteria.PeriodType == "Year")
                {

                    result = "Yearly report of " + model.Criteria.Year;
                }
                else if (model.Criteria.PeriodType == "Monthly")
                {
                    result = "Monthly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
                }
                else if (model.Criteria.PeriodType == "Quater")
                {
                    result = "Quarterly report of " + model.Criteria.Year + " - " + model.Criteria.TargetPeriod;
                }


                WriteExcelDetail(ref ws, 1, 1, 1, 18, isMerge: true, isBold: true, backgroundColor: "#9bc3e6",
                    value: result, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                WriteExcelDetail(ref ws, 2, 1, 2, 18, isMerge: true, isBold: true, backgroundColor: "#bdd7ee", value: kpiname == null ? "" : ("KPI-" + kpiname.TKIN_NO + " : " + kpiname.TKIN_NAME), isTable: true);
                foreach (var header in model.DepartmentHeader)
                {
                    WriteExcelDetail(ref ws, 40, col, 40, col++, isBold: true, value: header, backgroundColor: "#D3E2F9", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                }
                col = 1;
                WriteExcelDetail(ref ws, 41, col, 41, col + 1, isMerge: true, isBold: true, value: "Actual Score", isTable: true);
                col = 3;
                foreach (var header in model.DepartmentHeader)
                {
                    var data = model.DepartmentData.FirstOrDefault(x => x.TKID_DEPARTMENT == header);
                    WriteExcelDetail(ref ws, 41, col, 41, col++, isBold: true, value: data?.ACTUAL_SCORE, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                }
                col = 1;
                WriteExcelDetail(ref ws, 42, col, 42, col + 1, isMerge: true, isBold: true, value: "Scale Display", isTable: true);
                col = 3;
                foreach (var header in model.DepartmentHeader)
                {
                    var data = model.DepartmentData.FirstOrDefault(x => x.TKID_DEPARTMENT == header);
                    WriteExcelDetail(ref ws, 42, col, 42, col++, isBold: true, value: data?.KPI_SCALE, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                }
                var querydata = CallStore(model, "sp_maps_view");

                var row = 43;
                var groupdata = queryData?.Where(x => !x.TKID_SHIFT.IsNullOrEmpty()).OrderBy(x => x.TKID_AREA).ThenBy(x => x.TKID_SHIFT).GroupBy(x => x.TKID_PLANT);
                if (groupdata?.Any() ?? false)
                    foreach (var plant in groupdata)
                    {
                        var isIndividual = kpiname.TKPI_INDICATORS.FirstOrDefault(x => x.TKI_INDI_YEAR.ToString() == model.Criteria.Year)?.TKI_INDI_TYPE?.ToUpper() == "INDIVIDUAL";
                        row++;

                        WriteExcelDetail(ref ws, row, 1, row, isIndividual ? 5 + 7 : 3 + 7, isMerge: true, backgroundColor: "#2489c5", isBold: true, value: plant.Key, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        row++;
                        col = 1;
                        WriteExcelDetail(ref ws, row, col, row, col++, isBold: true, backgroundColor: "#D3E2F9", value: "Shift", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        if (isIndividual)
                        {
                            WriteExcelDetail(ref ws, row, col, row, col++, isBold: true, backgroundColor: "#D3E2F9", value: "First Name", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            WriteExcelDetail(ref ws, row, col, row, col++, isBold: true, backgroundColor: "#D3E2F9", value: "Last Name", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        }
                        WriteExcelDetail(ref ws, row, col, row, col++, isBold: true, backgroundColor: "#D3E2F9", value: "KPI Scale", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        WriteExcelDetail(ref ws, row, col, row, col++, isBold: true, backgroundColor: "#D3E2F9", value: "KPI Display", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        WriteExcelDetail(ref ws, row, col, row, col + 6, isMerge: true, isBold: true, backgroundColor: "#D3E2F9", value: "Note", horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                        row++;
                        foreach (var data in plant)
                        {
                            col = 1;
                            WriteExcelDetail(ref ws, row, col, row, col++, value: data.TKID_SHIFT, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            if (isIndividual)
                            {
                                WriteExcelDetail(ref ws, row, col, row, col++, value: data.TKID_FIRST_NAME, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                                WriteExcelDetail(ref ws, row, col, row, col++, value: data.TKID_LAST_NAME, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            }
                            WriteExcelDetail(ref ws, row, col, row, col++, backgroundColor: data.SCALE_COLOR_CODE, isBold: true, value: data.ACTUAL_SCORE, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            WriteExcelDetail(ref ws, row, col, row, col++, value: data.KPI_SCALE, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            WriteExcelDetail(ref ws, row, col, row, col + 6, isMerge: true, value: data.NOTE, horizontal: ExcelHorizontalAlignment.Center, isTable: true);
                            row++;
                        }
                    }
                excel.Save();
            }

            return file_path;
        }

        private void WriteExcelDetail(ref ExcelWorksheet ws, int fromRow, int fromCol, int toRow, int toCol, bool isWordWarp = false, bool isMerge = false, bool isBold = false, bool isUnderLine = false,
        object value = null, float fontSize = 9, ExcelHorizontalAlignment horizontal = ExcelHorizontalAlignment.General, ExcelVerticalAlignment vertical = ExcelVerticalAlignment.Center,
        Color? fontColor = null, string backgroundColor = null, bool isTable = true, string NumberFormat = "")
        {
            ws.Cells[fromRow, fromCol, toRow, toCol].Merge = isMerge;
            ws.Cells[fromRow, fromCol, toRow, toCol].Value = value;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Bold = isBold;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.HorizontalAlignment = horizontal;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.VerticalAlignment = vertical;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.UnderLine = isUnderLine;
            ws.Cells[fromRow, fromCol, toRow, toCol].Style.WrapText = isWordWarp;
            if (!NumberFormat.IsNullOrEmpty())
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Numberformat.Format = NumberFormat;
            }
            if (fontSize > 0)
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Size = fontSize;
            }

            if (fontColor.HasValue)
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Color.SetColor(fontColor.Value);
            }
            else
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Font.Color.SetColor(Color.Black);
            }

            if (isTable)
            {
                AllBorders(ws.Cells[fromRow, fromCol, toRow, toCol].Style.Border);
            }

            if (!string.IsNullOrWhiteSpace(backgroundColor))
            {
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[fromRow, fromCol, toRow, toCol].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(backgroundColor));
            }

            ws.Row(fromRow).Height = 20;
        }
        internal void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = ExcelBorderStyle.Thin;
            _borders.Left.Style = ExcelBorderStyle.Thin;
            _borders.Right.Style = ExcelBorderStyle.Thin;
            _borders.Bottom.Style = ExcelBorderStyle.Thin;
        }
        public List<sp_executive_summary_Result> GetSearchResult(ReportMapViewModel Model, bool isExcel = false)
        {
            var jsonPath = isExcel ? "coordinateExcel.json" : "coordinate.json";
            string root_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", jsonPath);
            StringBuilder stringBuilder = new StringBuilder();
            var querydata = CallStore(Model, "sp_maps_view");
            try
            {
                using (StreamReader r = new StreamReader(root_path))
                {
                    string json = r.ReadToEnd();
                    Coordinate cooodinate = JsonConvert.DeserializeObject<Coordinate>(json);
                    var items = cooodinate.Coords;
                    Model.viewType = unitOfWork.KpiNameRepository.GetAll().FirstOrDefault(x => x.TKIN_NO.ToString() == Model.Criteria.KPIIndicator)?.TKPI_MAP_CONFIG.FirstOrDefault()?.TMC_VIIEW_MODE;
                    if (Model.viewType == "SHIFT")
                    {
                        items = items.Where(x => x.Shift != null).ToList();
                    }
                    else
                    {
                        items = items.Where(x => x.Shift == null).ToList();
                    }
                    var pinShift = new List<Pin>();
                    foreach (var item in items)
                    {
                        var data = querydata?.FirstOrDefault(x => x.TKID_SHIFT.IsNullOrEmpty() && x.TKID_PLANT == item.Plant);
                        if (Model.viewType == "SHIFT")
                        {
                            data = querydata?.FirstOrDefault(x => x.TKID_SHIFT == "A" && x.TKID_PLANT == item.Plant);
                            stringBuilder.Append(string.Format(PinFormat, item.Plant + "A", item.Plant, "A", data?.SCALE_COLOR_CODE ?? "#babfc7", item.Coords.Lat, item.Coords.Long) + ",");
                            var itemA = new Pin();
                            itemA.Pinbutton = data?.SCALE_COLOR_CODE ?? "#babfc7";
                            itemA.Coords.Lat = item.Coords.Lat;
                            itemA.Coords.Long = item.Coords.Long;
                            pinShift.Add(itemA);

                            data = querydata?.FirstOrDefault(x => x.TKID_SHIFT == "B" && x.TKID_PLANT == item.Plant);
                            stringBuilder.Append(string.Format(PinFormat, item.Plant + "B", item.Plant, "B", data?.SCALE_COLOR_CODE ?? "#babfc7", item.Coords.Lat + cooodinate.diffLat, item.Coords.Long) + ",");
                            var itemB = new Pin();
                            itemB.Pinbutton = data?.SCALE_COLOR_CODE ?? "#babfc7";
                            itemB.Coords.Lat = item.Coords.Lat + cooodinate.diffLat;
                            itemB.Coords.Long = item.Coords.Long;
                            pinShift.Add(itemB);

                            data = querydata?.FirstOrDefault(x => x.TKID_SHIFT == "C" && x.TKID_PLANT == item.Plant);
                            stringBuilder.Append(string.Format(PinFormat, item.Plant + "C", item.Plant, "C", data?.SCALE_COLOR_CODE ?? "#babfc7", item.Coords.Lat, item.Coords.Long + cooodinate.diffatLong) + ",");
                            var itemC = new Pin();
                            itemC.Pinbutton = data?.SCALE_COLOR_CODE ?? "#babfc7";
                            itemC.Coords.Lat = item.Coords.Lat;
                            itemC.Coords.Long = item.Coords.Long + cooodinate.diffatLong;
                            pinShift.Add(itemC);

                            data = querydata?.FirstOrDefault(x => x.TKID_SHIFT == "D" && x.TKID_PLANT == item.Plant);
                            stringBuilder.Append(string.Format(PinFormat, item.Plant + "D", item.Plant, "D", data?.SCALE_COLOR_CODE ?? "#babfc7", item.Coords.Lat + cooodinate.diffLat, item.Coords.Long + cooodinate.diffatLong) + ",");
                            var itemD = new Pin();
                            itemD.Pinbutton = data?.SCALE_COLOR_CODE ?? "#babfc7";
                            itemD.Coords.Lat = item.Coords.Lat + cooodinate.diffLat;
                            itemD.Coords.Long = item.Coords.Long + cooodinate.diffatLong;
                            pinShift.Add(itemD);
                        }
                        else
                        {
                            stringBuilder.Append(string.Format(PinFormat, item.Plant + item.Shift, item.Plant, item.Shift, data?.SCALE_COLOR_CODE ?? "#babfc7", item.Coords.Lat, item.Coords.Long) + ",");
                            item.Pinbutton = data?.SCALE_COLOR_CODE ?? "#babfc7";
                        }
                    }
                    items.AddRange(pinShift);
                    Model.listpin = items;
                }
                Model.DepartmentHeader = Const.User.AuthDepartmentNameLs;
                Model.DepartmentData = querydata?.Where(x => x.TKID_AREA == null).ToList() ?? new List<sp_executive_summary_Result>();
                Model.Json = stringBuilder.ToString().Trim(',');
            }
            catch (Exception ex)
            {

            }
            return querydata;
        }
        private List<sp_executive_summary_Result> CallStore(ReportMapViewModel Model, string storename)
        {
            var querydata = new List<sp_executive_summary_Result>();
            using (MFKPIEntities context = new MFKPIEntities())
            {
                using (context.Database.Connection)
                {//Unwind Delete Normal
                    context.Database.Connection.Open();
                    DbCommand cmd = context.Database.Connection.CreateCommand();
                    cmd.CommandText = storename;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new OracleParameter("KPI_NO", Model.Criteria.KPIIndicator));
                    cmd.Parameters.Add(new OracleParameter("p_Department", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Area", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Plant", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Shift", "A,B,C,D"));
                    cmd.Parameters.Add(new OracleParameter("p_Type", Model.Criteria.PeriodType));
                    cmd.Parameters.Add(new OracleParameter("p_Year", Model.Criteria.Year.StringToInt()));
                    cmd.Parameters.Add(new OracleParameter("p_Period", Model.Criteria.TargetPeriod));
                    OracleParameter oraP = new OracleParameter();
                    oraP.ParameterName = "p_recordset";
                    oraP.OracleDbType = OracleDbType.RefCursor;
                    oraP.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(oraP);
                    using (var reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            var sh = reader.GetSchemaTable();
                            querydata = reader.MapToList<sp_executive_summary_Result>();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            return querydata;
        }
    }
}