﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public enum ScoreColor
    {
        bad = 0,
        warning = 1,
        good = 2
    }
    public class GroupPosition
    {
        public string positionName { get; set; }
        public ScoreColor scoreColor { get; set; }
        public decimal score { get; set; }

    }
}