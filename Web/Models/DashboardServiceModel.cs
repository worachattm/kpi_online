﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_ONLINE.ViewModel
{
    public class DashboardViewModel
    {
        public class Graph
        {
            public Graph()
            {
            }

            public String Title { get; set; }
            public String SubTitle { get; set; }
            public List<String> Categories { get; set; }
            public String XLable { get; set; }
            public String YLable { get; set; }
            public String YLable2 { get; set; }
            public String YLable3 { get; set; }
            public List<Series> Series { get; set; }
            public List<Series> Series2 { get; set; }
            public List<Series> Series3 { get; set; }
            public double Selector { get; set; }
            public PlotLines PlotLines { get; set; }
            public String other { get; set; }
        }

        public class Series
        {
            public Series()
            {
                this.showInLegend = true;
                this.marker = new Marker(false);
            }
            public String id { get; set; }
            public String name { get; set; }
            public String type { get; set; }
            public int yAxis { get; set; }
            public String color { get; set; }
            public String onSeries { get; set; }
            public List<Object> data { get; set; }
            public Tooltip tooltip { get; set; }
            public Marker marker { get; set; }
            public Boolean showInLegend { get; set; }
            public Boolean colorByPoint { get; set; }
            public string dashStyle { get; set; }
        }

        public class Tooltip
        {
            public Tooltip(Boolean enabled, String valueSuffix)
            {
                this.enabled = enabled;
                this.valueSuffix = valueSuffix;
            }
            public Boolean enabled { get; set; }
            public String valueSuffix { get; set; }
        }

        public class Marker
        {
            public Marker(Boolean enabled)
            {
                this.enabled = enabled;
            }
            public Boolean enabled { get; set; }
            public int radius { get; set; }
        }

        public class Comment
        {
            public Comment(double x, String title, String text)
            {
                this.x = x;
                this.title = title;
                this.text = text;
            }
            public double x { get; set; }
            public String title { get; set; }
            public String text { get; set; }
        }

        public class Pie
        {
            public Pie(String name, Decimal y)
            {
                this.name = name;
                this.y = y;
            }
            public String name { get; set; }
            public Decimal y { get; set; }
        }

        public class PlotLines
        {
            public String id { get; set; }
            public String color { get; set; }
            public String width { get; set; }
            public String value { get; set; }
        }
    }
}