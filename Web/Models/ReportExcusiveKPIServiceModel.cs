﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using KPI_ONLINE.ViewModel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using Web.Utilitie;
using static KPI_ONLINE.ViewModel.ReportExcrusiveKPIViewModel;

namespace KPI_ONLINE.Models
{
    public class ReportExcusiveKPIServiceModel : BaseClass
    {
        public void testc(ReportExcrusiveKPIViewModel Model)
        {
            var ReportConfig = unitOfWork.ReportConfigRepository.GetAll().ToList();
            var con = ReportConfig.Last();
            var ReportDisplay = unitOfWork.ReportDisplayRepository.GetAll(x => x.TKRC_ID == con.TKRC_ID).ToList();

            var listDefault = new List<string>() { "ASM", "LTO", "LTR", "JG 7-8", "JG 9-12" };
            var count = 1;

            listDefault.ForEach(c =>
            {
                var configUse = ReportDisplay.Where(report => report.TKRD_COLUMN.Equals(c, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                var isEnabled = true;
                if (configUse != null)
                {
                    Model.Template.UserConfig.Add(new HedgeDealSummaryColumnDetail
                    {
                        ColumnName = configUse.TKRD_COLUMN,
                        DisplayName = configUse.TKRD_DISPLAY_NAME,
                        FieldName = configUse.TKRD_COLUMN,
                        IsEnabled = isEnabled,
                        Order = configUse.TKRD_ORDER
                    });
                }

                Model.Template.SystemConfig.Add(new HedgeDealSummaryColumnDetail
                {
                    ColumnName = c,
                    DisplayName = c,
                    FieldName = c,
                    IsEnabled = isEnabled,
                    Order = count

                });


                count++;
            });

            Model.Template.SystemConfig = Model.Template.SystemConfig.OrderBy(c => c.Order).ToList();
            Model.Template.UserConfig = Model.Template.UserConfig.OrderBy(c => c.Order).ToList();
            var ReportWeight = unitOfWork.ReportWeightRepository.GetAll(x => x.TKRC_ID == con.TKRC_ID).ToList();
        }
        public List<SelectListItem> GetDepartmentCode()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.DepartmentRepository.GetAll();
                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKD_ID.ToString(), Text = item.TKD_DEPARTMENT_NAME });
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;
            }
        }
        public List<SelectListItem> GetAreaCode(string departmentId = "")
        {

            List<SelectListItem> rtn = new List<SelectListItem>();

            try
            {
                var query = unitOfWork.AreaRepository.GetAll(c => c.TKD_ID == departmentId);

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKA_ID.ToString(), Text = item.TKA_AREA_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;
            }
        }
        public List<SelectListItem> GetPlantCode(string areaId = "")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.PlantRepository.GetAll(c => c.TKA_ID == areaId);

                if (query != null)
                {

                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKP_ID.ToString(), Text = item.TKP_PLANT_NAME });
                    }
                }


                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }
        public List<SelectListItem> GetShiftCode(int plantId = 0)
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                var query = unitOfWork.ShiftRepository.GetAll();
                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.TKS_ID.ToString(), Text = item.TKS_SHIFT_NAME });
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {
                ls.InsertErrorLog(log, ex, ERROR_SEVERITY.LOW, Const.User != null ? (string.IsNullOrEmpty(Const.User.UserName) ? "" : Const.User.UserName) : "", ex.Message);
                return rtn;

            }
        }

        public void GetSearchResult(ReportExcrusiveKPIViewModel Model, bool isIncludeSoftKpi = false)
        {
            Model.Result = new List<SearchResult>();

            var querydata = new List<sp_executive_summary_Result>();
            using (MFKPIEntities context = new MFKPIEntities())
            {
                var year = 0m;
                decimal.TryParse(Model.Criteria.Year, out year);
                var tkrcId = context.TKPI_REPORT_CONFIG.Where(w => w.TKRC_YEAR == year).Select(s => s.TKRC_ID).FirstOrDefault();
                var listConfigCol = context.TKPI_REPORT_DISPLAY.OrderBy(x => x.TKRD_ORDER).Where(w => w.TKRC_ID == tkrcId).Select(s => new { s.TKRD_COLUMN, s.TKRD_DISPLAY_NAME }).ToList();
                using (context.Database.Connection)
                {//Unwind Delete Normal
                    context.Database.Connection.Open();
                    DbCommand cmd = context.Database.Connection.CreateCommand();
                    if (isIncludeSoftKpi)
                    {
                        cmd.CommandText = "sp_dashboard";
                    }
                    else
                    {
                        cmd.CommandText = "sp_executive_summary";
                    }
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new OracleParameter("p_Department", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Area", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Plant", ""));
                    cmd.Parameters.Add(new OracleParameter("p_Shift", "A,B,C,D"));
                    cmd.Parameters.Add(new OracleParameter("p_Type", Model.Criteria.PeriodType));
                    cmd.Parameters.Add(new OracleParameter("p_Year", Model.Criteria.Year.StringToInt()));
                    cmd.Parameters.Add(new OracleParameter("p_Period", Model.Criteria.TargetPeriod));
                    OracleParameter oraP = new OracleParameter();
                    oraP.ParameterName = "p_recordset";
                    oraP.OracleDbType = OracleDbType.RefCursor;
                    oraP.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(oraP);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var testSchema = reader.GetSchemaTable();
                        try
                        {
                            querydata = reader.MapToList<sp_executive_summary_Result>();
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                }

                //querydata = querydata.Where(x => x.TKID_DEPARTMENT == "MMVP").ToList();
                var dep = querydata?.GroupBy(x => x.TKID_DEPARTMENT).Where(x => Const.User.AuthDepartmentNameLs.Contains(x.Key)).ToList();
                dep?.ForEach(de =>
                {
                    SearchResult searchResult = new SearchResult();
                    searchResult.DepartMent = de.Key;
                    searchResult.DepartMentID = unitOfWork.DepartmentRepository.GetAll(d => d.TKD_DEPARTMENT_NAME == de.Key).FirstOrDefault()?.TKD_ID;
                    var are = de?.GroupBy(x => x.TKID_AREA).Where(x => Const.User.AuthAreaNameLs.Contains(x.Key) || x.Key.IsNullOrEmpty()).ToList();
                    are?.ForEach(ar =>
                    {
                        Area area = new Area();
                        area.AreaName = ar.Key;
                        area.AreaID = unitOfWork.AreaRepository.GetAll(d => d.TKA_AREA_NAME == ar.Key && d.TKD_ID == searchResult.DepartMentID).FirstOrDefault()?.TKA_ID;
                        var pln = ar?.GroupBy(x => x.TKID_PLANT).Where(x => Const.User.AuthPlantNameLs.Contains(x.Key) || x.Key.IsNullOrEmpty()).ToList();
                        pln?.ForEach(pn =>
                        {
                            Plant plant = new Plant();
                            plant.PlantName = pn.Key;
                            plant.PlantID = unitOfWork.PlantRepository.GetAll(d => d.TKP_PLANT_NAME == pn.Key && d.TKA_ID == area.AreaID).FirstOrDefault()?.TKP_ID;
                            var shf = pn?.GroupBy(x => x.TKID_SHIFT).ToList();
                            shf?.ForEach(sh =>
                            {
                                Shift shifts = new Shift();
                                shifts.ShiftName = sh.Key;
                                if (area.AreaName.IsNullOrEmpty())
                                {
                                    shifts.ShiftID = searchResult.DepartMentID;
                                }
                                else if (plant.PlantName.IsNullOrEmpty())
                                {
                                    shifts.ShiftID = area.AreaID;
                                }
                                else if (shifts.ShiftName.IsNullOrEmpty())
                                {
                                    shifts.ShiftID = plant.PlantID;
                                }
                                else
                                {

                                    shifts.ShiftID = unitOfWork.ShiftRepository.GetAll(d => d.TKS_SHIFT_NAME == sh.Key && d.TKP_ID == plant.PlantID).FirstOrDefault()?.TKS_ID;
                                    if (!Const.User.AuthShiftIdLs.Contains(shifts.ShiftID))
                                    {
                                        goto _continue;
                                    }

                                }
                                sh?.ToList().ForEach(detail =>
                                {

                                    var detal = new KPIDetail();
                                    if (!Const.User.AuthKpiIdLs.Contains(getKpiNameIdFormKpiNo(detail.TKI_INDI_NO) ?? ""))
                                    {
                                        goto _continueKPI;
                                    }
                                    shifts.Detail.Add(detal);
                                    detal.NO = detail.TKI_INDI_NO;
                                    detal.Area = detail.TKI_INDI_NAME;
                                    detal.ActualScore = detail.ACTUAL_SCORE?.ToString("0.##");
                                    detal.Scale1 = detail.SCALE_1_DISPLAY;
                                    detal.Scale2 = detail.SCALE_2_DISPLAY;
                                    detal.Scale3 = detail.SCALE_3_DISPLAY;
                                    detal.Scale4 = detail.SCALE_4_DISPLAY;
                                    detal.Scale5 = detail.SCALE_5_DISPLAY;

                                    detal.KpiScore = detail.SET_INDEX_COLOR ?? 0;
                                    detal.ColorCode = detail.SCALE_COLOR_CODE;

                                    detal.WeightASM = detail.ASM?.ToString();
                                    detal.WeightLTO = detail.LTO?.ToString();
                                    detal.WeightLTR = detail.LTR?.ToString();
                                    detal.WeightJG7_8 = detail.JG78?.ToString();
                                    detal.Weight9_12 = detail.JG912?.ToString();
                                    _continueKPI:;
                                });
                                shifts.Detail = shifts.Detail.OrderBy(x => x.NO).ToList();
                                if (shifts.Detail.Count > 0)
                                {
                                    plant.ShiftList.Add(shifts);
                                }
                                _continue:;
                            });
                            if (plant.ShiftList.Count > 0)
                            {
                                area.PlantList.Add(plant);
                            }
                        });
                        if (area.PlantList.Count > 0)
                        {
                            searchResult.AreaList.Add(area);
                        }
                    });
                    if (searchResult.AreaList.Count > 0)
                    {
                        Model.Result.Add(searchResult);
                    }
                });
                Model.CountWeightCol = listConfigCol.Count;
                foreach (var columnS in listConfigCol)
                {
                    Model.ColumnWeight.Add(columnS.TKRD_COLUMN, columnS.TKRD_DISPLAY_NAME);
                }

                List<KPIDetail> softDataList = new List<KPIDetail>();
                //---------------Summary Weight
                var configID = unitOfWork.ReportConfigRepository.GetAll().Where(d => d.TKRC_YEAR.ToString() == DateTime.Now.ToString("yyyy")).FirstOrDefault().TKRC_ID;

                var weightList = unitOfWork.ReportWeightRepository.GetAll(d => d.TKRC_ID == configID).ToList();
                decimal? asmSum = 0;
                decimal? ltoSum = 0;
                decimal? ltrSum = 0;
                decimal? jg7_8Sum = 0;
                decimal? jg9_12Sum = 0;
                foreach (var s in weightList)
                {
                    asmSum += s.TKCW_ASM_VAL ?? 0; 
                    ltoSum += s.TKCW_LTO_VAL ?? 0;
                    ltrSum += s.TKCW_LTR_VAL ?? 0;
                    jg7_8Sum += s.TKCW_JG_7_8 ?? 0;
                    jg9_12Sum += s.TKCW_JG_9_12 ?? 0;
                }
                KPIDetail weighItem = new KPIDetail();
                weighItem.NO = null;
                weighItem.Area = "";
                weighItem.ActualScore = "Summary";
                weighItem.Scale1 = "";
                weighItem.Scale2 = "";
                weighItem.Scale3 = "";
                weighItem.Scale4 = "";
                weighItem.Scale5 = "";
                weighItem.WeightASM = asmSum == 0 ? "" : asmSum.ToString();
                weighItem.WeightLTO = ltoSum == 0 ? "" : ltoSum.ToString();
                weighItem.WeightLTR = ltrSum == 0 ? "" : ltrSum.ToString();
                weighItem.WeightJG7_8 = jg7_8Sum == 0 ? "" : jg7_8Sum.ToString();
                weighItem.Weight9_12 = jg9_12Sum == 0 ? "" : jg9_12Sum.ToString();

                softDataList.Add(weighItem);
                asmSum = 0;
                ltoSum = 0;
                ltrSum = 0;
                jg7_8Sum = 0;
                jg9_12Sum = 0;
                //-------------------------------------------
                //---------------Soft kpi list
                var softData = unitOfWork.ReportSoftKpiRepository.GetAll().Where(d => d.TSK_YEAR.ToString() == DateTime.Now.ToString("yyyy")).ToList();  
                foreach (var s in softData)
                {
                    KPIDetail item = new KPIDetail();
                    item.NO = s.TSK_NO;
                    item.Area = s.TSK_NAME;
                    item.Scale1 = s.TSK_KPI1;
                    item.Scale2 = s.TSK_KPI2;
                    item.Scale3 = s.TSK_KPI3;
                    item.Scale4 = s.TSK_KPI4;
                    item.Scale5 = s.TSK_KPI5;
                    item.WeightASM = s.TSK_ASM.ToString();
                    item.WeightLTO = s.TSK_LTO.ToString();
                    item.WeightLTR = s.TSK_LTR.ToString();
                    item.WeightJG7_8 = s.TSK_JG7_8.ToString();
                    item.Weight9_12 = s.TSK_JG9_12.ToString();
                    softDataList.Add(item);

                    asmSum += s.TSK_ASM ?? 0;
                    ltoSum += s.TSK_LTO ?? 0;
                    ltrSum += s.TSK_LTR ?? 0;
                    jg7_8Sum += s.TSK_JG7_8 ?? 0;
                    jg9_12Sum += s.TSK_JG9_12 ?? 0;
                }
                //-------------------------------------------
                //---------------Summary Soft kpi
                KPIDetail softItem = new KPIDetail();
                softItem.NO = null;
                softItem.Area = "";
                softItem.ActualScore = "Summary";
                softItem.Scale1 = "";
                softItem.Scale2 = "";
                softItem.Scale3 = "";
                softItem.Scale4 = "";
                softItem.Scale5 = "";
                softItem.WeightASM = asmSum == 0 ? "" : asmSum.ToString();
                softItem.WeightLTO = ltoSum == 0 ? "" : ltoSum.ToString();
                softItem.WeightLTR = ltrSum == 0 ? "" : ltrSum.ToString();
                softItem.WeightJG7_8 = jg7_8Sum == 0 ? "" : jg7_8Sum.ToString();
                softItem.Weight9_12 = jg9_12Sum == 0 ? "" : jg9_12Sum.ToString();

                softDataList.Add(softItem);

                Model.KPISoftList = softDataList; 
            }
            

            Model.IndicatorNameList = IndicatorModel.GetIndicatorName();
        }

        private string getKpiNameIdFormKpiNo(decimal? tKI_INDI_NO)
        {
            return unitOfWork.KpiNameRepository.GetAll(x => x.TKIN_NO == tKI_INDI_NO).FirstOrDefault()?.TKIN_ID;
        }

        public ConfigKPIIndator GetConfigKPIIndator(decimal? year)
        {
            ConfigKPIIndator configKPIIndator = new ConfigKPIIndator();
            using (MFKPIEntities context = new MFKPIEntities())
            {
                var displayConfig = (from TRD in context.TKPI_REPORT_DISPLAY
                                     join TRC in context.TKPI_REPORT_CONFIG on TRD.TKRC_ID equals TRC.TKRC_ID
                                     where TRC.TKRC_YEAR == year
                                     orderby TRD.TKRD_ORDER
                                     select TRD).ToList();

                var indicatorConfig = (from TRC in context.TKPI_REPORT_CONFIG
                                       join TRW in context.TKPI_REPORT_WEIGHT on TRC.TKRC_ID equals TRW.TKRC_ID
                                       join TIN in context.TKPI_INDICATORS_NAME on TRW.TKIN_ID equals TIN.TKIN_ID
                                       where TRC.TKRC_YEAR == year
                                       select new
                                       {
                                           TRW.TKCW_ID,
                                           TRW.TKIN_ID,
                                           TIN.TKIN_NAME,
                                           TRW.TKCW_ASM_VAL,
                                           TRW.TKCW_LTO_VAL,
                                           TRW.TKCW_LTR_VAL,
                                           TRW.TKCW_JG_7_8,
                                           TRW.TKCW_JG_9_12
                                       }).ToList();

                var reportConfig = (from TRC in context.TKPI_REPORT_CONFIG
                                    select new { TRC.TKRC_YEAR, TRC.TKRC_CREATED_BY, TRC.TKRC_CREATED_DATE, TRC.TKRC_ID, TRC.TKRC_REPORT_NAME }
                                    ).ToList();

                var count = 0;
                var ListDefault = new List<string>() { "ASM", "LTO", "LTR", "JG 7-8", "JG 9-12" };

                ListDefault.ForEach(c =>
                {
                    var configUse = displayConfig.Where(report => report.TKRD_COLUMN.Equals(c, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    var isEnabled = true;
                    if (configUse != null)
                    {
                        DisplayConfig displayConfigN = new DisplayConfig()
                        {
                            ColumnName = configUse.TKRD_COLUMN,
                            DisplayName = configUse.TKRD_DISPLAY_NAME,
                            FieldName = configUse.TKRD_COLUMN,
                            Order = configUse.TKRD_ORDER,
                            IsEnabled = isEnabled
                        };
                        configKPIIndator.TableDisplayConfig.Add(displayConfigN);
                    }

                    count++;
                });


                foreach (var indicator in indicatorConfig)
                {
                    IndicatorConfig indicatorN = new IndicatorConfig()
                    {
                        TKWCId = indicator.TKCW_ID,
                        TKINId = indicator.TKIN_ID,
                        TKINName = indicator.TKIN_NAME,
                        ASM = indicator.TKCW_ASM_VAL,
                        LTO = indicator.TKCW_LTO_VAL,
                        LTR = indicator.TKCW_LTR_VAL,
                        JG7_8 = indicator.TKCW_JG_7_8,
                        JG9_12 = indicator.TKCW_JG_9_12
                    };
                    configKPIIndator.TableIndicatorConfig.Add(indicatorN);
                }

                foreach (var report in reportConfig)
                {
                    ReportConfig reportConfigN = new ReportConfig()
                    {
                        TKRCId = report.TKRC_ID,
                        ReportName = report.TKRC_REPORT_NAME,
                        Year = report.TKRC_YEAR,
                        CreateBy = report.TKRC_CREATED_BY,
                        CreateDate = report.TKRC_CREATED_DATE,
                        CreateDateEx = report.TKRC_CREATED_DATE.ToString("dd/MM/yyyy")
                    };
                    configKPIIndator.TableReportConfig.Add(reportConfigN);
                }
                configKPIIndator.TableReportConfig = configKPIIndator.TableReportConfig.OrderBy(c => c.Year).ToList();
                configKPIIndator.Template = new HedgeDealSummaryExcelTemplate();

                /////////////////////
                var countConfig = 1;
                ListDefault.ForEach(c =>
                {
                    var configUse = displayConfig.Where(report => report.TKRD_COLUMN.Equals(c, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    var isEnabled = true;
                    if (configUse != null)
                    {
                        configKPIIndator.Template.UserConfig.Add(new HedgeDealSummaryColumnDetail
                        {
                            ColumnName = configUse.TKRD_COLUMN,
                            DisplayName = configUse.TKRD_DISPLAY_NAME,
                            FieldName = configUse.TKRD_COLUMN,
                            IsEnabled = isEnabled,
                            Order = configUse.TKRD_ORDER
                        });
                    }

                    configKPIIndator.Template.SystemConfig.Add(new HedgeDealSummaryColumnDetail
                    {
                        ColumnName = c,
                        DisplayName = c,
                        FieldName = c,
                        IsEnabled = isEnabled,
                        Order = countConfig

                    });


                    count++;
                });

                configKPIIndator.Template.SystemConfig = configKPIIndator.Template.SystemConfig.OrderBy(c => c.Order).ToList();
                configKPIIndator.Template.UserConfig = configKPIIndator.Template.UserConfig.OrderBy(c => c.Order).ToList();
                /////////////////////
                configKPIIndator.SoftKPIs = unitOfWork.ReportSoftKpiRepository.GetAll(x => x.TSK_YEAR == year).OrderBy(x=>x.TSK_NO).ToList();

            }
            return configKPIIndator;
        }

        public bool SaveConfigKPIIndicator(decimal year, ConfigKPIIndator configKPIIndator)
        {
            try
            {
                using (MFKPIEntities context = new MFKPIEntities())
                {
                    string tkrcId = (from TRC in context.TKPI_REPORT_CONFIG
                                     where TRC.TKRC_YEAR == year
                                     select TRC.TKRC_ID).FirstOrDefault();

                    if (!tkrcId.IsNullOrEmpty())
                    {
                        //var result = context.Database.ExecuteSqlCommand("DELETE FROM TKPI_REPORT_DISPLAY WHERE TKRC_ID = '" + tkrcId + "'");
                        unitOfWork.ReportDisplayRepository.DeleteAll(w => w.TKRC_ID == tkrcId);
                        //TABLE KPI DISPLAY
                        List<TKPI_REPORT_DISPLAY> listReportDisplay = new List<TKPI_REPORT_DISPLAY>();
                        foreach (var trd in configKPIIndator.TableDisplayConfig)
                        {
                            TKPI_REPORT_DISPLAY TRD = new TKPI_REPORT_DISPLAY()
                            {
                                TKRC_ID = tkrcId,
                                TKRD_COLUMN = trd.ColumnName,
                                TKRD_DISPLAY_NAME = trd.DisplayName,
                                TKRD_ORDER = trd.Order,
                                TKRD_ID = Guid.NewGuid().ToString("N")
                            };
                            listReportDisplay.Add(TRD);
                        }
                        unitOfWork.ReportDisplayRepository.Insert(listReportDisplay);
                        //TABLE KPI WEIGHT
                        foreach (var trw in configKPIIndator.TableIndicatorConfig)
                        {
                            TKPI_REPORT_WEIGHT TRW = new TKPI_REPORT_WEIGHT()
                            {
                                TKCW_ID = trw.TKWCId,
                                TKRC_ID = tkrcId,
                                TKIN_ID = trw.TKINId,
                                TKCW_ASM_VAL = trw.ASM,
                                TKCW_LTO_VAL = trw.LTO,
                                TKCW_LTR_VAL = trw.LTR,
                                TKCW_JG_7_8 = trw.JG7_8,
                                TKCW_JG_9_12 = trw.JG9_12
                            };
                            //unitOfWork.ReportWeightRepository.Update(TRW);
                            //var obj = unitOfWork.ReportWeightRepository.GetById(TRW.TKCW_ID);
                            var update = context.TKPI_REPORT_WEIGHT.Where(w => w.TKCW_ID == TRW.TKCW_ID).FirstOrDefault();
                            update.TKCW_ASM_VAL = TRW.TKCW_ASM_VAL;
                            update.TKCW_LTO_VAL = TRW.TKCW_LTO_VAL;
                            update.TKCW_LTR_VAL = TRW.TKCW_LTR_VAL;
                            update.TKCW_JG_7_8 = TRW.TKCW_JG_7_8;
                            update.TKCW_JG_9_12 = TRW.TKCW_JG_9_12;
                            context.SaveChanges();
                        }
                        unitOfWork.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool SaveSoftKpi(TKPI_REPORT_SOFT_KPI data)
        {
            try
            {
                try
                {
                    var updatedata = unitOfWork.ReportSoftKpiRepository.GetById(data.TSK_ROW_ID);
                    updatedata.TSK_NO = data.TSK_NO;
                    updatedata.TSK_NAME = data.TSK_NAME;
                    updatedata.TSK_KPI1 = data.TSK_KPI1;
                    updatedata.TSK_KPI2 = data.TSK_KPI2;
                    updatedata.TSK_KPI3 = data.TSK_KPI3;
                    updatedata.TSK_KPI4 = data.TSK_KPI4;
                    updatedata.TSK_KPI5 = data.TSK_KPI5;
                    updatedata.TSK_ASM = data.TSK_ASM;
                    updatedata.TSK_LTO = data.TSK_LTO;
                    updatedata.TSK_LTR = data.TSK_LTR;
                    updatedata.TSK_JG7_8 = data.TSK_JG7_8;
                    updatedata.TSK_JG9_12 = data.TSK_JG9_12;
                    unitOfWork.ReportSoftKpiRepository.Update(updatedata);
                }
                catch {
                    unitOfWork.ReportSoftKpiRepository.Insert(data);
                }
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool DeleteSoftKpi(string data)
        {
            try
            {
                try
                {
                    unitOfWork.ReportSoftKpiRepository.Delete(data);
                }
                catch
                {
                }
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }



        public string DeleteConfigKPIIndicator(decimal year)
        {
            var status = "";
            try
            {
                using (MFKPIEntities context = new MFKPIEntities())
                {
                    var chkDel = context.TKPI_REPORT_CONFIG.Where(w => w.TKRC_YEAR == year).Count();
                    var tkrcId = context.TKPI_REPORT_CONFIG.Where(w => w.TKRC_YEAR == year).Select(s => s.TKRC_ID).FirstOrDefault();
                    if (chkDel > 1)
                    {
                        unitOfWork.ReportConfigRepository.DeleteAll(w => w.TKRC_YEAR == year);
                        unitOfWork.ReportDisplayRepository.DeleteAll(w => w.TKRC_ID == tkrcId);
                        unitOfWork.ReportWeightRepository.DeleteAll(w => w.TKRC_ID == tkrcId);
                        status = "SUCCESS";
                    }
                    else
                    {
                        status = "Report row more than 1";
                    }
                }
            }
            catch (Exception ex)
            {
                status = ex.Message.ToString();
                return status;
            }
            return status;
        }

    }
}