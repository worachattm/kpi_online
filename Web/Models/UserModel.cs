﻿using KPI_ONLINE.DAL.Entity;
using KPI_ONLINE.DAL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Web.Models
{
    public class UserModel
    {
        public string EmployeeId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string CompanyName { get; set; }
        public string ImagePath { get; set; }

        public List<string> AuthDepartmentIdLs { get; set; } = new List<string>();
        public List<string> AuthAreaIdLs { get; set; } = new List<string>();
        public List<string> AuthPlantIdLs { get; set; } = new List<string>();
        public List<string> AuthShiftIdLs { get; set; } = new List<string>();
        public List<string> AuthKpiIdLs { get; set; } = new List<string>();

        public List<string> AuthDepartmentNameLs { get; set; } = new List<string>();
        public List<string> AuthAreaNameLs { get; set; } = new List<string>();
        public List<string> AuthPlantNameLs { get; set; } = new List<string>();
        public List<string> AuthShifNameLs { get; set; } = new List<string>();

        public void AuthenView(ref UnitOfWork unitOfWork)
        {
            AuthDepartmentIdLs = new List<string>();
            AuthPlantIdLs = new List<string>();
            AuthShiftIdLs = new List<string>();
            AuthAreaIdLs = new List<string>();
            AuthKpiIdLs = new List<string>();

            AuthDepartmentNameLs = new List<string>();
            AuthPlantNameLs = new List<string>();
            AuthShifNameLs = new List<string>();
            AuthAreaNameLs = new List<string>();

            var roleView = unitOfWork.RoleDataRepository.GetAll(c => c.TKR_ID == this.RoleId);
            var kpiView = unitOfWork.RoleIndicatorRepository.GetAll(c => c.TKR_ID == this.RoleId);
            Func<TKPI_ROLE_DATA, bool> expressionDepartment = c => (this.AuthDepartmentIdLs.Count > 0 && this.AuthDepartmentIdLs.Contains(c.TKR_ID) || this.AuthDepartmentIdLs.Count == 0);
            Func<TKPI_ROLE_DATA, bool> expressionPlant = c => (this.AuthPlantIdLs.Count > 0 && this.AuthPlantIdLs.Contains(c.TKR_ID) || this.AuthPlantIdLs.Count == 0);
            Func<TKPI_ROLE_DATA, bool> expressionArea = c => (this.AuthAreaIdLs.Count > 0 && this.AuthAreaIdLs.Contains(c.TKR_ID) || this.AuthAreaIdLs.Count == 0);
            Func<TKPI_ROLE_DATA, bool> expressionShift = c => (this.AuthShiftIdLs.Count > 0 && this.AuthShiftIdLs.Contains(c.TKR_ID) || this.AuthShiftIdLs.Count == 0);

            Func<TKPI_ROLE_INDICATORS, bool> expressionKpi = c => this.AuthKpiIdLs.Count > 0 && this.AuthKpiIdLs.Contains(c.TKRK_INDICATOR_NO) || this.AuthKpiIdLs.Count == 0;

            roleView.Where(expressionDepartment).GroupBy(c => c.TKD_ID).Select(c => c.Select(f => new { f.TKD_ID, f.TKPI_MASTER_DEPARTMENT.TKD_DEPARTMENT_NAME }).First()).ToList().ForEach(item =>
            {
                this.AuthDepartmentIdLs.Add(item.TKD_ID);
                this.AuthDepartmentNameLs.Add(item.TKD_DEPARTMENT_NAME);
            });

            roleView.Where(expressionArea).GroupBy(c => c.TKA_ID).Select(c => c.Select(f => new { f.TKA_ID, f.TKPI_MASTER_AREA.TKA_AREA_NAME }).First()).ToList().ForEach(item =>
            {
                this.AuthAreaIdLs.Add(item.TKA_ID);
                this.AuthAreaNameLs.Add(item.TKA_AREA_NAME);
            });

            roleView.Where(expressionPlant).GroupBy(c => c.TKP_ID).Select(c => c.Select(f => new { f.TKP_ID, f.TKPI_MASTER_PLANT.TKP_PLANT_NAME }).First()).ToList().ForEach(item =>
            {
                this.AuthPlantIdLs.Add(item.TKP_ID);
                this.AuthPlantNameLs.Add(item.TKP_PLANT_NAME);
            });
            roleView.Where(expressionShift).Where(c => (this.AuthDepartmentIdLs.Count > 0 && this.AuthDepartmentIdLs.Contains(c.TKD_ID) || this.AuthDepartmentIdLs.Count == 0)).GroupBy(c => c.TKS_ID).Select(c => c.Select(f => new { f.TKS_ID, f.TKPI_MASTER_SHIFT.TKS_SHIFT_NAME }).First()).ToList().ForEach(item =>
            {
                this.AuthShiftIdLs.Add(item.TKS_ID);
                this.AuthShifNameLs.Add(item.TKS_SHIFT_NAME);
            });



            this.AuthKpiIdLs = kpiView.Where(expressionKpi).GroupBy(c => c.TKRK_INDICATOR_NO).Select(c => c.Select(f => f.TKRK_INDICATOR_NO).First()).ToList();

        }

    }

}