﻿$(function () {
    $('#container2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'พื้นที่ให้บริการ รัศมี 200 เมตร'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}ตารางเมตร</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.1f} ตารางเมตร',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'เนื้อที่',
            colorByPoint: true,
            data: [{
                name: 'DMA1',
                y: 7376323.86738221
            }, {
                name: 'DMA2',
                y: 7376323.86738221,
                sliced: true,
                selected: true
            }, {
                name: 'DMA3',
                y: 7376323.86738221
            }, {
                name: 'DMA4',
                y: 7376323.86738221
            }, {
                name: 'DMA5',
                y: 7376323.86738221
            }, {
                name: 'DMA6',
                y: 7376323.86738221
            }, {
                name: 'DMA7',
                y: 7376323.86738221
            }, {
                name: 'DMA8',
                y: 7376323.86738221
            }]
        }]
    });
});