﻿$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'พื้นที่ให้บริการ รัศมี 100 เมตร'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}ตารางเมตร</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.1f} ตารางเมตร',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'เนื้อที่',
            colorByPoint: true,
            data: [{
                name: 'DMA1',
                y: 7376323.86738221
            }, {
                name: 'DMA2',
                y: 7376323.86738221,
                sliced: true,
                selected: true
            }, {
                name: 'DMA3',
                y: 7376323.86738221
            }, {
                name: 'DMA4',
                y: 7376323.86738221
            }, {
                name: 'DMA5',
                y: 7376323.86738221
            }, {
                name: 'DMA6',
                y: 7376323.86738221
            }, {
                name: 'DMA7',
                y: 7376323.86738221
            }, {
                name: 'DMA8',
                y: 7376323.86738221
            }]
        }]
    });

    $('#container5').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'จำนวนผู้ใช้น้ำ เปรียบเทียบกับ จำนวนหลังคาเรือน ตามขอบเขตการปกครอง'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'จำนวน',
            colorByPoint: true,
            data: [{
                name: 'จำนวนผู้ใช้น้ำ อื่นๆ',
                y: 50
            }, {
                name: 'จำนวนผู้ใช้น้ำ กปภ.',
                y: 30,
                sliced: true,
                selected: true
            }]
        }]
    });

    $('#container3').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: ['พื้นที่ให้บริการ'
            ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} ตร.ม.',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'พื้นที่',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        tooltip: {
            shared: true
        },
        series: [{
            name: 'พื้นที่ให้บริการ รัศมี 100M',
            type: 'column',
            data: [323242],
            tooltip: {
                valueSuffix: ' ตร.ม.'
            }

        }, {
            name: 'พื้นที่ให้บริการ รัศมี 200M',
            type: 'column',
            data: [646484],
            tooltip: {
                valueSuffix: ' ตร.ม.'
            }

        }, {
            name: 'พื้นที่ตามขอบเขตการปกครอง',
            type: 'column',
            data: [795344],
            tooltip: {
                valueSuffix: ' ตร.ม.'
            }

        }]
    });
});