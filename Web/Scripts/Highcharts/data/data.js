﻿$(function () {
    Highcharts.setOptions({
        colors: ['#058DC7', '#50B432', '#ED561B', '#FF4101']
    });
    var data2 = ['ส.ค. 2558', 'ก.ย. 2558', 'ต.ค. 2558', 'พ.ย. 2558', 'ธ.ค. 2558', 'ม.ค. 2559',
                'ก.พ. 2559', 'มี.ค. 2559', 'เม.ษ. 2559', 'พ.ค. 2559', 'มิ.ย. 2559', 'ก.ค. 2559'];
    //$.ajax({
    //    type: 'GET',
    //    url: 'http://nrwms.pwa.co.th/WebPortal/handler_prozy.ashx?http://nrwms.pwa.co.th/WebAPI/api/Gis/GetPipe?pwa_code=5512029&dma_no=all&flag=2',
    //    dataType: 'json',
    //    success: function (data) {
    //        data2 = data;
    //    }
    //});

    var data3 = [];
    data3[0] = {
        name: 'ปริมาณน้ำผลิตจ่ายxx',
        type: 'column',
        data: [795344, 742916, 711573, 758832, 774416, 787528, 771655, 760479, 851217, 878631, 821030, 815560],
        tooltip: {
            valueSuffix: ' ลบ.ม.'
        }
    };

    $('#container').highcharts({
        chart: {
            zoomType: 'xy'
            //events: {
            //    load: function () {
            //        var p1 = this.series[0].points[11];
            //        var p2 = this.series[1].points[11];
            //        var p3 = this.series[2].points[10];
            //        var p4 = this.series[3].points[11];
            //        this.tooltip.refresh(p1);
            //    }
            //}
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: data2,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} ลบ.ม.',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'ปริมาณน้ำ',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'อัตราน้ำสูญเสีย',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        series: [data3[0], {
            name: 'ปริมาณน้ำจำหน่าย',
            type: 'column',
            data: [646483, 616186, 575581, 597174, 589363, 615912, 586853, 581638, 680543, 666634, 638065, 596957],
            tooltip: {
                valueSuffix: ' ลบ.ม.'
            }

        }, {
            name: 'ปริมาณน้ำสูญเสีย',
            type: 'column',
            data: [148861, 126730, 135992, 161658, 185053, 171616, 184802, 178841, 170674, 211997, 182965, 218603],
            tooltip: {
                valueSuffix: ' ลบ.ม.'
            }

        }, {
            name: 'อัตราน้ำสูญเสีย',
            type: 'spline',
            yAxis: 1,
            data: [18.69, 17.03, 19.09, 21.27, 23.88, 21.78, 23.92, 23.48, 20.03, 24.10, 22.26, 26.78],
            tooltip: {
                valueSuffix: ' %'
            }
        }]
    });
});