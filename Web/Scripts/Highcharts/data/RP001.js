﻿$(function () {
    Highcharts.setOptions({
        colors: ['#058DC7', '#50B432', '#ED561B', '#FF4101']
    });
    $('#container').highcharts({
        chart: {
            zoomType: 'xy'
            //events: {
            //    load: function () {
            //        var p1 = this.series[0].points[11];
            //        var p2 = this.series[1].points[11];
            //        var p3 = this.series[2].points[10];
            //        var p4 = this.series[3].points[11];
            //        this.tooltip.refresh(p1);
            //    }
            //}
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: ['8.00', '9.00', '10.00', '11.00', '12.00', '13.00',
                '14.00', '15.00', '16.00', '17.00', '18.00', '19.00', '20.00', '21.00', '22.00', '23.00', '0.00',
                '1.00', '2.00', '3.00', '4.00', '5.00', '6.00', '7.00', '8.00'
            ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} ลบ.ม.',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'ปริมาณน้ำ',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        tooltip: {
            shared: true
        },
        series: [{
            name: 'ปริมาณน้ำจ่าย',
            type: 'spline',
            data: [0, 15, 300, 100, 50, 150, 50, 50, 50, 50, 50, 70, 180, 100, 100, 20, 20, 20, 10, 10, 20, 50, 150, 200, 200],
            tooltip: {
                valueSuffix: ' ลบ.ม.'
            }
        }]
    });
});