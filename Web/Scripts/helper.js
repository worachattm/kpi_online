﻿const msgConfirm = "Are you confirm ?";
const msgSuccess = "Successfully";
const msgError = "error";

function alertFieldRequire() {
    Swal.fire(
        'Message',
        'Please input required field !',
        'error'
    );
}

function alertSucess() {
    Swal.fire(
        'Message',
        'Successfully !',
        'success'
    );
}

function alertError() {
    Swal.fire(
        'Message',
        'Error : Please try again!',
        'error'
    );
}


function setPeriodType(PeriodType, TargetPeriod) {
    $('#' + PeriodType).on("change", function () {
        var period = document.getElementById(TargetPeriod);
        period.innerHTML = '';
        if ($(this).val() == "Monthly") {
            period.appendChild(createOption("-- Select --", ""));
            period.appendChild(createOption("January", "January"));
            period.appendChild(createOption("Febuary", "Febuary"));
            period.appendChild(createOption("March", "March"));
            period.appendChild(createOption("April", "April"));
            period.appendChild(createOption("May", "May"));
            period.appendChild(createOption("June", "June"));
            period.appendChild(createOption("July", "July"));
            period.appendChild(createOption("August", "August"));
            period.appendChild(createOption("September", "September"));
            period.appendChild(createOption("October", "October"));
            period.appendChild(createOption("November", "November"));
            period.appendChild(createOption("Decrmber", "Decrmber"));
            period.removeAttribute("disabled", "");

        }
        else if ($(this).val() == "Quater") {
            period.appendChild(createOption("-- Select --", ""));
            period.appendChild(createOption("Q1", "Q1"));
            period.appendChild(createOption("Q2", "Q2"));
            period.appendChild(createOption("Q3", "Q3"));
            period.appendChild(createOption("Q4", "Q4"));
            period.removeAttribute("disabled", "");

        }
        else {
            period.appendChild(createOption("-- Select --", ""));
            period.setAttribute("disabled", "");
        }
    });
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isNumberAndComma(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[0-9.,]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function createOption(text, value) {
    var op = document.createElement('option');
    op.value = value;
    op.innerText = text;
    return op;
}

function loading() {
    $('.loader').show();
    $('.loader-inner').loaders();
    $.blockUI({ message: null }); 
}

function hideLoading() {
    $('.loader').hide();
    //$('.loader-inner').loaders();
    $.unblockUI();
}

function successMessage(message) {
    bootbox.alert(message);
}

function getWebSitePath(appName) {
    let _location = document.location.toString();
    let applicationNameIndex = _location.indexOf('/', _location.indexOf('://') + 3);
    let applicationName = _location.substring(0, applicationNameIndex) + '/';
    //let webFolderIndex = _location.indexOf('/', _location.indexOf(applicationName) + applicationName.length);
    //let webFolderFullPath = _location.substring(0, webFolderIndex).toString().replace('/MASTER', '');
    return applicationName + appName;
}



if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fn, scope) {
        for (var i = 0, len = this.length; i < len; ++i) {
            fn.call(scope, this[i], i, this);
        }
    };
}

if (!String.prototype.trim) {
    (function () {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function () {
            return this.replace(rtrim, '');
        };
    })();
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        }
    });
}