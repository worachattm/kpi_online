﻿$(document).ready(function () {

    $(document).on('click', '.btn-edit', function () {
        loading();
    });

    $('#searchResultTable').DataTable({
        "columnDefs": [
            {
                "targets": [8],
                "orderable": false
            }
        ]
    });

    $(document).on('click', '#btn-clear-all', function () {
        Swal.fire({
            title: 'Message',
            text: "Do you want to reset ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                resetData();
            }
        });
    });

});

function getGenerateExcel() {

    let _url = $('#URL_GEN_EXCEL').val(); //, result = 
    //ForExportExcel();

    $.ajax({
        url: _url,
        type: 'POST',
        data: $('.form-information').serialize(),
        beforeSend: function () { loading(); },
        success: function (data) {
            hideLoading();
            if (data) {
                let appName = $('#AppName').val();
                let path = getWebSitePath(appName) + "/FileUpload/User/" + data + "";
                window.open(path, '_blank');
            } else {
                let html = "<div class='card-header'>";
                html += "<div class='row'>";
                html += "<div class='col-md-12'>";
                html += "<div class='alert bg-danger alert-dismissible mb-2' role='alert'>";
                html += "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
                html += "<span aria-hidden='true'>&times;</span>";
                html += "</button>";
                html += "<strong>Data not found or error server please try again</strong>.";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";

                $('#description').prepend(html);
            }
        },
        error: function (response) {
            hideLoading();

            let html = "<div class='card-header'>";
            html += "<div class='row'>";
            html += "<div class='col-md-12'>";
            html += "<div class='alert bg-danger alert-dismissible mb-2' role='alert'>";
            html += "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            html += "<span aria-hidden='true'>&times;</span>";
            html += "</button>";
            html += "<strong>"+ response.message +"</strong>.";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";

            $('#description').prepend(html);
        }
    });

}

function assignDropdown(id, data) {

    let html = ""
        , $obj = $("#" + id)
        , htmlDefault = "";

    htmlDefault = "<option value=''>"
    htmlDefault += "--- เลือกทั้งหมด ---";
    htmlDefault += "</option>";

    html = htmlDefault;

    $obj.children().remove();

    data.forEach(function (element) {
        html += "<option value='" + element.Value + "'>";
        html += element.Text;
        html += "</option>";
    });

    $obj.append(html);

    if (id == "users_Search_sAreaId") {
        $objOld = $('#users_Search_sPlantId'), $objShift = $('#users_Search_sShiftId');
        $objOld.children().remove();
        $objShift.children().remove();

        $objOld.append(htmlDefault);
        $objShift.append(htmlDefault);
    }

    if (id == "users_Search_sPlantId") {
        $objShift = $('#users_Search_sShiftId');
        $objShift.children().remove();

        $objShift.append(htmlDefault);
    }



}

function getShiftByPlantIdDDL($this) {
    try {
        let id = $($this).val() ? $($this).val() : 0;

        loading();
        //
        let URL = $('#URL_GetShift_DDL').val();
        $.getJSON(URL, { plantId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    assignDropdown("users_Search_sShiftId", data.result);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });

    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getPlantByAreaIdDDL($this) {
    try {
        let id = $($this).val() ? $($this).val() : 0;

        loading();
        //
        let URL = $('#URL_GetPlant_DDL').val();
        $.getJSON(URL, { areaId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    assignDropdown("users_Search_sPlantId", data.result);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });

    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getAreaByDepartmentIdDDL($this) {

    try {
        let id = $($this).val() ? $($this).val() : 0;
        //if ($(id).val()) {
        loading();
        let URL_GetAreaByDepartment = $('#URL_GetArea_DDL').val();
        $.getJSON(URL_GetAreaByDepartment
            , { departmentId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    assignDropdown("users_Search_sAreaId", data.result);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
        //}
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function resetData() {
    $('.form-control').val('');
    $('.ddl-reset :not(:first-child)').remove();
}

function searchUser() {
    loading();
    var $form = $(this).closest('form');
    $form.find('*[data-validation]').attr('data-validation', null);
    $('#form-information').validate().cancelSubmit = true;
    $('#form-information').submit();
}