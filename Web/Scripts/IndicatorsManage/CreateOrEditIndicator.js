﻿$(function () {

    $(document).on('click', '.btn-modal-edit' , function () {
        let id = $(this).data('id');

        let $name = $(this).data('action') == "SAVE" ? $('#modal-indicator-txt') : $('#modal-indicator-txt-' + id);
        let $no = $(this).data('action') == "SAVE" ? $('#modal-indicator-no') : $('#modal-indicator-no-' + id);

        let fileData = ($(this).data('action') == "SAVE" ? $('#modal-indicator-file').prop('files')[0] : $('#modal-indicator-file-' + id).prop('files')[0]);
        

        if ($name.val().trim() != "" && $no.val().trim() != "") {

            Swal.fire({
                title: 'Message',
                text: "Are you confirm ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B5B8',
                cancelButtonColor: '#FFA87D',
                confirmButtonText: 'Yes'
            }).then(function(result){
                if (result.value) {
                    loading();
                    //let data = {
                    //    Id: id,
                    //    Name: $name.val().trim(),
                    //    Status: "ACTIVE",
                    //    No: parseInt($no.val().trim())
                    //};

                    let formData = new FormData();
                    formData.append('Id', (id ? id : ""));
                    formData.append('Name', $name.val().trim());
                    formData.append('Status', "ACTIVE");
                    formData.append('No', parseInt($no.val().trim()));
                    formData.append('FileUpload', fileData);
                    
                    createOrEditIndicatorName(formData);
                    
                }
            });

        } else {

            if ($no.val().trim() == "") {
                $no.addClass('input-validation-error');
            }

            if ($name.val().trim() == "") {
                $name.addClass('input-validation-error');
            }

            alertFieldRequire();
        }

    });

    $("#btn-save").click(function (e) {

        let valid = $(".form-information").valid();
        let $to = $('input[name="To"]')
            , $from = $('input[name="From"]');

        if (valid && $to.val() != "" && $from.val() != "") {
            Swal.fire({
                title: 'Message',
                text: "Are you confirm ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B5B8',
                cancelButtonColor: '#FFA87D',
                confirmButtonText: 'Yes'
            }).then(function(result){
                if (result.value) {
                    loading();
                    $('.form-information').submit();
                }
            });
        } else {

            if ($to.val() == "" && $from.val() == "") {
                $to.addClass('input-validation-error');
                $from.addClass('input-validation-error');
            }
      
            alertFieldRequire();
        }
    });

    //$('#btn-modal-save').click(function () {
    //    let $no = $('#modal-indicator-no')
    //        , $name = $('#modal-indicator-txt');

    //    $no.removeClass('input-validation-error');
    //    $name.removeClass('input-validation-error');

    //    if ($name.trim().val() != "" && $no.trim().val() != "") {

    //        Swal.fire({
    //            title: 'Message',
    //            text: "Are you confirm ?",
    //            type: 'warning',
    //            showCancelButton: true,
    //            confirmButtonColor: '#00B5B8',
    //            cancelButtonColor: '#FFA87D',
    //            confirmButtonText: 'Yes'
    //        }).then(function(result){
    //            if (result.value) {
    //                loading();

    //            }
    //        });
    //    } else {

    //        if ($no.val() == "") {
    //            $no.addClass('input-validation-error');
    //        }

    //        if ($name.val() == "") {
    //            $name.addClass('input-validation-error');
    //        }

            
    //        alertFieldRequire();
    //    }
    //});

    $(document).on('change', '.ddl-color', function () {
        let id = $(this).data('id');
        let value = $(this).val().toLowerCase();
        $('#span_' + id).removeClass().addClass('tracking ' + value);
    });

    $("#From").datepicker({
        format: "MM-yyyy",
        autoclose: true,
        viewMode: "months",
        minViewMode: "months"
    }).on('changeDate',
        function (ev) {
        let $to = $('input[name="To"]')
            , $from = $('input[name="From"]');
        
        let momentString = moment(ev.date).add('11', 'months').format('MMMM-YYYY');
        $to.val(momentString);

        $to.removeClass('input-validation-error').addClass('valid');
        $from.removeClass('input-validation-error').addClass('valid');
        });

    $("#To").datepicker({
        format: "MM-yyyy",
        autoclose: true,
        viewMode: "months",
        minViewMode: "months"
    });

    $('#btn-clear-all').click(function () {
        Swal.fire({
            title: 'Message',
            text: "Do you want to reset ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                resetData();
            }
        });
    });

    $('#IndicatorNameId').change(function () {
        let idKpiName = $(this).val();
        getIndicatorNameLast(idKpiName);
    });

    //$(document).on('click', '.btn-modal-download', function () {
    //    let base64 = $(this).data('value');
    //    let fileName = $(this).data('name');
    //    window.open('data:application/octet-stream;base64,' + base64, '_blank');
    //    //window.location.href = 'data:application/octet-stream;base64,' + base64;
    //});
});

function resetData() {
    $('.reset-data-text').val('');
    $('.reset-data-number').val('');
    $('.reset-data-color').val('GRAY');

    $("span[id^='span_']").removeClass().addClass('tracking gray');
    //$('.reset-data-color').removeClass().addClass('tracking gray');
}

function getIndicatorNameLast(idKpiName) {
    try {
        loading();
        let _url = $('#url_get_last_kpi').val();
        $.getJSON(_url,
            {
                IdIndiatorName: idKpiName
            }
            ,
            function (data) {

                console.log(data);
             
                if (data.isSuccess && idKpiName && data.result) {
                    let result = data.result;
                    $('#Type').val(result.Type);
                    $('#CalculateMode').val(result.CalculateMode);
                    $('#From').val(result.PeriodFrom);
                    $('#To').val(result.PeriodTo);

                    for (let i = 0; i < result.KPIScaleList.length; i++) {

                        $('#KPIScaleList_' + i + '__FromValue').val(result.KPIScaleList[i].FromValue);
                        $('#KPIScaleList_' + i + '__ToValue').val(result.KPIScaleList[i].ToValue);
                        $('#KPIScaleList_' + i + '__DisplayName').val(result.KPIScaleList[i].DisplayName);
                    }
                }
                else
                {
                    resetData();
                    $('#IndicatorNameId').val(idKpiName);
                }
                
                hideLoading();
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getIndicatorNameList() {
    try {
        loading();
        let _url = $('#url_get_list_kpi').val();
        $.getJSON(_url
            , 
            function (data) {
                let action_edit = $('#action_edit').val();
                let $table = $('#modal-table-indicator-name'), htmlTr = '';
                let $select = $('#IndicatorNameId');
                let selectValue = $select.val();
                let htmlOption = '<option value="">-- กรุณาเลือก --</option>';
                if (data.isSuccess) {
                    data.result.forEach(function (item) {
                        
                        htmlOption += "<option value='" + item.Id + "' " + (item.Id == selectValue ? "selected" : "") +">" + item.Name + "</option>";

                        htmlTr += "<tr>";
                        htmlTr += "<td><input class='form-control' id='modal-indicator-no-" + item.Id + "' onkeypress='return isNumber(event)' type='text' value='" + item.No + "' autocomplete='off'/></td>";
                        htmlTr += "<td style='text-align: left !important;'><input class='form-control' id='modal-indicator-txt-" + item.Id + "' style='max-width:100% !important;' type='text' value='" + item.Name + "' autocomplete='off'/></td>";

                        htmlTr += "<td>";
                        htmlTr += "<div class='input-group' style='text-align:left;'>";
                        htmlTr += "<div class='custom-file'>";
                        htmlTr += "<input type='file' class='custom-file-input' id='modal-indicator-file-" + item.Id +"' name='fileToUpload' aria-describedby='inputGroupFileAddon04'>";
                        htmlTr += "<label class='custom-file-label' for='inputGroupFile04'>Browse</label>";
                        htmlTr += "</div>";
                        htmlTr += "</div>";
                        htmlTr += "</td>";


                        htmlTr += "<td>";

                        htmlTr += "<a href='data:application/octet-stream;base64,"+ item.File +"' download='"+ item.FileName +"' class='btn btn-primary btn-modal-download m-1 "+ (item.File == null ? "disabled" : "") +"'><i class='fa ft-download'></i> Download </a>";

                        if (action_edit) {
                            htmlTr += "<button type='button' class='btn btn-primary btn-modal-edit m-1' id='btn-modal-edit-" + item.Id + "' data-id=" + item.Id + "><i class='fa ft-edit'></i> Edit </button>";
                        }

                        htmlTr += "</td>";
                        htmlTr += "</tr>";
                    });
                }

                $select.empty().append(htmlOption);
                $table.find('tbody tr:not(.tr-new-item)').empty();
                $table.prepend(htmlTr);

                hideLoading();
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}


function createOrEditIndicatorName(formData) {
    let _url = $('#url_create_or_edit_kpi').val();
    
    $.ajax({
        cache: false,
        type: 'POST',
        url: _url,
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: loading(),
        success: function (data) {

            $('#modal-indicator-no').val('');
            $('#modal-indicator-txt').val('');
            getIndicatorNameList();

            if (data.result.Status) {
                alertSucess();
            } else {
                alertError();
            }

            //hideLoading();
        }, error: function (err) {
            hideLoading();
            alert('fail :' + err.message);
        }
    });
}