﻿$(function () {
    $('#table-indicator').DataTable();

    $(document).on('click', '#btn-clear-all', function () {
        Swal.fire({
            title: 'Message',
            text: "Do you want to reset ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                resetData();
            }
        });
    });

    $('#btn-modal-save').click(function () {
    
        Swal.fire({
            title: 'Message',
            text: "Are you confirm ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                loading();
                editScale();
            }
        });
        
    });

    $(document).on('click', '.btn-edit', function () {
        loading();
    });

    $(document).on('click', '.btn-delete', function () {
        let path = $(this).data('path');
        Swal.fire({
            title: 'Message',
            text: "Do you want to delete ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                loading();
                window.location.href = path;
            }
        });
    });


    $(document).on('change', '.ddl-color', function () {
        let id = $(this).data('id');
        let value = $(this).val().toLowerCase();
        $('#span_' + id).removeClass().addClass('tracking ' + value);
    });


});



function editScale() {
    let _url = $('#url_edit_scale').val();

    let list = [];

    for (let i = 0; i < 5; i++) {
        let data =
        {
            Id: $('input[name="Id[' + i + ']"]').val(),
            DisplayColor: $('select[name="DisplayColor[' + i + ']"]').val()
        }
        list.push(data);
    }

    $.ajax({
        cache: false,
        type: 'POST',
        url: _url,
        data: JSON.stringify(list),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: loading(),
        success: function (data) {
            
            hideLoading();

            if (data.result.Status) {
                alertSucess();
            } else {
                alertError();
            }

            //hideLoading();
        }, error: function (err) {
            hideLoading();
            alert('fail :' + err.message);
        }
    });
}

function resetData() {
    $('.reset-data').val('');
}

function searchResult() {
    loading();
    $('.form-information').submit();
}

