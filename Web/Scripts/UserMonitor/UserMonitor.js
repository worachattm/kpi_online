﻿$(document).ready(function () {
    //$('#searchResultTable').DataTable();
    $('#searchResultTable').DataTable({ paging: false });
    $('.import-btn').click(function () {
        let valid = true;

        if (valid) {

            Swal.fire({
                title: 'Message',
                text: "Are you confirm ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B5B8',
                cancelButtonColor: '#FFA87D',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.value) {
                    loading();
                    $('#buttonAction').val("Import");
                    //$(".form-information").data('validator').cancelSubmit = true;
                    $(".form-information").submit();
                }
            });

        } else {
            alertFieldRequire();
        }
    });

    $(".save-user-monitor").click(function () {

        $.blockUI({
            message: null,
            onBlock: function () {

                $('.loader').show();
                $('.loader-inner').loaders();

                var dt = $('#searchResultTable').DataTable();
                //console.log(dt.$('#checkbox-id').prop('checked'));

                let $departmentList = dt.$('.dep-ddl');
                let error = false;

                $('.form-control').removeClass('input-validation-error');

                $departmentList.each(function () {
                    
                    let id = $(this).data('id');

                    let $plant = dt.$('#UserMonitorDiffLs_' + id + '__mPlantName');
                    let $area = dt.$('#UserMonitorDiffLs_' + id + '__mAreaName');
                    let $shift = dt.$('#UserMonitorDiffLs_' + id + '__mShiftName');

                    let $firstName = dt.$('#UserMonitorDiffLs_' + id + '__MFirstName');
                    let $company = dt.$('#UserMonitorDiffLs_' + id + '__mCompanyName');
                    let $role = dt.$('#UserMonitorDiffLs_' + id + '__TUserRoleId');

                    if ($(this).val() == "") {
                        $(this).addClass('input-validation-error');
                        error = true;
                    }

                    if ($firstName == "") {
                        $firstName.addClass('input-validation-error');
                        error = true;
                    }

                    if ($company == "") {
                        $company.addClass('input-validation-error');
                        error = true;
                    }

                    if ($role == "") {
                        $role.addClass('input-validation-error');
                        error = true;
                    }

                    if ($shift.val() && ($area.val() == "" || $plant.val() == "")) {

                        if ($area.val() == "") {
                            $area.addClass('input-validation-error');
                        }
                        if ($plant.val() == "") {
                            $plant.addClass('input-validation-error');
                        }

                        error = true;
                    } else if ($plant.val() && $area.val() == "") {
                        $area.addClass('input-validation-error');
                        error = true;
                    }

                });

                hideLoading();

                if (!error) {
                    Swal.fire({
                        title: 'Message',
                        text: "Are you confirm ?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#00B5B8',
                        cancelButtonColor: '#FFA87D',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.value) {

                            $.blockUI({
                                message: null,
                                onBlock: function () {


                                    $('.loader').show();
                                    $('.loader-inner').loaders();

                                    $('#buttonAction').val("Save");

                                    //$('#searchResultTable').DataTable().search('').draw(false);
                                    //sleep(20);
                                    //var form = $(".form-information");
                                    //// Encode a set of form elements from all pages as an array of names and values
                                    //var params = $('#searchResultTable').DataTable().$('input,select').serializeArray();

                                    ////form.append(params);
                                    //$.each(params, function () {
                                    //    // If element doesn't exist in DOM
                                    //    if (!$.contains(document, form[this.name])) {
                                    //        // Create a hidden element
                                    //        $(form).append(
                                    //            $('<input>')
                                    //                .attr('type', 'hidden')
                                    //                .attr('name', this.name)
                                    //                .val(this.value)
                                    //        );
                                    //    }
                                    //});

                                    $(".form-information").submit();
                                }
                            });
                     
                        }
                    });
                } else {
                    alertFieldRequire();
                }
            } 
        }); 
    
    


    });
});

function sleep(miliseconds) {
    var currentTime = new Date().getTime();

    while (currentTime + miliseconds >= new Date().getTime()) {
    }
}

function getGenerateExcel() {

    let _url = $('#URL_GEN_EXCEL').val();

    $.ajax({
        url: _url,
        type: 'GET',
        beforeSend: function () { loading(); },
        success: function (data) {
            hideLoading();
            if (data) {
                let appName = $('#AppName').val();
                let path = getWebSitePath(appName) + "/FileUpload/UserMonitor/" + data + "";
                window.open(path, '_blank');
            } else {
                let html = "<div class='card-header'>";
                html += "<div class='row'>";
                html += "<div class='col-md-12'>";
                html += "<div class='alert bg-danger alert-dismissible mb-2' role='alert'>";
                html += "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
                html += "<span aria-hidden='true'>&times;</span>";
                html += "</button>";
                html += "<strong>Data not found or error server please try again</strong>.";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";

                $('#description').prepend(html);
            }
        },
        error: function (response) {
            hideLoading();

            let html = "<div class='card-header'>";
            html += "<div class='row'>";
            html += "<div class='col-md-12'>";
            html += "<div class='alert bg-danger alert-dismissible mb-2' role='alert'>";
            html += "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            html += "<span aria-hidden='true'>&times;</span>";
            html += "</button>";
            html += "<strong>" + response.message + "</strong>.";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";

            $('#description').prepend(html);
        }
    });
}
