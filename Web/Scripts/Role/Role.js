﻿
$(document).ready(function () {

    $('#searchResultTable').DataTable();

    $(document).on('click', '.btn-edit', function () {
        loading();
    });

    $(document).on('click', '.btn-delete', function () {

        let path = $(this).data('path');
        Swal.fire({
            title: 'Message',
            text: "Do you want to delete ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                loading();
                window.location.href = path;
            }
        });
    });



    $('#btn-clear-all').click(function () {

        Swal.fire({
            title: 'Message',
            text: "Do you want to reset ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                resetData();
            }
        });
    });


});

function resetData() {
    $('.form-control').val('');
}

function searchRole() {
    loading();
    $('.form-information').submit();
}

function addRole(url) {
    loading();
    window.location.href = url;
}
