﻿$(function () {
    //checkboxToogle();

    //MENU
    initialTreeview("menu");
    //KPI
    initialTreeview("kpi");
    //DEPARTMENT
    initialTreeview("department");

 
    //if ($('#departmentTreeId').val()) {
    //    getAreaByDepartmentId($('#departmentTreeId').val(), $('#areaTreeId').val());
    //}

    //if ($('#areaTreeId').val()) {
    //    getPlantByArea($('#areaTreeId').val(), $('#plantTreeId').val());
    //}

    //if ($('#plantTreeId').val()) {
    //    getShiftByPlant($('#plantTreeId').val(), $('#shiftTreeId').val());
    //}

    $('#table-user').DataTable();
    $('#user-table-modal').DataTable();

    $('#btn-clear-all').click(function () {

        Swal.fire({
            title: 'Message',
            text: "Do you want to reset ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00B5B8',
            cancelButtonColor: '#FFA87D',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                resetData();
            }
        });
    });

    $('#search-user-modal').on("click", function () {
        $("input[name='chk[]']").prop('checked', false);
        //$("#modal-user").modal('show');
        let data = bindSearchUserData();
        getUserList(data);

        //let $table = $('#user-table-modal');
        //$table.DataTable();
    });

    $("#btn-search").click(function () {
        let data = bindSearchUserData();
        getUserList(data);
    });

    $("#btn-save").click(function (e) {

        let valid = $(".form-information").valid();

        if (valid) {
            Swal.fire({
                title: 'Message',
                text: "Are you confirm ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B5B8',
                cancelButtonColor: '#FFA87D',
                confirmButtonText: 'Yes'
            }).then(function(result){
                if (result.value) {

                    $.blockUI({
                        message: null,
                        onBlock: function () {
                            
                            $('.loader').show();
                            $('.loader-inner').loaders();

                            let form = $(".form-information");
                            // Encode a set of form elements from all pages as an array of names and values
                            let params = $('#table-user').DataTable().$('input').serializeArray();

                            //form.append(params);
                            $.each(params, function () {
                                // If element doesn't exist in DOM
                                if (!$.contains(document, form[this.name])) {
                                    // Create a hidden element
                                    $(form).append(
                                        $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', this.name)
                                            .val(this.value)
                                    );
                                }
                            });

                            $('.form-information').submit();

                        }
                    });
                  
                }
            });
        } else {
            alertFieldRequire();
        }
    });

});

function resetData() {
    $('.reset-data').val('');
    let $tableUser = $('#table-user');

    if ($tableUser.DataTable()) {
        //$tableUser.dataTable().fnDestroy();
        $tableUser.dataTable().fnClearTable();
    }
    //MENU
    initialTreeview("menu" ,true);
    //KPI
    initialTreeview("kpi", true);
    //DEPARTMENT
    initialTreeview("department", true);

}

function initialTreeview(id , reset) {

    //if (selected) {
    //    let split = selected.split('&').filter(function (c) { return c && c !== '|'; });
    //    if (split && split.length > 0) {

    //        $.each(json, function (index, element) {
    //            if (split.indexOf(element.href.split('node-')[1]) >= 0) {
    //                element.state.checked = true;
    //            }
    //        });
    //    }
    //} 


    //id = kpi
    var data = $("#" + id + "Tree").val();
    var json = [];
    var tempdata = $("#" + id + "TreeId").val();

    if (reset) {
        json = JSON.parse(data.replace(/,"checked": "true"/g, ''));
        tempdata = '';
        $("#" + id + "TreeId").val('');
    } else {
        json = JSON.parse(data);
    }



    var firsttime_check = true;
    var head_check = false;
    var checknode = "";
    var $checkableTree = $('#' + id + 'Treeview-checkable').treeview({
        data: json,
        showIcon: true,
        showCheckbox: true,
        color: "#428bca",
        onNodeChecked: function (event, node) {

            if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                tempdata = tempdata.replace("&" + node.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
                if (tempdata != "") {
                    tempdata = tempdata + "|&" + node.href.replace("#node-shift_", "").replace("#node-", "") + "&";
                } else {
                    tempdata = "&" + node.href.replace("#node-shift_", "").replace("#node-", "") + "&";
                }
            } 

            //if (node.parentId != null) {
            //    $(this).treeview(true).checkNode(node.parentId);
            //}

            //checknode = node.nodeId;
            if (firsttime_check) {
                checknode = node.nodeId;
                firsttime_check = false;
                if (node.parentId == null || (id == "kpi" && node.parentId == 0)) {
                    head_check = true;
                } else {
                    head_check = false;
                }
            }

            if (head_check) {
                for (var i in node.nodes) {
                    var child = node.nodes[i];

                    if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                        tempdata = tempdata.replace("&" + child.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
                        if (tempdata != "") {
                            tempdata = tempdata + "|&" + child.href.replace("#node-shift_", "").replace("#node-", "") + "&";
                        }
                        else {
                            tempdata = "|&" + child.href.replace("#node-shift_", "").replace("#node-", "");
                        }
                    }

                    $(this).treeview(true).checkNode(child.nodeId);
                }
            } else {
                if (node.parentId != null ) {
                    $(this).treeview(true).checkNode(node.parentId);
                    if (checknode == node.nodeId) {
                        if (node.nodes != null) {
                            for (let i in node.nodes) {
                                let child = node.nodes[i];

                                if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                                    tempdata = tempdata.replace("&" + child.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
                                    if (tempdata != "") {
                                        tempdata = tempdata + "|&" + child.href.replace("#node-shift_", "").replace("#node-", "") + "&";
                                    }
                                    else {
                                        tempdata = "|&" + child.href.replace("#node-shift_", "").replace("#node-", "");
                                    }
                                }
                      

                                if (child.nodes != null) {
                                    for (let j in child.nodes) {
                                        let childInChild = child.nodes[j];

                                        if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                                            tempdata = tempdata.replace("&" + childInChild.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
                                            if (tempdata != "") {
                                                tempdata = tempdata + "|&" + childInChild.href.replace("#node-shift_", "").replace("#node-", "") + "&";
                                            }
                                            else {
                                                tempdata = "|&" + child.href.replace("#node-shift_", "").replace("#node-", "");
                                            }
                                        }

                                        $(this).treeview(true).checkNode(childInChild.nodeId);

                                    }
                                }


                                $(this).treeview(true).checkNode(child.nodeId);

                            }
                        }
                    }
                }
            }

            if (checknode == node.nodeId) {
                head_check = false;
                firsttime_check = true;
                checknode = "";
            }
            //    $checkableTree.treeview('checkAll', { silent: $('#chk-check-silent').is(':checked') });

            $("#" + id + "TreeId").val(tempdata);



            //if (id == "department") {
            //    getAreaByDepartmentId(tempdata);
            //    $("#areaTreeId").val("");
            //    $("#plantTreeId").val("");
            //    getPlantByArea(0);
            //    getShiftByPlant(0);
            //} else if (id == "area") {
            //    $("#plantTreeId").val("");
            //    getPlantByArea(tempdata);
            //    getShiftByPlant(0);
            //} else if (id == "plant") {
            //    getShiftByPlant(tempdata);
            //}

        },
        onNodeUnchecked: function (event, node) {

            if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                tempdata = tempdata.replace("&" + node.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
            }

            for (var i in node.nodes) {
                let child = node.nodes[i];

                if (node.href.indexOf('shift_') >= 0 && id == "department" || node.href.indexOf('shift_') < 0 && id != "department") {
                    tempdata = tempdata.replace("&" + child.href.replace("#node-shift_", "").replace("#node-", "") + "&", "");
                }
                
                $(this).treeview(true).uncheckNode(child.nodeId);
            }
            $("#" + id + "TreeId").val(tempdata);

            //if (id == "department") {
            //    getAreaByDepartmentId(tempdata);
            //    $("#areaTreeId").val("");
            //    $("#plantTreeId").val("");
            //    getPlantByArea(0);
            //    getShiftByPlant(0);
            //} else if (id == "area") {
            //    $("#plantTreeId").val("");
            //    getPlantByArea(tempdata);
            //    getShiftByPlant(0);
            //} else if (id == "plant") {
            //    getShiftByPlant(tempdata);
            //}
        }
    });


    $checkableTree.treeview('collapseAll', { silent: true });

    if (id == "kpi") {
        $checkableTree.treeview('expandAll', { levels: 2, silent: true });
    }
    
}

//function getAllNodes() {
//    var treeViewObject = $('#departmentTreeview-checkable').data('treeview'),
//        allCollapsedNodes = treeViewObject.getCollapsed(),
//        allExpandedNodes = treeViewObject.getExpanded(),
//        allNodes = allCollapsedNodes.concat(allExpandedNodes);

//    return allNodes;
//}

function bindSearchUserData() {

    let listEmpCode = [];

    //var dt = $('#user-table-modal').DataTable();
    //let $checked = dt.$("input[name='chkbox[]']");table
    //.rows()
     //   .data();


    $("#table-user").DataTable().rows().data().each(function (value, index) {
        var dataEmpCode = value[1];
        if (dataEmpCode.trim()) {
            listEmpCode.push(dataEmpCode.trim());
        }
    });
    

    let data =
    {
        "sFirstName": $('#users_Search_sFirstName').val(),
        "sLastName": $('#users_Search_sLastName').val(),
        "sDepartmentId": parseInt($('#users_Search_sDepartmentId').val()) || "",
        "sAreaId": parseInt($('#users_Search_sAreaId').val()) || "",
        "sPlantId": parseInt($('#users_Search_sPlantId').val()) || "",
        "sCompanyId": parseInt($('#users_Search_sCompanyId').val()) || "",
        "EmployeeCodeList": listEmpCode
    };

    let res = {
        "users_Search": data
    };

    return res;
}

function uncheckAll(ele) {
    let checkArr = [];

    let dt = $('#user-table-modal').DataTable();
    let $checked = dt.$("input[name='chkbox[]']");

    $checked.each(function (index, obj) {
        let res = this.checked ? true : false;
        checkArr.push(res);
    });

    let result = checkArr.filter(function (x) { return x == true; }); 

    if (result.length == 0) {
        $("input[name='chk[]']").prop('checked', false);
    }
}

function confirmDataModal() {


    $.blockUI({
        message: null,
        onBlock: function () {


            $('.loader').show();
            $('.loader-inner').loaders();

            let $tableUser = $('#table-user');

            if ($tableUser.DataTable()) {
                $tableUser.dataTable().fnDestroy();
            }

            var dt = $('#user-table-modal').DataTable();
            let $checked = dt.$("input[name='chkbox[]']");


            $checked.each(function () {

                let res = this.checked ? true : false;
                if (res) {
                    let value = $(this).val();
                    let rowCount = $tableUser.find('tbody > tr').length;
                    let modalEmpCode = dt.$('#tr-modal-id-' + value).find("td:eq(1)").text()
                        , modalName = dt.$('#tr-modal-id-' + value).find("td:eq(2)").text()
                        , modalDepartment = dt.$('#tr-modal-id-' + value).find("td:eq(3)").text()
                        , modalArea = dt.$('#tr-modal-id-' + value).find("td:eq(4)").text()
                        , modalPlant = dt.$('#tr-modal-id-' + value).find("td:eq(5)").text()
                        , modalCompany = dt.$('#tr-modal-id-' + value).find("td:eq(6)").text();
                    //, modalShift = dt.$('#tr-modal-id-' + value).find("td:eq(7)").text()
                    //, modalStatus = dt.$('#tr-modal-id-' + value).find("td:eq(8)").text();

                    let haveData = $tableUser.find('tbody tr').find("[id^=tr-id-" + value + "]");

                    if (!haveData.length) {
                        tr = "<tr id='tr-id-" + value + "'>"
                            + "<td style='display:none;'><input type='hidden' class='re-input-number' id='role_Detail_UserInRoleList_" + rowCount + "__EmpId' name='role_Detail.UserInRoleList[" + rowCount + "].EmpId' type='number' value='" + value + "'/>" + value + "</td>"
                            + "<td>" + modalEmpCode + "</td>"
                            + "<td>" + modalName + "</td>"
                            + "<td>" + modalDepartment + "</td>"
                            + "<td>" + modalArea + "</td>"
                            + "<td>" + modalPlant + "</td>"
                            + "<td>" + modalCompany + "</td>"
                            + "<td><a href='javascript:void(0)' data-id='" + value + "' onclick=deleteTableUser('" + value + "')><i class='fa fa-trash-o'></i></span></td>"
                            + "</tr>";
                        $tableUser.find('tbody').append(tr);
                    }
                }
            });

            $tableUser.dataTable({
                'order': [
                    [0, 'asc']
                ]
            });

            $("#modal-user").modal('hide');
            hideLoading();
        }
    });
    
}

function deleteTableUser(id) {

    Swal.fire({
        title: 'Message',
        text: "Are you confirm ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#00B5B8',
        cancelButtonColor: '#FFA87D',
        confirmButtonText: 'Yes'
    }).then(function(result){
        if (result.value) {
            let $tableUser = $('#table-user');

            if ($tableUser.DataTable()) {
                $tableUser.dataTable().fnDestroy();
            }

            $('#tr-id-' + id).remove();

            $('.re-input-number').each(function (index) {
                let $this = $(this);
                $this.attr('id', 'role_Detail_UserInRoleList_' + (index) + '__EmpId')
                    .attr('name', 'role_Detail.UserInRoleList[' + (index) + '].EmpId');
            });

            $tableUser.dataTable({
                'order': [
                    [0, 'asc']
                ]
            });
            
            alertSucess();
        }
    });
}

function checkAll(ele) {

    let dt = $('#user-table-modal').DataTable();
    let $checked = dt.$("input");


    //var checkboxes = document.getElementsByTagName('input');
    if (ele.checked) {
        for (let i = 0; i < $checked.length; i++) {
            if ($checked[i].type == 'checkbox') {
                $checked[i].checked = true;
            }
        }
    } else {
        for (let i = 0; i < $checked.length; i++) {

            if ($checked[i].type == 'checkbox') {
                $checked[i].checked = false;
            }
        }
    }
}

function getUserList(data) {
    try {
        loading();

        let _url = $('#URL_SEARCH_USER').val()
            , json = JSON.stringify(data);

        $.ajax({
            cache: false,
            type: 'POST',
            url: _url,
            data: json,
            contentType: "application/json",
            success: function (data) {
                let $table = $('#user-table-modal'), tr = "";
                let items = data.result;

                if ($table.DataTable()) {
                    $table.dataTable().fnClearTable();
                    $table.dataTable().fnDestroy();
                }

                //$table.find('tbody').empty();

                $.each(items, function (index, element) {
                    tr = "<tr id='tr-modal-id-" + element.Id + "'>"
                        + "<td><input type='checkbox' name='chkbox[]' onchange='uncheckAll(this)' value='"+ element.Id + "'/></td>"
                        + "<td>" + element.dUserCode + "</td>"
                        + "<td>" + element.fullName + "</td>"
                        + "<td>" + element.DepartmentName + "</td>"
                        + "<td>" + (element.AreaName ? element.AreaName : "-") + "</td>"
                        + "<td>" + (element.PlantName ? element.PlantName : "-") + "</td>"
                        + "<td>" + element.CompanyName + "</td>"
                        + "<td style='display:none;'>" + element.ShiftName + "</td>"
                        + "<td style='display:none;'>" + element.Status + "</td>"
                        + "</tr>";

                    $table.find('tbody').append(tr);
                });

                $table.dataTable({
                    'order': [
                        [1, 'asc']
                    ]
                });

                hideLoading();
            }, error: function (err) {
                alert('fail :' + err.message);
            }
        });

    }
    catch (err) {
        hideLoading();
        alert('Error : ' + err.message);
    }
}

function getAreaByDepartmentId(id, selected) {
    try {
        loading();
       // 
        let URL_GetAreaByDepartment = $('#URL_GetArea').val();
        $.getJSON(URL_GetAreaByDepartment
            , { departmentId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    $('#areaTree').val(data.result);
                    initialTreeview("area", selected);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getPlantByArea(id, selected) {
    try {
        loading();
        let _URL = $('#URL_GetPlant').val();
        $.getJSON(_URL
            , { areaId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    $('#plantTree').val(data.result);
                    initialTreeview("plant", selected);

                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getShiftByPlant(id, selected) {
    try {
        loading();
        let _URL = $('#URL_GetShift').val();
        $.getJSON(_URL
            , { plantId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    $('#shiftTree').val(data.result);
                    initialTreeview("shift", selected);

                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function assignDropdown(id, data) {

    let html = ""
        , $obj = $("#" + id);


    html = "<option value=''>";
    html += "--- เลือกทั้งหมด ---";
    html += "</option>";

    $obj.children().remove();

    data.forEach(function (element) {
        html += "<option value='"+ element.Value +"'>";
        html += element.Text;
        html += "</option>";
    });

    $obj.append(html);

    if (id == "users_Search_sAreaId") {
        $objOld = $('#users_Search_sPlantId');
        $objOld.children().remove();
 

        html = "<option value=''>";
        html += "--- เลือกทั้งหมด ---";
        html += "</option>";
        $objOld.append(html);
    }

}


function getPlantByAreaIdDDL($this) {
    try {
        let id = $($this).val() ? $($this).val() : 0;

        loading();
        // 
        let URL = $('#URL_GetPlant_DDL').val();
        $.getJSON(URL, { areaId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    assignDropdown("users_Search_sPlantId", data.result);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });

    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

function getAreaByDepartmentIdDDL($this) {

    try {
        let id = $($this).val() ? $($this).val() : 0;
        //if ($(id).val()) {
        loading();
        let URL_GetAreaByDepartment = $('#URL_GetArea_DDL').val();
        $.getJSON(URL_GetAreaByDepartment
            , { departmentId: id },
            function (data) {
                hideLoading();
                if (data.isSuccess) {
                    assignDropdown("users_Search_sAreaId", data.result);
                }
            })
            .fail(function (err) {
                hideLoading();
                alert('fail.');
            });
        //}
    }
    catch (err) {
        alert('Error : ' + err.message);
    }
}

//function checkboxToogle() {
//    $(".checkboxStatus").bootstrapToggle({
//        on: 'ACTIVE',
//        off: 'INACTIVE'
//    });
//}
