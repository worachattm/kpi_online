﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
            "~/Scripts/bootstrap-datepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
"~/Scripts/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/role").Include(
                  "~/Scripts/Role/Role.js"));


            bundles.Add(new ScriptBundle("~/bundles/user").Include(
                  "~/Scripts/User/User.js"));

            bundles.Add(new ScriptBundle("~/bundles/user-monitor").Include(
          "~/Scripts/UserMonitor/UserMonitor.js"));

            bundles.Add(new ScriptBundle("~/bundles/indicator-manage").Include(
      "~/Scripts/IndicatorsManage/IndicatorsManage.js"));

            bundles.Add(new ScriptBundle("~/bundles/createoredit-indicator").Include(
                "~/Scripts/IndicatorsManage/CreateOrEditIndicator.js"));

            //bundles.Add(new ScriptBundle("~/bundles/createoredit-user").Include(
            // "~/Scripts/User/CreateOrEditUser.js"));

            bundles.Add(new ScriptBundle("~/bundles/createoredit-role").Include(
                "~/Scripts/Role/CreateOrEditRole.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootbox").Include(
                        "~/Scripts/bootbox.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-treeview").Include(
                        "~/Scripts/bootstrap-treeview.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jsvalidator").Include(
                    "~/Scripts/js-form-validator-2.1/js-form-validator.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/helper").Include(
                    "~/Content/Theme/vendors.min.js",
                    "~/Content/Theme/app-menu.js",
                    "~/Content/Theme/app.js",
                    "~/Scripts/swal.min.js",
                    "~/Scripts/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js",
                    "~/Content/Theme/datatable-styling.min.js",
                    "~/Content/Theme/components-modal.min.js",
                    "~/Scripts/helper.js"));

            bundles.Add(new ScriptBundle("~/bundles/themejs").Include(
               "~/Content/Theme/app-menu.js",
               "~/Content/Theme/app.js"
              
               ));

            bundles.Add(new ScriptBundle("~/bundles/loader").Include(
               "~/Content/loaders.css-master/loaders.css.js",
               "~/Content/jqueryBlockUI/blockUI.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-toggle").Include(
                "~/Scripts/bootstrap-toggle.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/alertify").Include(
                    "~/Scripts/alertifyjs/alertify.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                "~/Scripts/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/Theme/vendors.min.css",
                "~/Content/bootstrap.css",
                "~/Content/Theme/bootstrap-extended.min.css",
                "~/Content/Theme/colors.min.css",
                "~/Content/Theme/components.min.css",
                "~/Content/Theme/palette-gradient.min.css",
                "~/Content/Theme/style.css",
                
                "~/Content/Theme/vertical-menu-modern.min.css",
                "~/Scripts/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/select2.min.css",
                "~/Content/loaders.css-master/loaders.min.css",
                "~/Content/bootstrap-datepicker.min.css",
                "~/Content/bootstrap-datepicker.min.css",
                //"~/Content/top-kpi-css.css",

                "~/Scripts/alertifyjs/css/alertify.min.css",
                "~/Content/bootstrap-toggle.min.css",
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/error").Include(
                "~/Content/Error.css"));

            bundles.Add(new ScriptBundle("~/bundles/highchart").Include(
                "~/Scripts/Highcharts/highcharts.js"));
        }
    }
}
